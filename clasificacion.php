<?php

// Incluye los objetos necesarios
require("admin/objetos/club.php");
require("admin/objetos/equipo.php");
require("admin/objetos/cate.php");
require("admin/objetos/jornada.php");

// Incluye las funciones necesarios
require("admin/fun/funciones.php");

// Ordena los equipos en un array de mayor a menor puntuacion
function ordenarEquipos($equiposPuntos,$equiposPuntosOrdenados,$categoria) {
	// Para cada equipo de la categoria
	for($i=0;$i<$categoria->numteams;$i++) {						
		$conta = 0;
		// Mientras que no hayamos recorrido todos los equipos de la categoria
		while($conta<$categoria->numteams) {
			// Toma los puntos de la lista de equipos desordenados y de la clasificacion
			$puntosE = $equiposPuntos[$i]["puntos"];
			$puntosT = $equiposPuntosOrdenados[$conta]["puntos"];
			
			// Si el equipo tratado tiene menos puntos que el equipo que nos hemos encontrado en la clasificacion
			if($puntosE<$puntosT) {
				// Pasamos a la siguiente posicion
				$conta++;
			} else {
				$conta2 = $categoria->numteams;
				// Vamos pasando a un puesto inferior a todos los equipos que tienen menos puntos que el equipo tratado
				// desde la ultima posicion hasta la que tenemos que hacer el hueco
				while($conta2>$conta) {
					$equiposPuntosOrdenados[$conta2]["puntos"]   = $equiposPuntosOrdenados[$conta2-1]["puntos"];
					$equiposPuntosOrdenados[$conta2]["name"]     = $equiposPuntosOrdenados[$conta2-1]["name"];
					$equiposPuntosOrdenados[$conta2]["sfavor"]   = $equiposPuntosOrdenados[$conta2-1]["sfavor"];
					$equiposPuntosOrdenados[$conta2]["scontra"]  = $equiposPuntosOrdenados[$conta2-1]["scontra"];
					$equiposPuntosOrdenados[$conta2]["ganados"]  = $equiposPuntosOrdenados[$conta2-1]["ganados"];
					$equiposPuntosOrdenados[$conta2]["perdidos"] = $equiposPuntosOrdenados[$conta2-1]["perdidos"];
					$equiposPuntosOrdenados[$conta2]["jugados"]  = $equiposPuntosOrdenados[$conta2-1]["jugados"];
					$equiposPuntosOrdenados[$conta2]["pfavor"]   = $equiposPuntosOrdenados[$conta2-1]["pfavor"];
					$equiposPuntosOrdenados[$conta2]["pcontra"]  = $equiposPuntosOrdenados[$conta2-1]["pcontra"];
					$conta2--;
				}
				// Metemos el equipo en el hueco de la clasificacion generado
				$equiposPuntosOrdenados[$conta]["puntos"]    = $equiposPuntos[$i]["puntos"];
				$equiposPuntosOrdenados[$conta]["name"]      = $equiposPuntos[$i]["name"];
				$equiposPuntosOrdenados[$conta]["sfavor"]    = $equiposPuntos[$i]["sfavor"];
				$equiposPuntosOrdenados[$conta]["scontra"]   = $equiposPuntos[$i]["scontra"];
				$equiposPuntosOrdenados[$conta]["ganados"]   = $equiposPuntos[$i]["ganados"];
				$equiposPuntosOrdenados[$conta]["perdidos"]  = $equiposPuntos[$i]["perdidos"];
				$equiposPuntosOrdenados[$conta]["jugados"]   = $equiposPuntos[$i]["jugados"];
				$equiposPuntosOrdenados[$conta]["pfavor"]    = $equiposPuntos[$i]["pfavor"];
				$equiposPuntosOrdenados[$conta]["pcontra"]   = $equiposPuntos[$i]["pcontra"];
				// Salimos del primer while de la funcion
				$conta = $categoria->numteams;
			}
		}
	}

	// Devolvemos el array con la clasificacion
	return $equiposPuntosOrdenados;
}
?>

<!DOCTYPE html>

<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms</title>
		<link rel="stylesheet"    type="text/css"     href="css/menu_izquierda.css" />
    </head>

    <body bgcolor="#e8e8e8" style="margin:0px;">
		
		<!-- cabecera -->
        <?php require('cabecera.php'); ?>
        
		<!-- cuerpo -->
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="35" align="center"><tr><td></td></tr></table>
        <table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="700" align="center">
            <tr>
				<td bgcolor="#ffffff" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    <?php
					include("cuerpo1.php");
					include("cuerpo3.php");
					include("cuerpo4.php");
					?>
                </td>
                <td></td>
				<td bgcolor="#ffffff" width="800" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
					
					<?php
					$idcat = $_REQUEST["idcat"];
					$categoria = obtenerUnaCate($idcat);					
					?>
					
					<table style="position:relative;top:30px;margin-left:5px;text-align:justify;" width="98%" align="center">
						
						<tr>
							<td colspan="9" style="border-bottom:1px dashed #333333;">
								<font face="Arial" color="#394752" style="font-size:26px; font-style:normal; color:#000000;">
									<?php echo "Clasificación"; ?>
								</font>
							</td>
						</tr>
						
						<tr height="20" bgcolor="#ffffff"><td></td></tr>
						
						<tr>
							<td colspan="9" height="20" width="100%" bgcolor="#ffffff" align="right">
								<font face="verdana" color="black" style="margin-left: 0px;font-size: 12px;">
									<?php echo "Jornadas disputadas: " . ultimaJornadaDisputada($idcat); ?>
								</font>
							</td>
						</tr>
						
						<tr height="20" bgcolor="#ffffff"><td></td></tr>
						
						<tr>
							<td height="30" width="300" bgcolor="#394752">
								<font face="arial" color="#c8c8c8" style="margin-left: 5px;font-size: 14px;">
									<?php echo "Equipo"; ?>
								</font>
							</td>
							<td height="30" width="50" bgcolor="#394752" align="center">
								<font face="arial" color="#c8c8c8" style="margin-left: 0px;font-size: 14px;">
									<?php echo "J"; ?>
								</font>
							</td>
							<td height="30" width="50" bgcolor="#394752" align="center">
								<font face="arial" color="#c8c8c8" style="margin-left: 0px;font-size: 14px;">
									<?php echo "W"; ?>
								</font>
							</td>
							<td height="30" width="50" bgcolor="#394752" align="center">
								<font face="arial" color="#c8c8c8" style="margin-left: 0px;font-size: 14px;">
									<?php echo "L"; ?>
								</font>
							</td>
							<td height="30" width="50" bgcolor="#394752" align="center">
								<font face="arial" color="#c8c8c8" style="margin-left: 0px;font-size: 14px;">
									<?php echo "Ptos"; ?>
								</font>
							</td>
							<td height="30" width="50" bgcolor="#394752" align="center">
								<font face="arial" color="#c8c8c8" style="margin-left: 0px;font-size: 14px;">
									<?php echo "S.F."; ?>
								</font>
							</td>
							<td height="30" width="50" bgcolor="#394752" align="center">
								<font face="arial" color="#c8c8c8" style="margin-left: 0px;font-size: 14px;">
									<?php echo "S.C."; ?>
								</font>
							</td>
							<td height="30" width="50" bgcolor="#394752" align="center">
								<font face="arial" color="#c8c8c8" style="margin-left: 0px;font-size: 14px;">
									<?php echo "p.F."; ?>
								</font>
							</td>
							<td height="30" width="50" bgcolor="#394752" align="center">
								<font face="arial" color="#c8c8c8" style="margin-left: 0px;font-size: 14px;">
									<?php echo "p.C."; ?>
								</font>
							</td>
						</tr>
					
						<?php                    
						// Inicializo la variable rival
						$rival = "";
						// Creamos un array con los equipos que forman la liga
						$equiposPuntos=array();
						$equiposPuntosOrdenados=array();
						
						// Por cada equipo de la categoria, obtenemos su nombre e inicializamos un array de 2 valores
						// con su nombre y con 0 puntos al iniciar el cálculo de puntos
						for($i=0;$i<$categoria->numteams;$i++) {
							// Pintamos el nombre de los equipos que forman la liga
							$numero = $i+1;
							if((strlen($numero))==1) $numero ="0".$numero;
							$$rival = "rival".$numero;

							$equiposPuntos[$i]          = array("name" => $categoria->$$rival, "puntos" => "0", "sfavor" => "0", "scontra" => "0", "jugados" => "0", "ganados" => "0", "perdidos" => "0", "pfavor" => "0", "pcontra" => "0");
							$equiposPuntosOrdenados[$i] = array("name" => $categoria->$$rival, "puntos" => "0", "sfavor" => "0", "scontra" => "0", "jugados" => "0", "ganados" => "0", "perdidos" => "0", "pfavor" => "0", "pcontra" => "0");
						}	
						
						// Calculamos puntuación acumulada hasta la ultima jornada disponible
						for($i=0;$i<$categoria->numteams;$i++) {
							$numero = $i+1;
							if((strlen($numero))==1) $numero ="0".$numero;
							$$rival = "rival".$numero;
							
							for($j=0;$j<numRowsJornadasDisputadas($idcat);$j++) {
								$jornada = obtenerUnaJornadaDeUnEquipo($idcat,$j+1,$categoria->$$rival);

								//////////////////////////////////////////
								// Sistema de puntuacion
								//////////////////////////////////////////
								
								// Si el equipo tratado es el local
								if($categoria->$$rival==$jornada->local) {
									if($jornada->plocal>$jornada->pvisit) {
										$puntos   = 3;
										$ganados  = 1;
										$perdidos = 0;
									}
									if($jornada->plocal<$jornada->pvisit) {
										$puntos   = 0;
										$ganados  = 0;
										$perdidos = 1;
									}
									if($jornada->plocal==$jornada->pvisit) {
										$puntos   = 0;
										$ganados  = 0;
										$perdidos = 0;
									}
									$pfavor  = $jornada->plocal;
									$pcontra = $jornada->pvisit;
									$pfavorset  = $jornada->plocal1+$jornada->plocal2+$jornada->plocal3+$jornada->plocal4+$jornada->plocal5;
									$pcontraset = $jornada->pvisit1+$jornada->pvisit2+$jornada->pvisit3+$jornada->pvisit4+$jornada->pvisit5;
								}
								// Si el equipo tratado es el visitante
								else {
									if($jornada->plocal<$jornada->pvisit) {
										$puntos   = 3;
										$ganados  = 1;
										$perdidos = 0;
									} 
									if($jornada->plocal>$jornada->pvisit) {
										$puntos   = 0;
										$ganados  = 0;
										$perdidos = 1;
									}
									if($jornada->plocal==$jornada->pvisit) {
										$puntos   = 0;
										$ganados  = 0;
										$perdidos = 0;
									}
									$pfavor  = $jornada->pvisit;
									$pcontra = $jornada->plocal;
									$pcontraset  = $jornada->plocal1+$jornada->plocal2+$jornada->plocal3+$jornada->plocal4+$jornada->plocal5;
									$pfavorset   = $jornada->pvisit1+$jornada->pvisit2+$jornada->pvisit3+$jornada->pvisit4+$jornada->pvisit5;
								}
								
								$equiposPuntos[$i]["puntos"]   = $equiposPuntos[$i]["puntos"]   + $puntos;
								$equiposPuntos[$i]["sfavor"]   = $equiposPuntos[$i]["sfavor"]   + $pfavor;
								$equiposPuntos[$i]["scontra"]  = $equiposPuntos[$i]["scontra"]  + $pcontra;
								$equiposPuntos[$i]["ganados"]  = $equiposPuntos[$i]["ganados"]  + $ganados;
								$equiposPuntos[$i]["perdidos"] = $equiposPuntos[$i]["perdidos"] + $perdidos;
								$equiposPuntos[$i]["pfavor"]   = $equiposPuntos[$i]["pfavor"]   + $pfavorset;
								$equiposPuntos[$i]["pcontra"]  = $equiposPuntos[$i]["pcontra"]  + $pcontraset;
							}
							
							$equiposPuntos[$i]["jugados"] = ultimaJornadaDisputada($idcat);
						}
						
						// Hasta aquí ya tenemos las puntuaciones de los equipos en el array $equiposPuntos, asi que
						// vamos a ordenarlos a continuación:
						
						$equiposPuntosOrdenados = ordenarEquipos($equiposPuntos,$equiposPuntosOrdenados,$categoria);
						
						for($i=0;$i<$categoria->numteams;$i++) {
							?>
							<tr bgcolor="<?php if(($i%2)==1) { echo "#f8f8f8"; } else { echo "#e8e8e8"; } ?>">
								<td height="30" width="300">
									<font face="arial" color="black" style="margin-left: 5px;font-size: 14px;">
										<?php echo $equiposPuntosOrdenados[$i]["name"]; ?>
									</font>
								</td>
								<td height="30" width="50" align="center">
									<font face="arial" color="black" style="margin-left: 0px;font-size: 14px;">
										<?php echo $equiposPuntosOrdenados[$i]["jugados"]; ?>
									</font>
								</td>
								<td height="30" width="50" align="center">
									<font face="arial" color="black" style="margin-left: 0px;font-size: 14px;">
										<?php echo $equiposPuntosOrdenados[$i]["ganados"]; ?>
									</font>
								</td>
								<td height="30" width="50" align="center">
									<font face="arial" color="black" style="margin-left: 0px;font-size: 14px;">
										<?php echo $equiposPuntosOrdenados[$i]["perdidos"]; ?>
									</font>
								</td>
								<td height="30" width="50" align="center">
									<font face="arial" color="black" style="margin-left: 0px;font-size: 14px;">
										<?php echo $equiposPuntosOrdenados[$i]["puntos"]; ?>
									</font>
								</td>
								<td height="30" width="50" align="center">
									<font face="arial" color="black" style="margin-left: 0px;font-size: 14px;">
										<?php echo $equiposPuntosOrdenados[$i]["sfavor"]; ?>
									</font>
								</td>
								<td height="30" width="50" align="center">
									<font face="arial" color="black" style="margin-left: 0px;font-size: 14px;">
										<?php echo $equiposPuntosOrdenados[$i]["scontra"]; ?>
									</font>
								</td>
								<td height="30" width="50" align="center">
									<font face="arial" color="black" style="margin-left: 0px;font-size: 14px;">
										<?php echo $equiposPuntosOrdenados[$i]["pfavor"]; ?>
									</font>
								</td>
								<td height="30" width="50" align="center">
									<font face="arial" color="black" style="margin-left: 0px;font-size: 14px;">
										<?php echo $equiposPuntosOrdenados[$i]["pcontra"]; ?>
									</font>
								</td>
							</tr>
							<?php
						}
						?>
					</table>
                </td>

            </tr>
        </table>
        
		<!-- pie -->
        <?php require('pie.php'); ?>

    </body>
</html>