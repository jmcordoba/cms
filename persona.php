<?php

// Incluye los objetos necesarios
require("admin/objetos/club.php");
require("admin/objetos/equipo.php");
require("admin/objetos/cate.php");
require("admin/objetos/persona.php");

// Incluye las funciones necesarios
require("admin/fun/funciones.php");

?>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms</title>
		<link rel="stylesheet"    type="text/css"     href="css/menu_izquierda.css" />
    </head>

    <body bgcolor="#e8e8e8" style="margin:0px;">
		
		<!--- cabecera --->
        <?php require('cabecera.php'); ?>
        <!--- cuerpo --->
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="35" align="center"><tr><td></td></tr></table>
        <table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="700" align="center">
            <tr>
				<td bgcolor="#ffffff" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    <?php
					include("cuerpo1.php");
					include("cuerpo3.php");
					include("cuerpo4.php");
					?>
                </td>
                <td></td>
				<td bgcolor="#ffffff" width="800" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    
					<?php
                    
                    $idpersona = $_REQUEST["idpersona"];
                    
                    $person = obtenerUnaPersona($idpersona);
                    
					$_POST["idperson"]  = $person->idperson;
					$_POST["idteam"]    = $person->idteam;
					$_POST["nombre"]    = $person->nombre;
					$_POST["surname"]   = $person->surname;
					$_POST["foto"]      = $person->foto;
					$_POST["fechan"]    = $person->fechan;
					$_POST["clubyears"] = $person->clubyears;
					$_POST["cargo"]     = $person->cargo;
					$_POST["palmares"]  = $person->palmares;
					$_POST["facebook"]  = $person->facebook;
					$_POST["twitter"]   = $person->twitter;
					$_POST["rol"]       = $person->rol;
					$_POST["fechaalta"] = $person->fechaalta;
					$_POST["fechabaja"] = $person->fechabaja;
					$_POST["conta"]     = $person->conta;
					
					$team = obtenerUnEquipo($person->idteam);
					$cate = obtenerUnaCate($team->idcat);
                    
					?>	
					
					<table style="position:relative;top:30px;margin-left:5px;text-align:justify;" width="99%" align="center">
					
						<tr>
							<td colspan="2" height=20 width=700 bgcolor=#c8c8c8>
								<font face="verdana" color="black" style="margin-left: 0px;font-size: 24px;">
									<?php echo $person->nombre; ?>
								</font>
							</td>
						</tr>

						<?php
						if ($_POST["foto"]!="") {
							?>
							<tr valign="bottom" align="center">
								<td colspan="2" width=700 bgcolor=#ffffff>
									<img height=350 src="http://www.apuestta.com/cmsweb/admin/<?php echo $_POST["foto"];?>">
								</td>
							</tr>
							<?php
						}
						?>
						<tr>
							<td height=20 width=150 bgcolor=#c8c8c8>idteam</td>
							<td height=20 width=550 bgcolor=#f8f8f8>
								<font face="verdana" color="black" style="margin-left: 0px;font-size: 12;">
									<?php echo $team->nombre; ?>
								</font>
							</td>
						</tr>
						<tr>
							<td height=20 width=150 bgcolor=#c8c8c8>nombre</td>
							<td height=20 width=550 bgcolor=#f8f8f8>
								<font face="verdana" color="black" style="margin-left: 0px;font-size: 12;">
									<?php echo $person->nombre; ?>
								</font>
							</td>
						</tr>
						<tr>
							<td height=20 width=150 bgcolor=#c8c8c8>surname</td>
							<td height=20 width=550 bgcolor=#f8f8f8>
								<font face="verdana" color="black" style="margin-left: 0px;font-size: 12;">
									<?php echo $person->surname; ?>
								</font>
							</td>
						</tr>
						<tr>
							<td height=20 width=150 bgcolor=#c8c8c8>fechan</td>
							<td height=20 width=550 bgcolor=#f8f8f8>
								<font face="verdana" color="black" style="margin-left: 0px;font-size: 12;">
									<?php echo $person->fechan; ?>
								</font>
							</td>
						</tr>
						<tr>
							<td height=20 width=150 bgcolor=#c8c8c8>clubyears</td>
							<td height=20 width=550 bgcolor=#f8f8f8>
								<font face="verdana" color="black" style="margin-left: 0px;font-size: 12;">
									<?php echo $person->clubyears; ?>
								</font>
							</td>
						</tr>
						<tr>
							<td height=20 width=150 bgcolor=#c8c8c8>cargo</td>
							<td height=20 width=550 bgcolor=#f8f8f8>
								<font face="verdana" color="black" style="margin-left: 0px;font-size: 12;">
									<?php echo $person->cargo; ?>
								</font>
							</td>
						</tr>
						<tr>
							<td height=20 width=150 bgcolor=#c8c8c8>palmares</td>
							<td height=20 width=550 bgcolor=#f8f8f8>
								<font face="verdana" color="black" style="margin-left: 0px;font-size: 12;">
									<?php echo $person->palmares; ?>
								</font>
							</td>
						</tr>
						<tr>
							<td height=20 width=150 bgcolor=#c8c8c8>facebook</td>
							<td height=20 width=550 bgcolor=#f8f8f8>
								<font face="verdana" color="black" style="margin-left: 0px;font-size: 12;">
									<?php echo $person->facebook; ?>
								</font>
							</td>
						</tr>
						<tr>
							<td height=20 width=150 bgcolor=#c8c8c8>twitter</td>
							<td height=20 width=550 bgcolor=#f8f8f8>
								<font face="verdana" color="black" style="margin-left: 0px;font-size: 12;">
									<?php echo $person->twitter; ?>
								</font>
							</td>
						</tr>
						<tr>
							<td height=20 width=150 bgcolor=#c8c8c8>rol</td>
							<td height=20 width=550 bgcolor=#f8f8f8>
								<font face="verdana" color="black" style="margin-left: 0px;font-size: 12;">
									<?php echo $person->rol; ?>
								</font>
							</td>
						</tr>
						<tr>
							<td height=20 width=150 bgcolor=#c8c8c8>fechaalta</td>
							<td height=20 width=550 bgcolor=#f8f8f8>
								<font face="verdana" color="black" style="margin-left: 0px;font-size: 12;">
									<?php echo $person->fechaalta; ?>
								</font>
							</td>
						</tr>
						<tr>
							<td height=20 width=150 bgcolor=#c8c8c8>fechabaja</td>
							<td height=20 width=550 bgcolor=#f8f8f8>
								<font face="verdana" color="black" style="margin-left: 0px;font-size: 12;">
									<?php echo $person->fechabaja; ?>
								</font>
							</td>
						</tr>

					</table>
                </td>

            </tr>
        </table>
        <!--- pie --->
        <?php require('pie.php'); ?>

    </body>
</html>