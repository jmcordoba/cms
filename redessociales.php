<?php

// Incluye los objetos necesarios
require("admin/objetos/social.php");
// Incluye las funciones necesarios
require("admin/fun/funciones.php");

?>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms</title>
		<link rel="stylesheet"    type="text/css"     href="css/menu_izquierda.css" />
		<link rel="stylesheet"    type="text/css"     href="css/redessociales.css" />
    </head>

    <body bgcolor="#e8e8e8" style="margin:0px;">
		
		<!--- cabecera --->
        <?php require('cabecera.php'); ?>
        <!--- cuerpo --->
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="35" align="center"><tr><td></td></tr></table>
        <table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="700" align="center">
            <tr>
				<td bgcolor="#ffffff" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    <?php
					include("cuerpo1.php");
					include("cuerpo3.php");
					include("cuerpo4.php");
					?>
                </td>
                <td></td>
				<td bgcolor="#ffffff" width="800" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
					
					<table style="position:relative;top:30px;margin-left:5px;text-align:justify;" width="98%" align="center">
						
						<tr>
							<td colspan="9" style="border-bottom:1px dashed #333333;">
								<font face="Arial" color="#394752" style="font-size:26px; font-style:normal; color:#000000;">
									<?php echo "Redes sociels"; ?>
								</font>
							</td>
						</tr>
						
						<tr height="20" bgcolor="#ffffff"><td></td></tr>
						
					</table>
					
					<table style="position:relative;top:30px;margin-left:5px;text-align:justify;" width="98%" align="center">
						
						<?php
						
						$social = obtenerSociales();
						
						for($i=0;$i<numRows("CMS_sociales");$i++) {
							?>
							<td bgcolor=#ffffff align=center width="30%">
								
								<table class="tabla_foto" width="200" height="200" style="border: 1px solid #000000;">
									<tr height="25">
										<td>
											<font face="verdana" color="black" style="font-size: 10px;">
												<?php echo $social[$i]->nombre; ?>
											</font>
										</td>
									</tr>
									<tr height="150" bgcolor="#ffffff">
										<td>
											<a href="#" onClick="modificar(<?php echo $album[$i]->idalbum; ?>);">
												<img border="0" width="200" height="150" src="<?php echo $social[$i]->icono; ?>"></img>
											</a>
										</td>
									</tr>
								</table>
							</td>
							<?php
							if((($i+1)%3)==0) {
								echo "</tr><tr>";
							}
						}
						
						if(numRows("CMS_sociales")<3) {
							for($i=0;$i<(3-numRows("CMS_sociales"));$i++) {
								echo "</td><td bgcolor=#ffffff width=\"30%\">";
							}
						}
						
						if(numRows("CMS_sociales")>3) {
							for($i=0;$i<((numRows("CMS_sociales")+1)%3);$i++) {
								echo "</td><td bgcolor=#ffffff width=\"30%\">";
							}
						}
						?>
							
						<?php
						/*
						$idsocial = isset($_REQUEST['idsocial']) ? $_REQUEST['idsocial'] : "";
						
						if($idsocial=="") {
							$social = obtenerSociales();
							for($i=0;$i<numRows("CMS_sociales");$i++) {
								$_POST["idsocial"] = $social[$i]->idsocial;
								$_POST["nombre"]   = $social[$i]->nombre;
								$_POST["link"]     = $social[$i]->link;
								$_POST["icono"]    = $social[$i]->icono;
								?>
								<tr height=10>
									<td width=100 bgcolor=#ffffff>
										<?php
										if($_POST["icono"]!="") {
											?><img height="50" width="100" width="100" src="http://www.apuestta.com/cmsweb/<?php echo $_POST["icono"]; ?>"></img><?php
										}
										?>
									</td>
									<td width=100 bgcolor=#f8f8f8>
										<font face="verdana" color="black" style="font-size: 12px;">
											<?php echo $_POST["nombre"];?>
										</font>
									</td>
									<td width="<?php
										if($_POST["icono"]!="") {
											echo "500";
										} else {
											echo "600";
										}
										?>" bgcolor="#f8f8f8">
										<a href="http://<?php echo $_POST["link"];?>">
											<font face="verdana" color="black" style="font-size: 12px;">
												<?php echo $_POST["link"];?>
											</font>
										</a>
									</td>
								</tr>
								<?php
							}
						} else {
							$social = obtenerUnaSocial($idsocial);
							
							$_POST["idsocial"] = $social->idsocial;
							$_POST["nombre"]   = $social->nombre;
							$_POST["link"]     = $social->link;
							$_POST["icono"]    = $social->icono;
							?>
							<tr height=10>
								<td width=100 bgcolor=#ffffff>
									<img height="50" width="100" src="http://www.apuestta.com/cmsweb/admin/<?php echo $_POST["icono"]; ?>"></img>
								</td>
								<td width=100 bgcolor=#f8f8f8>
									<font face="verdana" color="black" style="font-size: 12px;">
										<?php echo $_POST["nombre"];?>
									</font>
								</td>
								<td width=500 bgcolor=#f8f8f8>
									<a href="http://<?php echo $_POST["link"];?>">
										<font face="verdana" color="black" style="font-size: 12px;">
											<?php echo $_POST["link"];?>
										</font>
									</a>
								</td>
							</tr>
							<?php
						}
						*/
						?>
					</table>
                </td>
            </tr>
        </table>
        <!--- pie --->
        <?php require('pie.php'); ?>

    </body>
</html>