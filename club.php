<?php

// Incluye los objetos necesarios
require("admin/objetos/club.php");

// Incluye las funciones necesarios
require("admin/fun/funciones.php");

?>

<!DOCTYPE html>

<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms</title>
		<link rel="stylesheet"    type="text/css"     href="css/menu_izquierda.css" />
		<link rel="stylesheet"    type="text/css"     href="css/club.css" />
    </head>

    <body bgcolor="#e8e8e8" style="margin:0px;">
		
		<!-- cabecera -->
        <?php require('cabecera.php'); ?>
        <!-- cuerpo -->
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="35" align="center"><tr><td></td></tr></table>
        <table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="700" align="center">
            <tr>
				<td bgcolor="#ffffff" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    <?php
					include("cuerpo1.php");
					include("cuerpo3.php");
					include("cuerpo4.php");
					?>
                </td>
                <td></td>
				<td bgcolor="#ffffff" width="800" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    
					<?php
		
					$equipo = obtenerClub();
					$_POST["name"]     = $equipo->name;
					$_POST["presi"]    = $equipo->presi;
					$_POST["street"]   = $equipo->street;
					$_POST["cpostal"]  = $equipo->cpostal;
					$_POST["telefono"] = $equipo->telefono;
					$_POST["fax"]      = $equipo->fax;
					$_POST["mail"]     = $equipo->mail;
					$_POST["palmares"] = $equipo->palmares;
					$_POST["historia"] = $equipo->historia;
					?>

					<table style="position:relative;top:30px;margin-left:5px;text-align:justify;" width="99%" align="center">

						<tr>
							<td colspan="2" style="border-bottom:1px dashed #333333;">
								<font class="titulo_pagina"><?php echo "Información de club"; ?></font>
							</td>
						</tr>
						
						<tr height="10" bgcolor="#ffffff"><td></td></tr>
						
						<tr>
							<td class="td_titulo"><font class="titulo">Nombre del club</font></td>
							<td class="td_info"><font class="informacion"><?php echo $_POST["name"];?></font></td>
						</tr>
						<tr>
							<td class="td_titulo"><font class="titulo">Presidente</font></td>
							<td class="td_info"><font class="informacion"><?php echo $_POST["presi"];?></font></td>
						</tr>
						<tr>
							<td class="td_titulo"><font class="titulo">Direccion</font></td>
							<td class="td_info"><font class="informacion"><?php echo $_POST["street"];?></font></td>
						</tr>
						<tr>
							<td class="td_titulo"><font class="titulo">Codigo Postal</font></td>
							<td class="td_info"><font class="informacion"><?php echo $_POST["cpostal"];?></font></td>
						</tr>
						<tr>
							<td class="td_titulo"><font class="titulo">Telefono</font></td>
							<td class="td_info"><font class="informacion"><?php echo $_POST["telefono"];?></font></td>
						</tr>
						<tr>
							<td class="td_titulo"><font class="titulo">Fax</font></td>
							<td class="td_info"><font class="informacion"><?php echo $_POST["fax"];?></font></td>
						</tr>
						<tr>
							<td class="td_titulo"><font class="titulo">Correo electronico</font></td>
							<td class="td_info"><font class="informacion"><?php echo $_POST["mail"];?></font></td>
						</tr>

						<tr height="10" bgcolor="#ffffff"><td></td></tr>
						
						<tr>
							<td colspan="2" height="20" width="800" style="border-bottom:1px dashed #333333;">
								<font class="titulo_pagina">Palmares</font>
							</td>
						</tr>
						<tr height="10" bgcolor="#ffffff"><td></td></tr>
						<tr>
							<td colspan="2">
								<font class="titulo"><?php echo $_POST["palmares"]; ?></font>
							</td>
						</tr>
						
						<tr height="10" bgcolor="#ffffff"><td></td></tr>
						
						<tr>
							<td colspan="2" height="20" width="800" style="border-bottom:1px dashed #333333;">
								<font class="titulo_pagina">Historia</font>
							</td>
						</tr>
						<tr height="10" bgcolor="#ffffff"><td></td></tr>
						<tr>
							<td colspan="2">
								<font class="titulo"><?php echo $_POST["historia"]; ?></font>
							</td>
						</tr>
					</table>
					
                </td>
            </tr>
        </table>

        <?php require('pie.php'); ?>

    </body>
</html>