<?php

// Incluye los objetos necesarios
require("../admin/objetos/comentario.php");

// Incluye las funciones necesarios
require("../admin/fun/funciones.php");

// Obtenemos las variables del formulario
$comen->idart     = addslashes($_POST["idcro"]);
$comen->fecha     = addslashes($_POST["fecha"]);
$comen->iduser    = addslashes($_POST["iduser"]);
$comen->contenido = addslashes($_POST["contenido"]);
$comen->conta     = "0";

// Intentamos guardar el usuario y redirigimos en funci�n de si hemos tenido �xito o no
if(guardarComentario($comen)==true) redirect_comentario("../cronica.php?idcro=$comen->idart" ,0);
else                                redirect_comentario("../index.php?origen=error",0);

?>