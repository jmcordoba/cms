<?php

// Incluye los objetos necesarios
require("../admin/objetos/comentario.php");

// Incluye las funciones necesarios
require("../admin/fun/funciones.php");

// Obtenemos las variables del formulario
$comen->idart     = addslashes($_POST["idart"]);
$comen->fecha     = addslashes($_POST["fecha"]);
$comen->iduser    = addslashes($_POST["iduser"]);
$comen->contenido = addslashes($_POST["contenido"]);
$comen->conta     = "0";

// Intentamos guardar el usuario y redirigimos en función de si hemos tenido éxito o no
if(guardarComentario($comen)==true) redirect_comentario("../articulo.php?idart=$comen->idart" ,0);
else                                redirect_comentario("../index.php?origen=error",0);

?>