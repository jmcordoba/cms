<?php

/* ************************************************************************************************
* Limpia un string de SQL injection
************************************************************************************************ */
function cleanQuery($string) {
    if(get_magic_quotes_gpc()) $string = stripslashes($string);
    if (phpversion() >= '4.3.0') $string = mysql_real_escape_string($string);
    else $string = mysql_escape_string($string);
    return $string;
}

?>