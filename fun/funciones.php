<?php

require_once($_SERVER['DOCUMENT_ROOT']."/cmsweb/conf.php");

function Conectarse() {
    if (!($link=mysql_connect(conf_HOST,conf_USER,conf_PSWD))) {
        echo "Error conectando a la base de datos.";
        exit();
    }
    if (!mysql_select_db(conf_BBDD,$link)) {
        echo "Error seleccionando la base de datos.";
        exit();
    }
    return $link;
}

function cleanQuery($string) {
    if(get_magic_quotes_gpc()) $string = stripslashes($string);
    if (phpversion() >= '4.3.0') $string = mysql_real_escape_string($string);
    else $string = mysql_escape_string($string);
    return $string;
}

function redirect($pagina, $tiempo){
    ?><script>location.href="<?php echo $pagina; ?>";</script><?php
	die();
}

function guardarUsuario($usuario) {

	$link     = Conectarse();
    $sql      = "SELECT * FROM `CMS_users` WHERE mail = '" . $usuario->mail . "'";
    $result   = mysql_query($sql, $link);
    $num_rows = mysql_num_rows($result);
    mysql_close($link);

	if($num_rows==0) {
	
		$link=Conectarse();

		$sql = "INSERT INTO  `CMS_users` (  
				`nombre` ,  
				`apellidos` ,  
				`usuario` ,  
				`password` ,  
				`telefono` ,  
				`mail` ,  
				`permisos` ,  
				`conta` ,  
				`validado` ,  
				`fecha_alta` , 
				`foto` ) 
		VALUES (
				'" . $usuario->nombre . "',
				'" . $usuario->apellidos . "' ,
				'" . $usuario->usuario . "' ,
				'" . $usuario->password . "' ,
				'" . $usuario->telefono . "' ,
				'" . $usuario->mail . "' ,
				'" . $usuario->permisos . "' ,
				'" . $usuario->conta . "' ,
				'" . $usuario->validado . "' ,
				'" . $usuario->fecha_alta . "' ,
				'" . $usuario->foto . "'
		);";
		
		$result = mysql_query($sql, $link);

		mysql_close($link);
		
		//define the receiver of the email
		$to = $usuario->mail;
		
		//define the headers we want passed. Note that they are separated with \r\n
		$headers  = "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/plain; charset=utf-8\r\n";
		$headers .= "From: Juanma Córdoba <admin@cmsport.com>\r\nReply-To: jmcordoba@gmail.com\r\n";
		
		//define the subject of the email
		$subject = 'Confirmación de su cuenta de usuario'; 
		
		//define the message to be sent.
		$body  = "Gracias por registrarse como usuario, a continuación se pueden ver los datos que usted ha introducido:\n\r";
		$body .= "Nombre:         " . $usuario->nombre . "\n\r";
		$body .= "Apellidos:      " . $usuario->apellidos  . "\n\r";
		$body .= "Fecha de alta : " . $usuario->fecha_alta . "\n\r";
		$body .= "Usuario:        " . $usuario->usuario    . "\n\r";
		$body .= "Password:       " . $usuario->password   . "\n\r";
		$body .= "Telefono:       " . $usuario->telefono   . "\n\r";
		$body .= "Por favor, haga click en el siguiente enlace como último paso para activar su cuenta:\n\r";
		$body .= "http://www.".$_SERVER["HTTP_HOST"]."/cmsweb/fun/valida.php?mail=" . $usuario->mail . "\n\r";
		$body .= "Gracias,\n\r";
		$body .= "El equipo de CMSport.\n\r";
		
		//send the email
		$mail_sent = @mail( $to, $subject, $body, $headers );
		
		return 1;
	} else {
		return $usuario->mail;
	}

	return false;
    
}

function encuentraUsuario($mail) {
    # seguridad
	$mail = addslashes($mail);
	// Buscamos al usuario que pretende hacer la validacion
    $link=Conectarse();
    $sql = "SELECT * FROM `CMS_users` WHERE mail = '" . $mail . "'";
    $result = mysql_query($sql, $link);
    
	if ($row = mysql_fetch_array($result)) {
        do{
            $usuario->nombre    = $row["nombre"];
            $usuario->apellidos = $row["apellidos"];
            $usuario->usuario   = $row["usuario"];
            $usuario->password  = $row["password"];
            $usuario->telefono  = $row["telefono"];
            $usuario->mail      = $row["mail"];
            $usuario->permisos  = $row["permisos"];
            $usuario->conta     = $row["conta"];
            $usuario->validado  = $row["validado"];
            $usuario->foto      = $row["foto"];

        } while ($row = mysql_fetch_array($result));
    }
    mysql_close($link);
    
    // Validamos al usuario si no está validado
    if($usuario->validado=="no")
    {
        $link=Conectarse();
        $sql = "UPDATE `CMS_users` SET `validado` = 'si' WHERE `mail` = '" . $usuario->mail . "'";
        $result = mysql_query($sql, $link);
        mysql_close($link);
    }
}

function loginUsuario($user, $pasw) {
    # seguridad
	$user = addslashes($user);
	$pasw = addslashes($pasw);
	// Inicializo variable
    $validado = "";
    // Buscamos al usuario que pretende hacer la validacion
    $link=Conectarse();
    $sql = "SELECT * FROM `CMS_users` WHERE usuario = '" . $user . "' AND password = '" . $pasw . "'";
    
    $result = mysql_query($sql, $link);
    $num_rows = mysql_num_rows($result);
    if($num_rows>0)
    {
        if ($row = mysql_fetch_array($result))
        {
            do{
                $usuario->nombre    = $row["nombre"];
                $usuario->apellidos = $row["apellidos"];
                $usuario->usuario   = $row["usuario"];
                $usuario->password  = $row["password"];
                $usuario->telefono  = $row["telefono"];
                $usuario->mail      = $row["mail"];
                $usuario->permisos  = $row["permisos"];
                $usuario->conta     = $row["conta"];
                $usuario->validado  = $row["validado"];
                $usuario->foto      = $row["foto"];
                
                if($usuario->validado=="si") $validado="si";

            } while ($row = mysql_fetch_array($result));
        }
        mysql_close($link);
    }
    
    if($validado!="") return $usuario;
    else return false;
}

function numRows($table) {
    
	$link=Conectarse();
    
    $result = mysql_query("select * from " . $table, $link);
    $num_rows = mysql_num_rows($result);
    
    mysql_close($link);
    
    return $num_rows;
}

?>
