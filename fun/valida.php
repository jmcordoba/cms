<?php

// Incluye las funciones necesarias para la validacion
include("funciones.php");

// Obtiene el mail del usuario que quiere validarse
$mail = addslashes($_GET["mail"]);

// Valida al usuario que ha hecho la petición de validacion
encuentraUsuario($mail);

// Redireccionamos para dar las gracias
redirect("../gracias.php?origen=valida",0);

?>
