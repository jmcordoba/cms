<?php

// Adjuntamos los archivos de objetos y funciones
require("../objetos/usuario.php");
require("funciones.php");

// Capturamos los datos que vienen por POST y los guardamos en el objeto equipo
$usuario->nombre     = addslashes($_POST["nombre"]);
$usuario->apellidos  = addslashes($_POST["apellidos"]);
$usuario->usuario    = addslashes($_POST["mail"]);
$usuario->password   = addslashes($_POST["password"]);
$usuario->telefono   = addslashes($_POST["telefono"]);
$usuario->mail       = addslashes($_POST["mail"]);
$usuario->permisos   = "usuario";

# validad >> si; porque cuando se instala en una máuina virtual NO hay configurado un gestor de correo
$usuario->validado   = "si";

$usuario->fecha_alta = date("d/m/Y"); 
$usuario->foto       = "images/usuario/" . $usuario->mail . ".gif";

// Capturamos los datos del fichero adjunto
$nombre_archivo = $_FILES["userfile"]["name"]; 
$tipo_archivo   = $_FILES["userfile"]["type"]; 
$tamano_archivo = $_FILES["userfile"]["size"];

// Si viene un fichero adjunto
if($nombre_archivo!="") {
    //compruebo si las caracter�sticas del archivo son las que deseo 
    if ($tamano_archivo < 100000) {
        // Guardamos el fichero en la ruta especificada
        if (move_uploaded_file($HTTP_POST_FILES['userfile']['tmp_name'], "../images/usuario/" . $usuario->mail . ".gif")){
            // Si hay algún tipo de error, redirigimos a otra página
            ?><script>location.href="../index.php?origen=error";</script><?php
        }
    }
} else {
	$usuario->foto = "images/usuario/user.jpg";
}

$sAux = guardarUsuario($usuario);

if($sAux==1) redirect("../gracias.php?origen=user",0);
else         redirect("../gracias.php?origen=existe:".$usuario->mail,0);
?>
