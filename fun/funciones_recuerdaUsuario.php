<?php

require_once($_SERVER['DOCUMENT_ROOT']."/cmsweb/conf.php");

function Conectarse_remember() {
    if (!($link=mysql_connect(conf_HOST,conf_USER,conf_PSWD))) {
        echo "Error conectando a la base de datos.";
        exit();
    }
    if (!mysql_select_db(conf_BBDD,$link)) {
        echo "Error seleccionando la base de datos.";
        exit();
    }
    return $link;
}

function recuerdaUsuario($mail) {
	# seguridad
	$mail = addslashes($mail);
	
	# conectamos
	$link     = Conectarse_remember();
    $sql      = "SELECT * FROM `CMS_users` WHERE mail = '$mail'";
    $result   = mysql_query($sql, $link);
    $num_rows = mysql_num_rows($result);
	
	if($num_rows>0) {
        if ($row = mysql_fetch_array($result)) {
            do {
                $usuario->nombre    = $row["nombre"];
                $usuario->apellidos = $row["apellidos"];
                $usuario->usuario   = $row["usuario"];
                $usuario->password  = $row["password"];
                $usuario->telefono  = $row["telefono"];
                $usuario->mail      = $row["mail"];
                $usuario->permisos  = $row["permisos"];
                $usuario->conta     = $row["conta"];
                $usuario->validado  = $row["validado"];
                $usuario->foto      = $row["foto"];

            } while ($row = mysql_fetch_array($result));
        }
        mysql_close($link);

        //define the receiver of the email
        $to = $usuario->mail;
        //define the headers we want passed. Note that they are separated with \r\n
        $headers  = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/plain; charset=utf-8\r\n";
        $headers .= "From: Juanma Cordoba <admin@cmsport.com>\r\nReply-To: admin@cmsport.com\r\n";
        //define the subject of the email
        $subject = "Confirmación de su cuenta de usuario"; 
        //define the message to be sent.
        $body  = "Hola ". $usuario->nombre    . ",\n\r";
        $body .= "el password que debes utilizar para identificarte como usuario es: ". $usuario->password  . "\n\r";
        $body .= "Gracias,\n\r";
        $body .= "El equipo de CMSweb.\n\r";
        //send the email
        $mail_sent = @mail( $to, $subject, $body, $headers );
        
        return true;
    }
    
    return false;
}

function redirect_remember($pagina, $tiempo){
    ?><script>location.href="<?php echo $pagina; ?>";</script><?php
	die();
}

?>