<?php

// Incluye los objetos necesarios
require("admin/objetos/club.php");
require("admin/objetos/equipo.php");

require("admin/objetos/album.php");
require("admin/objetos/fotografia.php");

// Incluye las funciones necesarios
require("admin/fun/funciones.php");

?>

<!DOCTYPE html>

<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms</title>
		<link rel="stylesheet"    type="text/css"     href="css/menu_izquierda.css" />
		<link rel="stylesheet"    type="text/css"     href="css/galerias_fotos.css" />
    </head>
	

    <body bgcolor="#e8e8e8" style="margin:0px;">
	
		<!--- cabecera --->
        <?php require('cabecera.php'); ?>
        <!--- cuerpo --->
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="35" align="center"><tr><td></td></tr></table>
        <table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="700" align="center">
            <tr>
				<td bgcolor="#ffffff" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    <?php
					include("cuerpo1.php");
					include("cuerpo3.php");
					include("cuerpo4.php");
					?>
                </td>
                <td></td>
				<td bgcolor="#ffffff" width="800" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
					
                    <table style="position:relative;top:30px;margin-left:5px;text-align:justify;" width="99%" align="center">
						
						<tr>
							
							<?php
							
							// Obtenemos el directorio del album
							$idalbum = $_GET["idalbum"];
							$album   = obtenerUnAlbum($idalbum);
							
							//
							// Aqui mostraremos las imagenes que existan en el directorio del album
							//
							
							$contador = 0;
							
							if(directoryexists("admin/".$album->ubicacion)) {												
								// Abrimos el directorio
								$directorio=opendir("admin/".$album->ubicacion); 																																																					// <cfdirectory action="LIST" directory="#ruta#logos/#dom#" name="logoTPV" sort="name ASC,datelastmodified" filter="LOGO*.*">
								// Obtenemos la lista de archivos y los vamos tratando uno a uno
								while ($archivo = readdir($directorio)) {																																																											// <cfloop list="#imagen#" index="nombre">
									if(($archivo!=".")and($archivo!="..")) {
										$fotografia = obtenerUnaFotoDeUnAlbum($album->idalbum,$archivo);
										if($fotografia!=false){
											?>
											<td align="center">
												<table class="tabla_foto" border="0" cellspacing=0>
													<tr bgcolor="#ffffff">
														<td>
															<font face="arial" style="font-size: 14px; color: #394752;">
																<?php 
																	if($fotografia->pie!="") echo $fotografia->pie;
																	else                     echo $fotografia->idfoto;
																?>
															</font>
														</td>
													</tr>
													<tr>
														<td>
															<img width="350" height="225" src="<?php echo "admin/".$fotografia->ruta; ?>"></img>
														</td>
													</tr>
												</table>
											</td>
											<?php
											$contador++;
											if(($contador%2)==0) {
												?></tr><tr><?php
											}
										}
									}
								}																																																																					// </cfoutput>
								// Cerramos el directorio
								closedir($directorio);
							}
							?>

						</tr>
						
					</table>

                </td>
            </tr>
        </table>
        <!--- pie --->
        <?php require('pie.php'); ?>

    </body>
</html>