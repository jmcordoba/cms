<?php

// Incluye los objetos necesarios
require("admin/objetos/torneo.php");

// Incluye las funciones necesarios
require("admin/fun/funciones.php");

?>

<!DOCTYPE html>

<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms</title>
		<link rel="stylesheet"    type="text/css"     href="css/menu_izquierda.css" />
		<link rel="stylesheet"    type="text/css"     href="css/torneos.css" />
    </head>

    <body bgcolor="#e8e8e8" style="margin:0px;">
		
		<!-- cabecera -->
        <?php require('cabecera.php'); ?>
        <!-- cuerpo -->
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="35" align="center"><tr><td></td></tr></table>
        <table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="700" align="center">
            <tr>
				<td bgcolor="#ffffff" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    <?php
					include("cuerpo1.php");
					include("cuerpo3.php");
					include("cuerpo4.php");
					?>
                </td>
                <td></td>
				<td bgcolor="#ffffff" width="800" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    
					<table style="position:relative;top:30px;margin-left:5px;text-align:justify;" width="98%" align="center">
						
						<tr>
							<td colspan="9" style="border-bottom:1px dashed #333333;">
								<font face="Arial" color="#394752" style="font-size:26px; font-style:normal; color:#000000;">
									<?php 
									$idtorneo = isset($_GET["idtorneo"]) ? $_GET["idtorneo"] : "";
									if($idtorneo=="") echo "Torneos"; 
									else              echo "Siguiente torneo";
									?>
								</font>
							</td>
						</tr>
						
						<tr height="20" bgcolor="#ffffff"><td></td></tr>
						
					</table>
					
					<?php 
					
					if($idtorneo=="") {
						$torneos = obtenerTorneo();
						
						for($i=0;$i<count($torneos);$i++) {
							?>
							<table class="tabla_torneo" style="vertical-align:top;margin-top:30px;margin-bottom:20px;margin-left:0px;" width="98%" align="center">

								<tr vAlign="top">
									<td width="600">
										<table style="margin-top:10px;">
											<tr>
												<td height=20 width=600 style="border-bottom: 1px solid #394752">
													<font face="verdana" color="#394752" style="margin-left: 4px;font-size: 16px;">
														<b><?php echo $torneos[$i]->name; ?></b>
													</font>
												</td>
											</tr>
											<tr>
												<td height=20 width=600>
													<font face="verdana" color="black" style="margin-left: 4px;font-size: 12px;">
														<?php echo "Fecha: ".$torneos[$i]->fecha; ?>
													</font>
												</td>
											</tr>
											<tr>
												<td height=20 width=600>
													<font face="verdana" color="black" style="margin-left: 4px;font-size: 12px;">
														<?php echo "Hora: ".$torneos[$i]->hour; ?>
													</font>
												</td>
											</tr>
											<tr>
												<td height=20 width=600>
													<font face="verdana" color="black" style="margin-left: 4px;font-size: 12px;">
														<?php echo "Direccion: ".$torneos[$i]->location; ?>
													</font>
												</td>
											</tr>
											<tr>
												<td height=20 width=600>
													<font face="verdana" color="black" style="margin-left: 4px;font-size: 12px;">
														<?php echo "Descripcion: ".$torneos[$i]->description; ?>
													</font>
												</td>
											</tr>
											<tr>
												<td height=20 width=600>
													<font face="verdana" color="black" style="margin-left: 4px;font-size: 12px;">
														<?php echo "Info: ".$torneos[$i]->info; ?>
													</font>
												</td>
											</tr>
											<tr>
												<td height=20 width=600>
													<font face="verdana" color="black" style="margin-left: 4px;font-size: 12px;">
														<?php echo "Numero de cuenta: ".$torneos[$i]->cuenta; ?>
													</font>
												</td>
											</tr>
											<tr>
												<td height=20 width=600>
													<font face="verdana" color="black" style="margin-left: 4px;font-size: 12px;">
														<?php echo "Titular de la cuenta: ".$torneos[$i]->titular; ?>
													</font>
												</td>
											</tr>
											<tr>
												<td height=20 width=600>
													<font face="verdana" color="black" style="margin-left: 4px;font-size: 12px;">
														<?php echo "Precio del torneo: ".$torneos[$i]->precio; ?>
													</font>
												</td>
											</tr>
											<tr>
												<td height=20 width=600>
													<font face="verdana" color="black" style="margin-left: 4px;font-size: 12px;">
														<?php echo $torneos[$i]->gmaps; ?>
													</font>
												</td>
											</tr>
										</table>
									</td>	
									<td height="20" width="200" height="282" bgcolor="#ffffff">
										<img src="<?php echo $torneos[$i]->cartel; ?>" width="200" height="282"></img>
									</td>	
								</tr>
							</table>
							<?php
						}
					} else {
						$torneos = obtenerUnTorneo($idtorneo);
						
						?>
						<table class="tabla_torneo" style="vertical-align:top;margin-top:30px;margin-bottom:20px;margin-left:0px;" width="98%" align="center">
							<tr vAlign="top">
								<td width="600">
									<table style="margin-top:10px;">
										<tr>
											<td height=20 width=600 style="border-bottom: 1px solid #394752">
												<font face="verdana" color="#394752" style="margin-left: 4px;font-size: 16px;">
													<b><?php echo $torneos->name; ?></b>
												</font>
											</td>
										</tr>
										<tr>
											<td height=20 width=600>
												<font face="verdana" color="black" style="margin-left: 4px;font-size: 12px;">
													<?php echo "Fecha: ".$torneos->fecha; ?>
												</font>
											</td>
										</tr>
										<tr>
											<td height=20 width=600>
												<font face="verdana" color="black" style="margin-left: 4px;font-size: 12px;">
													<?php echo "Hora: ".$torneos->hour; ?>
												</font>
											</td>
										</tr>
										<tr>
											<td height=20 width=600>
												<font face="verdana" color="black" style="margin-left: 4px;font-size: 12px;">
													<?php echo "Direccion: ".$torneos->location; ?>
												</font>
											</td>
										</tr>
										<tr>
											<td height=20 width=600>
												<font face="verdana" color="black" style="margin-left: 4px;font-size: 12px;">
													<?php echo "Descripcion: ".$torneos->description; ?>
												</font>
											</td>
										</tr>
										<tr>
											<td height=20 width=600>
												<font face="verdana" color="black" style="margin-left: 4px;font-size: 12px;">
													<?php echo "Info: ".$torneos->info; ?>
												</font>
											</td>
										</tr>
										<tr>
											<td height=20 width=600>
												<font face="verdana" color="black" style="margin-left: 4px;font-size: 12px;">
													<?php echo "Numero de cuenta: ".$torneos->cuenta; ?>
												</font>
											</td>
										</tr>
										<tr>
											<td height=20 width=600>
												<font face="verdana" color="black" style="margin-left: 4px;font-size: 12px;">
													<?php echo "Titular de la cuenta: ".$torneos->titular; ?>
												</font>
											</td>
										</tr>
										<tr>
											<td height=20 width=600>
												<font face="verdana" color="black" style="margin-left: 4px;font-size: 12px;">
													<?php echo "Precio del torneo: ".$torneos->precio; ?>
												</font>
											</td>
										</tr>
										<tr>
											<td height=20 width=600>
												<font face="verdana" color="black" style="margin-left: 4px;font-size: 12px;">
													<?php echo $torneos->gmaps; ?>
												</font>
											</td>
										</tr>
									</table>
								</td>	
								<td height="20" width="200" height="282" bgcolor="#ffffff">
									<img src="<?php echo $torneos->cartel; ?>" width="200" height="282"></img>
								</td>	
							</tr>
						</table>
						<?php
					}
					?>
					
                </td>
            </tr>
        </table>
        
		<!-- pie -->
        <?php require('pie.php'); ?>

    </body>
</html>