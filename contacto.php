<?php

// Incluye las funciones necesarios
require("admin/fun/funciones.php");

?>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>contacto</title>
		<link rel="stylesheet"    type="text/css"     href="css/menu_izquierda.css" />
		<link rel="stylesheet"    type="text/css"     href="css/contacto.css" />
    </head>

    <body bgcolor="#e8e8e8" style="margin:0px;">
		
		<!--- cabecera --->
        <?php require('cabecera.php'); ?>
        
		<!--- cuerpo --->
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="35" align="center"><tr><td></td></tr></table>
        <table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="700" align="center">
            <tr>
				<td bgcolor="#ffffff" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    <?php
					include("cuerpo1.php");
					include("cuerpo3.php");
					include("cuerpo4.php");
					?>
                </td>
                <td></td>
				<td bgcolor="#ffffff" width="800" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    
					<table style="position:relative;top:30px;margin-left:5px;text-align:justify;" width="99%" align="center">
						
						<tr>
							<td colspan="2" style="border-bottom:1px dashed #333333;">
								<font class="titulo_pagina"><?php echo "Contacta con nosotros"; ?></font>
							</td>
						</tr>
						
						<tr height="10" bgcolor="#ffffff"><td></td></tr>
						
						<form name="frm" action="contacto_enviar.php" method=post>
						
							<tr>
								<td class="td_titulo"><font class="titulo">Nombre</font></td>
								<td class="td_info"><input class="input" type="text" name="nombre"></td>
							</tr>
							
							<tr>
								<td class="td_titulo"><font class="titulo">Asunto</font></td>
								<td class="td_info"><input class="input" type="text" name="subject"></td>
							</tr>

							<tr>
								<td class="td_titulo"><font class="titulo">Email</font></td>
								<td class="td_info"><input class="input" type="text" name="email"></td>
							</tr>
							
							<tr>
								<td class="td_titulo"><font class="titulo">Comentarios</font></td>
								<td class="td_info"><textarea name="coment" style="width:550px;"></textarea></td>
							</tr>
							
							<tr valign="top" align="left" border="1">
								<td class="td_titulo"></td>
								<td class="td_info"><input type="submit" value="Enviar"></td>
							</tr>
							
							<input class="input" type="hidden" maxlength="9" name="telefono" value="666666666">
						</form>
					</table>
					
                </td>
            </tr>
        </table>
        
		<?php require('pie.php'); ?>

    </body>
</html>