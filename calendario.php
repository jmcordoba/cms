<?php

// Incluimos los objetos necesarios
require("admin/objetos/jornada.php");

// Incluye las funciones necesarios
require("admin/fun/funciones.php");

?>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>calendario</title>
		<link rel="stylesheet"    type="text/css"     href="css/menu_izquierda.css" />
		<link rel="stylesheet"    type="text/css"     href="css/calendario.css" />
    </head>

    <body bgcolor="#e8e8e8" style="margin:0px;">
		
		<!--- cabecera --->
        <?php require('cabecera.php'); ?>
        <!--- cuerpo --->
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="35" align="center"><tr><td></td></tr></table>
        <table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="700" align="center">
            <tr>
				<td bgcolor="#ffffff" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    <?php
					include("cuerpo1.php");
					include("cuerpo3.php");
					include("cuerpo4.php");
					?>
                </td>
                <td></td>
				<td bgcolor="#ffffff" width="800" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    
					<table style="position:relative;top:30px;margin-left:5px;text-align:justify;" width="99%" align="center">
						
						<tr>
							<td colspan="2" style="border-bottom:1px dashed #333333;">
								<font class="titulo_pagina"><?php echo "Calendario"; ?></font>
							</td>
						</tr>
						
						<tr height="10" bgcolor="#ffffff"><td></td></tr>
					
					</table>
					
					<table width="790" style="margin-top:30px;margin-left:5px;text-align:justify;" width="99%" align="center">
						
						<tr height="20" bgcolor="#ffffff"><td></td></tr>
						
						<tr>
							<td colspan="9" height="20" width="100%" bgcolor="#ffffff" align="right">
								<font face="verdana" color="black" style="margin-left: 0px;font-size: 12px;">
									<?php echo "Jornadas disputadas: " . ultimaJornadaDisputada($idcat); ?>
								</font>
							</td>
						</tr>
						
						<tr>
							<td height="30" width="40"  bgcolor="#394752"><font face="verdana" color="#c8c8c8" style="font-size: 12px;"><?php echo "Jornada"; ?>    </font></td>
							<td height="30" width="200" bgcolor="#394752"><font face="verdana" color="#c8c8c8" style="font-size: 12px;"><?php echo "Local"; ?>      </font></td>
							<td height="30" width="200" bgcolor="#394752"><font face="verdana" color="#c8c8c8" style="font-size: 12px;"><?php echo "Visitante"; ?>  </font></td>
							<td height="30" width="75"  bgcolor="#394752"><font face="verdana" color="#c8c8c8" style="font-size: 12px;"><?php echo "Fecha"; ?>      </font></td>
							<td height="30" width="75"  bgcolor="#394752"><font face="verdana" color="#c8c8c8" style="font-size: 12px;"><?php echo "Hora"; ?>       </font></td>
							<td height="30" width="200" bgcolor="#394752"><font face="verdana" color="#c8c8c8" style="font-size: 12px;"><?php echo "Instalacion"; ?></font></td>
						</tr>
						
						<?php
						
						// obtenemos el nombre del equipo del club y de la categoria
						$equipo       = obtenerUnEquipoByCat($idcat);
						$nombreEquipo = trim(strtolower($equipo->nombre));

						// obtenemos el numero de jornadas que hay en el campeonato
						$jornadas = obtenerJornadas($idcat);
						
						// color de la primera fila del calendario, variará para par e impar
						$colordefondo = "#f8f8f8";
						
						// por cada una de las jornadas disputadas
						for($j=0;$j<numRowsJornadas($idcat);$j++) {
							// si encontramos al equipo del club
							if((strtolower(strcasecmp($jornadas[$j]->local01,$nombreEquipo)==0))or(strtolower(strcasecmp($jornadas[$j]->visit01,$nombreEquipo)==0))) {
								?>
								<tr>
									<td height="30" width="40"  bgcolor="<?php echo $colordefondo; ?>"><a href="jornada.php?idcat=<?php echo $idcat; ?>&idjorn=<?php echo $j; ?>" style="text-decoration:none;"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $j+1; ?></a></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->local01; ?></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->visit01; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->fecha01; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->hour01; ?> </font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->inst01; ?> </font></td>
								</tr>
								<?php
							}
							// si encontramos al equipo del club
							if((strtolower(strcasecmp($jornadas[$j]->local02,$nombreEquipo)==0))or(strtolower(strcasecmp($jornadas[$j]->visit02,$nombreEquipo)==0))) {
								?>
								<tr>
									<td height="30" width="40"  bgcolor="<?php echo $colordefondo; ?>"><a href="jornada.php?idcat=<?php echo $idcat; ?>&idjorn=<?php echo $j; ?>" style="text-decoration:none;"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $j+1; ?></a></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->local02; ?></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->visit02; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->fecha02; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->hour02; ?> </font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->inst02; ?> </font></td>
								</tr>
								<?php
							}
							// si encontramos al equipo del club
							if((strtolower(strcasecmp($jornadas[$j]->local03,$nombreEquipo)==0))or(strtolower(strcasecmp($jornadas[$j]->visit03,$nombreEquipo)==0))) {
								?>
								<tr>
									<td height="30" width="40"  bgcolor="<?php echo $colordefondo; ?>"><a href="jornada.php?idcat=<?php echo $idcat; ?>&idjorn=<?php echo $j; ?>" style="text-decoration:none;"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $j+1; ?></a></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->local03; ?></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->visit03; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->fecha03; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->hour03; ?> </font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->inst03; ?> </font></td>
								</tr>
								<?php
							}
							// si encontramos al equipo del club
							if((strtolower(strcasecmp($jornadas[$j]->local04,$nombreEquipo)==0))or(strtolower(strcasecmp($jornadas[$j]->visit04,$nombreEquipo)==0))) {
								?>
								<tr>
									<td height="30" width="40"  bgcolor="<?php echo $colordefondo; ?>"><a href="jornada.php?idcat=<?php echo $idcat; ?>&idjorn=<?php echo $j; ?>" style="text-decoration:none;"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $j+1; ?></a></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->local04; ?></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->visit04; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->fecha04; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->hour04; ?> </font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->inst04; ?> </font></td>
								</tr>
								<?php
							}
							// si encontramos al equipo del club
							if((strtolower(strcasecmp($jornadas[$j]->local05,$nombreEquipo)==0))or(strtolower(strcasecmp($jornadas[$j]->visit05,$nombreEquipo)==0))) {
								?>
								<tr>
									<td height="30" width="40"  bgcolor="<?php echo $colordefondo; ?>"><a href="jornada.php?idcat=<?php echo $idcat; ?>&idjorn=<?php echo $j; ?>" style="text-decoration:none;"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $j+1; ?></a></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->local05; ?></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->visit05; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->fecha05; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->hour05; ?> </font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->inst05; ?> </font></td>
								</tr>
								<?php
							}
							// si encontramos al equipo del club
							if((strtolower(strcasecmp($jornadas[$j]->local06,$nombreEquipo)==0))or(strtolower(strcasecmp($jornadas[$j]->visit06,$nombreEquipo)==0))) {
								?>
								<tr>
									<td height="30" width="40"  bgcolor="<?php echo $colordefondo; ?>"><a href="jornada.php?idcat=<?php echo $idcat; ?>&idjorn=<?php echo $j; ?>" style="text-decoration:none;"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $j+1; ?></a></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->local06; ?></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->visit06; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->fecha06; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->hour06; ?> </font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->inst06; ?> </font></td>
								</tr>
								<?php
							}
							// si encontramos al equipo del club
							if((strtolower(strcasecmp($jornadas[$j]->local07,$nombreEquipo)==0))or(strtolower(strcasecmp($jornadas[$j]->visit07,$nombreEquipo)==0))) {
								?>
								<tr>
									<td height="30" width="40"  bgcolor="<?php echo $colordefondo; ?>"><a href="jornada.php?idcat=<?php echo $idcat; ?>&idjorn=<?php echo $j; ?>" style="text-decoration:none;"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $j+1; ?></a></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->local07; ?></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->visit07; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->fecha07; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->hour07; ?> </font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->inst07; ?> </font></td>
								</tr>
								<?php
							}
							// si encontramos al equipo del club
							if((strtolower(strcasecmp($jornadas[$j]->local08,$nombreEquipo)==0))or(strtolower(strcasecmp($jornadas[$j]->visit08,$nombreEquipo)==0))) {
								?>
								<tr>
									<td height="30" width="40"  bgcolor="<?php echo $colordefondo; ?>"><a href="jornada.php?idcat=<?php echo $idcat; ?>&idjorn=<?php echo $j; ?>" style="text-decoration:none;"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $j+1; ?></a></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->local08; ?></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->visit08; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->fecha08; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->hour08; ?> </font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->inst08; ?> </font></td>
								</tr>
								<?php
							}
							// si encontramos al equipo del club
							if((strtolower(strcasecmp($jornadas[$j]->local09,$nombreEquipo)==0))or(strtolower(strcasecmp($jornadas[$j]->visit09,$nombreEquipo)==0))) {
								?>
								<tr>
									<td height="30" width="40"  bgcolor="<?php echo $colordefondo; ?>"><a href="jornada.php?idcat=<?php echo $idcat; ?>&idjorn=<?php echo $j; ?>" style="text-decoration:none;"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $j+1; ?></a></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->local09; ?></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->visit09; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->fecha09; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->hour09; ?> </font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->inst09; ?> </font></td>
								</tr>
								<?php
							}
							// si encontramos al equipo del club
							if((strtolower(strcasecmp($jornadas[$j]->local10,$nombreEquipo)==0))or(strtolower(strcasecmp($jornadas[$j]->visit10,$nombreEquipo)==0))) {
								?>
								<tr>
									<td height="30" width="40"  bgcolor="<?php echo $colordefondo; ?>"><a href="jornada.php?idcat=<?php echo $idcat; ?>&idjorn=<?php echo $j; ?>" style="text-decoration:none;"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $j+1; ?></a></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->local10; ?></font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->visit10; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->fecha10; ?></font></td>
									<td height="30" width="75"  bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->hour10; ?> </font></td>
									<td height="30" width="200" bgcolor="<?php echo $colordefondo; ?>"><font face="verdana" color="black" style="font-size: 12px;"><?php echo $jornadas[$j]->inst10; ?> </font></td>
								</tr>
								<?php
							}
							// cambiamos el color para filas pares
							$colordefondo = "#e8e8e8";
							
						}
						?>
					</table>
                </td>
            </tr>
        </table>

        <?php require('pie.php'); ?>

    </body>
</html>