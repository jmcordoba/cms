<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>bienvenido al cms</title>
		<link rel="stylesheet"    type="text/css"     href="css/menu_izquierda.css" />
		<link rel="stylesheet"    type="text/css"     href="css/gracias.css" />
    </head>
	
	<body bgcolor="#e8e8e8" style="margin:0px;">
        
		<!-- cabecera -->
        <?php require('cabecera.php'); ?>
        
		<!-- cuerpo -->
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="35" align="center"><tr><td></td></tr></table>
        
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="700" align="center">
            <tr>
				<td bgcolor="#ffffff" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    <?php
					include("cuerpo1.php");
					include("cuerpo3.php");
					include("cuerpo4.php");
					?>
                </td>
                
				<td></td>
				
				<td bgcolor="#ffffff" width="800" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
					
					<table style="position:relative;top:30px;margin-left:5px;text-align:justify;" width="99%" align="center">						
						<tr>
							<td colspan="2" style="border-bottom:1px dashed #333333;">
								<font class="titulo_pagina"><?php echo "Recuerde su contraseña"; ?></font>
							</td>
						</tr>
						
						<tr height="20" bgcolor="#ffffff"><td></td></tr>
					</table>
					
					<table bgcolor="#ffffff" border="0" color="black" cellspacing="0" width="96%" style="margin-top:30px;margin-left:10px;" align="left">
						<tr>
							<td style="text-align:justify;">
								<font face="verdana" color="black" style="font-size:16px; font-style:bold; color:#333333;">
									<?php
									// Capturamos desde donde viene la llamada
									$origen = $_GET["origen"];
									if($origen=="user") {
										echo "Gracias por registrarse como usuario. Hemos enviado un mail a tu cuenta de correo para que puedas validar tu usuario. Por favor, sigue las instrucciones del mail para poder utilizar tu cuenta.";
									}
									if($origen=="valida") {
										echo "Gracias por validarte como usuario.";
									}
									if($origen=="recuerda") {
										echo "Hemos enviado su password a su correo electrónico. Ya puede utilizarlo para identificarse como usuario.";
									}
									if($origen=="no_user") {
										echo "No hemos encontrado ningún usuario con esta dirección de correo electrónico.";
									}
									if($origen=="logged") {
										echo "Gracias por identificarte";
									}
									if($origen=="nologged") {
										echo "No hemos encontrado ningún usuario validado con estas credenciales. Por favor revise su correo electrónico para recordar la información de registro o bien solicite que le reenviemos su contraseña al mail que nos indicó.";
									}
									if(strstr($origen,"existe")!=false) {
										echo "El mail: " . substr($origen,7) . " ya está registrado. Por favor registrese con otro mail.";
									}
									?>
								</font>
							</td>
						</tr>
					</table>
					
					<table bgcolor="#ffffff" border="0" color="black" cellspacing="0" width="97%" style="margin-top:20px;margin-left:5px;" align="left">
						<tr>
							<td align="right">
								<a href="index.php">
									<font face="verdana" color="black" style="font-size:16px; font-style:bold; color:#000000;">
										<?php echo "Volver al inicio"; ?>
									</font>
								</a>
							</td>
						</tr>
					</table>
                </td>
            </tr>
        </table>

        <?php require('pie.php'); ?>
    </body>
</html>
