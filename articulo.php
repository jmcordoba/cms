<?php

include("txt/articulo.php");

$idart = $_GET["idart"];

require_once("admin/objetos/articulo.php");
require_once("admin/objetos/comentario.php");
require_once("admin/fun/funciones.php");

$articulo = obtenerUnArticulo($idart);

$idart     = $articulo->idart;
$fecha     = $articulo->fecha;
$user      = $articulo->iduser;
$titulo    = $articulo->titulo;
$contenido = $articulo->contenido;
$imagen    = $articulo->imagen;
$contador  = $articulo->conta;
$articulo->fechasi;
$articulo->fechano;

$foto_user = obtenerFotoDeUsuario($articulo->iduser);

?>

<!DOCTYPE html>

<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title><?php echo $TxT_05001; ?></title>
		<link rel="stylesheet"    type="text/css"     href="css/menu_izquierda.css" />
    </head>
	
	<?php flush(); ?>
	
	<script>
        // Verifica los campos del formulario antes de guardar los datos
        function verificar_form() {
            if(document.comen.contenido.value.length==0) {
                alert("Error:\nDebe ingresar el contenido"); 
                document.comen.contenido.focus() 
                return 0; 
            } else {
                document.forms['comen'].submit();
            }
        }
    </script>

    <body bgcolor="#e8e8e8" style="margin:0px;">
        <!-- cabecera -->
        <?php require('cabecera.php'); ?>
        <!-- cuerpo -->
        <table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="35" align="center"><tr><td></td></tr></table>
        <table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="700" align="center">
            <tr>
				<td bgcolor="#ffffff" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    <?php
					include("cuerpo1.php");
					include("cuerpo3.php");
					include("cuerpo4.php");
					?>
                </td>
                <td></td>
				<td bgcolor="#ffffff" width="800" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">

                    <table style="margin-top:30px;margin-left:5px;text-align:justify;" width="99%" align="center">
                        <tr>
							<td colspan="2">
								<a style="text-decoration:none;" href="#" onClick=";">
									<font face="Arial" color="#394752" style="font-size:26px; font-style:normal; color:#000000;"><?php echo $titulo; ?></font>
								</a>
							</td>
						</tr>
                    </table>
					
					<table bgcolor="#ffffff" border="0" color="black" cellspacing="0" width="95%" style="margin-top:0px;margin-left:5px;">
						<tr>
							<td width="60" height="60" style="padding-top:8px;">
								<img height="60" width="60" src="<?php echo $foto_user; ?>">
							</td>
							<td>
								<table height="50">
									<tr height="15">
										<td height="15" style="padding:0;">
											<font face="verdana" color="black" style="font-size:10px;font-style:normal;color:#3f5890;">
												<?php echo $TxT_05002 . $user ; ?>
											</font>
										</td>
									</tr>
									<tr height="15">
										<td height="15" style="padding:0;">
											<font face="verdana" color="black" style="font-size:10px;font-style:normal;color:#3f5890;">
												<?php echo $TxT_05003 . $fecha ; ?>
											</font>
										</td>
									</tr>
									<tr height="15">
										<td height="15" style="padding:0;">
											<a href="#" onClick="articulo(<?php echo $idart; ?>);">
												<font face="arial" style="font-size: 10px; color: #3f5890;"><?php echo numRowsDeArticulo("CMS_comentarios",$idart) . $TxT_05004; ?></font>
											</a>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					
                    <table border="0" color="black" cellspacing="0" width="98%" style="margin-bottom:10px;margin-left:5px;margin-right:5px;">
						<?php 
						if ($imagen!="") {
							?>
							<tr>
								<td align="center" colspan="2">
									<img class="imagen" style="vertical-align:top;margin-top:10px;margin-bottom:10px;margin-left:0px;" width="426" height="320" src="<?php echo conf_RUTA."admin/".$imagen; ?>"></img>
								</td>
							</tr>
							<?php
						}
						?>
						<tr>
							<td width="100%" style="vertical-align:top;margin-bottom:10px;margin-left:0px;text-align:justify;" colspan="2">
								<font face="Tahoma" color="black"  style="font-size:12px;"><?php echo $contenido; ?></font>
							</td>
						</tr>
					</table>
					
					<table bgcolor="#ffffff" border="0" color="black" cellspacing="0" width="98%" style="margin-top:5px;margin-left:5px;margin-right:5px;margin-bottom:20px;">
						<?php
						
						$comentario = obtenerComentarioDeArticulo($idart);
						$conta = numRowsDeArticulo("CMS_comentarios",$idart);
						
						for($i=0;$i<$conta;$i++) {
						
							$_POST["idcom"]     = $comentario[$i]->idcom;
							$_POST["fecha"]     = $comentario[$i]->fecha;
							$_POST["iduser"]    = $comentario[$i]->iduser;
							$_POST["contenido"] = $comentario[$i]->contenido;
							$_POST["idart"]     = $comentario[$i]->idart;
							$_POST["conta"]     = $comentario[$i]->conta;
							$foto_user          = obtenerFotoDeUsuario($comentario[$i]->iduser);
							?>
							<tr><td height="5" width="100%" bgcolor="#ffffff" colspan="2"></td></tr>
							<tr>
								<td height="60" width="60" bgcolor="#ffffff" valign="top">
									<img height="60" width="60" src="<?php echo $foto_user; ?>">
								</td>
								<td height="60" width="100%" bgcolor="#f8f8f8">
									<table>
										<tr><td style="font-family:Tahoma;font-size:12px;"><?php echo $TxT_05005 . $_POST["iduser"] . " el dia " . $_POST["fecha"]; ?></td></tr>
										<tr>
											<td style="font-family:Tahoma;font-size:12px;text-align:justify;">
												<?php echo utf8_decode($_POST["contenido"]); ?>
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<?php
						}
						
						// Si estamos logados, podremos escribir comentarios
						if ($_COOKIE["cmsweb_logged"]==true) {
							?>
							<form name="comen" action="fun/guardarComentario.php" method="post">
								
								<tr><td height="10" width="100%" bgcolor="#ffffff" colspan="2"></td></tr>
								<tr>
									<td height="20" width="100%" bgcolor="#ffffff" colspan="2">
										<font face="Tahoma" style="font-size: 14px; color: black;">
											<?php echo $TxT_05006; ?>
										</font>
									</td>
								</tr>
								<tr>
									<td colspan="2">
										<textarea class="textarea" name="contenido"></textarea>
									</td>
								</tr>
								
								<!-- Los campos a continuación son invisibles en el formulario: sirven para pasar el idart para guardar el comentario -->
								<tr><td colspan="2"><input type="hidden" name="idart"  size="1" value="<?php echo $idart;?>">           </input></td></tr>
								<tr><td colspan="2"><input type="hidden" name="fecha"  size="1" value="<?php echo date("d/m/Y");?>">    </input></td></tr>
                                    <tr><td colspan="2"><input type="hidden" name="iduser" size="1" value="<?php echo $_COOKIE["cmsweb_user"];?>"></input></td></tr>
								
								<tr>
									<td bgcolor="#ffffff" colspan="2">
										<a href="#" onclick="verificar_form();">
											<font face="arial" style="font-size: 12px; color: blue;">
												<?php echo $TxT_05007; ?>
											</font>
										</a>
									</td>
								</tr>
							
							</form>
							<?php
						}
						?>
                    </table>
                    
                </td>
            </tr>
        </table>
		
		<?php require('pie.php'); ?>
		
    </body>
</html>
