<?php

// Incluye los objetos necesarios
require("admin/objetos/club.php");
require("admin/objetos/equipo.php");
require("admin/objetos/cate.php");
require("admin/objetos/insta.php");

// Incluye las funciones necesarios
require("admin/fun/funciones.php");

?>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
		<title>cms</title>
		<link rel="stylesheet"    type="text/css"     href="css/menu_izquierda.css" />
		<link rel="stylesheet"    type="text/css"     href="css/instalacion.css" />
    </head>

    <body bgcolor="#e8e8e8" style="margin:0px;">
		
		<!-- cabecera -->
        <?php require('cabecera.php'); ?>
        <!-- cuerpo -->
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="35" align="center"><tr><td></td></tr></table>
        <table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="700" align="center">
            <tr>
				<td bgcolor="#ffffff" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    <?php
					include("cuerpo1.php");
					include("cuerpo3.php");
					include("cuerpo4.php");
					?>
                </td>
                <td></td>
				<td bgcolor="#ffffff" width="800" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    
					<?php
                    
                    $idinst = $_REQUEST["idinst"];

                    $instalacion = obtenerUnaInsta($idinst);
                    
                    if($instalacion != null) {

						$_POST["idinst"]    = $instalacion->idcat;
	                    $_POST["name"]      = $instalacion->name;
	                    $_POST["direccion"] = $instalacion->direccion;
	                    $_POST["telefono"]  = $instalacion->telefono;
	                    $_POST["mail"]      = $instalacion->mail;
	                    $_POST["persona"]   = $instalacion->persona;
	                    $_POST["conta"]     = $instalacion->conta;
	                    $_POST["foto"]      = $instalacion->foto;
	                    
						?>	
						
						<table style="position:relative;top:30px;margin-left:5px;text-align:justify;" width="99%" align="center">
							
							<tr>
								<td colspan="2" style="border-bottom:1px dashed #333333;">
									<font class="titulo_pagina"><?php echo $instalacion->name;?></font>
								</td>
							</tr>
							
							<tr height="10" bgcolor="#ffffff"><td></td></tr>
						
							<tr>
								<td class="td_titulo"><font class="titulo">direccion</font></td>
								<td class="td_info"><font class="informacion"><?php echo $instalacion->direccion;?></font></td>
							</tr>
							<tr>
								<td class="td_titulo"><font class="titulo">telefono</font></td>
								<td class="td_info"><font class="informacion"><?php echo $instalacion->telefono;?></font></td>
							</tr>
							<tr>
								<td class="td_titulo"><font class="titulo">mail</font></td>
								<td class="td_info"><font class="informacion"><?php echo $instalacion->mail;?></font></td>
							</tr>
							<tr>
								<td class="td_titulo"><font class="titulo">persona de contacto</font></td>
								<td class="td_info"><font class="informacion"><?php echo $instalacion->persona;?></font></td>
							</tr>
							
							<tr height="10" bgcolor="#ffffff"><td></td></tr>
							
							<!--tr height="10" bgcolor="#ffffff">
								<td class="td_titulo"></td>
								<td class="td_imagen">
									<?php 
									echo str_replace("\\\"","",rawurldecode($instalacion->conta)); 
									?>
								</td>
							</tr-->
							
							<?php
	                        if ($_POST["foto"]!="") {
	                            ?>
								<tr>
									<td class="td_titulo"></td>
									<td class="td_imagen">
										<img width="400" height="250" src="<?php echo conf_RUTA.$_POST["foto"];?>">
									</td>
								</tr>
	                            <?php
	                        }
	                        ?>
						</table>

						<?php
                    }
                    ?>					
                </td>
            </tr>
        </table>
        
		<!-- pie -->
        <?php require('pie.php'); ?>

    </body>
</html>