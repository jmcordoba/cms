<!-- MENU >> EQUIPO -->

<?php include("txt/cuerpo3.php"); ?>

<table bgcolor="#ffffff" border="0" color="black" cellspacing="0" width="100%" style="vertical-align:top;margin-top:20px;">
	<tr class="fila">
		<td bgcolor="#ffffff" width="200" align="left" style="border-bottom:1px solid #333333;">
			<font class="titulo_enlace">
				<b><?php echo "EQUIPOS"; ?></b>
			</font>
		</td>
	</tr>
</table>

<?php

$categoria = obtenerCate();

for($i=0;$i<numRows("CMS_cate");$i++) {
	$idcat  = $categoria[$i]->idcat;
	$equipo = obtenerUnEquipoByCat($idcat);
	?>
	
	<table bgcolor="#ffffff" border="0" color="black" cellspacing="0" width="100%" style="vertical-align:top;margin-top:5px;">
		<?php
		$equipo = obtenerUnEquipoByCat($categoria[$i]->idcat);
		?>
		<tr class="fila">
			<td align="left">
				<a href="javascript:mostrarOcultarTablas('tabla<?php echo $equipo->idteam;?>','desplegable<?php echo $equipo->idteam;?>')" style="text-decoration:none;">
					<font class="enlace">
						<?php echo strtoupper($equipo->nombre); ?>
					</font>
				</a>
			</td>
			<td align="center" width="20" title="desplega">
				<a style="text-decoration:none;" href="javascript:mostrarOcultarTablas('tabla<?php echo $equipo->idteam;?>','desplegable<?php echo $equipo->idteam;?>')">
					<font class="enlace_desplegable">
						<div id="desplegable<?php echo $equipo->idteam;?>">+</div>
					</font>
				</a> 
			</td>
		</tr>
	</table>
	
	<div id="tabla<?php echo $equipo->idteam;?>" style="display: none">
		<table width="100%" align="left" style="margin-top:5px;margin-bottom:5px;">
			<tr class="fila">
				<td colspan="2" align="left">
					<?php
					$equipo = obtenerUnEquipoByCat($categoria[$i]->idcat);
					?>
					<a href="equipo.php?idteam=<?php echo $equipo->idteam; ?>" style="text-decoration:none;">
						<font class="enlace_equipo">
							equipo
						</font>
					</a>
				</td>
			</tr>
			<tr class="fila">
				<td colspan="2" align="left">
					<a href="calendario.php?idcat=<?php echo $idcat; ?>" style="text-decoration:none;">
						<font class="enlace_equipo">
							calendario
						</font>
					</a>
				</td>
			</tr>
			
			<tr class="fila">
				<td colspan="2" align="left">
					<a href="cronicas.php?idcat=<?php echo $idcat; ?>&idteam=<?php echo $equipo->idteam; ?>" style="text-decoration:none;">
						<font class="enlace_equipo">
							cronicas
						</font>
					</a>
				</td>
			</tr>
			
			<tr class="fila">
				<td colspan="2" align="left">
					<a href="clasificacion.php?idcat=<?php echo $idcat; ?>" style="text-decoration:none;">
						<font class="enlace_equipo">
							clasificacion
						</font>
					</a>
				</td>
			</tr>
		</table>
	</div>
	<?php
}
?>

	



