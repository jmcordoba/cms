<?php

// incluimos las clases necesarias
require_once 'objetos/class.install.php';
require_once 'smarty/class.smarty.php';

// create an smarty object of our customized class
$smarty = new Mysmarty ;

// incluimos funcion de filtrado de variables
function init($que, $salida='') {
    if(isset($_REQUEST[$que])) {
		$salida = htmlspecialchars($_REQUEST[$que], ENT_QUOTES, 'ISO-8859-1');
	}
    return $salida;
}

// Capturamos los datos que nos llegan por URL
$form             = init('form');
$host             = init('host');
$bbdd             = init('bbdd');
$user             = init('user');
$pswd             = init('pswd');
$cuenta_nombre    = init('cuenta_nombre');
$cuenta_apellidos = init('cuenta_apellidos');
$cuenta_mail      = init('cuenta_mail');
$cuenta_password  = init('cuenta_password');

// cargamos variables
$smarty->assign('form',$form);
$smarty->assign('host',$host);
$smarty->assign('bbdd',$bbdd);
$smarty->assign('user',$user);
$smarty->assign('pswd',$pswd);
$smarty->assign('cNom',$cuenta_nombre);
$smarty->assign('cApe',$cuenta_apellidos);
$smarty->assign('cMai',$cuenta_mail);
$smarty->assign('cPas',$cuenta_password);

// cargamos vistas
switch($form) {
	case "1":
		$vista = 'vistas/install/01install.tpl';
		break;
	case "2":
		// creamos el objeto
		$oInstall = new Install($host, $user, $pswd, $bbdd);
		// conectamos con el motor de la bbdd
		$res = $oInstall->connect();
		// si NO podemos seleccionar la bbdd
		if (!mysql_select_db($bbdd,$oInstall->link)) {
			// no ha sido posible la conexion con la bbdd
			$vista = 'vistas/install/02installE1.tpl';
		} else {
			// instalamos tablas
			$res1 = $oInstall->createTables();
			// creamos fichero de configuracion
			$res2 = $oInstall->createConfigurationFile();
			// si hemos encontrado algun error en el proceso
			if(!$res1 || !$res2) {
				// no ha sido posible la conexion con la bbdd
				$vista = 'vistas/install/02installE2.tpl';
			} else {
				// instrucciones y siguiente paso
				$vista = 'vistas/install/02install.tpl';
			}
		}
		break;
	case "3":
		//
        $vista = 'vistas/install/03install.tpl';
		break;
	case "4":
		// creamos el objeto
		$oInstall = new Install($host, $user, $pswd, $bbdd);
        // conectamos con el motor de la bbdd
		$res = $oInstall->connect();
		// si NO podemos seleccionar la bbdd
		if (!mysql_select_db($bbdd,$oInstall->link)) {
            // no ha sido posible la conexion con la bbdd
			$vista = 'vistas/install/04installE1.tpl';
        } else {
            //
            $res = $oInstall->insertInfoInTables();
            // si todo ha ido bien
            if(!$res) {
                //
                $vista = 'vistas/install/04installE2.tpl';
            } else {
                //
                $vista = 'vistas/install/04install.tpl';
            }
        }
        break;
	case "5":
		//
        $vista = 'vistas/install/05install.tpl';
		break;
	default:
		//
        $vista = 'vistas/install/00install.tpl';
		break;
}

// cargamos vista
$smarty->display($vista);