<table bgcolor="#394752" border="0" color="black" cellspacing="0" cellpadding="0" width="100%" align="center" style="padding:20px;">
	
    <?php
    // redes sociales
    $social = obtenerSociales();
    if(numRows("CMS_sociales")>0){
        ?>
        <tr width="100%" align="center" height="40">
            <td bgcolor="#394752" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;">
                <?php
                for($i=0;$i<numRows("CMS_sociales");$i++) {
                    $soci = obtenerUnaSocial($social[$i]->idsocial);
                    ?>				
                    <a href="http://<?php echo $soci->link; ?>" style="text-decoration:none;" target="_blank">
                        <font style="font-family:arial;font-size:14px;color:#c8c8c8;">
                            <b><?php echo strtoupper($soci->nombre); ?></b>
                        </font>
                    </a>
                    <br>
                    <?php
                }
                ?>
            </td>
        </tr>
        <?php
    }
    
    // patrocinadores
    require("objetos/patro.php");
    $patro = obtenerPatro();
    if(numRows("CMS_patro")>0){
        ?>
        <tr width="100%" align="center" height="40">
            <td bgcolor="#394752" width="100%" style="vertical-align:top;margin-top:0px;margin-left:0px;">
                <?php	
                for($i=0;$i<numRows("CMS_patro");$i++) {
                    ?>
                    <a href="http://<?php echo $patro[$i]->web; ?>" style="text-decoration:none;" target="_blank">
                        <font style="font-family:arial;font-size:14px;color:#c8c8c8;">
                            <b>
                            <?php 
                            //if($i!=0) { echo "|&nbsp;"; }
                            echo strtoupper($patro[$i]->nombre);
                            ?>
                            </b>
                        </font>
                    </a>
                    <?php
                }
                ?>
            </td>
        </tr>
        <?php
    }
    ?>

	<!-- legal -->
	<tr width="100%" align="center" height="40">
		<td bgcolor="#394752" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;">
			<a href="redessociales.php" style="text-decoration:none;">
				<font style="font-family:arial;font-size:14px;color:#c8c8c8;">
					<b><?php echo "Condiciones de uso"; ?></b>
				</font>
			</a>
        </td>
	</tr>
    
    <tr width="100%" align="center" height="40">
        <td bgcolor="#394752" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;">
			<a href="redessociales.php" style="text-decoration:none;">
				<font style="font-family:arial;font-size:14px;color:#c8c8c8;">
					<b><?php echo "Aviso legal"; ?></b>
				</font>
			</a>
		</td>
	</tr>
	
</table>