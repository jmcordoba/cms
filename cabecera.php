<?php 

// Incluye las funciones necesarios
require_once("admin/fun/funciones.php");
require_once("admin/objetos/club.php");

// incluimos ficheros de texto
include("txt/cabecera.php"); 

// obtenemos el equipo
$equipo = obtenerClub();
?>

<!-- incluimos fichero de cabecera -->
<link rel="stylesheet" href="css/cabecera.css" type="text/css" />

<!-- barra horizontal superior -->
<table style="position:fixed; 
			left:0px;
			box-shadow: 0 4px 6px -5px #000000;
			-webkit-box-shadow: 0px 2px 20px 0px #888888;
			-moz-box-shadow: 0 5px 6px -6px #000000;
			z-index:10;
			filter: progid:DXImageTransform.Microsoft.Shadow(color='#777777', Direction=160, Strength=4);"
			bgcolor="#394752" border="0" color="black" cellspacing="0" cellpadding="0" width="100%" height="40">

	<tr align="left">        
		<td width="100%" style="vertical-align:middle">
			<form name="frm" action="login.php" method=post enctype="multipart/form-data">
				
				<table border="0" width="1020" align="center" bgcolor="#394752">
					<tr style="vertical-align:middle">
						
						<td class="nombre_club">
							<a href="index.php" style="text-decoration:none;">
								<font class="cabecera_titulo">
									<b><?php echo $equipo->name; ?></b>
								</font>
							</a>
						</td>
						
						<td width="800" bgcolor="#394752">
							<table>
								<tr>
									<?php
									// Si el usuario NO esta loginado
									if ($_COOKIE["cmsweb_logged"]=="") {
                                        ?>
										<td width="1">&nbsp;</td>
										<td width="15%" bgcolor="#394752">
											<?php 
											if(isset($_POST["mail"])and($_POST["mail"]!="")) {
												$valor = $_POST["mail"]; 
											} else {
												$valor = "mail@example.com";
											}
											?>
											<input class="input_user" type="text" name="mail" size="25" value="<?php echo $valor; ?>" onclick="this.value=''"></input>
										</td>
										<td width="10%" bgcolor="#394752">
											<?php 
											if(isset($_POST["pswd"])and($_POST["pswd"]!="")) $valor = $_POST["pswd"]; 
											else                                             $valor = "password";?>
											<input class="input_pswd" type="text" name="pasw" size="15" value="<?php echo $valor; ?>" onfocus="this.value='';this.type='password';" onclick="this.value='';this.type='password';"></input>
										</td>
										<td class="td_cabecera_entrar">
											<a href="#" style="text-decoration:none;" onclick="javascript:document.forms['frm'].submit();return false;">
												<font class="cabecera_entrar">
													<?php echo $TxT_01002; ?>
												</font>
											</a>
										</td>
										<td width="50%" bgcolor="#394752" align="right"></td>
										<td class="td_cabecera_entrar1">
											<a href="login_remember.php" style="text-decoration:none;">
												<font class="cabecera_entrar"><?php echo $TxT_01003; ?></font>
											</a>
										</td>
										<td><font class="cabecera_entrar">|</font></td>
										<td class="td_cabecera_entrar1">
											<a href="login_register.php" style="text-decoration:none;">
												<font class="cabecera_entrar"><?php echo $TxT_01004; ?></font>
											</a>
										</td>
										<?php
									}	
									
									// Si el usuario SI esta loginado
									else {
										?>
										<td width="5"></td>
										<td width="30" bgcolor=#394752 align=left>
											<img style="margin-top:0px;" height="25" width="25" src="<?php echo conf_RUTA.$_COOKIE["cmsweb_foto"]; ?>">
										</td>
										<td class="td_cabecera_entrar">
											<font class="cabecera_entrar">
												<b><?php echo $_COOKIE["cmsweb_user"]; ?></b>
											</font>
										</td>
										<td width="570" bgcolor="#394752"></td>
										<?php
										if($_COOKIE["cmsweb_permisos"]=="administrador") {
											?>
											<td class="td_cabecera_entrar">
												<a href="admin/index.php" style="text-decoration:none;">
													<font class="cabecera_entrar">
														<?php echo $TxT_01005; ?>
													</font>
												</a>
											</td>
											<td><font class="cabecera_entrar">|</font></td>
											<?php
										} else {
											?><td width="50" bgcolor="#394752"></td><?php
										}
										?>
										<td class="td_cabecera_entrar">
											<a href="fun/borrasesion.php" style="text-decoration:none;">
												<font class="cabecera_entrar"><?php echo $TxT_01006; ?></font>
											</a>
										</td>
										<?php
									}
									?>
								</tr>
							</table>
						</td>
					</tr>
				</table>

			</form>
		</td>
	</tr>	

</table>