<!-- MENU >> INFO DE CLUB -->

<?php

include("txt/cuerpo1.php");

// Incluye los objetos necesarios
require_once("admin/objetos/club.php");
require_once("admin/objetos/cate.php");
require_once("admin/objetos/equipo.php");
require_once("admin/objetos/social.php");
require_once("admin/objetos/torneo.php");

// Incluye las funciones necesarios
require_once("admin/fun/funciones.php");
?>

<script>
	function mostrarOcultarTablas(id,id2){
		// inicializamos variable
		mostrado=0;
		// mostramos u ocultamos div
		elem = document.getElementById(id);
		if(elem.style.display=='block')mostrado=1;
		elem.style.display='none';
		if(mostrado!=1)elem.style.display='block';
		// texto indicativo del desplegable
		elem2 = document.getElementById(id2);
		if(elem2.innerHTML=='+') elem2.innerHTML='-';
		else                     elem2.innerHTML='+';
		
	}
</script>

<table align="center" bgcolor="#ffffff" border="0" color="black" cellspacing="0" style="vertical-align:top;margin-top:30px;margin-left:0px;">
	<tr><td><a href="index.php"><img border="0" src="admin/images/escudo/escudo.jpg"/></a></td></tr>
</table>

<table bgcolor="#ffffff" border="0" color="black" cellspacing="0" width="200" style="margin-bottom:20px;border-bottom: 0px solid #cccccc;">

	<tr class="fila">
		<td bgcolor="#ffffff" width="200" align="left" style="border-bottom:1px solid #333333;">
			<font class="titulo_enlace">
				<b><?php echo "INFO"; ?></b>
			</font>
		</td>
	</tr>
	
	<tr class="fila" height="24">
		<td width="200" align="left">
			<a href="club.php" style="text-decoration:none;">
				<font class="enlace"><?php echo strtoupper($TxT_02001); ?></font>
			</a>
		</td>
	</tr>
	<tr class="fila" height="24">
		<td width="200" align="left">
			<a href="instalacion.php?idinst=1" style="text-decoration:none;">
				<font class="enlace"><?php echo strtoupper($TxT_02002); ?></font>
			</a>
		</td>
	</tr>
	<tr class="fila" height="24">
		<td width="200" align="left">
			<a href="contacto.php" style="text-decoration:none;">
				<font class="enlace"><?php echo strtoupper($TxT_02003); ?></font>
			</a>
		</td>
	</tr>
	<tr class="fila" height="24">
		<td width="200" align="left">
			<a href="galerias.php" style="text-decoration:none;">
				<font class="enlace"><?php echo strtoupper("galería de fotos"); ?></font>
			</a>
		</td>
	</tr>
</table>

<table bgcolor="#ffffff" border="0" color="black" cellspacing="0" width="100%" style="vertical-align:top;margin-top:-20px;">
	<tr class="fila">
		<td align="left">
			<a href="torneos.php" style="text-decoration:none;">
				<font class="enlace">
					<?php echo strtoupper("torneos"); ?>
				</font>
			</a>
		</td>
		<td align="center" width="20" title="desplega">
			<a style="text-decoration:none;" href="javascript:mostrarOcultarTablas('tablaT','desplegableT')">
				<font class="enlace_desplegable">
					<div id="desplegableT">+</div>
				</font>
			</a> 
		</td>
	</tr>
</table>

<div id="tablaT" style="display: none">
	<table bgcolor="#ffffff" border="0" color="black" cellspacing="0" width="200" style="vertical-align:top;margin-top:0px;margin-bottom:0px;">
		<?php 
		
		$torneo = obtenerSiguienteTorneo();
		
		if($torneo != null) {
			for($i=0;$i<count($torneo);$i++) {
				?>
				<tr class="fila">
					<td>
						<a href="torneos.php?idtorneo=<?php echo $torneo[$i]->idtorneo; ?>" style="text-decoration:none;">
							<font class="enlace_torneo">
								<?php echo $torneo[$i]->name; ?>
								<br>
								<?php echo $torneo[$i]->fecha; ?>
							</font>
						</a>
					</td>
				</tr>
				<?php
			}
		}
		?>
	</table>
</div>