<html>
    <head>
        <title>Instalación del CMS</title>
		<link rel="stylesheet" href="css/install.css" type="text/css" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script src="scripts/install.js"></script>
    </head>
    
	<body cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;">

		<table cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;" width="100%" height="100%" align="center">
			<tr align="center">
				<td align="center">
					
                    <form>
                        <table id="content">
                            <tr id="filaca">
                                <td id="fila_titulo">Configuración de usuario administrador</td>
                            </tr>
                            <tr height="5"><td colspan="2">&nbsp;</td></tr>
                            <tr id="filaca">
                                <td id="fila_input">
                                    Enhorabuena!
                                    <br>Ya está todo listo para que puedas disfrutar cmsweb.
                                    <br>Clica en el botón 'fin' y podrás acceder al gestor de contenidos.
                                    <br>Gracias!
                                </td>
                            </tr>
                            <tr id="filaca">
                                <td id="fila_submit">
                                    <input id="submit" type="submit" value="fin"></input>
                                </td>
                            </tr>
                        </table>
                        <input id="input" type="hidden" name="form" value="5"></input>
                    </form>
                    
				</td>
			</tr>
		</table>
    </body>
</html>