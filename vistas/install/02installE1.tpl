<html>
    <head>
        <title>Instalacion del CMS</title>
		<link rel="stylesheet" href="css/install.css" type="text/css" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script src="scripts/install.js"></script>
    </head>
    
	<body cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;">

		<table cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;" width="100%" height="100%" align="center">
			<tr align="center">
				<td align="center">
					
						<form>
							<table id="content">
								<tr id="filaca">
									<td id="fila_titulo">Configuración de mySQL</td>
								</tr>
								<tr height="5"><td colspan="2">&nbsp;</td></tr>
								<tr id="filaca">
									<td id="fila_input">La conexión con la base de datos no se ha podido realizar. Por favor, asegurate de haber introducido los datos correctos. Gracias.</td>
								</tr>
								<tr id="filaca">
									<td id="fila_submit">
										<input id="submit" type="submit" value="siguiente"></input>
									</td>
								</tr>
							</table>
							<input id="input" type="hidden" name="form" value="1"></input>
						</form>
						
				</td>
			</tr>
		</table>
    </body>
</html>