
<html>
    <head>
        <title>Instalacion del CMS</title>
		<link rel="stylesheet" href="css/install.css" type="text/css" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script src="scripts/install.js"></script>
    </head>
    
	<body cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;">

		<table cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;" width="100%" height="100%" align="center">
			<tr align="center">
				<td align="center">
					
                    <form>
                        <table id="content">
                            <tr id="filaca">
                                <td colspan="2" id="fila_titulo">Configuración de usuario administrador</td>
                            </tr>
                            <tr height="5"><td colspan="2">&nbsp;</td></tr>
                            <tr id="filaca">
                                <td id="fila_input">nombre</td>
                                <td id="fila_input2">
                                    <input id="input" type="text" name="cuenta_nombre"></input>
                                </td>
                            </tr>
                            <tr id="filaca">
                                <td id="fila_input">apellidos</td>
                                <td id="fila_input2">
                                    <input id="input" type="text" name="cuenta_apellidos"></input>
                                </td>
                            </tr>
                            <tr id="filaca">
                                <td id="fila_input">mail</td>
                                <td id="fila_input2">
                                    <input id="input" type="text" name="cuenta_mail"></input>
                                </td>
                            </tr>
                            <tr id="filaca">
                                <td id="fila_input">password</td>
                                <td id="fila_input2">
                                    <input id="input" type="password" name="cuenta_password"></input>
                                </td>
                            </tr>
                            <tr id="filaca">
                                <td colspan="2" id="fila_submit">
                                    <input id="submit" type="submit" value="siguiente"></input>
                                </td>
                            </tr>
                        </table>
                        <input id="input" type="hidden" name="form" value="4"></input>
                        <input id="input" type="hidden" name="host" value="{$host}"></input>
                        <input id="input" type="hidden" name="user" value="{$user}"></input>
                        <input id="input" type="hidden" name="pswd" value="{$pswd}"></input>
                        <input id="input" type="hidden" name="bbdd" value="{$bbdd}"></input>
                    </form>
						
				</td>
			</tr>
		</table>
    </body>
</html>