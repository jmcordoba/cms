<?php

// Incluye los objetos necesarios
require("admin/objetos/club.php");
require("admin/objetos/equipo.php");
require("admin/objetos/cate.php");
require("admin/objetos/persona.php");

// Incluye las funciones necesarios
require("admin/fun/funciones.php");

?>

<!DOCTYPE html>

<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms</title>
		<link rel="stylesheet"    type="text/css"     href="css/menu_izquierda.css" />
		<script type="text/javascript"> 
			function newWindow(url,texto) { 
			
				var img = new Image();
				
				img.onload     = function() {
					var day    = new Date(); 
					var id     = day.getTime(); 
					
					var ancho  = this.width+15;
					if(texto!=null) var alto   = this.height+30;
					else            var alto   = this.height+15;					
					
					var tamano = 'width='+ancho+',height='+alto;
					var win    = open(url,id,tamano+',scrollbars=no,resizable=no,titlebar=no');
					
					if(texto!=null) win.document.write("<font color=\"white\">"+texto+"</font>");
					win.document.write("<body bgcolor=\"#000000\"><table align=\"center\"><img src="+url+"></table></body>");
					win.focus()
				}
				
				img.src = url;
			} 
			</script>
    </head>
	

    <body bgcolor="#e8e8e8" style="margin:0px;">
		
		<!--- cabecera --->
        <?php require('cabecera.php'); ?>
        <!--- cuerpo --->
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="35" align="center"><tr><td></td></tr></table>
        <table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="700" align="center">
            <tr>
				<td bgcolor="#ffffff" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    <?php
					include("cuerpo1.php");
					include("cuerpo3.php");
					include("cuerpo4.php");
					?>
                </td>
                <td></td>
				<td bgcolor="#ffffff" width="800" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    
					<?php
                    
                    $idteam = $_REQUEST["idteam"];
                    
                    $team = obtenerUnEquipo($idteam);
                    
                    $_POST["idteam"]  = $team->idteam;
					$_POST["idcat"]   = $team->idcat;
					$_POST["nombre"]  = $team->nombre;
					$_POST["mister"]  = $team->mister;
					$_POST["foto"]    = $team->foto;
					$_POST["conta"]   = $team->conta;
					
					$cate = obtenerUnaCate($team->idcat);
					
					$personas = obtenerPersonaPorEquipo($idteam);
					?>
					
                    <table bgcolor="#ffffff" border="0" color="black" cellspacing="0" width="98%" style="margin-top:30px;margin-left:5px;">
						
						<tr>
							<td style="border-bottom:1px dashed #333333;">
								<a href="#" style="text-decoration:none;" onClick="articulo(<?php echo $idart; ?>);">
									<font face="Arial" color="#394752" style="font-size:26px; font-style:normal; color:#000000;">
										<?php echo $cate->name." - ".$_POST["nombre"]." - ".$_POST["mister"]; ?>
									</font>
								</a>
							</td>
						</tr>
						
						<tr height="20" bgcolor="#ffffff"><td></td></tr>

						<?php
						if ($_POST["foto"]!="") {
							?>
							<tr valign="bottom" align="center">
								<td colspan="2" bgcolor="#f8f8f8">
									<a href='javascript:newWindow("<?php echo conf_RUTA.$_POST["foto"];?>","<?php echo $_POST["nombre"];?>");'>
										<img width="426" height="320" src="<?php echo conf_RUTA.$_POST["foto"];?>">
									</a>
								</td>
							</tr>
							<?php
						}
						?>
					</table>
					
					<table width="98%" align="center">
						<tr><td height="20" width="700" bgcolor="#ffffff"></td></tr>
						<tr>
							<td style="border-bottom:1px dashed #333333;">
								<a href="#" style="text-decoration:none;" onClick="articulo(<?php echo $idart; ?>);">
									<font face="Arial" color="#394752" style="font-size:26px; font-style:normal; color:#000000;">
										<?php echo "Jugadores/as"; ?>
									</font>
								</a>
							</td>
						</tr>
						<tr height="10" bgcolor="#ffffff"><td></td></tr>
					</table>
					
					<table width="100%" align="center" style="margin-bottom:30px;">
						
						<?php
						$cpersonas = count($personas);
						$filas = count($personas)/5;
						
						for($i=0;$i<$cpersonas;$i++) {
						
							if($i==0) echo "<tr>";
							if(($i!=0)and($i%4==0)) echo "</tr><tr>";
							
							?>
							<td height="200" width="25%" bgcolor="#ffffff" border="1" align="center">
								<table>
									<tr>
										<td valign="top" bgcolor="#ffffff">
											<a href='javascript:newWindow("<?php echo conf_RUTA.$personas[$i]->foto; ?>");'>
												<img height="200" width="140" src="<?php echo conf_RUTA.$personas[$i]->foto; ?>"></img>
											</a>
										</td>
									</tr>
									<tr>
										<td valign="top" bgcolor="#ffffff">
											<font color="black" style="margin-left:0px;font-size:12px;font-family:arial;">
												<?php
												//echo $personas[$i]->nombre."<br>".$personas[$i]->surname."<br>".$personas[$i]->rol;
												echo $personas[$i]->nombre;
												?>
											</font>
										</td>
									</tr>
								</table>
							</td>
							<?php
						}
						
						echo "</tr>";
						
						?>
					</table>
                </td>
            </tr>
        </table>
        <!--- pie --->
        <?php require('pie.php'); ?>

    </body>
</html>