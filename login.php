<?php
// Adjuntamos los archivos de objetos y funciones
require_once("conf.php");
require_once("fun/funciones_login.php");

// Obtenemos el mail que introduce el usuario
$mail = $_POST["mail"];
$pasw = $_POST["pasw"];

// Ejecutamos la función para obtener los datos de usuario
$usuario = loginUsuario_login($mail,$pasw);

// Si el usuario existe
if($usuario->usuario!="") {
    // introducimos COOKIEs
	setcookie("cmsweb_user",     $usuario->usuario,  time()+3600);
	setcookie("cmsweb_pasw",     $usuario->password, time()+3600);
	setcookie("cmsweb_foto",     $usuario->foto,     time()+3600);
	setcookie("cmsweb_permisos", $usuario->permisos, time()+3600);
	setcookie("cmsweb_mail",     $usuario->mail,     time()+3600);
	setcookie("cmsweb_logged",   "true",             time()+3600);
    
	// redireccionamos
	?><script>location.href="index.php";</script><?php
	die();
}
// redireccionamos
else {
	redirect_login(conf_RUTA."gracias.php?origen=nologged",0);
}