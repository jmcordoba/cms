<?php

// Incluye las funciones necesarios
require("admin/fun/funciones.php");

?>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>contacto</title>
		<link rel="stylesheet"    type="text/css"     href="css/menu_izquierda.css" />
    </head>

    <body bgcolor="#FFFFFF" style="margin:0px">   
		
		<!--- cabecera --->
        <?php require('cabecera.php'); ?>
        <!--- cuerpo --->
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="90%" height="35" align="center"><tr><td></td></tr></table>
        <table style="z-index:9;" border="0" color="black" cellspacing="0" width="90%" height="700" align="center">
            <tr>
				<td bgcolor="#ffffff" width="150" style="vertical-align:top;margin-top:0px;margin-left:0px;">
                    <?php 
					include("cuerpo1.php"); 
					include("cuerpo3.php");
					include("cuerpo4.php");
					?>
                </td>
                <td width="700" style="vertical-align:top;margin-top:0px;margin-left:0px;">
                    
					<table style="position:relative;top:30px;margin-left:5px;text-align:justify;" width="99%" align="center">
						<tr>
							<td colspan="2" height=20 width=700 bgcolor=#ffffff>
								<font face="verdana" color="black" style="margin-left: 0px;font-size: 12px;">
									Gracias por enviarnos tu consulta, en brebe nos pondremos en contacto contigo.
								</font>
							</td>
						</tr>
					</table>
					
                </td>
            </tr>
        </table>
        <!--- pie --->
        <?php require('pie.php'); ?>

    </body>
</html>