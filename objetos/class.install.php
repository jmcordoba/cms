<?php

/**
 * 
 */
class Install
{
	// atributos
	public $link             = null;
	public $host             = '';
	public $bbdd             = '';
	public $user             = '';
	public $pswd             = '';
	public $cuenta_nombre    = '';
	public $cuenta_apellidos = '';
	public $cuenta_mail 	 = '';
	public $cuenta_password  = '';
	
	/**
	 * 
	 */
	public function __construct($host, $user, $pswd, $bbdd) {
		$this->host = $host;
        $this->bbdd = $bbdd;
		$this->user = $user;
		$this->pswd = $pswd;
	}
	
	/**
	 * 
	 */
	public function connect()
	{
		// conectamos con la base de datos
		$this->link = mysql_connect($this->host, $this->user, $this->pswd);
		// devolvemos resultado
		return $this->link;
	}
	
	/**
	 * 
	 */
	public function createTables()
	{
		$resultado = false;
	
		$sql = "CREATE TABLE CMS_club
		(
			idteam   varchar(10),
			name     varchar(100),
			presi    varchar(100),
			foto     varchar(100),
			street   varchar(100),
			cpostal  varchar(10),
			telefono varchar(20),
			fax      varchar(20),
			mail     varchar(100),
			historia varchar(100),
			palmares varchar(100)
		)";
		$resultado = mysql_query($sql);
        
		// **************************************************************************
		//
		// Creamos tabla para alojar la informacion de temporada
		//
		// **************************************************************************
		$sql = "CREATE TABLE CMS_temp
		(
			idtemp      varchar(100),
			name        varchar(100),
			yearstart   varchar(100),
			yearfinish  varchar(100),
			fede        varchar(100)
		)";

		// Execute query
		$resultado = mysql_query($sql);

		// **************************************************************************
		//
		// Creamos tabla para alojar la informacion de categorias
		//
		// **************************************************************************
		$sql = "CREATE TABLE CMS_cate
		(
			idcat       varchar(100),
			name        varchar(100),
			numteams    varchar(100),
			wpoints     varchar(100),
			dpoints     varchar(100),
			lpoints     varchar(100),
			jornadas    varchar(100),
			idavuelta   varchar(100),
			idtemp      varchar(100),
			rival01     varchar(100),
			rival02     varchar(100),
			rival03     varchar(100),
			rival04     varchar(100),
			rival05     varchar(100),
			rival06     varchar(100),
			rival07     varchar(100),
			rival08     varchar(100),
			rival09     varchar(100),
			rival10     varchar(100),
			rival11     varchar(100),
			rival12     varchar(100),
			rival13     varchar(100),
			rival14     varchar(100),
			rival15     varchar(100),
			rival16     varchar(100),
			rival17     varchar(100),
			rival18     varchar(100),
			rival19     varchar(100),
			rival20     varchar(100)
		)";

		// Execute query
		$resultado = mysql_query($sql);

		// **************************************************************************
		//
		// Creamos tabla para alojar la informacion de instalaciones
		//
		// **************************************************************************
		$sql = "CREATE TABLE CMS_insta
		(
			idinst    varchar(100),
			name      varchar(100),
			direccion varchar(100),
			telefono  varchar(100),
			mail      varchar(100),
			persona   varchar(100),
			conta     varchar(10000),
			foto      varchar(100)
		)";
		// Execute query
		$resultado = mysql_query($sql);

		// **************************************************************************
		//
		// Creamos tabla para alojar la informacion de usuarios
		//
		// **************************************************************************
		$sql = "CREATE TABLE CMS_users
		(
			nombre     varchar(100),
			apellidos  varchar(100),
			usuario    varchar(100),
			password   varchar(100),
			telefono   varchar(100),
			mail       varchar(100),
			permisos   varchar(100),
			conta      varchar(100),
			fecha_alta varchar(100),
			validado   varchar(100),
			foto       varchar(100)
		)";
		// Execute query
		$resultado = mysql_query($sql);

		// **************************************************************************
		//
		// Creamos tabla para alojar los patrocinadores
		//
		// **************************************************************************
		$sql = "CREATE TABLE CMS_patro
		(
			idpatro    varchar(100),
			nombre     varchar(100),
			imagen     varchar(100),
			fecha_alta varchar(100),
			fecha_baja varchar(100),
			importe    varchar(100),
			conta      varchar(100),
			web        varchar(100)
		)";
		// Execute query
		$resultado = mysql_query($sql);

		// **************************************************************************
		//
		// Creamos tabla para alojar los tags
		//
		// **************************************************************************
		$sql = "CREATE TABLE CMS_tags
		(
			idtags varchar(100),
			nombre varchar(100),
			conta  varchar(100)
		)";
		// Execute query
		$resultado = mysql_query($sql);

		// **************************************************************************
		//
		// Creamos tabla para alojar las redes sociales
		//
		// **************************************************************************
		$sql = "CREATE TABLE CMS_sociales
		(
			idsocial varchar(100),
			nombre   varchar(100),
			link     varchar(100),
			icono    varchar(100)
		)";
		// Execute query
		$resultado = mysql_query($sql);

		// **************************************************************************
		//
		// Creamos tabla para alojar los art�culo
		//
		// **************************************************************************
		$sql = "CREATE TABLE CMS_articulos
		(
			idart     varchar(100),
			fecha     varchar(100),
			iduser    varchar(100),
			titulo    varchar(300),
			contenido varchar(10000),
			idtag     varchar(100),
			conta     varchar(100),
			fechasi   varchar(100),
			fechano   varchar(100),
			imagen    varchar(100)
		)";
		// Execute query
		$resultado = mysql_query($sql);

		// **************************************************************************
		//
		// Creamos tabla para alojar las cronicas de los equipos del club
		//
		// **************************************************************************
		$sql = "CREATE TABLE CMS_cronicas
		(
			idcro     varchar(100),
			idteam    varchar(100),
			idcat     varchar(100),
			fecha     varchar(100),
			iduser    varchar(100),
			titulo    varchar(300),
			contenido varchar(10000),
			idtag     varchar(100),
			conta     varchar(100),
			fechasi   varchar(100),
			fechano   varchar(100),
			imagen    varchar(100)
		)";
		// Execute query
		$resultado = mysql_query($sql);

		// **************************************************************************
		//
		// Creamos tabla para alojar los comentarios de los articulos
		//
		// **************************************************************************
		$sql = "CREATE TABLE CMS_comentarios
		(
			idcom     varchar(100),
			fecha     varchar(100),
			iduser    varchar(100),
			contenido varchar(1000),
			idart     varchar(100),
			conta     varchar(100)
		)";
		// Execute query
		$resultado = mysql_query($sql);

		// **************************************************************************
		//
		// Creamos tabla para alojar la informacion de los torneos
		//
		// **************************************************************************
		$sql = "CREATE TABLE CMS_torneos
		(
			idtorneo    varchar(100),
			idformu     varchar(100),
			name        varchar(100),
			cartel      varchar(100),
			fecha       varchar(100),
			hour        varchar(100),
			location    varchar(100),
			gmaps       varchar(100),
			description varchar(1000),
			info        varchar(100),
			cuenta      varchar(100),
			titular     varchar(100),
			precio      varchar(100),
			conta       varchar(100)
		)";
		// Execute query
		$resultado = mysql_query($sql);

		// **************************************************************************
		//
		// Creamos tabla para alojar los formularios de los torneos
		//
		// **************************************************************************
		$sql = "CREATE TABLE CMS_formularios
		(
			idformu		varchar(100),
			formu		varchar(100),
			mail		varchar(100),
			name		varchar(100),
			fecha		varchar(100),
			precio		varchar(100),
			teamname	varchar(100),
			categ		varchar(100),
			capname		varchar(100),
			captel		varchar(100),
			capmail		varchar(100),
			n01name		varchar(100),
			n01tel		varchar(100),
			n01mail		varchar(100),
			n02name		varchar(100),
			n02tel		varchar(100),
			n02mail		varchar(100),
			n03name		varchar(100),
			n03tel		varchar(100),
			n03mail		varchar(100),
			n04name		varchar(100),
			n04tel		varchar(100),
			n04mail		varchar(100),
			n05name		varchar(100),
			n05tel		varchar(100),
			n05mail		varchar(100)
		)";
		// Execute query
		$resultado = mysql_query($sql);

		// **************************************************************************
		//
		// Creamos tabla para alojar os torneos
		//
		// **************************************************************************
		$sql = "CREATE TABLE CMS_torneos
		(
			idtorneo	varchar(100),
			idformu		varchar(100),
			name		varchar(100),
			cartel		varchar(100),
			fecha		varchar(100),
			hour		varchar(100),
			location	varchar(100),
			gmaps		varchar(100),
			description	varchar(100),
			info		varchar(100),
			cuenta		varchar(100),
			titular		varchar(100),
			precio		varchar(100),
			conta		varchar(100)
		)";
		// Execute query
		$resultado = mysql_query($sql);

		// **************************************************************************
		//
		// Creamos tabla para alojar la informacion de equipos
		//
		// **************************************************************************
		$sql = "CREATE TABLE CMS_equipos
		(
			idteam		varchar(100),
			idcat		varchar(100),
			nombre		varchar(100),
			mister		varchar(100),
			conta		varchar(100),
			foto		varchar(100)
		)";
		// Execute query
		$resultado = mysql_query($sql);

		// **************************************************************************
		//
		// Creamos tabla para alojar las personas del club
		//
		// **************************************************************************
		$sql = "CREATE TABLE CMS_personas
		(
			idperson	varchar(100),
			idteam		varchar(100),
			nombre		varchar(100),
			surname		varchar(100),
			fechan		varchar(100),
			foto		varchar(100),
			clubyears	varchar(100),
			cargo		varchar(100),
			palmares	varchar(100),
			facebook	varchar(100),
			twitter		varchar(100),
			rol			varchar(100),
			fechaalta	varchar(100),
			fechabaja	varchar(100),
			conta		varchar(100)
		)";
		// Execute query
		$resultado = mysql_query($sql);

		// **************************************************************************
		//
		// Creamos tabla para alojar las personas del club
		//
		// **************************************************************************
		$sql = "CREATE TABLE CMS_jornadas
		(
			idjorn		INT,
			idcat		varchar(100),
			
			local01    	varchar(100),
			visit01    	varchar(100),
			inst01     	varchar(100),
			plocal01   	varchar(100),
			pvisit01   	varchar(100),
			fecha01    	varchar(100),
			hour01     	varchar(100),
			plocal011  	varchar(100),
			plocal012  	varchar(100),
			plocal013  	varchar(100),
			plocal014  	varchar(100),
			plocal015  	varchar(100),
			pvisit011  	varchar(100),
			pvisit012  	varchar(100),
			pvisit013  	varchar(100),
			pvisit014  	varchar(100),
			pvisit015  	varchar(100),
			
			local02    	varchar(100),
			visit02    	varchar(100),
			inst02     	varchar(100),
			plocal02   	varchar(100),
			pvisit02   	varchar(100),
			fecha02    	varchar(100),
			hour02     	varchar(100),
			plocal021  	varchar(100),
			plocal022  	varchar(100),
			plocal023  	varchar(100),
			plocal024  	varchar(100),
			plocal025  	varchar(100),
			pvisit021  	varchar(100),
			pvisit022  	varchar(100),
			pvisit023  	varchar(100),
			pvisit024  	varchar(100),
			pvisit025  	varchar(100),
			
			local03    	varchar(100),
			visit03    	varchar(100),
			inst03     	varchar(100),
			plocal03   	varchar(100),
			pvisit03   	varchar(100),
			fecha03    	varchar(100),
			hour03     	varchar(100),
			plocal031  	varchar(100),
			plocal032  	varchar(100),
			plocal033  	varchar(100),
			plocal034  	varchar(100),
			plocal035  	varchar(100),
			pvisit031  	varchar(100),
			pvisit032  	varchar(100),
			pvisit033  	varchar(100),
			pvisit034  	varchar(100),
			pvisit035  	varchar(100),
			
			local04    	varchar(100),
			visit04    	varchar(100),
			inst04     	varchar(100),
			plocal04   	varchar(100),
			pvisit04   	varchar(100),
			fecha04    	varchar(100),
			hour04     	varchar(100),
			plocal041  	varchar(100),
			plocal042  	varchar(100),
			plocal043  	varchar(100),
			plocal044  	varchar(100),
			plocal045  	varchar(100),
			pvisit041  	varchar(100),
			pvisit042  	varchar(100),
			pvisit043  	varchar(100),
			pvisit044  	varchar(100),
			pvisit045  	varchar(100),
			
			local05    	varchar(100),
			visit05    	varchar(100),
			inst05     	varchar(100),
			plocal05   	varchar(100),
			pvisit05   	varchar(100),
			fecha05    	varchar(100),
			hour05     	varchar(100),
			plocal051  	varchar(100),
			plocal052  	varchar(100),
			plocal053  	varchar(100),
			plocal054  	varchar(100),
			plocal055  	varchar(100),
			pvisit051  	varchar(100),
			pvisit052  	varchar(100),
			pvisit053  	varchar(100),
			pvisit054  	varchar(100),
			pvisit055  	varchar(100),
			
			local06    	varchar(100),
			visit06    	varchar(100),
			inst06     	varchar(100),
			plocal06   	varchar(100),
			pvisit06   	varchar(100),
			fecha06    	varchar(100),
			hour06     	varchar(100),
			plocal061  	varchar(100),
			plocal062  	varchar(100),
			plocal063  	varchar(100),
			plocal064  	varchar(100),
			plocal065  	varchar(100),
			pvisit061  	varchar(100),
			pvisit062  	varchar(100),
			pvisit063  	varchar(100),
			pvisit064  	varchar(100),
			pvisit065  	varchar(100),
			
			local07    	varchar(100),
			visit07    	varchar(100),
			inst07     	varchar(100),
			plocal07   	varchar(100),
			pvisit07   	varchar(100),
			fecha07    	varchar(100),
			hour07     	varchar(100),
			plocal071  	varchar(100),
			plocal072  	varchar(100),
			plocal073  	varchar(100),
			plocal074  	varchar(100),
			plocal075  	varchar(100),
			pvisit071  	varchar(100),
			pvisit072  	varchar(100),
			pvisit073  	varchar(100),
			pvisit074  	varchar(100),
			pvisit075  	varchar(100),
			
			local08    	varchar(100),
			visit08    	varchar(100),
			inst08     	varchar(100),
			plocal08   	varchar(100),
			pvisit08   	varchar(100),
			fecha08    	varchar(100),
			hour08     	varchar(100),
			plocal081  	varchar(100),
			plocal082  	varchar(100),
			plocal083  	varchar(100),
			plocal084  	varchar(100),
			plocal085  	varchar(100),
			pvisit081  	varchar(100),
			pvisit082  	varchar(100),
			pvisit083  	varchar(100),
			pvisit084  	varchar(100),
			pvisit085  	varchar(100),
			
			local09    	varchar(100),
			visit09    	varchar(100),
			inst09     	varchar(100),
			plocal09   	varchar(100),
			pvisit09   	varchar(100),
			fecha09    	varchar(100),
			hour09     	varchar(100),
			plocal091  	varchar(100),
			plocal092  	varchar(100),
			plocal093  	varchar(100),
			plocal094  	varchar(100),
			plocal095  	varchar(100),
			pvisit091  	varchar(100),
			pvisit092  	varchar(100),
			pvisit093  	varchar(100),
			pvisit094  	varchar(100),
			pvisit095  	varchar(100),
			
			local10    	varchar(100),
			visit10    	varchar(100),
			inst10     	varchar(100),
			plocal10   	varchar(100),
			pvisit10   	varchar(100),
			fecha10    	varchar(100),
			hour10     	varchar(100),
			plocal101  	varchar(100),
			plocal102  	varchar(100),
			plocal103  	varchar(100),
			plocal104  	varchar(100),
			plocal105  	varchar(100),
			pvisit101  	varchar(100),
			pvisit102  	varchar(100),
			pvisit103  	varchar(100),
			pvisit104  	varchar(100),
			pvisit105  	varchar(100),
			
			cronica		varchar(100)
		)";
		// Execute query
		$resultado = mysql_query($sql);

		// **************************************************************************
		//
		// Creamos tabla para alojar la clasificacion
		//
		// **************************************************************************
		$sql = "CREATE TABLE CMS_clasificacion
		(
			idclas     varchar(100),
			idjorn     varchar(100),
			idcat      varchar(100),
			
			nombre01   varchar(100),
			jugado01   varchar(100),
			points01   varchar(100),
			ufavor01   varchar(100),
			ucontr01   varchar(100),
			nvicto01   varchar(100),
			nempat01   varchar(100),
			nderro01   varchar(100),
			
			nombre02   varchar(100),
			jugado02   varchar(100),
			points02   varchar(100),
			ufavor02   varchar(100),
			ucontr02   varchar(100),
			nvicto02   varchar(100),
			nempat02   varchar(100),
			nderro02   varchar(100),
			
			nombre03   varchar(100),
			jugado03   varchar(100),
			points03   varchar(100),
			ufavor03   varchar(100),
			ucontr03   varchar(100),
			nvicto03   varchar(100),
			nempat03   varchar(100),
			nderro03   varchar(100),
			
			nombre04   varchar(100),
			jugado04   varchar(100),
			points04   varchar(100),
			ufavor04   varchar(100),
			ucontr04   varchar(100),
			nvicto04   varchar(100),
			nempat04   varchar(100),
			nderro04   varchar(100),
			
			nombre05   varchar(100),
			jugado05   varchar(100),
			points05   varchar(100),
			ufavor05   varchar(100),
			ucontr05   varchar(100),
			nvicto05   varchar(100),
			nempat05   varchar(100),
			nderro05   varchar(100),
			
			nombre06   varchar(100),
			jugado06   varchar(100),
			points06   varchar(100),
			ufavor06   varchar(100),
			ucontr06   varchar(100),
			nvicto06   varchar(100),
			nempat06   varchar(100),
			nderro06   varchar(100),
			
			nombre07   varchar(100),
			jugado07   varchar(100),
			points07   varchar(100),
			ufavor07   varchar(100),
			ucontr07   varchar(100),
			nvicto07   varchar(100),
			nempat07   varchar(100),
			nderro07   varchar(100),
			
			nombre08   varchar(100),
			jugado08   varchar(100),
			points08   varchar(100),
			ufavor08   varchar(100),
			ucontr08   varchar(100),
			nvicto08   varchar(100),
			nempat08   varchar(100),
			nderro08   varchar(100),
			
			nombre09   varchar(100),
			jugado09   varchar(100),
			points09   varchar(100),
			ufavor09   varchar(100),
			ucontr09   varchar(100),
			nvicto09   varchar(100),
			nempat09   varchar(100),
			nderro09   varchar(100),
			
			nombre10   varchar(100),
			jugado10   varchar(100),
			points10   varchar(100),
			ufavor10   varchar(100),
			ucontr10   varchar(100),
			nvicto10   varchar(100),
			nempat10   varchar(100),
			nderro10   varchar(100),
			
			nombre11   varchar(100),
			jugado11   varchar(100),
			points11   varchar(100),
			ufavor11   varchar(100),
			ucontr11   varchar(100),
			nvicto11   varchar(100),
			nempat11   varchar(100),
			nderro11   varchar(100),
			
			nombre12   varchar(100),
			jugado12   varchar(100),
			points12   varchar(100),
			ufavor12   varchar(100),
			ucontr12   varchar(100),
			nvicto12   varchar(100),
			nempat12   varchar(100),
			nderro12   varchar(100),
			
			nombre13   varchar(100),
			jugado13   varchar(100),
			points13   varchar(100),
			ufavor13   varchar(100),
			ucontr13   varchar(100),
			nvicto13   varchar(100),
			nempat13   varchar(100),
			nderro13   varchar(100),
			
			nombre14   varchar(100),
			jugado14   varchar(100),
			points14   varchar(100),
			ufavor14   varchar(100),
			ucontr14   varchar(100),
			nvicto14   varchar(100),
			nempat14   varchar(100),
			nderro14   varchar(100),
			
			nombre15   varchar(100),
			jugado15   varchar(100),
			points15   varchar(100),
			ufavor15   varchar(100),
			ucontr15   varchar(100),
			nvicto15   varchar(100),
			nempat15   varchar(100),
			nderro15   varchar(100),
			
			nombre16   varchar(100),
			jugado16   varchar(100),
			points16   varchar(100),
			ufavor16   varchar(100),
			ucontr16   varchar(100),
			nvicto16   varchar(100),
			nempat16   varchar(100),
			nderro16   varchar(100),
			
			nombre17   varchar(100),
			jugado17   varchar(100),
			points17   varchar(100),
			ufavor17   varchar(100),
			ucontr17   varchar(100),
			nvicto17   varchar(100),
			nempat17   varchar(100),
			nderro17   varchar(100),
			
			nombre18   varchar(100),
			jugado18   varchar(100),
			points18   varchar(100),
			ufavor18   varchar(100),
			ucontr18   varchar(100),
			nvicto18   varchar(100),
			nempat18   varchar(100),
			nderro18   varchar(100),
			
			nombre19   varchar(100),
			jugado19   varchar(100),
			points19   varchar(100),
			ufavor19   varchar(100),
			ucontr19   varchar(100),
			nvicto19   varchar(100),
			nempat19   varchar(100),
			nderro19   varchar(100),
			
			nombre20   varchar(100),
			jugado20   varchar(100),
			points20   varchar(100),
			ufavor20   varchar(100),
			ucontr20   varchar(100),
			nvicto20   varchar(100),
			nempat20   varchar(100),
			nderro20   varchar(100)	
			
		)";
		// Execute query
		$resultado = mysql_query($sql);

		// **************************************************************************
		//
		// Creamos tabla para alojar los nombres y las ubicaciones de los albumes
		//
		// **************************************************************************
		$sql = "CREATE TABLE  CMS_album (idalbum VARCHAR( 1000 ),nombre VARCHAR( 100 ),ubicacion VARCHAR( 100 ) ,numero VARCHAR( 1000 )) ;";
		// Execute query
		$resultado = mysql_query($sql);

		// **************************************************************************
		//
		// Creamos tabla para alojar los nombres y las ubicaciones de los albumes
		//
		// **************************************************************************
		$sql = "CREATE TABLE CMS_fotos (idfoto VARCHAR( 100 ) ,idalbum VARCHAR( 100 ) ,fecha VARCHAR( 10 ) ,pie VARCHAR( 200 ) ,ruta VARCHAR( 100 ) ,conta VARCHAR( 100 )) ;";
		// Execute query
		$resultado = mysql_query($sql);

		// liberamos memoria
		//mysql_free_result($resultado);
		
		//
		return $resultado;
	}
	
	/**
	 *
	 */
	function createConfigurationFile()
	{
		// Obtenemos la ruta donde alojaremos el CMS en el servidor
		$ruta = "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
		$ruta = substr($ruta,0,strpos($ruta,"install.php"));
		
		// Abrimos el fichero para borrarlo y escribirlo de cero
		// (w+ Apertura para lectura y escritura; coloca el puntero al fichero al principio del fichero y trunca el fichero a longitud cero. Si el fichero no existe se intenta crear.)
		$ar = fopen("conf.php","w+");

		//
		if($ar!=false) {
			$cabecera = "
				<?php
				define('conf_RUTA',\"$ruta\");
				define('conf_HOST',\"$this->host\");
				define('conf_BBDD',\"$this->bbdd\");
				define('conf_USER',\"$this->user\");
				define('conf_PSWD',\"$this->pswd\");
				?>
			";

			fputs($ar,$cabecera);
			fclose($ar);
		}
		
		// salida
		return $ar;
	}
    
    /**
     * 
     */
    public function insertInfoInTables()
    {
        // enlaces sociales
        $sql = "INSERT INTO  `CMS_sociales` (  `idsocial` ,  `nombre` ,  `link` ,  `icono` ) VALUES ('0',  'red1',  'www.red1.com', NULL);";
        $result = mysql_query($sql);

        // usuario administrador
        $sql = "insert into `CMS_users` (  `nombre` ,  `apellidos` ,  `usuario` ,  `password` ,  `telefono` ,  `mail` ,  `permisos` ,  `conta` ,  `validado` ,  `fecha_alta` ,  `foto` ) 
        values ('$cuenta_nombre',  '$cuenta_apellidos',  '$cuenta_mail',  '$cuenta_password', NULL ,  '$cuenta_mail',  'administrador',  '0',  'si',  '', 'images/usuario/user.jpg' );";
        $result = mysql_query($sql);

        // el primer art?culo
        $sql = "INSERT INTO  `CMS_articulos` (  `idart` ,  `fecha` ,  `iduser` ,  `titulo` ,  `contenido` ,  `idtag` ,  `conta` ,  `fechasi` ,  `fechano` ,  `imagen` ) 
        VALUES ('0',  '14/02/2013',  'jmcordoba',  'titulo de prueba',  'contenido de prueba', NULL , NULL , NULL , NULL , NULL);";
        $result = mysql_query($sql);

        // el primer club
        $sql = "INSERT INTO  `CMS_club` (  `idteam` ,  `name` ,  `presi` ,  `foto` ,  `street` ,  `cpostal` ,  `telefono` ,  `fax` ,  `mail` ,  `historia` ,  `palmares` ) 
        VALUES ('0',  'nombre del equipo', NULL , NULL , NULL , NULL , NULL , NULL , NULL , NULL , NULL);";
        $result = mysql_query($sql);

        // el primer partocinador
        $sql = "INSERT INTO `CMS_patro` (`idpatro`,`nombre`,`fecha_alta`,`fecha_baja`,`importe`,`web`,`conta`,`imagen`) VALUES ('1' , 'puma' , NULL , NULL , NULL , NULL , NULL , NULL);";
        $result = mysql_query($sql);

        // cerramos la conexi?n con la base de datos
        mysql_close($this->link);

        // salida
        return $result;
    }
}
