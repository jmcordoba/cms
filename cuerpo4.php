<!-- MENU >> CARTEL DE TORNEO -->

<table bgcolor="#ffffff" border="0" color="black" cellspacing="0" width="100%" style="vertical-align:top;margin-top:20px;">
	<?php 
	// obtenemos el siguiente torneo que se vaya a disputar
	$torneo = obtenerSiguienteTorneo();
	// si existe un torneo por disputar
	if($torneo != null) {
		?>
		<tr>
			<td>
				<a href="torneos.php?idtorneo=<?php echo $torneo[0]->idtorneo; ?>">
					<img border="0" src="<?php echo $torneo[0]->cartel; ?>" width="200" height="282"></img>
				</a>
			</td>
		</tr>
		<?php
	}
	?>
</table>