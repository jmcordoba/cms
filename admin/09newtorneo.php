<?php

// Incluye los objetos necesarios
require("objetos/torneo.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::torneos::nuevo</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>
    
    <script>
        // Verifica los campos del formulario antes de guardar los datos
        function verificar_form() {
            if(document.datos.name.value.length==0) {
                alert("Error:\nDebe ingresar el nombre"); 
                document.datos.name.focus() 
                return 0; 
            }
            else {
                document.forms['datos'].submit();
            }
        }
    </script>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - torneos - nuevo'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=628 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <form name="datos" action="guardarTorneo.php" method=post enctype="multipart/form-data">
                        <table border=0 width=850>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="09torneos.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <table width=850>
                            <!--tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">idform</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=idformu size=108></input></td>
                            </tr-->
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Nombre</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=name size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Fecha</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=fecha size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Hora</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=hour size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Localización</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=location size=108></input></td>
                            </tr>
                            <!--tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">gmaps</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=gmaps size=108></input></td>
                            </tr-->
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Descripción</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=description size=108></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Información adicional</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=info size=108></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Número de cuenta</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=cuenta size=108></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Titular de la cuenta</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=titular size=108></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Precio total</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=precio size=108></input></td>
                            </tr><tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">¿Disputado?</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=conta size=108 value="no"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Cartel del torneo</font></td>
                                <td height=20 width=700 bgcolor=#ffffff>
									<input type="file" name="cartel" style="width:400px;border:0px solid #000000;"></input>
									<br>
									<font face="verdana" color="red" style="margin-left: 4px;font-size: 11px;;">400x564 pixels, 100kB max</font>
								</td>
                            </tr>
                            <tr>
                                <td bgcolor=#ffffff><a href="#" onclick="verificar_form();"><font face="arial" style="font-size: 11px;; color: blue;">Guardar torneo</font></a></td>
                            </tr>
                        </table>
                    </form>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>