<?php

// Incluye los objetos necesarios
require("objetos/formulario.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <script language="JavaScript" type="text/javascript">
		function modificar(idformu) { document.location.href="10modformulario.php?idformu=" + idformu; }
        function eliminar(idformu) {  document.location.href="10delformulario.php?idformu=" + idformu; }
        function nuevo(){             document.location.href="10newformulario.php"; }
    </script>

    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>cms::administración::formularios</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - formularios'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=700 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <table border=0 width=850>
                        <tr align="right">
                            <td width=850 bgcolor=#ffffff>
                                <a href="#" onClick="nuevo();">
                                    <font face="arial" style="font-size: 11px;; color: blue;">Introduce nuevo formulario</font>
                                </a>
                            </td>
                        </tr>
                    </table>
                    
                    <table width=850>
                        <tr height=10>
                            <td width=50 bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">idformu</font></td>
                            <td width=650 bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">formu</font></td>
                            <td width=50 bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">mail</font></td>
							<td width=50 bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">name</font></td>
                            <td width=50 bgcolor=#c8c8c8></td>
                            <td width=50 bgcolor=#c8c8c8></td>
                        </tr>


                    <?php
                    $form = obtenerFormulario();
                    
                    for($i=0;$i<numRows("CMS_formularios");$i++)
                    {
                        $_POST["idformu"]  = $form[$i]->idformu;
						$_POST["formu"]    = $form[$i]->formu;
						$_POST["mail"]     = $form[$i]->mail;
						$_POST["name"]     = $form[$i]->name;
						$_POST["fecha"]    = $form[$i]->fecha;
						$_POST["precio"]   = $form[$i]->precio;
						$_POST["teamname"] = $form[$i]->teamname;
						$_POST["categ"]    = $form[$i]->categ;
						$_POST["capname"]  = $form[$i]->capname;
						$_POST["captel"]   = $form[$i]->captel;
						$_POST["capmail"]  = $form[$i]->capmail;
						$_POST["n01name"]  = $form[$i]->n01name;
						$_POST["n01tel"]   = $form[$i]->n01tel;
						$_POST["n01mail"]  = $form[$i]->n01mail;
						$_POST["n02name"]  = $form[$i]->n02name;
						$_POST["n02tel"]   = $form[$i]->n02tel;
						$_POST["n02mail"]  = $form[$i]->n02mail;
						$_POST["n03name"]  = $form[$i]->n03name;
						$_POST["n03tel"]   = $form[$i]->n03tel;
						$_POST["n03mail"]  = $form[$i]->n03mail;
						$_POST["n04name"]  = $form[$i]->n04name;
						$_POST["n04tel"]   = $form[$i]->n04tel;
						$_POST["n04mail"]  = $form[$i]->n04mail;
						$_POST["n05name"]  = $form[$i]->n05name;
						$_POST["n05tel"]   = $form[$i]->n05tel;
						$_POST["n05mail"]  = $form[$i]->n05mail;
                        ?>

                        <form name="datos" action="guardarTags.php" method=post>
                            <tr height=10>
                                <td width=50   bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $_POST["idformu"];?> </font></td>
                                <td width=200  bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $_POST["formu"];?> </font></td>
                                <td width=250  bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $_POST["mail"];?>  </font></td>
                                <td width=250  bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $_POST["name"];?>  </font></td>
								<td width=50   bgcolor=#f8f8f8><a href="#" onClick="modificar('<?php echo $form[$i]->idformu; ?>');"><font face="arial" style="font-size: 11px;; color: blue;">modificar</font></a></td>
                                <td width=50   bgcolor=#f8f8f8><a href="#" onClick="eliminar('<?php echo $form[$i]->idformu; ?>');"> <font face="arial" style="font-size: 11px;; color: blue;">eliminar </font></a></td>
                            </tr>
                        </form>
                    <?php
                    }
                    ?>
                    </table>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>