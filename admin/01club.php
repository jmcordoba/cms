<?php
// Incluye los objetos necesarios
require("objetos/club.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Control de usuario identificado
require("check.php");
?>

<!DOCTYPE html>

<html>

    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>cms</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body bgcolor="#FFFFFF" style="margin:0px">   
        <!-- cabecera -->
        <?php
        $titulo = 'cms - administración - club'; 
        require('cabecera.php');
        ?>

        <!-- cuerpo -->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=800 align=center>
            <tr>
                <!-- menu -->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!-- contenido -->
                <td width=850 style="vertical-align:top">

                    <?php
                    $equipo = obtenerClub();
                    $_POST["name"]     = $equipo->name;
                    $_POST["presi"]    = $equipo->presi;
                    $_POST["street"]   = $equipo->street;
                    $_POST["cpostal"]  = $equipo->cpostal;
                    $_POST["telefono"] = $equipo->telefono;
                    $_POST["fax"]      = $equipo->fax;
                    $_POST["mail"]     = $equipo->mail;
                    $_POST["palmares"] = strip_tags($equipo->palmares);
                    $_POST["historia"] = strip_tags($equipo->historia);
                    ?>

                    <form name="datos" action="guardarClub.php" method=post enctype="multipart/form-data">
                        <table width="840">
							<tr valign="top">
								
								<td width="200" bgcolor="#ffffff">
									<table width="200" bgcolor="#ffffff">
										<tr valign="bottom" align="left">
											<td colspan="2" width="200" bgcolor="#ffffff">
												<img width="200" height="200" src="images/escudo/escudo.jpg">
											</td>
										</tr>
									</table>
								</td>
								
								<td width="640">
									<table width="640" cellspacing="1" cellpadding="0" style="margin:0px;padding:0px;">
										<tr>
											<td height="20" width="200" bgcolor="#c8c8c8">
												<font class="admin_font">Club</font>
											</td>
											<td height="20" width="440" bgcolor="#c8c8c8">
												<input class="admin_input" type="text" name="name" size="80" value="<?php echo $_POST["name"];?>" >
											</td>
										</tr>
										<tr>
											<td height="20" width="200" bgcolor="#c8c8c8">
												<font class="admin_font">Presidente</font>
											</td>
											<td height=20 width=440 bgcolor=#c8c8c8>
												<input class="admin_input" align=left type=text name=presi size=80 value="<?php echo $_POST["presi"];?>">
											</td>
										</tr>
										<tr>
											<td height="20" width="200" bgcolor="#c8c8c8">
												<font class="admin_font">Calle</font>
											</td>
											<td height=20 width=440 bgcolor=#c8c8c8>
												<input class="admin_input" align=left type=text name=street size=80 value="<?php echo $_POST["street"];?>">
											</td>
										</tr>
										<tr>
											<td height="20" width="200" bgcolor="#c8c8c8">
												<font class="admin_font">C. Postal</font>
											</td>
											<td height=20 width=440 bgcolor=#c8c8c8>
												<input class="admin_input" align=left type=text name=cpostal size=80 value="<?php echo $_POST["cpostal"];?>">
											</td>
										</tr>
										<tr>
											<td height="20" width="200" bgcolor="#c8c8c8">
												<font class="admin_font">Teléfono</font>
											</td>
											<td height=20 width=440 bgcolor=#c8c8c8>
												<input class="admin_input" align=left type=text name=telefono size=80 value="<?php echo $_POST["telefono"];?>">
											</td>
										</tr>
										<tr>
											<td height="20" width="200" bgcolor="#c8c8c8">
												<font class="admin_font">Fax</font>
											</td>
											<td height=20 width=440 bgcolor=#c8c8c8>
												<input class="admin_input" align=left type=text name=fax size=80 value="<?php echo $_POST["fax"];?>">
											</td>
										</tr>
										<tr>
											<td height="20" width="200" bgcolor="#c8c8c8">
												<font class="admin_font">Mail</font>
											</td>
											<td height=20 width=440 bgcolor=#c8c8c8>
												<input class="admin_input" align=left type=text name=mail size=80 value="<?php echo $_POST["mail"];?>">
											</td>
										</tr>
										<tr>
											<td height="20" width="200" bgcolor="#c8c8c8">
												<font class="admin_font">Escudo*</font>
											</td>
											<td height=20>
												<input type="file" name="userfile" style="width:400px;border:0px solid #000000;">
												<br>
												<font face="verdana" color="red" style="margin-left: 4px;font-size: 11px;;">200x200 pixels, 100 kB max</font>
											</td>
										<tr>
									</table>
								</td>
							</tr>
							
							<!-- palmares -->
							<tr>
								<td colspan="3" height="20" width="800" bgcolor="#ffffff">
									<font class="admin_font">Palmarés</font>
								</td>
							</tr>
                            <tr>
								<td colspan="3" >
									<textarea class="admin_textarea" name="palmares"><?php echo $_POST["palmares"];?></textarea>
								</td>
							</tr>
                            
							<!-- historia -->
							<tr>
								<td colspan="3" height=20 width=840 bgcolor="#ffffff">
									<font class="admin_font">Historia</font>
								</td>
							</tr>
                            <tr>
								<td colspan="3" >
									<textarea class="admin_textarea" name="historia"><?php echo $_POST["historia"];?></textarea>
								</td>
							</tr>
                            
							<tr>
								<td colspan="3" bgcolor="#ffffff">
									<a href="#" onclick="javascript:document.forms['datos'].submit(); return false;">
										<font face="arial" style="font-size: 11px;; color: blue;">Guardar información</font>
									</a>
								</td>
							</tr>
                        </table>
						
                    </form>
                </td>
            </tr>		
        </table>

        <!-- pie -->
    </body>
</html>
