<?php
// Incluye los objetos necesarios
require("objetos/temp.php");
require("objetos/cate.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Capturamos los datos que vienen por POST y los guardamos en el objeto equipo
$categ->idcat     = init("idcat");
$categ->name      = init("name");
$categ->numteams  = init("numteams");
$categ->wpoints   = init("wpoints");
$categ->dpoints   = init("dpoints");
$categ->lpoints   = init("lpoints");
$categ->jornadas  = init("jornadas");
$categ->idavuelta = init("idavuelta");
$categ->idtemp    = init("idtemp");

// Rivales
$categ->rival01   = init("rival01");
$categ->rival02   = init("rival02");
$categ->rival03   = init("rival03");
$categ->rival04   = init("rival04");
$categ->rival05   = init("rival05");
$categ->rival06   = init("rival06");
$categ->rival07   = init("rival07");
$categ->rival08   = init("rival08");
$categ->rival09   = init("rival09");
$categ->rival10   = init("rival10");
$categ->rival11   = init("rival11");
$categ->rival12   = init("rival12");
$categ->rival13   = init("rival13");
$categ->rival14   = init("rival14");
$categ->rival15   = init("rival15");
$categ->rival16   = init("rival16");
$categ->rival17   = init("rival17");
$categ->rival18   = init("rival18");
$categ->rival19   = init("rival19");
$categ->rival20   = init("rival20");

// Intentamos guardar el usuario y redirigimos en función de si hemos tenido éxito o no
if(guardarCate($categ)==true) redirect("index.php?origen=cate",0);
else                          redirect("index.php?origen=error",0);
?>
