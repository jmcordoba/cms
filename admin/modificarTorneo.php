<?php

// Incluímos Objetos necesarios
require("objetos/torneo.php");
// Incluímos funciones necesarias
require("fun/funciones.php");

// Obtenemos los datos del formulario
$torneo->idtorneo    = init("idtorneo");
$torneo->idformu     = init("idformu");
$torneo->name        = str_replace("'","\'",init("name"));
$torneo->fecha       = init("fecha");
$torneo->hour        = init("hour");
$torneo->location    = init("location");
$torneo->gmaps       = init("gmaps");
$torneo->description = str_replace("'","\'",init("description"));
$torneo->info        = init("info");
$torneo->cuenta      = init("cuenta");
$torneo->titular     = init("titular");
$torneo->precio      = init("precio");
$torneo->conta       = init("conta");
//$torneo->conta       = "";
$torneo->cartel      = init("cartel");

// Capturamos los datos del fichero adjunto
$nombre_archivo = $_FILES['cartel']['name']; 
$tipo_archivo   = $_FILES['cartel']['type']; 
$tamano_archivo = $_FILES['cartel']['size'];

// Si viene un fichero adjunto
if($nombre_archivo!="") {
    //compruebo si las caracter�sticas del archivo son las que deseo 
    if ($tamano_archivo < 100000) {
		ob_clean();
		// Redimensionamos el fichero a 200x200
		include('simpleImage.php');
		$imagen = new SimpleImage();
		$imagen->load($_FILES['userfile']['tmp_name']);
		$imagen->resize(400,564);
		$imagen->save($_FILES['userfile']['tmp_name']);
        // Inicializamos la ruta y el nombre del avatar del usuario
        $torneo->cartel    = "admin/images/torneos/torneo" . $torneo->idtorneo . ".gif";
        // Guardamos el fichero en la ruta especificada
        if (!move_uploaded_file($_FILES['cartel']['tmp_name'], "images/torneos/torneo" . $torneo->idtorneo . ".gif")) {
            // Si hay algún tipo de error, redirigimos a otra página
            ?><script>location.href='index.php?origen=error';</script><?php
			die();
        }
    } else {
		?>
		<script>
			alert('La imagen que desea subir es mayor de 100kB.\nPor favor, suba una imagen con un peso inferior.');
		</script>
		<?php
	}
}



// Guardamos los cambios si es posible y redireccionamos en función del resultado
if(modificarTorneo($torneo,$torneo->idtorneo)==true) redirect("index.php?origen=torneo",0);
else                                                 redirect("index.php?origen=error" ,0);

?>