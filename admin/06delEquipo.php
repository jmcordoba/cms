<?php

// Incluimos objetos y funciones
require("objetos/equipo.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
// Capturamos el id de la categoría a eliminar
$idteam = $_GET["idteam"];
// Eliminamos categoria y redirigimos
if(borrarEquipo($idteam)==true) redirect("index.php?origen=equipo" ,0);
else                            redirect("index.php?origen=error"  ,0);

?>