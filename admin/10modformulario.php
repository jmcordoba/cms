<?php

// Incluye los objetos necesarios
require("objetos/formulario.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::formulario::modificar</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>
    
    <script>
        // Verifica los campos del formulario antes de guardar los datos
        function verificar_form() {
            if(document.datos.formu.value.length==0) {
                alert("Error:\nDebe ingresar el nombre"); 
                document.datos.formu.focus() 
                return 0; 
            }
            else {
                document.forms['datos'].submit();
            }
        }
    </script>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - formulario - modificar'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=800 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <?php
                    
                    $idformu = $_GET["idformu"];
                    
                    $form = obtenerUnFormulario($idformu);
                    
                    $_POST["idformu"]  = $form->idformu;
					$_POST["formu"]    = $form->formu;
					$_POST["mail"]     = $form->mail;
					$_POST["name"]     = $form->name;
					$_POST["fecha"]    = $form->fecha;
					$_POST["precio"]   = $form->precio;
					$_POST["teamname"] = $form->teamname;
					$_POST["categ"]    = $form->categ;
					$_POST["capname"]  = $form->capname;
					$_POST["captel"]   = $form->captel;
					$_POST["capmail"]  = $form->capmail;
					$_POST["n01name"]  = $form->n01name;
					$_POST["n01tel"]   = $form->n01tel;
					$_POST["n01mail"]  = $form->n01mail;
					$_POST["n02name"]  = $form->n02name;
					$_POST["n02tel"]   = $form->n02tel;
					$_POST["n02mail"]  = $form->n02mail;
					$_POST["n03name"]  = $form->n03name;
					$_POST["n03tel"]   = $form->n03tel;
					$_POST["n03mail"]  = $form->n03mail;
					$_POST["n04name"]  = $form->n04name;
					$_POST["n04tel"]   = $form->n04tel;
					$_POST["n04mail"]  = $form->n04mail;
					$_POST["n05name"]  = $form->n05name;
					$_POST["n05tel"]   = $form->n05tel;
					$_POST["n05mail"]  = $form->n05mail;
                    ?>

                    <form name="datos" action="modificarFormulario.php" method=post enctype="multipart/form-data">
                        <table border=0 width=850>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="10formularios.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <table width=850>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">idformu</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input readonly="readonly" align=left type=text name=idformu size=108 value="<?php echo $_POST["idformu"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">formu</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=formu size=108 value="<?php echo $_POST["formu"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">mail</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=mail size=108 value="<?php echo $_POST["mail"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">name</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=name size=108 value="<?php echo $_POST["name"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">fecha</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=fecha size=108 value="<?php echo $_POST["fecha"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">precio</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=precio size=108 value="<?php echo $_POST["precio"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">teamname</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=teamname size=108 value="<?php echo $_POST["teamname"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">categ</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=categ size=108 value="<?php echo $_POST["categ"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">capname</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=capname size=108 value="<?php echo $_POST["capname"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">captel</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=captel size=108 value="<?php echo $_POST["captel"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">capmail</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=capmail size=108 value="<?php echo $_POST["capmail"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">n01name</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=n01name size=108 value="<?php echo $_POST["n01name"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">n01tel</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=n01tel size=108 value="<?php echo $_POST["n01tel"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">n01mail</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=n01mail size=108 value="<?php echo $_POST["n01mail"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">n02name</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=n02name size=108 value="<?php echo $_POST["n02name"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">n02tel</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=n02tel size=108 value="<?php echo $_POST["n02tel"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">n02mail</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=n02mail size=108 value="<?php echo $_POST["n02mail"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">n03name</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=n03name size=108 value="<?php echo $_POST["n03name"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">n03tel</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=n03tel size=108 value="<?php echo $_POST["n03tel"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">n03mail</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=n03mail size=108 value="<?php echo $_POST["n03mail"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">n04name</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=n04name size=108 value="<?php echo $_POST["n04name"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">n04tel</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=n04tel size=108 value="<?php echo $_POST["n04tel"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">n04mail</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=n04mail size=108 value="<?php echo $_POST["n04mail"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">n05name</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=n05name size=108 value="<?php echo $_POST["n05name"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">n05tel</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=n05tel size=108 value="<?php echo $_POST["n05tel"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">n05mail</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=n05mail size=108 value="<?php echo $_POST["n05mail"];?>"></input></td>
                            </tr>
                            <tr><td bgcolor=#ffffff><a href="#" onclick="verificar_form();"><font face="arial" style="font-size: 11px;; color: blue;">Guardar formulario</font></a></td></tr>
                        </table>
                    </form>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>