<?php

// 
require_once($_SERVER['DOCUMENT_ROOT']."/cmsweb/conf.php");

//
//
// FUNCIONES GENERALES
//
//

//
// Conecta con la base de datos disponible
//
function Conectarse() {
    // Conecta con la base de datos
    // @param1: URL donde está alojada la bbdd
    // @param2: nombre de usuario con acceso a la bbdd
    // @param3: password de la bbdd
    if (!($link=mysql_connect(conf_HOST,conf_USER,conf_PSWD))) {
        echo "Error conectando a la base de datos.";
        exit();
    }
    // Selecciona la bbdd y obtiene un puntero de conexión
    // @param1: nombre de la bbdde
    // @param2: puntero a la bbdd
    if (!mysql_select_db(conf_BBDD,$link)) {
        echo "Error seleccionando la base de datos.";
        exit();
    }
    return $link;
}

function loginUsuario_login($mail, $pasw){
    
	# Inicializo variable
    $validado = "";
    
	# Buscamos al usuario que pretende hacer la validacion
    $link     = Conectarse();
    $sql      = "SELECT * FROM `CMS_users` WHERE mail = '".trim($mail)."' AND password = '".trim($pasw)."'";
    $result   = mysql_query($sql, $link);
    $num_rows = mysql_num_rows($result);
    
	if($num_rows>0){
        if ($row = mysql_fetch_array($result)){
            do{
                $usuario->nombre    = $row["nombre"];
                $usuario->apellidos = $row["apellidos"];
                $usuario->usuario   = $row["usuario"];
                $usuario->password  = $row["password"];
                $usuario->telefono  = $row["telefono"];
                $usuario->mail      = $row["mail"];
                $usuario->permisos  = $row["permisos"];
                $usuario->conta     = $row["conta"];
                $usuario->validado  = $row["validado"];
                $usuario->foto      = $row["foto"];
                
                if($usuario->validado=="si") {
					$validado="si";
				}

            } while ($row = mysql_fetch_array($result));
        }
        mysql_close($link);
    }
    
    if($validado!="") return $usuario;
    else              return false;
}

//
// Limpia un string de SQL injection
//
function cleanQuery($string) {
    if(get_magic_quotes_gpc()) $string = stripslashes($string);
    if (phpversion() >= '4.3.0') $string = mysql_real_escape_string($string);
    else $string = mysql_escape_string($string);
    return $string;
}

//
// Muestra un mensaje como popup en el navegador
//
function redirect($pagina, $tiempo) {
    ?><script>location.href='<?php echo $pagina; ?>';</script><?php
	die();
}

//
// Muestra un mensaje como popup en el navegador
//
function alert($texto) {
    ?><script>alert ('<?php echo $texto;?>');</script><?php
}

//
// Devuelve el numero de filas de una tabla
//
function numRows($table) {
    $link=Conectarse();																				// conectamos con la bbdd mysql
    $result   = mysql_query("select * from " . $table, $link);										// seleccionamos todos los registros de la table
    $num_rows = mysql_num_rows($result);															// obtenemos el numero de filas
    mysql_close($link);																				// cerramos la conexion con la bbdd
    return $num_rows;																				// devolvemos el numero de filas
}

/////////////////
//
// 01club
//
/////////////////

//
// Guarda los datos del club si no había datos en la bbdd
// Parametro 1: objeto equipo que contiene los capturados en la web
//
function guardarClub($equipo) {
    if(numRows("CMS_club")==0) {
        $link=Conectarse();

	$sql = "INSERT INTO  `CMS_club` (  
		`idteam` ,  
		`name` ,  
		`presi` ,  
		`foto` ,  
		`street` ,  
		`cpostal` ,  
		`telefono` ,  
		`fax` ,  
		`mail` ,  
		`historia` ,  
		`palmares` ) 
	VALUES (
		'" . $equipo->idteam . "',
		'" . $equipo->name . "' ,
		'" . $equipo->presi . "' ,
                'algo' ,
		'" . $equipo->street . "' ,
		'" . $equipo->cpostal . "' ,
		'" . $equipo->telefono . "' ,
		'" . $equipo->fax . "' ,
		'" . $equipo->mail . "' ,
		'" . $equipo->palmares . "' ,
		'" . $equipo->historia . "'
	);";
        
        $sql = cleanQuery($sql);

	$result = mysql_query($sql, $link);
        
        mysql_close($link);
	
	if($result==true) return true;
	else return false;
    }
}

//
// Inserta los nuevos datos del club en su tabla, modificando los existentes
//
function modificarClub($equipo) {
    if(numRows("CMS_club")==1) {
        $link=Conectarse();
        
        $sql = "UPDATE `CMS_club` SET 
            `name`      = '" . $equipo->name     . "' ,
            `presi`     = '" . $equipo->presi    . "' ,
            `foto`      = '" . $equipo->foto     . "' ,
            `street`    = '" . $equipo->street   . "' ,
            `cpostal`   = '" . $equipo->cpostal  . "' ,
            `telefono`  = '" . $equipo->telefono . "' ,
            `fax`       = '" . $equipo->fax      . "' ,
            `mail`      = '" . $equipo->mail     . "' ,
            `historia`  = '" . $equipo->historia . "' ,
            `palmares`  = '" . $equipo->palmares . "'
            WHERE `idteam` = '0' ";

		$result = mysql_query($sql, $link);
        
        mysql_close($link);
	
		if($result==true) return true;
		else return false;
    } else {
        echo "no ha entrado a modificar club";
    }
}

//
// Borra la fila de los datos del club en la bbdd, actualmente no tiene sentido ya que siempre se
// modifica, incluso para dejarlo vacío
//
function borrarClub() {
    $result = true;
    
    if(numRows("CMS_club")==1) {
        $link=Conectarse();

        $result = mysql_query("delete from `CMS_club` where idteam=0", $link);

        mysql_close($link);
    }
    
    if($result==true) return true;
    else return false;
}

//
// Obtiene los datos del club, los almacena en el objeto club y devuelve el objeto club
//
function obtenerClub() {
    $equipo = null;
    
    if(numRows("CMS_club")>0) {
        $equipo = new club();
        
        $link=Conectarse();

        $result = mysql_query("select * from `CMS_club`", $link);
        
        if ($row = mysql_fetch_array($result)) {
            do {
                $equipo->idteam   = $row["idteam"];
                $equipo->name     = $row["name"];
                $equipo->presi    = $row["presi"];
                $equipo->foto     = $row["foto"];
                $equipo->street   = $row["street"];
                $equipo->cpostal  = $row["cpostal"];
                $equipo->telefono = $row["telefono"];
                $equipo->fax      = $row["fax"];
                $equipo->mail     = $row["mail"];
                $equipo->historia = $row["historia"];
                $equipo->palmares = $row["palmares"];
                
            } while ($row = mysql_fetch_array($result));
        }
        
        mysql_close($link);
    }
    
    return $equipo;
}

/////////////////
//
// 02temp
//
/////////////////

//
//
//
function guardarTemp($temporada) {
    
	$numero = 0;																					// Inicializamos la variable numero
    $link=Conectarse();																				// Conexión con la base de datos
    $sql = "select * from `CMS_temp` order by idtemp";												// Construcción de la query
    wlog("guardarTemp",$sql,1);																		// Registro de log
    $result = mysql_query($sql, $link);																// Ejecutamos la query y obtenemos el resultado
    if ($row = mysql_fetch_array($result)) {														// Obtenemos el ultimo idtemp
        do {
            $numero = $row["idtemp"];
        } while ($row = mysql_fetch_array($result));
    }
    $numero++;																						// Sumamos uno al ultimo idtemp
    // Construimos la nueva query para guardar los datos de la nueva temporada
    $sql = "INSERT INTO `CMS_temp` (`idtemp`,`name`,`yearstart`,`yearfinish`,`fede`) VALUES ('" . $numero . "','" . $temporada->name . "','" . $temporada->yearstart . "','" . $temporada->yearfinish . "','" . $temporada->fede . "');";
    wlog("guardarTemp",$sql,1);																		// Guardamos el log
    $result = mysql_query($sql, $link);																// Ejecutamos la query y obtenemos el resultado
    mysql_close($link);																				// Cerramos la conexion con la base de datos
    if($result==true) return true;																	// Devolvemos el resultado
    else return false;
}

//
//
//
function modificarTemp($temporada,$idtemp) {
    
    $link=Conectarse();																				// Conexión con la base de datos
    // Construimos la query
    $sql = "UPDATE `CMS_temp` SET `name` = '" . $temporada->name . "' ,`yearstart` = '" . $temporada->yearstart . "' ,`yearfinish` = '" . $temporada->yearfinish . "' ,`fede` = '" . $temporada->fede . "' WHERE `idtemp` = '" . $idtemp . "';";
    wlog("modificarTemp",$sql,1);																	// Guardamos el log
    $result = mysql_query($sql, $link);																// Ejecutamos la query y obtenemos el resultado
    mysql_close($link);																				// Cerramos la conexión con la base de datos
    if($result==true) return true;																	// Devolvemos el resultado
    else return false;
}

//
//
//
function borrarTemp($temp) {
    
    $result = false;																				// Inicializo variable
    $link=Conectarse();																				// Conexión con la base de datos
    $sql = "delete from `CMS_temp` where idtemp=" . $temp;											// construimos la sql
    wlog("borrarTemp",$sql,1);																		// Guardamos el log
    $result = mysql_query($sql,$link);																// Ejecutamos la query y obtenemos el resultado
    mysql_close($link);																				// Cerramos conexión con la base de datos
    if($result==true) return true;																	// Devolvemos el resultado de la ejecución
    else return false;
}

//
// Obtiene los datos de todas las temporadas, los almacena en el objeto temporada y lo devuelve
//
function obtenerTemp() {
    
    $equipo = null;																					// Inicializo variable
	$temporada = null;																				// Inicializo variable
    for ($i=0;$i<numRows("CMS_temp");$i++) {														// Para cada fila de la tabla inicializo una temporada
        $temporada[$i] = new temp();
    }
    $i = 0;																							// Pongo i a cero
    $link=Conectarse();																				// Obtengo conexión a la base de datos
    $sql = "select * from `CMS_temp` order by idtemp desc";											// Construyo la query
    wlog("obtenerTemp", $sql, 1);																	// Guardamos el log
    $result = mysql_query($sql, $link);																// Obtengo el resultado
    if ($row = mysql_fetch_array($result)) {														// Para cada fila, guardo las columnas en el objeto
        do {
            $temporada[$i]->idtemp         = $row["idtemp"];
            $temporada[$i]->name           = $row["name"];
            $temporada[$i]->yearstart      = $row["yearstart"];
            $temporada[$i]->yearfinish     = $row["yearfinish"];
            $temporada[$i]->fede           = $row["fede"];
			$i++;
        } while ($row = mysql_fetch_array($result));
    }
    mysql_close($link);																				// Cierro la conexión con la base de datos
    return $temporada;																				// Devuelvo el objeto que contiene los resultados de la búsqueda
}

//
//
//
function obtenerUnaTemp($idtemp) {
    
    $link=Conectarse();																				// Conexion con la base de datos
    $sql = "select * from `CMS_temp` where idtemp='" . $idtemp . "'";								// Construcción de la query
    wlog("obtenerUnaTemp",$sql,1);																	// GUardamos el log
    $result = mysql_query($sql, $link);																// Ejecutamos y guardamos la query
    if ($row = mysql_fetch_array($result)) {														// Por cada resultado, que sólo deberia ser uno
        do {
            $temporada->idtemp         = $row["idtemp"];
            $temporada->name           = $row["name"];
            $temporada->yearstart      = $row["yearstart"];
            $temporada->yearfinish     = $row["yearfinish"];
            $temporada->fede           = $row["fede"];
        } while ($row = mysql_fetch_array($result));
    }
    mysql_close($link);																				// Cerramos conexion con la base de datos
    return $temporada;																				// Devolvemos el objeto temporada
}

/////////////////
//
// 03cate
//
/////////////////

//
//
//
function obtenerCate() {
    
    $categoria = null;																				// Inicializo variable
    for ($i=0;$i<numRows("CMS_cate");$i++) {														// Para cada fila de la tabla inicializo una temporada
        $categoria[$i] = new cate();
    }
    $i = 0;																							// Pongo i a cero
    $link=Conectarse();																				// Obtengo conexión a la base de datos
    $sql = "select * from `CMS_cate` order by idcat desc";											// Construyo la query
    wlog("obtenerCate", $sql, 1);																	// Guardamos el log
    $result = mysql_query($sql, $link);																// Obtengo el resultado
    if ($row = @mysql_fetch_array($result)) {														// Para cada fila, guardo las columnas en el objeto
        do {
            $categoria[$i]->idcat     = $row["idcat"];
            $categoria[$i]->name      = $row["name"];
            $categoria[$i]->numteams  = $row["numteams"];
            $categoria[$i]->wpoints   = $row["wpoints"];
            $categoria[$i]->dpoints   = $row["dpoints"];
            $categoria[$i]->lpoints   = $row["lpoints"];
            $categoria[$i]->jornadas  = $row["jornadas"];
            $categoria[$i]->idavuelta = $row["idavuelta"];
            $categoria[$i]->idtemp    = $row["idtemp"];
			
			$categoria[$i]->rival01   = $row["rival01"];
			$categoria[$i]->rival02   = $row["rival02"];
			$categoria[$i]->rival03   = $row["rival03"];
			$categoria[$i]->rival04   = $row["rival04"];
			$categoria[$i]->rival05   = $row["rival05"];
			$categoria[$i]->rival06   = $row["rival06"];
			$categoria[$i]->rival07   = $row["rival07"];
			$categoria[$i]->rival08   = $row["rival08"];
			$categoria[$i]->rival09   = $row["rival09"];
			$categoria[$i]->rival10   = $row["rival10"];
			$categoria[$i]->rival11   = $row["rival11"];
			$categoria[$i]->rival12   = $row["rival12"];
			$categoria[$i]->rival13   = $row["rival13"];
			$categoria[$i]->rival14   = $row["rival14"];
			$categoria[$i]->rival15   = $row["rival15"];
			$categoria[$i]->rival16   = $row["rival16"];
			$categoria[$i]->rival17   = $row["rival17"];
			$categoria[$i]->rival18   = $row["rival18"];
			$categoria[$i]->rival19   = $row["rival19"];
			$categoria[$i]->rival20   = $row["rival20"];

            $i++;

        } while ($row = mysql_fetch_array($result));
    }
    mysql_close($link);																				// Cierro la conexión con la base de datos
    return $categoria;																				// Devuelvo el objeto que contiene los resultados de la búsqueda
}

//
//
//
function guardarCate($categoria) {
    
    $numero = 0;																					// Inicializamos la variable numero
	$link=Conectarse();																				// Conexión con la base de datos
    $sql = "select * from `CMS_cate` order by idcat";												// Construcción de la query
    wlog("guardarCate",$sql,1);																		// Registro de log
    $result = mysql_query($sql, $link);																// Ejecutamos la query y obtenemos el resultado
    if ($row = mysql_fetch_array($result)) {														// Obtenemos el ultimo idtemp
        do {
            $numero = $row["idcat"];
        } while ($row = mysql_fetch_array($result));
    }
    $numero++;																						// Sumamos uno al ultimo idtemp
    // Construimos la nueva query para guardar los datos de la nueva temporada
    $sql = "INSERT INTO `CMS_cate` (`idcat`,`name`,`numteams`,`wpoints`,`dpoints`,`lpoints`,`jornadas`,`idavuelta`,`idtemp`,`rival01`,`rival02`,`rival03`,`rival04`,`rival05`,`rival06`,`rival07`,`rival08`,`rival09`,`rival10`,`rival11`,`rival12`,`rival13`,`rival14`,`rival15`,`rival16`,`rival17`,`rival18`,`rival19`,`rival20`) VALUES ('" . $numero . "' , '" . $categoria->name . "' , '" . $categoria->numteams . "' , '" . $categoria->wpoints . "' , '" . $categoria->dpoints . "' , '" . $categoria->lpoints . "' , '" . $categoria->jornadas . "' , '" . $categoria->idavuelta . "' , '" . $categoria->idtemp . "' , '" . $categoria->rival01 . "' , '" . $categoria->rival02 . "' , '" . $categoria->rival03 . "' , '" . $categoria->rival04 . "' , '" . $categoria->rival05 . "' , '" . $categoria->rival06 . "' , '" . $categoria->rival07 . "' , '" . $categoria->rival08 . "' , '" . $categoria->rival09 . "' , '" . $categoria->rival10 . "' , '" . $categoria->rival11 . "' , '" . $categoria->rival12 . "' , '" . $categoria->rival13 . "' , '" . $categoria->rival14 . "' , '" . $categoria->rival15 . "' , '" . $categoria->rival16 . "' , '" . $categoria->rival17 . "' , '" . $categoria->rival18 . "' , '" . $categoria->rival19 . "' , '" . $categoria->rival20 . "');";
    wlog("guardarCate",$sql,1);																		// Guardamos el log
    $result = mysql_query($sql, $link);																// Ejecutamos la query y obtenemos el resultado
    mysql_close($link);																				// Cerramos la conexion con la base de datos
    if($result==true) return true;																	// Devolvemos el resultado
    else return false;
}

//
//
//
function borrarCate($cate) {
    
    $result = false;																				// Inicializo variable
    $link=Conectarse();																				// Conexión con la base de datos
    $sql = "delete from `CMS_cate` where idcat=" . $cate;											// construimos la query
    wlog("borrarCate",$sql,1);																		// Guardamos el log
    $result = mysql_query($sql,$link);																// Ejecutamos la query y obtenemos el resultado
    mysql_close($link);																				// Cerramos conexión con la base de datos
    if($result==true) return true;																	// Devolvemos el resultado de la ejecución
    else return false;
}

//
//
//
function obtenerUnaCate($idcat) {
    
    $link=Conectarse();																				// Conexion con la base de datos
    $sql = "select * from `CMS_cate` where idcat='" . $idcat . "'";									// Construcción de la query
    wlog("obtenerUnaCate",$sql,1);																	// GUardamos el log
    $result = mysql_query($sql, $link);																// Ejecutamos y guardamos la query
    if ($row = mysql_fetch_array($result)) {														// Por cada resultado, que sólo deberia ser uno
        do {
            $categoria->idcat     = $row["idcat"];
            $categoria->name      = $row["name"];
            $categoria->numteams  = $row["numteams"];
            $categoria->wpoints   = $row["wpoints"];
            $categoria->dpoints   = $row["dpoints"];
            $categoria->lpoints   = $row["lpoints"];
            $categoria->jornadas  = $row["jornadas"];
            $categoria->idavuelta = $row["idavuelta"];
            $categoria->idtemp    = $row["idtemp"];
			
			$categoria->rival01   = $row["rival01"];
			$categoria->rival02   = $row["rival02"];
			$categoria->rival03   = $row["rival03"];
			$categoria->rival04   = $row["rival04"];
			$categoria->rival05   = $row["rival05"];
			$categoria->rival06   = $row["rival06"];
			$categoria->rival07   = $row["rival07"];
			$categoria->rival08   = $row["rival08"];
			$categoria->rival09   = $row["rival09"];
			$categoria->rival10   = $row["rival10"];
			$categoria->rival11   = $row["rival11"];
			$categoria->rival12   = $row["rival12"];
			$categoria->rival13   = $row["rival13"];
			$categoria->rival14   = $row["rival14"];
			$categoria->rival15   = $row["rival15"];
			$categoria->rival16   = $row["rival16"];
			$categoria->rival17   = $row["rival17"];
			$categoria->rival18   = $row["rival18"];
			$categoria->rival19   = $row["rival19"];
			$categoria->rival20   = $row["rival20"];
            
        } while ($row = mysql_fetch_array($result));
    }
    mysql_close($link);																				// Cerramos conexion con la base de datos
    return $categoria;																				// Devolvemos el objeto temporada
}

//
//
//
function modificarCate($categoria,$idcat) {
    
    $link=Conectarse();																				// Conexión con la base de datos
    // Construimos la query
    $sql = "UPDATE `CMS_cate` SET 	`name` = '" . $categoria->name . "' ,
									`numteams` = '" . $categoria->numteams . "' ,
									`wpoints` = '" . $categoria->wpoints . "' ,
									`dpoints` = '" . $categoria->dpoints . "' ,
									`lpoints` = '" . $categoria->lpoints . "' ,
									`jornadas` = '" . $categoria->jornadas . "' ,
									`idavuelta` = '" . $categoria->idavuelta . "' ,
									`idtemp` = '" . $categoria->idtemp . "' ,
									`rival01` = '" . $categoria->rival01 . "' ,
									`rival02` = '" . $categoria->rival02 . "' ,
									`rival03` = '" . $categoria->rival03 . "' ,
									`rival04` = '" . $categoria->rival04 . "' ,
									`rival05` = '" . $categoria->rival05 . "' ,
									`rival06` = '" . $categoria->rival06 . "' ,
									`rival07` = '" . $categoria->rival07 . "' ,
									`rival08` = '" . $categoria->rival08 . "' ,
									`rival09` = '" . $categoria->rival09 . "' ,
									`rival10` = '" . $categoria->rival10 . "' ,
									`rival11` = '" . $categoria->rival11 . "' ,
									`rival12` = '" . $categoria->rival12 . "' ,
									`rival13` = '" . $categoria->rival13 . "' ,
									`rival14` = '" . $categoria->rival14 . "' ,
									`rival15` = '" . $categoria->rival15 . "' ,
									`rival16` = '" . $categoria->rival16 . "' ,
									`rival17` = '" . $categoria->rival17 . "' ,
									`rival18` = '" . $categoria->rival18 . "' ,
									`rival19` = '" . $categoria->rival19 . "' ,
									`rival20` = '" . $categoria->rival20 . "' 
									WHERE `idcat` = '" . $idcat . "';";
    wlog("modificarCate",$sql,1);																	// Guardamos el log
    $result = mysql_query($sql, $link);																// Ejecutamos la query y obtenemos el resultado
    mysql_close($link);																				// Cerramos la conexión con la base de datos
    if($result==true) return true;																	// Devolvemos el resultado
    else return false;
}

/////////////////
//
// 08insta
//
/////////////////

//
//
//
function obtenerInsta() {
    
    $instalacion = null;																			// Inicializo variable
    for ($i=0;$i<numRows("CMS_insta");$i++) {														// Para cada fila de la tabla inicializo una temporada
        $instalacion[$i] = new insta();
    }
    $i = 0;																							// Pongo i a cero
    $link=Conectarse();																				// Obtengo conexión a la base de datos
    $sql = "select * from `CMS_insta` order by idinst desc";										// Construyo la query
    wlog("obtenerInsta", $sql, 1);																	// Guardamos el log
    $result = mysql_query($sql, $link);																// Obtengo el resultado
    if ($row = @mysql_fetch_array($result)) {														// Para cada fila, guardo las columnas en el objeto
        do {
            $instalacion[$i]->idinst    = $row["idinst"];
            $instalacion[$i]->name      = $row["name"];
            $instalacion[$i]->direccion = $row["direccion"];
            $instalacion[$i]->telefono  = $row["telefono"];
            $instalacion[$i]->mail      = $row["mail"];
            $instalacion[$i]->persona   = $row["persona"];
            $instalacion[$i]->conta     = $row["conta"];
            $instalacion[$i]->foto      = $row["foto"];

            $i++;

        } while ($row = mysql_fetch_array($result));
    }
    mysql_close($link);																				// Cierro la conexión con la base de datos
    return $instalacion;																			// Devuelvo el objeto que contiene los resultados de la búsqueda
}

//
//
//
function guardarInsta($instalacion) {
    
    $numero = 0;																					// Inicializamos la variable numero
	$link=Conectarse();																				// Conexión con la base de datos
    $sql = "select * from `CMS_insta` order by idinst";												// Construcción de la query
    wlog("guardarInsta",$sql,1);																	// Registro de log
	$result = mysql_query($sql, $link);																// Ejecutamos la query y obtenemos el resultado
    if ($row = mysql_fetch_array($result)) {														// Obtenemos el ultimo idtemp
        do {
            $numero = $row["idinst"];
        } while ($row = mysql_fetch_array($result));
    }
    $numero++;																						// Sumamos uno al ultimo idtemp
    // Construimos la nueva query para guardar los datos de la nueva temporada
    $sql = "INSERT INTO `CMS_insta` (`idinst`,`name`,`direccion`,`telefono`,`mail`,`persona`,`conta`,`foto`) VALUES ('" . $numero . "' , '" . $instalacion->name . "' , '" . $instalacion->direccion . "' , '" . $instalacion->telefono . "' , '" . $instalacion->mail . "' , '" . $instalacion->persona . "' , '" . $instalacion->conta . "' , '" . $instalacion->foto . "');";
    wlog("guardarInsta",$sql,1);																	// Guardamos el log
    $result = mysql_query($sql, $link);																// Ejecutamos la query y obtenemos el resultado
    mysql_close($link);																				// Cerramos la conexion con la base de datos
    if($result==true) return true;																	// Devolvemos el resultado
    else return false;
}

//
//
//
function borrarInsta($insta) {
    
    $instalacion = obtenerUnaInsta($insta);															// Obtener datos completos de instalacion
    $result = false;																				// Inicializo variable
    $link=Conectarse();																				// Conexión con la base de datos
    $sql = "delete from `CMS_insta` where idinst=" . $insta;										// Construimos la query
    wlog("borrarInsta",$sql,1);																		// Guardamos el log
    $result = mysql_query($sql,$link);																// Ejecutamos la query y obtenemos el resultado
	mysql_close($link);																				// Cerramos conexión con la base de datos
	if($instalacion->foto!="") {																	// Borramos el fichero del disco duro
		$fichero = str_replace("admin/","",$instalacion->foto);										// Borramos el fichero del disco duro si existe
		if(file_exists($fichero)) unlink($fichero);
	}
    if($result==true) return true;																	// Devolvemos el resultado de la ejecución
    else return false;
}

//
//
//
function obtenerUnaInsta($idinst) {
    
    $instalacion = null;																			// Inicializamos la veriable instalacion
	$link=Conectarse();																				// Conexion con la base de datos
    $sql = "select * from `CMS_insta` where idinst='" . $idinst . "'";								// Construcción de la query
    wlog("obtenerUnaInsta",$sql,1);																	// GUardamos el log
    $result = mysql_query($sql, $link);																// Ejecutamos y guardamos la query
    if ($row = mysql_fetch_array($result)) {														// Por cada resultado, que sólo deberia ser uno
        do {
            $instalacion->idinst    = $row["idinst"];
            $instalacion->name      = $row["name"];
            $instalacion->direccion = $row["direccion"];
            $instalacion->telefono  = $row["telefono"];
            $instalacion->mail      = $row["mail"];
            $instalacion->persona   = $row["persona"];
            $instalacion->conta     = $row["conta"];
            $instalacion->foto      = $row["foto"];
        } while ($row = mysql_fetch_array($result));
    }
    mysql_close($link);																				// Cerramos conexion con la base de datos
    return $instalacion;																			// Devolvemos el objeto temporada
}

//
//
//
function obtenerUnaInstaPorNombre($inst) {
    
    $instalacion = null;																			// Inicializamos la veriable instalacion
    $link=Conectarse();																				// Conexion con la base de datos
    $sql = "select * from `CMS_insta` where name='" . $inst . "'";									// Construcción de la query
    wlog("obtenerUnaInsta",$sql,1);																	// GUardamos el log
	$result = mysql_query($sql, $link);																// Ejecutamos y guardamos la query
    if ($row = mysql_fetch_array($result)) {														// Por cada resultado, que sólo deberia ser uno
        do {
            $instalacion->idinst    = $row["idinst"];
            $instalacion->name      = $row["name"];
            $instalacion->direccion = $row["direccion"];
            $instalacion->telefono  = $row["telefono"];
            $instalacion->mail      = $row["mail"];
            $instalacion->persona   = $row["persona"];
            $instalacion->conta     = $row["conta"];
            $instalacion->foto      = $row["foto"];
        } while ($row = mysql_fetch_array($result));
    }
    mysql_close($link);																				// Cerramos conexion con la base de datos
	return $instalacion;																			// Devolvemos el objeto temporada
}

//
//
//
function modificarInsta($instalacion,$idinst) {
    
    $link=Conectarse();																				// Conexión con la base de datos
    // Construimos la query
    $sql = "UPDATE `CMS_insta` SET `name` = '" . $instalacion->name . "' ,`direccion` = '" . $instalacion->direccion . "' ,`telefono` = '" . $instalacion->telefono . "' ,`mail` = '" . $instalacion->mail . "' ,`persona` = '" . $instalacion->persona . "' ,`conta` = '" . $instalacion->conta . "' ,`foto` = '" . $instalacion->foto . "' WHERE `idinst` = '" . $idinst . "';";
    wlog("modificarInsta",$sql,1);																	// Guardamos el log
    $result = mysql_query($sql, $link);																// Ejecutamos la query y obtenemos el resultado
    mysql_close($link);																				// Cerramos la conexión con la base de datos
    if($result==true) return true;																	// Devolvemos el resultado
    else return false;
}

/////////////////
//
// 13users
//
/////////////////

//
//
//
function obtenerUsers() {
    
    $usuario = null;																				// Inicializo variable
    for ($i=0;$i<numRows("CMS_users");$i++) {														// Para cada fila de la tabla inicializo una temporada
        $usuario[$i] = new usuario();
    }
    $i = 0;																							// Pongo i a cero
    $link=Conectarse();																				// Obtengo conexión a la base de datos
    $sql = "select * from `CMS_users` order by nombre desc";										// Construyo la query
    wlog("obtenerUsers", $sql, 1);																	// Guardamos el log
    $result = mysql_query($sql, $link);																// Obtengo el resultado
    if ($row = @mysql_fetch_array($result)) {														// Para cada fila, guardo las columnas en el objeto
        do {
            $usuario[$i]->nombre     = $row["nombre"];
            $usuario[$i]->apellidos  = $row["apellidos"];
            $usuario[$i]->usuario    = $row["usuario"];
            $usuario[$i]->password   = $row["password"];
            $usuario[$i]->telefono   = $row["telefono"];
            $usuario[$i]->mail       = $row["mail"];
            $usuario[$i]->permisos   = $row["permisos"];
            $usuario[$i]->conta      = $row["conta"];
            $usuario[$i]->validado   = $row["validado"];
            $usuario[$i]->fecha_alta = $row["fecha_alta"];
            $usuario[$i]->foto       = $row["foto"];
			$i++;
        } while ($row = mysql_fetch_array($result));
    }
    mysql_close($link);																				// Cierro la conexión con la base de datos
    return $usuario;																				// Devuelvo el objeto que contiene los resultados de la búsqueda
}

//
//
//
function guardarUser($usuario) {
    
    $link=Conectarse();																				// Conexión con la base de datos
    // Construimos la nueva query para guardar los datos de la nueva temporada
    $sql = "INSERT INTO `CMS_users` (`nombre`,`apellidos`,`usuario`,`password`,`telefono`,`mail`,`permisos`,`conta`,`validado`,`fecha_alta`,`foto`) VALUES ('" . $usuario->nombre . "' , '" . $usuario->apellidos . "' , '" . $usuario->usuario . "' , '" . $usuario->password . "' , '" . $usuario->telefono . "' , '" . $usuario->mail . "' , '" . $usuario->permisos . "' , '" . $usuario->conta  . "' , '" . $usuario->validado . "' , '" . $usuario->fecha_alta . "' , '" . $usuario->foto . "');";
    wlog("guardarUser",$sql,1);																		// Guardamos el log
    $result = mysql_query($sql, $link);																// Ejecutamos la query y obtenemos el resultado
    mysql_close($link);																				// Cerramos la conexion con la base de datos
    if($result==true) return true;																	// Devolvemos el resultado
    else return false;
}

//
// Borramos al usuario de la bbdd y su avatar del disco duro
//
function borrarUser($user) {
    
    $usuario = obtenerUnUser($user);																// Obtenemos el usuario
    $result = false;																				// Inicializo variable
    $link=Conectarse();																				// Conexión con la base de datos
    $sql = "delete from `CMS_users` where usuario='" . $user . "'";									// Construir query
    wlog("borrarUser",$sql,1);																		// Guardamos el log
    $result = mysql_query($sql,$link);																// Ejecutamos la query y obtenemos el resultado
    mysql_close($link);																				// Cerramos conexión con la base de datos
    $fichero = str_replace("admin/","",$usuario->foto);												// Si la imagen del usuario que queremos borrar no es la imagen original de usuario desconocido
	if($usuario->foto!="images/usuario/user.jpg") {	
		if(file_exists($fichero)) unlink($fichero);													// Borramos el fichero del disco duro si existe
	}
	if($result==true) return true;																	// Devolvemos el resultado de la ejecución
    else return false;
}

//
//
//
function obtenerUnUser($user) {
    
    $link=Conectarse();																				// Conexion con la base de datos
    $sql = "select * from `CMS_users` where usuario='" . $user . "'";								// Construcción de la query
    wlog("obtenerUnUser",$sql,1);																	// GUardamos el log
    $result = mysql_query($sql, $link);																// Ejecutamos y guardamos la query
    if ($row = mysql_fetch_array($result)) {														// Por cada resultado, que sólo deberia ser uno
        do {
            $usuario->nombre    = $row["nombre"];
            $usuario->apellidos = $row["apellidos"];
            $usuario->usuario   = $row["usuario"];
            $usuario->password  = $row["password"];
            $usuario->telefono  = $row["telefono"];
            $usuario->mail      = $row["mail"];
            $usuario->permisos  = $row["permisos"];
            $usuario->conta     = $row["conta"];
            $usuario->validado  = $row["validado"];
            $usuario->foto      = $row["foto"];
        } while ($row = mysql_fetch_array($result));
    }
    mysql_close($link);																				// Cerramos conexion con la base de datos
    return $usuario;																				// Devolvemos el objeto temporada
}

//
//
//
function modificarUser($usuario) {
    
    $link=Conectarse();																				// Conexión con la base de datos
    // Construimos la query
    $sql = "UPDATE `CMS_users` SET `nombre` = '" . $usuario->nombre . "' ,`apellidos` = '" . $usuario->apellidos . "' ,`usuario` = '" . $usuario->usuario . "' ,`password` = '" . $usuario->password . "' ,`telefono` = '" . $usuario->telefono . "' ,`mail` = '" . $usuario->mail . "' ,`permisos` = '" . $usuario->permisos . "' ,`conta` = '" . $usuario->conta . "' ,`validado` = '" . $usuario->validado . "' ,`foto` = '" . $usuario->foto . "' WHERE `usuario` = '" . $usuario->usuario . "';";
    wlog("modificarUser",$sql,1);																	// Guardamos el log
    $result = mysql_query($sql, $link);																// Ejecutamos la query y obtenemos el resultado
    mysql_close($link);																				// Cerramos la conexión con la base de datos
    if($result==true) return true;																	// Devolvemos el resultado
    else return false;
}

/////////////////
//
// 17patro
//
/////////////////

//
//
//
function obtenerPatro() {
    
    $patro = null;																					// Inicializo variable
    for ($i=0;$i<numRows("CMS_patro");$i++) {														// Para cada fila de la tabla inicializo una temporada
        $patro[$i] = new patro();
    }
	$i = 0;																							// Pongo i a cero
    $link=Conectarse();																				// Obtengo conexión a la base de datos
    $sql = "select * from `CMS_patro` order by idpatro desc";										// Construyo la query
	wlog("obtenerPatro", $sql, 1);																	// Guardamos el log
    $result = mysql_query($sql, $link);																// Obtengo el resultado
    if ($row = @mysql_fetch_array($result)) {														// Para cada fila, guardo las columnas en el objeto
        do {
            $patro[$i]->idpatro    = $row["idpatro"];
            $patro[$i]->nombre     = $row["nombre"];
            $patro[$i]->imagen     = $row["imagen"];
            $patro[$i]->fecha_alta = $row["fecha_alta"];
            $patro[$i]->fecha_baja = $row["fecha_baja"];
            $patro[$i]->importe    = $row["importe"];
            $patro[$i]->web        = $row["web"];
            $patro[$i]->conta      = $row["conta"];
            $i++;
        } while ($row = mysql_fetch_array($result));
    }
    mysql_close($link);																				// Cierro la conexión con la base de datos
    return $patro;																					// Devuelvo el objeto que contiene los resultados de la búsqueda
}

//
//
//
function guardarPatro($patro) { 
    
	$numero = 0;																					// Inicializamos la variable numero
    $link=Conectarse();																				// Conexión con la base de datos
    $sql = "select * from `CMS_patro` order by idpatro";											// Construcción de la query
    wlog("guardarPatro",$sql,1);																	// Registro de log
    $result = mysql_query($sql, $link);																// Ejecutamos la query y obtenemos el resultado
    if ($row = mysql_fetch_array($result)) {														// Obtenemos el ultimo idtemp
        do {
            $numero = $row["idpatro"];
        } while ($row = mysql_fetch_array($result));
    }
    $numero++;																						// Sumamos uno al ultimo idtemp
    // Construimos la nueva query para guardar los datos de la nueva temporada
    $sql = "INSERT INTO `CMS_patro` (`idpatro`,`nombre`,`fecha_alta`,`fecha_baja`,`importe`,`web`,`conta`,`imagen`) VALUES ('" . $numero . "' , '" . $patro->nombre . "' , '" . $patro->fecha_alta . "' , '" . $patro->fecha_baja . "' , '" . $patro->importe . "' , '" . $patro->web . "' , '" . $patro->conta . "' , '" . $patro->imagen . "');";
    wlog("guardarPatro",$sql,1);																	// Guardamos el log
    $result = mysql_query($sql, $link);																// Ejecutamos la query y obtenemos el resultado
    mysql_close($link);																				// Cerramos la conexion con la base de datos
    if($result==true) return true;																	// Devolvemos el resultado
    else return false;
}

//
//
//
function borrarPatro($idpatro) {
    
    $patro = obtenerUnPatro($idpatro);																// Obtenemos el patrocinador a partir del idpatro
    $result = false;																				// Inicializo variable
	$link=Conectarse();																				// Conexión con la base de datos
    $sql = "delete from `CMS_patro` where idpatro=" . $idpatro;										// Construimos la query
    wlog("borrarPatro",$sql,1);																		// Guardamos el log
    $result = mysql_query($sql,$link);																// Ejecutamos la query y obtenemos el resultado
    mysql_close($link);																				// Cerramos conexión con la base de datos
	$fichero = str_replace("admin/","",$patro->imagen);												// construimos la ruta del fichero
	if(file_exists($fichero)) unlink($fichero);														// Borramos el fichero del disco duro si existe
	if($result==true) return true;																	// Devolvemos el resultado de la ejecución
    else return false;
}

//
//
//
function obtenerUnPatro($idpatro) {
    
    $link=Conectarse();																				// Conexion con la base de datos
	$sql = "select * from `CMS_patro` where idpatro='" . $idpatro . "'";							// Construcción de la query
    wlog("obtenerUnPatro",$sql,1);																	// GUardamos el log
    $result = mysql_query($sql, $link);																// Ejecutamos y guardamos la query
    if ($row = mysql_fetch_array($result)) {														// Por cada resultado, que sólo deberia ser uno
        do {
            $patro->idpatro    = $row["idpatro"];
            $patro->nombre     = $row["nombre"];
            $patro->imagen     = $row["imagen"];
            $patro->fecha_alta = $row["fecha_alta"];
            $patro->fecha_baja = $row["fecha_baja"];
            $patro->importe    = $row["importe"];
            $patro->web        = $row["web"];
            $patro->conta      = $row["conta"];
        } while ($row = mysql_fetch_array($result));
    }
    mysql_close($link);																				// Cerramos conexion con la base de datos
    return $patro;																					// Devolvemos el objeto temporada
}

//
//
//
function modificarPatro($patro,$idpatro) {
    
    $link=Conectarse();																				// Conexión con la base de datos
    // Construimos la query
    $sql = "UPDATE `CMS_patro` SET `nombre` = '" . $patro->nombre . "' ,`imagen` = '" . $patro->imagen . "' ,`fecha_alta` = '" . $patro->fecha_alta . "' ,`fecha_baja` = '" . $patro->fecha_baja . "' ,`importe` = '" . $patro->importe . "' ,`web` = '" . $patro->web . "' ,`conta` = '" . $patro->conta . "' WHERE `idpatro` = '" . $idpatro . "';";
    wlog("modificarPatro",$sql,1);																	// Guardamos el log
    $result = mysql_query($sql, $link);																// Ejecutamos la query y obtenemos el resultado
    mysql_close($link);																				// Cerramos la conexión con la base de datos
	if($result==true) return true;																	// Devolvemos el resultado
    else return false;
}

/////////////////
//
// 16tags
//
/////////////////

//
//
//
function obtenerTags() {
    // Inicializo variable
    $tag = null;
    // Para cada fila de la tabla inicializo una temporada
    for ($i=0;$i<numRows("CMS_tags");$i++) {
        $tag[$i] = new tags();
    }
    // Pongo i a cero
    $i = 0;
    // Obtengo conexión a la base de datos
    $link=Conectarse();
    // Construyo la query
    $sql = "select * from `CMS_tags` order by idtags desc";
    // Guardamos el log
    wlog("obtenerTags", $sql, 1);
    // Obtengo el resultado
    $result = mysql_query($sql, $link);
    // Para cada fila, guardo las columnas en el objeto
    if ($row = @mysql_fetch_array($result)) {
        do {
            $tag[$i]->idtags = $row["idtags"];
            $tag[$i]->nombre = $row["nombre"];
            $tag[$i]->conta  = $row["conta"];
            
            $i++;

        } while ($row = mysql_fetch_array($result));
    }
    // Cierro la conexión con la base de datos
    mysql_close($link);
    // Devuelvo el objeto que contiene los resultados de la búsqueda
    return $tag;
}

//
//
//
function guardarTags($tag) {
    // Inicializamos la variable numero
    $numero = 0;
    // Conexión con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_tags` order by idtags";
    // Registro de log
    wlog("guardarTags",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Obtenemos el ultimo idtemp
    if ($row = mysql_fetch_array($result)) {
        do {
            $numero = $row["idtags"];
        } while ($row = mysql_fetch_array($result));
    }
    // Sumamos uno al ultimo idtemp
    $numero++;
    // Construimos la nueva query para guardar los datos de la nueva temporada
    $sql = "INSERT INTO `CMS_tags` (`idtags`,`nombre`,`conta`) VALUES ('" . $numero . "' , '" . $tag->nombre . "' , '" . $tag->conta . "');";
    // Guardamos el log
    wlog("guardarTags",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexion con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

//
//
//
function borrarTags($idtags) {
    // Inicializo variable
    $result = false;
    // Conexión con la base de datos
    $link=Conectarse();
    $sql = "delete from `CMS_tags` where idtags=" . $idtags;
    // Guardamos el log
    wlog("borrarTags",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql,$link);
    // Cerramos conexión con la base de datos
    mysql_close($link);
    // Devolvemos el resultado de la ejecución
    if($result==true) return true;
    else return false;
}

//
//
//
function obtenerUnTag($idtags) {
    // Conexion con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_tags` where idtags='" . $idtags . "'";
    // GUardamos el log
    wlog("obtenerUnTag",$sql,1);
    // Ejecutamos y guardamos la query
    $result = mysql_query($sql, $link);
    // Por cada resultado, que sólo deberia ser uno
    if ($row = mysql_fetch_array($result)) {
        do {
            $tag->idtags     = $row["idtags"];
            $tag->nombre     = $row["nombre"];
            $tag->conta      = $row["conta"];
            
        } while ($row = mysql_fetch_array($result));
    }
    // Cerramos conexion con la base de datos
    mysql_close($link);
    // Devolvemos el objeto temporada
    return $tag;
}

//
//
//
function modificarTags($tags) {
    // Conexión con la base de datos
    $link=Conectarse();
    // Construimos la query
    $sql = "UPDATE `CMS_tags` SET `nombre` = '" . $tags->nombre . "' ,`conta` = '" . $tags->conta . "' WHERE `idtags` = '" . $tags->idtags . "';";
    // Guardamos el log
    wlog("modificarTags",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexión con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

/////////////////
//
// 12sociales
//
/////////////////

//
//
//
function obtenerSociales() {
    // Inicializo variable
    $social = null;
    // Para cada fila de la tabla inicializo una temporada
    for ($i=0;$i<numRows("CMS_sociales");$i++) {
        $social[$i] = new social();
    }
    // Pongo i a cero
    $i = 0;
    // Obtengo conexión a la base de datos
    $link=Conectarse();
    // Construyo la query
    $sql = "select * from `CMS_sociales` order by idsocial desc";
    // Guardamos el log
    wlog("obtenerSociales", $sql, 1);
    // Obtengo el resultado
    $result = mysql_query($sql, $link);
    // Para cada fila, guardo las columnas en el objeto
    if ($row = @mysql_fetch_array($result)) {
        do {
            $social[$i]->idsocial = $row["idsocial"];
            $social[$i]->nombre   = $row["nombre"];
            $social[$i]->link     = $row["link"];
            $social[$i]->icono    = $row["icono"];
            
            $i++;

        } while ($row = mysql_fetch_array($result));
    }
    // Cierro la conexión con la base de datos
    mysql_close($link);
    // Devuelvo el objeto que contiene los resultados de la búsqueda
    return $social;
}

//
//
//
function guardarSocial($social,$numero) {
    // Conexión con la base de datos
    $link=Conectarse();
    // Construimos la nueva query para guardar los datos de la nueva temporada
    $sql = "INSERT INTO `CMS_sociales` (`idsocial`,`nombre`,`link`,`icono`) VALUES ('" . $numero . "' , '" . $social->nombre . "' , '" . $social->link . "' , '" . $social->icono . "');";
    // Guardamos el log
    wlog("guardarSocial",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexion con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else              return false;
}

//
//
//
function borrarSocial($idsocial) {
    // Obtenemos el usuario
    $social = obtenerUnaSocial($idsocial);
    // Inicializo variable
    $result = false;
    // Conexión con la base de datos
    $link=Conectarse();
    $sql = "delete from `CMS_sociales` where idsocial=" . $idsocial;
    // Guardamos el log
    wlog("borrarSocial",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql,$link);
    // Cerramos conexión con la base de datos
    mysql_close($link);
    // Borramos el fichero del disco duro
    if($social->icono!="") unlink($social->icono);
    // Devolvemos el resultado de la ejecución
    if($result==true) return true;
    else return false;
}

//
//
//
function obtenerUnaSocial($idsocial) {
    // Conexion con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_sociales` where idsocial='" . $idsocial . "'";
    // GUardamos el log
    wlog("obtenerUnaSocial",$sql,1);
    // Ejecutamos y guardamos la query
    $result = mysql_query($sql, $link);
    // Por cada resultado, que sólo deberia ser uno
    if ($row = mysql_fetch_array($result)) {
        do {
            $social->idsocial = $row["idsocial"];
            $social->nombre   = $row["nombre"];
            $social->link     = $row["link"];
            $social->icono    = $row["icono"];
            
        } while ($row = mysql_fetch_array($result));
    }
    // Cerramos conexion con la base de datos
    mysql_close($link);
    // Devolvemos el objeto temporada
    return $social;
}

//
//
//
function modificarSocial($social,$idsocial) {
    // Conexión con la base de datos
    $link=Conectarse();
    // Construimos la query
    $sql = "UPDATE `CMS_sociales` SET `nombre` = '" . $social->nombre . "' ,`link` = '" . $social->link . "' ,`icono` = '" . $social->icono . "' WHERE `idsocial` = '" . $idsocial . "';";
    // Guardamos el log
    wlog("modificarSocial",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexión con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

////////////
//
// Articulos
//
////////////

//
//
//
function obtenerArticulo() {
    // Inicializo variable
    $articulo = null;
    // Para cada fila de la tabla inicializo una temporada
    for ($i=0;$i<numRows("CMS_articulos");$i++) {
        $articulo[$i] = new articulo();
    }
    // Pongo i a cero
    $i = 0;
    // Obtengo conexión a la base de datos
    $link=Conectarse();
    // Construyo la query
    $sql = "select * from `CMS_articulos` order by idart desc";
    // Guardamos el log
    wlog("obtenerArticulo", $sql, 1);
    // Obtengo el resultado
    $result = mysql_query($sql, $link);
    // Para cada fila, guardo las columnas en el objeto
    if ($row = @mysql_fetch_array($result)) {
        do {
            $articulo[$i]->idart     = $row["idart"];
            $articulo[$i]->fecha     = $row["fecha"];
            $articulo[$i]->iduser    = $row["iduser"];
            $articulo[$i]->titulo    = $row["titulo"];
            $articulo[$i]->contenido = $row["contenido"];
            $articulo[$i]->idtag     = $row["idtag"];
            $articulo[$i]->conta     = $row["conta"];
            $articulo[$i]->fechasi   = $row["fechasi"];
            $articulo[$i]->fechano   = $row["fechano"];            
            $articulo[$i]->imagen    = $row["imagen"];

            $i++;

        } while ($row = mysql_fetch_array($result));
    }
    // Cierro la conexión con la base de datos
    mysql_close($link);
    // Devuelvo el objeto que contiene los resultados de la búsqueda
    return $articulo;
}

//
//
//
function guardarArticulo($articulo) {
    // Inicializamos la variable numero
    $numero = 0;
	$numeroC = 0;
    // Conexión con la base de datos
    $link=Conectarse();
    
	// Construcción de la query
    $sql = "select * from `CMS_articulos` order by idart";
    // Registro de log
    wlog("guardarArticulo",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Obtenemos el ultimo idtemp
    if ($row = mysql_fetch_array($result)) {
        do {
            $numero = $row["idart"];
        } while ($row = mysql_fetch_array($result));
    }
	
	// Construcción de la query
    $sql = "select * from `CMS_cronicas` order by idcro";
    // Registro de log
    wlog("guardarArticulo",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Obtenemos el ultimo idtemp
    if ($row = mysql_fetch_array($result)) {
        do {
            $numeroC = $row["idcro"];
        } while ($row = mysql_fetch_array($result));
    }
    
	// nos quedamos con el identificador mayor entre el de los articulos y el de las cronicas
	if($numeroC>$numero) $numero = $numeroC;
	
	// Sumamos uno al ultimo idtemp
    $numero++;
    
	// Construimos la nueva query para guardar los datos de la nueva temporada
    $sql = "INSERT INTO `CMS_articulos` (`idart`,`fecha`,`iduser`,`titulo`,`contenido`,`idtag`,`conta`,`fechasi`,`fechano`,`imagen`) VALUES ('" . $numero . "' , '" . $articulo->fecha . "' , '" . $articulo->iduser . "' , '" . $articulo->titulo . "' , '" . $articulo->contenido . "' , '" . $articulo->idtag . "' , '" . $articulo->conta . "' , '" . $articulo->fechasi . "' , '" . $articulo->fechano . "' , '" . $articulo->imagen . "');";
    // Guardamos el log
    wlog("guardarArticulo",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexion con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

//
//
//
function borrarArticulo($idart) {
    // Obtenemos el usuario
    $articulo = obtenerUnArticulo($idart);
    // Inicializo variable
    $result = false;
    // Conexión con la base de datos
    $link=Conectarse();
    $sql = "delete from `CMS_articulos` where idart=" . $idart;
    // Guardamos el log
    wlog("borrarArticulo",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql,$link);
    // Cerramos conexión con la base de datos
    mysql_close($link);
    // Borramos el fichero del disco duro
    if($articulo->imagen!="") unlink($articulo->imagen);
    // Devolvemos el resultado de la ejecución
    if($result==true) return true;
    else return false;
}

//
//
//
function obtenerUnArticulo($idart) {
    // Conexion con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_articulos` where idart='" . $idart . "'";
    // GUardamos el log
    wlog("obtenerUnArticulo",$sql,1);
    // Ejecutamos y guardamos la query
    $result = mysql_query($sql, $link);
    // Por cada resultado, que sólo deberia ser uno
    if ($row = mysql_fetch_array($result)) {
        do {
            $articulo->idart     = $row["idart"];
            $articulo->fecha     = $row["fecha"];
            $articulo->iduser    = $row["iduser"];
            $articulo->titulo    = $row["titulo"];
            $articulo->contenido = $row["contenido"];
            $articulo->idtag     = $row["idtag"];
            $articulo->conta     = $row["conta"];
            $articulo->fechasi   = $row["fechasi"];
            $articulo->fechano   = $row["fechano"];
            $articulo->imagen    = $row["imagen"];
            
        } while ($row = mysql_fetch_array($result));
    }
    // Cerramos conexion con la base de datos
    mysql_close($link);
    // Devolvemos el objeto temporada
    return $articulo;
}

//
//
//
function modificarArticulo($articulo,$idart) {
    // Conexión con la base de datos
    $link=Conectarse();
    // Construimos la query
    $sql = "UPDATE `CMS_articulos` SET `fecha` = '" . $articulo->fecha . "' ,`iduser` = '" . $articulo->iduser . "' ,`titulo` = '" . $articulo->titulo . "' ,`contenido` = '" . $articulo->contenido . "' ,`idtag` = '" . $articulo->idtag . "' ,`conta` = '" . $articulo->conta . "' ,`imagen` = '" . $articulo->imagen . "' ,`fechasi` = '" . $articulo->fechasi . "' ,`fechano` = '" . $articulo->fechano . "' WHERE `idart` = '" . $idart . "';";
    // Guardamos el log
    wlog("modificarArticulo",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexión con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

////////////
//
// Cronicas
//
////////////

//
//
//
function obtenerCronica() {
    // Inicializo variable
    $cronica = null;
    // Para cada fila de la tabla inicializo una temporada
    for ($i=0;$i<numRows("CMS_cronicas");$i++) {
        $cronica[$i] = new cronica();
    }
    // Pongo i a cero
    $i = 0;
    // Obtengo conexión a la base de datos
    $link=Conectarse();
    // Construyo la query
    $sql = "select * from `CMS_cronicas` order by idcro desc";
    // Guardamos el log
    wlog("obtenerCronica", $sql, 1);
    // Obtengo el resultado
    $result = mysql_query($sql, $link);
    // Para cada fila, guardo las columnas en el objeto
    if ($row = @mysql_fetch_array($result)) {
        do {
            $cronica[$i]->idcro     = $row["idcro"];
			$cronica[$i]->idteam    = $row["idteam"];
			$cronica[$i]->idcat     = $row["idcat"];
            $cronica[$i]->fecha     = $row["fecha"];
            $cronica[$i]->iduser    = $row["iduser"];
            $cronica[$i]->titulo    = $row["titulo"];
            $cronica[$i]->contenido = $row["contenido"];
            $cronica[$i]->idtag     = $row["idtag"];
            $cronica[$i]->conta     = $row["conta"];
            $cronica[$i]->fechasi   = $row["fechasi"];
            $cronica[$i]->fechano   = $row["fechano"];            
            $cronica[$i]->imagen    = $row["imagen"];

            $i++;

        } while ($row = mysql_fetch_array($result));
    }
    // Cierro la conexión con la base de datos
    mysql_close($link);
    // Devuelvo el objeto que contiene los resultados de la búsqueda
    return $cronica;
}

//
//
//
function obtenerCronicaPorEquipo($idteam) {
    // Inicializo variable
    $cronica = null;
    // Para cada fila de la tabla inicializo una temporada
    for ($i=0;$i<numRowsCronicasPorEquipo($idteam);$i++) {
        $cronica[$i] = new cronica();
    }
    // Pongo i a cero
    $i = 0;
    // Obtengo conexión a la base de datos
    $link=Conectarse();
    // Construyo la query
    $sql = "select * from `CMS_cronicas` where idteam='" . $idteam . "' order by idcro desc ";
    // Guardamos el log
    wlog("obtenerCronica", $sql, 1);
    // Obtengo el resultado
    $result = mysql_query($sql, $link);
    // Para cada fila, guardo las columnas en el objeto
    if ($row = @mysql_fetch_array($result)) {
        do {
            $cronica[$i]->idcro     = $row["idcro"];
			$cronica[$i]->idteam    = $row["idteam"];
			$cronica[$i]->idcat     = $row["idcat"];
            $cronica[$i]->fecha     = $row["fecha"];
            $cronica[$i]->iduser    = $row["iduser"];
            $cronica[$i]->titulo    = $row["titulo"];
            $cronica[$i]->contenido = $row["contenido"];
            $cronica[$i]->idtag     = $row["idtag"];
            $cronica[$i]->conta     = $row["conta"];
            $cronica[$i]->fechasi   = $row["fechasi"];
            $cronica[$i]->fechano   = $row["fechano"];            
            $cronica[$i]->imagen    = $row["imagen"];

            $i++;

        } while ($row = mysql_fetch_array($result));
    }
    // Cierro la conexión con la base de datos
    mysql_close($link);
    // Devuelvo el objeto que contiene los resultados de la búsqueda
    return $cronica;
}

//
//
//
function numRowsCronicasPorEquipo($idteam) {
    $link=Conectarse();
	$sql = "select * from `CMS_cronicas` WHERE `idteam` = '" . $idteam . "' order by idcro";
    $result = mysql_query($sql, $link);
    $num_rows = mysql_num_rows($result);
    mysql_close($link);
    return $num_rows;
}

//
//
//
function guardarCronica($cronica) {
    // Inicializamos la variable numero
    $numero = 0;
	$numeroC = 0;
    // Conexión con la base de datos
    $link=Conectarse();
    
	// Construcción de la query
    $sql = "select * from `CMS_articulos` order by idart";
    // Registro de log
    wlog("guardarArticulo",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Obtenemos el ultimo idtemp
    if ($row = mysql_fetch_array($result)) {
        do {
            $numero = $row["idart"];
        } while ($row = mysql_fetch_array($result));
    }
	
	// Construcción de la query
    $sql = "select * from `CMS_cronicas` order by idcro";
    // Registro de log
    wlog("guardarArticulo",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Obtenemos el ultimo idtemp
    if ($row = mysql_fetch_array($result)) {
        do {
            $numeroC = $row["idcro"];
        } while ($row = mysql_fetch_array($result));
    }
    
	// nos quedamos con el identificador mayor entre el de los articulos y el de las cronicas
	if($numeroC>$numero) $numero = $numeroC;
	
    // Sumamos uno al ultimo idtemp
    $numero++;
    // Construimos la nueva query para guardar los datos de la nueva temporada
    $sql = "INSERT INTO `CMS_cronicas` (`idcro`,`idteam`,`idcat`,`fecha`,`iduser`,`titulo`,`contenido`,`idtag`,`conta`,`fechasi`,`fechano`,`imagen`) VALUES ('" . $numero . "' , '" . $cronica->idteam . "' , '" . $cronica->idcat . "' , '" . $cronica->fecha . "' , '" . $cronica->iduser . "' , '" . $cronica->titulo . "' , '" . $cronica->contenido . "' , '" . $cronica->idtag . "' , '" . $cronica->conta . "' , '" . $cronica->fechasi . "' , '" . $cronica->fechano . "' , '" . $cronica->imagen . "');";
    // Guardamos el log
    wlog("guardarcronica",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexion con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

//
//
//
function borrarCronica($idcro) {
    // Obtenemos el usuario
    $cronica = obtenerUnaCronica($idcro);
	// si hemos encontrado registro
	if($cronica!=null) {
		// Inicializo variable
		$result = false;
		// Conexión con la base de datos
		$link=Conectarse();
		$sql = "delete from `CMS_cronicas` where idcro=" . $idcro;
		// Guardamos el log
		wlog("borrarCronica",$sql,1);
		// Ejecutamos la query y obtenemos el resultado
		$result = mysql_query($sql,$link);
		// Cerramos conexión con la base de datos
		mysql_close($link);
		// Borramos el fichero del disco duro
		if($cronica->imagen!="") unlink($cronica->imagen);
	}
    // Devolvemos el resultado de la ejecución
    if($result==true) return true;
    else return false;
}

//
//
//
function obtenerUnaCronica($idcro) {
    // Conexion con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_cronicas` where idcro='" . $idcro . "'";
    // Guardamos el log
    wlog("obtenerUnaCronica",$sql,1);
    // Ejecutamos y guardamos la query
    $result = mysql_query($sql, $link);
    // Por cada resultado, que sólo deberia ser uno
    if ($row = mysql_fetch_array($result)) {
        do {
            $cronica->idcro     = $row["idcro"];
			$cronica->idteam    = $row["idteam"];
			$cronica->idcat     = $row["idcat"];
            $cronica->fecha     = $row["fecha"];
            $cronica->iduser    = $row["iduser"];
            $cronica->titulo    = $row["titulo"];
            $cronica->contenido = $row["contenido"];
            $cronica->idtag     = $row["idtag"];
            $cronica->conta     = $row["conta"];
            $cronica->fechasi   = $row["fechasi"];
            $cronica->fechano   = $row["fechano"];
            $cronica->imagen    = $row["imagen"];
            
        } while ($row = mysql_fetch_array($result));
    }
    // Cerramos conexion con la base de datos
    mysql_close($link);
    // Devolvemos el objeto temporada
    return $cronica;
}

//
//
//
function modificarCronica($cronica,$idcro) {
    // Conexión con la base de datos
    $link=Conectarse();
    // Construimos la query
    $sql = "UPDATE `CMS_cronicas` SET `idteam` = '" . $cronica->idteam . "' ,`idcat` = '" . $cronica->idcat . "' ,`fecha` = '" . $cronica->fecha . "' ,`iduser` = '" . $cronica->iduser . "' ,`titulo` = '" . $cronica->titulo . "' ,`contenido` = '" . $cronica->contenido . "' ,`idtag` = '" . $cronica->idtag . "' ,`conta` = '" . $cronica->conta . "' ,`imagen` = '" . $cronica->imagen . "' ,`fechasi` = '" . $cronica->fechasi . "' ,`fechano` = '" . $cronica->fechano . "' WHERE `idcro` = '" . $idcro . "';";
    // Guardamos el log
    wlog("modificarCronica",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexión con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

/////////////////
//
// Comentarios
//
/////////////////

//
//
//
function obtenerComentario() {
    // Inicializo variable
    $comentario = null;
    // Para cada fila de la tabla inicializo una temporada
    for ($i=0;$i<numRows("CMS_comentarios");$i++) {
        $comentario[$i] = new comentario();
    }
    // Pongo i a cero
    $i = 0;
    // Obtengo conexión a la base de datos
    $link=Conectarse();
    // Construyo la query
    $sql = "select * from `CMS_comentarios` order by idcom desc";
    // Guardamos el log
    wlog("obtenerComentario", $sql, 1);
    // Obtengo el resultado
    $result = mysql_query($sql, $link);
    // Para cada fila, guardo las columnas en el objeto
    if ($row = @mysql_fetch_array($result)) {
        do {
            $comentario[$i]->idcom     = $row["idcom"];
            $comentario[$i]->fecha     = $row["fecha"];
            $comentario[$i]->iduser    = $row["iduser"];
            $comentario[$i]->contenido = $row["contenido"];
            $comentario[$i]->idart     = $row["idart"];
            $comentario[$i]->conta     = $row["conta"];

            $i++;

        } while ($row = mysql_fetch_array($result));
    }
    // Cierro la conexión con la base de datos
    mysql_close($link);
    // Devuelvo el objeto que contiene los resultados de la búsqueda
    return $comentario;
}

//
//
//
function guardarComentario($comentario) {
    // Inicializamos la variable numero
    $numero = 0;
    // Conexión con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_comentarios` order by idcom";
    // Registro de log
    wlog("guardarComentario",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Obtenemos el ultimo idtemp
    if ($row = mysql_fetch_array($result)) {
        do {
            $numero = $row["idcom"];
        } while ($row = mysql_fetch_array($result));
    }
    // Sumamos uno al ultimo idtemp
    $numero++;
    // Construimos la nueva query para guardar los datos de la nueva temporada
    $sql = "INSERT INTO `CMS_comentarios` (`idcom`,`fecha`,`iduser`,`contenido`,`idart`,`conta`) VALUES ('" . $numero . "' , '" . $comentario->fecha . "' , '" . $comentario->iduser . "' , '" . $comentario->contenido . "' , '" . $comentario->idart . "' , '" . $comentario->conta . "');";
    // Guardamos el log
    wlog("guardarComentario",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexion con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

//
//
//
function borrarComentario($idcom) {
    // Inicializo variable
    $result = false;
    // Conexión con la base de datos
    $link=Conectarse();
    $sql = "delete from `CMS_comentarios` where idcom=" . $idcom;
    // Guardamos el log
    wlog("borrarComentario",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql,$link);
    // Cerramos conexión con la base de datos
    mysql_close($link);
    // Devolvemos el resultado de la ejecución
    if($result==true) return true;
    else return false;
}

//
//
//
function obtenerUnComentario($idcom) {
    // Conexion con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_comentarios` where idcom='" . $idcom . "'";
    // GUardamos el log
    wlog("obtenerUnComentario",$sql,1);
    // Ejecutamos y guardamos la query
    $result = mysql_query($sql, $link);
    // Por cada resultado, que sólo deberia ser uno
    if ($row = mysql_fetch_array($result)) {
        do {
            $comentario->idart     = $row["idart"];
            $comentario->fecha     = $row["fecha"];
            $comentario->iduser    = $row["iduser"];
            $comentario->contenido = $row["contenido"];
            $comentario->idcom     = $row["idcom"];
            $comentario->conta     = $row["conta"];
        } while ($row = mysql_fetch_array($result));
    }
    // Cerramos conexion con la base de datos
    mysql_close($link);
    // Devolvemos el objeto temporada
    return $comentario;
}

//
//
//
function modificarComentario($comentario,$idcom) {
    // Conexión con la base de datos
    $link=Conectarse();
    // Construimos la query
    $sql = "UPDATE `CMS_comentarios` SET `fecha` = '" . $comentario->fecha . "' ,`iduser` = '" . $comentario->iduser . "' ,`contenido` = '" . $comentario->contenido . "' ,`idart` = '" . $comentario->idart . "' ,`conta` = '" . $comentario->conta . "' WHERE `idcom` = '" . $idcom . "';";
    // Guardamos el log
    wlog("modificarComentario",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexión con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

/////////////////
//
// 09torneos
//
/////////////////

//
//
//
function obtenerTorneo() {
    // Inicializo variable
    $tag = null;
	$torneo = null;
    // Para cada fila de la tabla inicializo una temporada
    for ($i=0;$i<numRows("CMS_torneos");$i++) {
        $torneo[$i] = new torneo();
    }
    // Pongo i a cero
    $i = 0;
    // Obtengo conexión a la base de datos
    $link=Conectarse();
    // Construyo la query
    $sql = "select * from `CMS_torneos` order by idtorneo";
    // Guardamos el log
    //wlog("obtenerTorneo", $sql, 1);
    // Obtengo el resultado
    $result = mysql_query($sql, $link);
    // Para cada fila, guardo las columnas en el objeto
    if ($row = @mysql_fetch_array($result)) {
        do {
            $torneo[$i]->idtorneo    = $row["idtorneo"];
            $torneo[$i]->idformu     = $row["idformu"];
            $torneo[$i]->name        = $row["name"];
			$torneo[$i]->cartel      = $row["cartel"];
            $torneo[$i]->fecha       = $row["fecha"];
            $torneo[$i]->hour        = $row["hour"];
            $torneo[$i]->location    = $row["location"];
			$torneo[$i]->gmaps       = $row["gmaps"];
            $torneo[$i]->description = $row["description"];
            $torneo[$i]->info        = $row["info"];
			$torneo[$i]->cuenta      = $row["cuenta"];
            $torneo[$i]->titular     = $row["titular"];
            $torneo[$i]->precio      = $row["precio"];
			$torneo[$i]->conta       = $row["conta"];
            $i++;

        } while ($row = mysql_fetch_array($result));
    }
    // Cierro la conexión con la base de datos
    mysql_close($link);
    // Devuelvo el objeto que contiene los resultados de la búsqueda
    return $torneo;
}

//
//
//
function guardarTorneo($torneo) {
    // Inicializamos la variable numero
    $numero = 0;
    // Conexión con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_torneos` order by idtorneo";
    // Registro de log
    //wlog("guardarTorneo",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Obtenemos el ultimo idtemp
    if ($row = mysql_fetch_array($result)) {
        do {
            $numero = $row["idtorneo"];
        } while ($row = mysql_fetch_array($result));
    }
    // Sumamos uno al ultimo idtemp
    $numero++;
    // Construimos la nueva query para guardar los datos de la nueva temporada
    $sql = "INSERT INTO `CMS_torneos` (`idtorneo`,`idformu`,`name`,`cartel`,`fecha`,`hour`,`location`,`gmaps`,`description`,`info`,`cuenta`,`titular`,`precio`,`conta`) VALUES ('" . $numero . "' , '" . $torneo->idformu . "' , '" . $torneo->name . "' , '" . $torneo->cartel . "' , '" . $torneo->fecha . "' , '" . $torneo->hour . "' , '" . $torneo->location . "' , '" . $torneo->gmaps . "' , '" . $torneo->description . "' , '" . $torneo->info . "' , '" . $torneo->cuenta . "' , '" . $torneo->titular . "' , '" . $torneo->precio . "' , '" . $torneo->conta . "');";
    // Guardamos el log
    //wlog("guardarFormulario",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexion con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

//
//
//
function borrarTorneo($idtorneo) {
	$torneo = obtenerUnTorneo($idtorneo);												// Obtenemos toda la informacion del torneo por su id
    $result = false;																	// Inicializo variable
    $link=Conectarse();																	// Conexión con la base de datos
    $sql = "delete from `CMS_torneos` where idtorneo=" . $idtorneo;						// Construimos la query
    wlog("borrarTorneo",$sql,1);														// Guardamos el log
    $result = mysql_query($sql,$link);													// Ejecutamos la query y obtenemos el resultado
    mysql_close($link);																	// Cerramos conexión con la base de datos
	if($torneo->cartel!="") {															// Borramos el fichero del disco duro si no esta vacio
		$fichero = str_replace("admin/","",$torneo->cartel);							// Acomodamos la ruta del fichero para poder borrarlo
		if(file_exists($fichero)) unlink($fichero);										// Borramos el fichero del disco duro si existe
	}
	if($result==true) return true;														// Devolvemos el resultado de la ejecución
    else return false;
}

//
//
//
function obtenerUnTorneo($idtorneo) {
    // Conexion con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_torneos` where idtorneo='" . $idtorneo . "'";
	// GUardamos el log
    //wlog("obtenerUnTorneo",$sql,1);
    // Ejecutamos y guardamos la query
    $result = mysql_query($sql, $link);
    // Por cada resultado, que sólo deberia ser uno
    if ($row = mysql_fetch_array($result)) {
        do {
            $form->idtorneo    = $row["idtorneo"];
            $form->idformu     = $row["idformu"];
            $form->name        = $row["name"];
            $form->cartel      = $row["cartel"];
            $form->fecha       = $row["fecha"];
            $form->hour        = $row["hour"];
			$form->location    = $row["location"];
            $form->gmaps       = $row["gmaps"];
            $form->description = $row["description"];
			$form->info        = $row["info"];
            $form->cuenta      = $row["cuenta"];
            $form->titular     = $row["titular"];
			$form->precio      = $row["precio"];
            $form->conta       = $row["conta"];
            
        } while ($row = mysql_fetch_array($result));
    }
    // Cerramos conexion con la base de datos
    mysql_close($link);
    // Devolvemos el objeto temporada
    return $form;
}

//
//
//
function modificarTorneo($torneo) {
    // Conexión con la base de datos
    $link=Conectarse();
    // Construimos la query
    $sql = "UPDATE `CMS_torneos` SET `idformu` = '" . $torneo->idformu . "' ,`name` = '" . $torneo->name . "' ,`cartel` = '" . $torneo->cartel . "' ,`fecha` = '" . $torneo->fecha . "' ,`hour` = '" . $torneo->hour . "' ,`location` = '" . $torneo->location . "' ,`gmaps` = '" . $torneo->gmaps . "' ,`description` = '" . $torneo->description . "' ,`info` = '" . $torneo->info . "' ,`cuenta` = '" . $torneo->cuenta . "' ,`titular` = '" . $torneo->titular . "' ,`precio` = '" . $torneo->precio . "' ,`conta` = '" . $torneo->conta . "' WHERE `idtorneo` = '" . $torneo->idtorneo . "';";
    // Guardamos el log
    wlog("modificarTorneo",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexión con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

//
//
//
function obtenerSiguienteTorneo() {
    // Inicializo variable
    $tag = null;
	$torneo = null;
    // Para cada fila de la tabla inicializo una temporada
    for ($i=0;$i<numRows("CMS_torneos");$i++) {
        $torneo[$i] = new torneo();
    }
    // Pongo i a cero
    $i = 0;
    // Obtengo conexión a la base de datos
    $link=Conectarse();
    // Construyo la query
    $sql = "select * from `CMS_torneos` where conta <> 'si' order by idtorneo";
    // Guardamos el log
    wlog("obtenerTorneo", $sql, 1);
    // Obtengo el resultado
    $result = mysql_query($sql, $link);
    // Para cada fila, guardo las columnas en el objeto
    if ($row = @mysql_fetch_array($result)) {
        do {
            $torneo[$i]->idtorneo    = $row["idtorneo"];
            $torneo[$i]->idformu     = $row["idformu"];
            $torneo[$i]->name        = $row["name"];
			$torneo[$i]->cartel      = $row["cartel"];
            $torneo[$i]->fecha       = $row["fecha"];
            $torneo[$i]->hour        = $row["hour"];
            $torneo[$i]->location    = $row["location"];
			$torneo[$i]->gmaps       = $row["gmaps"];
            $torneo[$i]->description = $row["description"];
            $torneo[$i]->info        = $row["info"];
			$torneo[$i]->cuenta      = $row["cuenta"];
            $torneo[$i]->titular     = $row["titular"];
            $torneo[$i]->precio      = $row["precio"];
			$torneo[$i]->conta       = $row["conta"];
            $i++;

        } while ($row = mysql_fetch_array($result));
    }
    // Cierro la conexión con la base de datos
    mysql_close($link);
    // Devuelvo el objeto que contiene los resultados de la búsqueda
    return $torneo;
}

/////////////////
//
// 10formularios
//
/////////////////

//
//
//
function obtenerFormulario() {
    // Inicializo variable
    $tag = null;
	$form = null;
    // Para cada fila de la tabla inicializo una temporada
    for ($i=0;$i<numRows("CMS_formularios");$i++) {
        $form[$i] = new formulario();
    }
    // Pongo i a cero
    $i = 0;
    // Obtengo conexión a la base de datos
    $link=Conectarse();
    // Construyo la query
    $sql = "select * from `CMS_formularios` order by idformu desc";
    // Guardamos el log
    wlog("obtenerFormulario", $sql, 1);
    // Obtengo el resultado
    $result = mysql_query($sql, $link);
    // Para cada fila, guardo las columnas en el objeto
    if ($row = @mysql_fetch_array($result)) {
        do {
            $form[$i]->idformu  = $row["idformu"];
            $form[$i]->formu    = $row["formu"];
            $form[$i]->mail     = $row["mail"];
            $form[$i]->name     = $row["name"];
            $form[$i]->fecha    = $row["fecha"];
            $form[$i]->precio   = $row["precio"];
			$form[$i]->teamname = $row["teamname"];
            $form[$i]->categ    = $row["categ"];
            $form[$i]->capname  = $row["capname"];
			$form[$i]->captel   = $row["captel"];
            $form[$i]->capmail  = $row["capmail"];
            $form[$i]->n01name  = $row["n01name"];
			$form[$i]->n01tel   = $row["n01tel"];
            $form[$i]->n01mail  = $row["n01mail"];
            $form[$i]->n02name  = $row["n02name"];
			$form[$i]->n02tel   = $row["n02tel"];
            $form[$i]->n02mail  = $row["n02mail"];
            $form[$i]->n03name  = $row["n03name"];
			$form[$i]->n03tel   = $row["n03tel"];
            $form[$i]->n03mail  = $row["n03mail"];
            $form[$i]->n04name  = $row["n04name"];
			$form[$i]->n04tel   = $row["n04tel"];
            $form[$i]->n04mail  = $row["n04mail"];
            $form[$i]->n05name  = $row["n05name"];
			$form[$i]->n05tel   = $row["n05tel"];
            $form[$i]->n05mail  = $row["n05mail"];
            $i++;

        } while ($row = mysql_fetch_array($result));
    }
    // Cierro la conexión con la base de datos
    mysql_close($link);
    // Devuelvo el objeto que contiene los resultados de la búsqueda
    return $form;
}

//
//
//
function guardarFormulario($form) {
    // Inicializamos la variable numero
    $numero = 0;
    // Conexión con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_formularios` order by idformu";
    // Registro de log
    wlog("guardarFormulario",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Obtenemos el ultimo idtemp
    if ($row = mysql_fetch_array($result)) {
        do {
            $numero = $row["idformu"];
        } while ($row = mysql_fetch_array($result));
    }
    // Sumamos uno al ultimo idtemp
    $numero++;
    // Construimos la nueva query para guardar los datos de la nueva temporada
    $sql = "INSERT INTO `CMS_formularios` (`idformu`,`formu`,`mail`,`name`,`fecha`,`precio`,`teamname`,`categ`,`capname`,`captel`,`capmail`,`n01name`,`n01tel`,`n01mail`,`n02name`,`n02tel`,`n02mail`,`n03name`,`n03tel`,`n03mail`,`n04name`,`n04tel`,`n04mail`,`n05name`,`n05tel`,`n05mail`) VALUES ('" . $numero . "' , '" . $form->formu . "' , '" . $form->mail . "' , '" . $form->name . "' , '" . $form->fecha . "' , '" . $form->precio . "' , '" . $form->teamname . "' , '" . $form->categ . "' , '" . $form->capname . "' , '" . $form->captel . "' , '" . $form->capmail . "' , '" . $form->n01name . "' , '" . $form->n01tel . "' , '" . $form->n01mail . "' , '" . $form->n02name . "' , '" . $form->n02tel . "' , '" . $form->n02mail . "' , '" . $form->n03name . "' , '" . $form->n03tel . "' , '" . $form->n03mail . "' , '" . $form->n04name . "' , '" . $form->n04tel . "' , '" . $form->n04mail . "' , '" . $form->n05name . "' , '" . $form->n05tel . "' , '" . $form->n05mail . "');";
    // Guardamos el log
    wlog("guardarFormulario",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexion con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

//
//
//
function borrarFormulario($idformu) {
    // Inicializo variable
    $result = false;
    // Conexión con la base de datos
    $link=Conectarse();
    $sql = "delete from `CMS_formularios` where idformu=" . $idformu;
    // Guardamos el log
    wlog("borrarFormulario",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql,$link);
    // Cerramos conexión con la base de datos
    mysql_close($link);
    // Devolvemos el resultado de la ejecución
    if($result==true) return true;
    else return false;
}

//
//
//
function obtenerUnFormulario($idformu) {
    // Conexion con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_formularios` where idformu='" . $idformu . "'";
    // GUardamos el log
    wlog("obtenerUnFormulario",$sql,1);
    // Ejecutamos y guardamos la query
    $result = mysql_query($sql, $link);
    // Por cada resultado, que sólo deberia ser uno
    if ($row = mysql_fetch_array($result)) {
        do {
            $form->idformu  = $row["idformu"];
            $form->formu    = $row["formu"];
            $form->mail     = $row["mail"];
            $form->name     = $row["name"];
            $form->fecha    = $row["fecha"];
            $form->precio   = $row["precio"];
			$form->teamname = $row["teamname"];
            $form->categ    = $row["categ"];
            $form->capname  = $row["capname"];
			$form->captel   = $row["captel"];
            $form->capmail  = $row["capmail"];
            $form->n01name  = $row["n01name"];
			$form->n01tel   = $row["n01tel"];
            $form->n01mail  = $row["n01mail"];
            $form->n02name  = $row["n02name"];
			$form->n02tel   = $row["n02tel"];
            $form->n02mail  = $row["n02mail"];
            $form->n03name  = $row["n03name"];
			$form->n03tel   = $row["n03tel"];
            $form->n03mail  = $row["n03mail"];
            $form->n04name  = $row["n04name"];
			$form->n04tel   = $row["n04tel"];
            $form->n04mail  = $row["n04mail"];
            $form->n05name  = $row["n05name"];
			$form->n05tel   = $row["n05tel"];
            $form->n05mail  = $row["n05mail"];
            
        } while ($row = mysql_fetch_array($result));
    }
    // Cerramos conexion con la base de datos
    mysql_close($link);
    // Devolvemos el objeto temporada
    return $form;
}

//
//
//
function modificarFormulario($form) {
    // Conexión con la base de datos
    $link=Conectarse();
    // Construimos la query
    $sql = "UPDATE `CMS_formularios` SET `formu` = '" . $form->formu . "' ,`mail` = '" . $form->mail . "' ,`name` = '" . $form->name . "' ,`fecha` = '" . $form->fecha . "' ,`precio` = '" . $form->precio . "' ,`teamname` = '" . $form->teamname . "' ,`categ` = '" . $form->categ . "' ,`capname` = '" . $form->capname . "' ,`captel` = '" . $form->captel . "' ,`capmail` = '" . $form->capmail . "' ,`n01name` = '" . $form->n01name . "' ,`n01tel` = '" . $form->n01tel . "' ,`n01mail` = '" . $form->n01mail . "' ,`n02name` = '" . $form->n02name . "' ,`n02tel` = '" . $form->n02tel . "' ,`n02mail` = '" . $form->n02mail . "' ,`n03name` = '" . $form->n03name . "' ,`n03tel` = '" . $form->n03tel . "' ,`n03mail` = '" . $form->n03mail . "' ,`n04name` = '" . $form->n04name . "' ,`n04tel` = '" . $form->n04tel . "' ,`n04mail` = '" . $form->n04mail . "' ,`n05name` = '" . $form->n05name . "' ,`n05tel` = '" . $form->n05tel . "' ,`n05mail` = '" . $form->n05mail . "' WHERE `idformu` = '" . $form->idformu . "';";
    // Guardamos el log
    wlog("modificarTags",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexión con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

/////////////////
//
// 06equipos
//
/////////////////

//
//
//
function obtenerEquipo() {
    // Inicializo variable
    $instalacion = null;
	$team = null;
    // Para cada fila de la tabla inicializo una temporada
    for ($i=0;$i<numRows("CMS_equipos");$i++) {
        $team[$i] = new equipo();
    }
    // Pongo i a cero
    $i = 0;
    // Obtengo conexión a la base de datos
    $link=Conectarse();
    // Construyo la query
    $sql = "select * from `CMS_equipos` order by idteam desc";
    // Guardamos el log
    wlog("obtenerEquipo", $sql, 1);
    // Obtengo el resultado
    $result = mysql_query($sql, $link);
    // Para cada fila, guardo las columnas en el objeto
    if ($row = @mysql_fetch_array($result)) {
        do {
            $team[$i]->idteam    = $row["idteam"];
            $team[$i]->idcat     = $row["idcat"];
            $team[$i]->nombre    = $row["nombre"];
            $team[$i]->mister    = $row["mister"];
            $team[$i]->foto      = $row["foto"];
            $team[$i]->conta     = $row["conta"];
            $i++;

        } while ($row = mysql_fetch_array($result));
    }
    // Cierro la conexión con la base de datos
    mysql_close($link);
    // Devuelvo el objeto que contiene los resultados de la búsqueda
    return $team;
}

//
//
//
function guardarEquipo($team) {
    // Inicializamos la variable numero
    $numero = 0;
    // Conexión con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_equipos` order by idteam";
    // Registro de log
    wlog("guardarEquipo",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Obtenemos el ultimo idtemp
    if ($row = mysql_fetch_array($result)) {
        do {
            $numero = $row["idteam"];
        } while ($row = mysql_fetch_array($result));
    }
    // Sumamos uno al ultimo idtemp
    $numero++;
    // Construimos la nueva query para guardar los datos de la nueva temporada
    $sql = "INSERT INTO `CMS_equipos` (`idteam`,`idcat`,`nombre`,`mister`,`foto`,`conta`) VALUES ('" . $numero . "' , '" . $team->idcat . "' , '" . $team->nombre . "' , '" . $team->mister . "' , '" . $team->foto . "' , '" . $team->conta . "');";
    // Guardamos el log
    wlog("guardarEquipo",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexion con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

//
//
//
function borrarEquipo($idteam) {
    $team = obtenerUnEquipo($idteam);													// Obtener datos completos de instalacion
    $result = false;																	// Inicializo variable
    $link=Conectarse();																	// Conexión con la base de datos
    $sql = "delete from `CMS_equipos` where idteam=" . $idteam;							// Construimos la query
    wlog("borrarEquipo",$sql,1);														// Guardamos el log
    $result = mysql_query($sql,$link);													// Ejecutamos la query y obtenemos el resultado
    mysql_close($link);																	// Cerramos conexión con la base de datos
    if($team->foto!="") {																// Borramos el fichero del disco duro si no esta vacio
		$fichero = str_replace("admin/","",$team->foto);								// Acomodamos la ruta del fichero para poder borrarlo
		if(file_exists($fichero)) unlink($fichero);										// Borramos el fichero del disco duro si existe
	}
	if($result==true) return true;														// Devolvemos el resultado de la ejecución
    else return false;
}

//
//
//
function obtenerUnEquipo($idteam) {
    // Conexion con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_equipos` where idteam='" . $idteam . "'";
    // GUardamos el log
    wlog("obtenerUnEquipo",$sql,1);
    // Ejecutamos y guardamos la query
    $result = mysql_query($sql, $link);
    // Por cada resultado, que sólo deberia ser uno
    if ($row = mysql_fetch_array($result)) {
        do {
            $team->idteam    = $row["idteam"];
            $team->idcat     = $row["idcat"];
            $team->nombre    = $row["nombre"];
            $team->mister    = $row["mister"];
            $team->foto      = $row["foto"];
            $team->conta     = $row["conta"];
            
        } while ($row = mysql_fetch_array($result));
    }
    // Cerramos conexion con la base de datos
    mysql_close($link);
    // Devolvemos el objeto temporada
    return $team;
}

//
//
//
function obtenerUnEquipoByCat($idcat) {
    // Conexion con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_equipos` where idcat='" . $idcat . "'";
    // GUardamos el log
    wlog("obtenerUnEquipoByCat",$sql,1);
    // Ejecutamos y guardamos la query
    $result = mysql_query($sql, $link);
    // Por cada resultado, que sólo deberia ser uno
    if ($row = mysql_fetch_array($result)) {
        do {
            $team->idteam    = $row["idteam"];
            $team->idcat     = $row["idcat"];
            $team->nombre    = $row["nombre"];
            $team->mister    = $row["mister"];
            $team->foto      = $row["foto"];
            $team->conta     = $row["conta"];
            
        } while ($row = mysql_fetch_array($result));
    }
    // Cerramos conexion con la base de datos
    mysql_close($link);
    // Devolvemos el objeto temporada
    return $team;
}

//
//
//
function obtenerUnaCateByTeam($idteam) {
    // inicializamos
	$cate = null;
	// Conexion con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_equipos` where idteam='" . $idteam . "'";
	// GUardamos el log
    wlog("obtenerUnaCateByTeam",$sql,1);
    // Ejecutamos y guardamos la query
    $result = mysql_query($sql, $link);
    // Por cada resultado, que sólo deberia ser uno
    if ($row = mysql_fetch_array($result)) {
        do {
            $cate->idteam    = $row["idteam"];
            $cate->idcat     = $row["idcat"];
            $cate->nombre    = $row["nombre"];
            $cate->mister    = $row["mister"];
            $cate->foto      = $row["foto"];
            $cate->conta     = $row["conta"];
            
        } while ($row = mysql_fetch_array($result));
    }
    // Cerramos conexion con la base de datos
    mysql_close($link);
    // Devolvemos el objeto temporada
    return $cate;
}

//
//
//
function modificarEquipo($team,$idteam) {
    // Conexión con la base de datos
    $link=Conectarse();
    // Construimos la query
    $sql = "UPDATE `CMS_equipos` SET `idcat` = '" . $team->idcat . "' ,`nombre` = '" . $team->nombre . "' ,`mister` = '" . $team->mister . "' ,`foto` = '" . $team->foto . "' ,`conta` = '" . $team->conta . "' WHERE `idteam` = '" . $idteam . "';";
    // Guardamos el log
    wlog("modificarEquipo",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexión con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

/////////////////
//
// 07personas
//
/////////////////

//
//
//
function obtenerPersona() {
    
    $person = null;																					// Inicializo variable
    for ($i=0;$i<numRows("CMS_personas");$i++) {													// Para cada fila de la tabla inicializo una temporada
        $person[$i] = new persona();
    }
    $i = 0;																							// Pongo i a cero
    $link=Conectarse();																				// Obtengo conexión a la base de datos
    $sql = "select * from `CMS_personas` order by idperson desc";									// Construyo la query
    wlog("obtenerPersona", $sql, 1);																// Guardamos el log
    $result = mysql_query($sql, $link);																// Obtengo el resultado
    if ($row = @mysql_fetch_array($result)) {														// Para cada fila, guardo las columnas en el objeto
        do {
            $person[$i]->idperson  = $row["idperson"];
            $person[$i]->idteam    = $row["idteam"];
            $person[$i]->nombre    = $row["nombre"];
            $person[$i]->surname   = $row["surname"];
            $person[$i]->fechan    = $row["fechan"];
            $person[$i]->foto      = $row["foto"];
			$person[$i]->clubyears = $row["clubyears"];
            $person[$i]->cargo     = $row["cargo"];
            $person[$i]->palmares  = $row["palmares"];
            $person[$i]->facebook  = $row["facebook"];
            $person[$i]->twitter   = $row["twitter"];
            $person[$i]->rol       = $row["rol"];
            $person[$i]->fechaalta = $row["fechaalta"];
            $person[$i]->fechabaja = $row["fechabaja"];
            $person[$i]->conta     = $row["conta"];
            $i++;
        } while ($row = mysql_fetch_array($result));
    }
    mysql_close($link);																				// Cierro la conexión con la base de datos
    return $person;																					// Devuelvo el objeto que contiene los resultados de la búsqueda
}

//
//
//
function obtenerPersonaPorEquipo($idteam) {
    
    $person = null;																					// Inicializo variable
    for ($i=0;$i<numRowsPersonasPorEquipo($idteam);$i++) {											// Para cada fila de la tabla inicializo una temporada
        $person[$i] = new persona();
    }
    $i = 0;																							// Pongo i a cero
    $link=Conectarse();																				// Obtengo conexión a la base de datos
    $sql = "select * from `CMS_personas` where idteam='" . $idteam . "'order by idperson desc";		// Construyo la query
    wlog("obtenerPersona", $sql, 1);																// Guardamos el log
    $result = mysql_query($sql, $link);																// Obtengo el resultado
    if ($row = @mysql_fetch_array($result)) {														// Para cada fila, guardo las columnas en el objeto
        do{            
            $person[$i]->idperson  = $row["idperson"];
            $person[$i]->idteam    = $row["idteam"];
            $person[$i]->nombre    = $row["nombre"];
            $person[$i]->surname   = $row["surname"];
            $person[$i]->fechan    = $row["fechan"];
            $person[$i]->foto      = $row["foto"];
			$person[$i]->clubyears = $row["clubyears"];
            $person[$i]->cargo     = $row["cargo"];
            $person[$i]->palmares  = $row["palmares"];
            $person[$i]->facebook  = $row["facebook"];
            $person[$i]->twitter   = $row["twitter"];
            $person[$i]->rol       = $row["rol"];
            $person[$i]->fechaalta = $row["fechaalta"];
            $person[$i]->fechabaja = $row["fechabaja"];
            $person[$i]->conta     = $row["conta"];
            $i++;
        } while ($row = mysql_fetch_array($result));
    }
    mysql_close($link);																				// Cierro la conexión con la base de datos
    return $person;																					// Devuelvo el objeto que contiene los resultados de la búsqueda
}

//
//
//
function numRowsPersonasPorEquipo($idteam) {
    $link=Conectarse();
	$sql = "select * from `CMS_personas` WHERE `idteam` = '" . $idteam . "' order by idperson";
    $result = mysql_query($sql, $link);
    $num_rows = mysql_num_rows($result);
    mysql_close($link);
    return $num_rows;
}

//
//
//
function guardarPersona($person) {
    
    $numero = 0;																					// Inicializamos la variable numero
    $link=Conectarse();																				// Conexión con la base de datos
    $sql = "select * from `CMS_personas` order by idperson";										// Construcción de la query
    wlog("guardarPersona",$sql,1);																	// Registro de log
    $result = mysql_query($sql, $link);																// Ejecutamos la query y obtenemos el resultado
    if ($row = mysql_fetch_array($result)) {														// Obtenemos el ultimo idtemp
        do {
            $numero = $row["idperson"];
        } while ($row = mysql_fetch_array($result));
    }
    $numero++;																						// Sumamos uno al ultimo idtemp
    // Construimos la nueva query para guardar los datos de la nueva temporada
    $sql = "INSERT INTO `CMS_personas` (`idperson`,`idteam`,`nombre`,`surname`,`fechan`,`foto`,`clubyears`,`cargo`,`palmares`,`facebook`,`twitter`,`rol`,`fechaalta`,`fechabaja`,`conta`) VALUES ('" . $numero . "' , '" . $person->idteam . "' , '" . $person->nombre . "' , '" . $person->surname . "' , '" . $person->fechan . "' , '" . $person->foto . "' , '" . $person->clubyears . "' , '" . $person->cargo . "' , '" . $person->palmares . "' , '" . $person->facebook . "' , '" . $person->twitter . "' , '" . $person->rol . "' , '" . $person->fechaalta . "' , '" . $person->fechabaja . "' , '" . $person->conta . "');";
    wlog("guardarPersona",$sql,1);																	// Guardamos el log
    $result = mysql_query($sql, $link);																// Ejecutamos la query y obtenemos el resultado
    mysql_close($link);																				// Cerramos la conexion con la base de datos
    if($result==true) return true;																	// Devolvemos el resultado
    else return false;
}

//
//
//
function borrarPersona($idperson) {
    $person = obtenerUnaPersona($idperson);															// Obtener datos completos de instalacion
    $result = false;																				// Inicializo variable
    $link=Conectarse();																				// Conexión con la base de datos
    $sql = "delete from `CMS_personas` where idperson=" . $idperson;								// Construimos la query
    wlog("borrarPersona",$sql,1);																	// Guardamos el log
    $result = mysql_query($sql,$link);																// Ejecutamos la query y obtenemos el resultado
    mysql_close($link);																				// Cerramos conexión con la base de datos
    if($person->foto!="") {																			// Borramos el fichero del disco duro si no esta vacio
		$fichero = str_replace("admin/","",$person->foto);											// Acomodamos la ruta del fichero para poder borrarlo
		if(file_exists($fichero)) unlink($fichero);													// Borramos el fichero del disco duro si existe
	}
	if($result==true) return true;																	// Devolvemos el resultado de la ejecución
    else return false;
}

//
//
//
function obtenerUnaPersona($idperson) {
    $link=Conectarse();																				// Conexion con la base de datos
    $sql = "select * from `CMS_personas` where idperson='" . $idperson . "'";						// Construcción de la query
    wlog("obtenerUnaPersona",$sql,1);																// GUardamos el log
    $result = mysql_query($sql, $link);																// Ejecutamos y guardamos la query
    if ($row = mysql_fetch_array($result))	{														// Por cada resultado, que sólo deberia ser uno
        do {
            $person->idperson  = $row["idperson"];
			$person->idteam    = $row["idteam"];
			$person->nombre    = $row["nombre"];
			$person->surname   = $row["surname"];
			$person->foto      = $row["foto"];
			$person->fechan    = $row["fechan"];
			$person->clubyears = $row["clubyears"];
			$person->cargo     = $row["cargo"];
			$person->palmares  = $row["palmares"];
			$person->facebook  = $row["facebook"];
			$person->twitter   = $row["twitter"];
			$person->rol       = $row["rol"];
			$person->fechaalta = $row["fechaalta"];
			$person->fechabaja = $row["fechabaja"];
			$person->conta     = $row["conta"];
        } while ($row = mysql_fetch_array($result));
    }
    mysql_close($link);																				// Cerramos conexion con la base de datos
    return $person;																					// Devolvemos el objeto temporada
}

//
//
//
function modificarPersona($person,$idperson) {
    // Conexión con la base de datos
    $link=Conectarse();
    // Construimos la query
    $sql = "UPDATE `CMS_personas` SET `idteam` = '" . $person->idteam . "' ,`nombre` = '" . $person->nombre . "' ,`surname` = '" . $person->surname . "' ,`fechan` = '" . $person->fechan . "' ,`foto` = '" . $person->foto . "' ,`clubyears` = '" . $person->clubyears . "' ,`cargo` = '" . $person->cargo . "' ,`palmares` = '" . $person->palmares . "' ,`facebook` = '" . $person->facebook . "' ,`twitter` = '" . $person->twitter . "' ,`rol` = '" . $person->rol . "' ,`fechaalta` = '" . $person->fechaalta . "' ,`fechabaja` = '" . $person->fechabaja . "' ,`conta` = '" . $person->conta . "' WHERE `idperson` = '" . $idperson . "';";
    // Guardamos el log
    wlog("modificarPersona",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexión con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

/////////////////
//
// 04jornadas
//
/////////////////

//
//
//
function obtenerJornadas($idcat) {
    // Inicializamos la variable jornada
    $jornada = null;
    // Inicializo variable
    $person = null;
    // Para cada fila de la tabla inicializo una temporada
    for ($i=0;$i<numRows("CMS_jornadas");$i++) {
        $jornada[$i] = new jornada();
    }
    // Pongo i a cero
    $i = 0;
    // Obtengo conexión a la base de datos
    $link=Conectarse();
    // Construyo la query
    $sql = "select * from `CMS_jornadas` WHERE `idcat` = '" . $idcat . "' order by idjorn";
    // Guardamos el log
    wlog("obtenerJornada", $sql, 1);
    // Obtengo el resultado
    $result = mysql_query($sql, $link);
    // Para cada fila, guardo las columnas en el objeto
    if ($row = @mysql_fetch_array($result)) {
        do {
            $jornada[$i]->idcat    = $row["idcat"];
			$jornada[$i]->idjorn   = $row["idjorn"];

			$jornada[$i]->local01  = $row["local01"];
			$jornada[$i]->visit01  = $row["visit01"];
			$jornada[$i]->inst01   = $row["inst01"];
			$jornada[$i]->plocal01 = $row["plocal01"];
			$jornada[$i]->pvisit01 = $row["pvisit01"];
			$jornada[$i]->fecha01  = $row["fecha01"];
			$jornada[$i]->hour01   = $row["hour01"];
			// puntuaciones parciales
			$jornada[$i]->plocal011 = $row["plocal011"];
			$jornada[$i]->plocal012 = $row["plocal012"];
			$jornada[$i]->plocal013 = $row["plocal013"];
			$jornada[$i]->plocal014 = $row["plocal014"];
			$jornada[$i]->plocal015 = $row["plocal015"];
			$jornada[$i]->pvisit011 = $row["pvisit011"];
			$jornada[$i]->pvisit012 = $row["pvisit012"];
			$jornada[$i]->pvisit013 = $row["pvisit013"];
			$jornada[$i]->pvisit014 = $row["pvisit014"];
			$jornada[$i]->pvisit015 = $row["pvisit015"];

			$jornada[$i]->local02  = $row["local02"];
			$jornada[$i]->visit02  = $row["visit02"];
			$jornada[$i]->inst02   = $row["inst02"];
			$jornada[$i]->plocal02 = $row["plocal02"];
			$jornada[$i]->pvisit02 = $row["pvisit02"];
			$jornada[$i]->fecha02  = $row["fecha02"];
			$jornada[$i]->hour02   = $row["hour02"];
			// puntuaciones parciales
			$jornada[$i]->plocal021 = $row["plocal021"];
			$jornada[$i]->plocal022 = $row["plocal022"];
			$jornada[$i]->plocal023 = $row["plocal023"];
			$jornada[$i]->plocal024 = $row["plocal024"];
			$jornada[$i]->plocal025 = $row["plocal025"];
			$jornada[$i]->pvisit021 = $row["pvisit021"];
			$jornada[$i]->pvisit022 = $row["pvisit022"];
			$jornada[$i]->pvisit023 = $row["pvisit023"];
			$jornada[$i]->pvisit024 = $row["pvisit024"];
			$jornada[$i]->pvisit025 = $row["pvisit025"];

			$jornada[$i]->local03  = $row["local03"];
			$jornada[$i]->visit03  = $row["visit03"];
			$jornada[$i]->inst03   = $row["inst03"];
			$jornada[$i]->plocal03 = $row["plocal03"];
			$jornada[$i]->pvisit03 = $row["pvisit03"];
			$jornada[$i]->fecha03  = $row["fecha03"];
			$jornada[$i]->hour03   = $row["hour03"];
			// puntuaciones parciales
			$jornada[$i]->plocal031 = $row["plocal031"];
			$jornada[$i]->plocal032 = $row["plocal032"];
			$jornada[$i]->plocal033 = $row["plocal033"];
			$jornada[$i]->plocal034 = $row["plocal034"];
			$jornada[$i]->plocal035 = $row["plocal035"];
			$jornada[$i]->pvisit031 = $row["pvisit031"];
			$jornada[$i]->pvisit032 = $row["pvisit032"];
			$jornada[$i]->pvisit033 = $row["pvisit033"];
			$jornada[$i]->pvisit034 = $row["pvisit034"];
			$jornada[$i]->pvisit035 = $row["pvisit035"];

			$jornada[$i]->local04  = $row["local04"];
			$jornada[$i]->visit04  = $row["visit04"];
			$jornada[$i]->inst04   = $row["inst04"];
			$jornada[$i]->plocal04 = $row["plocal04"];
			$jornada[$i]->pvisit04 = $row["pvisit04"];
			$jornada[$i]->fecha04  = $row["fecha04"];
			$jornada[$i]->hour04   = $row["hour04"];
			// puntuaciones parciales
			$jornada[$i]->plocal041 = $row["plocal041"];
			$jornada[$i]->plocal042 = $row["plocal042"];
			$jornada[$i]->plocal043 = $row["plocal043"];
			$jornada[$i]->plocal044 = $row["plocal044"];
			$jornada[$i]->plocal045 = $row["plocal045"];
			$jornada[$i]->pvisit041 = $row["pvisit041"];
			$jornada[$i]->pvisit042 = $row["pvisit042"];
			$jornada[$i]->pvisit043 = $row["pvisit043"];
			$jornada[$i]->pvisit044 = $row["pvisit044"];
			$jornada[$i]->pvisit045 = $row["pvisit045"];

			$jornada[$i]->local05  = $row["local05"];
			$jornada[$i]->visit05  = $row["visit05"];
			$jornada[$i]->inst05   = $row["inst05"];
			$jornada[$i]->plocal05 = $row["plocal05"];
			$jornada[$i]->pvisit05 = $row["pvisit05"];
			$jornada[$i]->fecha05  = $row["fecha05"];
			$jornada[$i]->hour05   = $row["hour05"];
			// puntuaciones parciales
			$jornada[$i]->plocal051 = $row["plocal051"];
			$jornada[$i]->plocal052 = $row["plocal052"];
			$jornada[$i]->plocal053 = $row["plocal053"];
			$jornada[$i]->plocal054 = $row["plocal054"];
			$jornada[$i]->plocal055 = $row["plocal055"];
			$jornada[$i]->pvisit051 = $row["pvisit051"];
			$jornada[$i]->pvisit052 = $row["pvisit052"];
			$jornada[$i]->pvisit053 = $row["pvisit053"];
			$jornada[$i]->pvisit054 = $row["pvisit054"];
			$jornada[$i]->pvisit055 = $row["pvisit055"];

			$jornada[$i]->local06  = $row["local06"];
			$jornada[$i]->visit06  = $row["visit06"];
			$jornada[$i]->inst06   = $row["inst06"];
			$jornada[$i]->plocal06 = $row["plocal06"];
			$jornada[$i]->pvisit06 = $row["pvisit06"];
			$jornada[$i]->fecha06  = $row["fecha06"];
			$jornada[$i]->hour06   = $row["hour06"];
			// puntuaciones parciales
			$jornada[$i]->plocal061 = $row["plocal061"];
			$jornada[$i]->plocal062 = $row["plocal062"];
			$jornada[$i]->plocal063 = $row["plocal063"];
			$jornada[$i]->plocal064 = $row["plocal064"];
			$jornada[$i]->plocal065 = $row["plocal065"];
			$jornada[$i]->pvisit061 = $row["pvisit061"];
			$jornada[$i]->pvisit062 = $row["pvisit062"];
			$jornada[$i]->pvisit063 = $row["pvisit063"];
			$jornada[$i]->pvisit064 = $row["pvisit064"];
			$jornada[$i]->pvisit065 = $row["pvisit065"];

			$jornada[$i]->local07  = $row["local07"];
			$jornada[$i]->visit07  = $row["visit07"];
			$jornada[$i]->inst07   = $row["inst07"];
			$jornada[$i]->plocal07 = $row["plocal07"];
			$jornada[$i]->pvisit07 = $row["pvisit07"];
			$jornada[$i]->fecha07  = $row["fecha07"];
			$jornada[$i]->hour07   = $row["hour07"];
			// puntuaciones parciales
			$jornada[$i]->plocal071 = $row["plocal071"];
			$jornada[$i]->plocal072 = $row["plocal072"];
			$jornada[$i]->plocal073 = $row["plocal073"];
			$jornada[$i]->plocal074 = $row["plocal074"];
			$jornada[$i]->plocal075 = $row["plocal075"];
			$jornada[$i]->pvisit071 = $row["pvisit071"];
			$jornada[$i]->pvisit072 = $row["pvisit072"];
			$jornada[$i]->pvisit073 = $row["pvisit073"];
			$jornada[$i]->pvisit074 = $row["pvisit074"];
			$jornada[$i]->pvisit075 = $row["pvisit075"];

			$jornada[$i]->local08  = $row["local08"];
			$jornada[$i]->visit08  = $row["visit08"];
			$jornada[$i]->inst08   = $row["inst08"];
			$jornada[$i]->plocal08 = $row["plocal08"];
			$jornada[$i]->pvisit08 = $row["pvisit08"];
			$jornada[$i]->fecha08  = $row["fecha08"];
			$jornada[$i]->hour08   = $row["hour08"];
			// puntuaciones parciales
			$jornada[$i]->plocal081 = $row["plocal081"];
			$jornada[$i]->plocal082 = $row["plocal082"];
			$jornada[$i]->plocal083 = $row["plocal083"];
			$jornada[$i]->plocal084 = $row["plocal084"];
			$jornada[$i]->plocal085 = $row["plocal085"];
			$jornada[$i]->pvisit081 = $row["pvisit081"];
			$jornada[$i]->pvisit082 = $row["pvisit082"];
			$jornada[$i]->pvisit083 = $row["pvisit083"];
			$jornada[$i]->pvisit084 = $row["pvisit084"];
			$jornada[$i]->pvisit085 = $row["pvisit085"];

			$jornada[$i]->local09  = $row["local09"];
			$jornada[$i]->visit09  = $row["visit09"];
			$jornada[$i]->inst09   = $row["inst09"];
			$jornada[$i]->plocal09 = $row["plocal09"];
			$jornada[$i]->pvisit09 = $row["pvisit09"];
			$jornada[$i]->fecha09  = $row["fecha09"];
			$jornada[$i]->hour09   = $row["hour09"];
			// puntuaciones parciales
			$jornada[$i]->plocal091 = $row["plocal091"];
			$jornada[$i]->plocal092 = $row["plocal092"];
			$jornada[$i]->plocal093 = $row["plocal093"];
			$jornada[$i]->plocal094 = $row["plocal094"];
			$jornada[$i]->plocal095 = $row["plocal095"];
			$jornada[$i]->pvisit091 = $row["pvisit091"];
			$jornada[$i]->pvisit092 = $row["pvisit092"];
			$jornada[$i]->pvisit093 = $row["pvisit093"];
			$jornada[$i]->pvisit094 = $row["pvisit094"];
			$jornada[$i]->pvisit095 = $row["pvisit095"];

			$jornada[$i]->local10  = $row["local10"];
			$jornada[$i]->visit10  = $row["visit10"];
			$jornada[$i]->inst10  = $row["inst10"];
			$jornada[$i]->plocal10 = $row["plocal10"];
			$jornada[$i]->pvisit10 = $row["pvisit10"];
			$jornada[$i]->fecha10  = $row["fecha10"];
			$jornada[$i]->hour10   = $row["hour10"];
			// puntuaciones parciales
			$jornada[$i]->plocal101 = $row["plocal101"];
			$jornada[$i]->plocal102 = $row["plocal102"];
			$jornada[$i]->plocal103 = $row["plocal103"];
			$jornada[$i]->plocal104 = $row["plocal104"];
			$jornada[$i]->plocal105 = $row["plocal105"];
			$jornada[$i]->pvisit101 = $row["pvisit101"];
			$jornada[$i]->pvisit102 = $row["pvisit102"];
			$jornada[$i]->pvisit103 = $row["pvisit103"];
			$jornada[$i]->pvisit104 = $row["pvisit104"];
			$jornada[$i]->pvisit105 = $row["pvisit105"];

            $i++;

        } while ($row = mysql_fetch_array($result));
    }
    // Cierro la conexión con la base de datos
    mysql_close($link);
    // Devuelvo el objeto que contiene los resultados de la búsqueda
    return $jornada;
}

//
//
//
function obtenerUnaJornada($idcat,$idjorn) {
    // Pongo i a cero
    $i = 0;
    // Obtengo conexión a la base de datos
    $link=Conectarse();
    // Construyo la query
    $sql = "select * from `CMS_jornadas` where idcat='" . $idcat . "' and idjorn='" . $idjorn . "'";
    // Guardamos el log
    wlog("obtenerUnaJornada", $sql, 1);
    // Obtengo el resultado
    $result = mysql_query($sql, $link);
    // Para cada fila, guardo las columnas en el objeto
    if ($row = @mysql_fetch_array($result)) {
        do {            
            $jornada->idcat    = $row["idcat"];
			$jornada->idjorn   = $row["idjorn"];

			$jornada->local01  = $row["local01"];
			$jornada->visit01  = $row["visit01"];
			$jornada->inst01   = $row["inst01"];
			$jornada->plocal01 = $row["plocal01"];
			$jornada->pvisit01 = $row["pvisit01"];
			$jornada->fecha01  = $row["fecha01"];
			$jornada->hour01   = $row["hour01"];
			// puntuaciones parciales
			$jornada->plocal011 = $row["plocal011"];
			$jornada->plocal012 = $row["plocal012"];
			$jornada->plocal013 = $row["plocal013"];
			$jornada->plocal014 = $row["plocal014"];
			$jornada->plocal015 = $row["plocal015"];
			$jornada->pvisit011 = $row["pvisit011"];
			$jornada->pvisit012 = $row["pvisit012"];
			$jornada->pvisit013 = $row["pvisit013"];
			$jornada->pvisit014 = $row["pvisit014"];
			$jornada->pvisit015 = $row["pvisit015"];

			$jornada->local02  = $row["local02"];
			$jornada->visit02  = $row["visit02"];
			$jornada->inst02   = $row["inst02"];
			$jornada->plocal02 = $row["plocal02"];
			$jornada->pvisit02 = $row["pvisit02"];
			$jornada->fecha02  = $row["fecha02"];
			$jornada->hour02   = $row["hour02"];
			// puntuaciones parciales
			$jornada->plocal021 = $row["plocal021"];
			$jornada->plocal022 = $row["plocal022"];
			$jornada->plocal023 = $row["plocal023"];
			$jornada->plocal024 = $row["plocal024"];
			$jornada->plocal025 = $row["plocal025"];
			$jornada->pvisit021 = $row["pvisit021"];
			$jornada->pvisit022 = $row["pvisit022"];
			$jornada->pvisit023 = $row["pvisit023"];
			$jornada->pvisit024 = $row["pvisit024"];
			$jornada->pvisit025 = $row["pvisit025"];

			$jornada->local03  = $row["local03"];
			$jornada->visit03  = $row["visit03"];
			$jornada->inst03   = $row["inst03"];
			$jornada->plocal03 = $row["plocal03"];
			$jornada->pvisit03 = $row["pvisit03"];
			$jornada->fecha03  = $row["fecha03"];
			$jornada->hour03   = $row["hour03"];
			// puntuaciones parciales
			$jornada->plocal031 = $row["plocal031"];
			$jornada->plocal032 = $row["plocal032"];
			$jornada->plocal033 = $row["plocal033"];
			$jornada->plocal034 = $row["plocal034"];
			$jornada->plocal035 = $row["plocal035"];
			$jornada->pvisit031 = $row["pvisit031"];
			$jornada->pvisit032 = $row["pvisit032"];
			$jornada->pvisit033 = $row["pvisit033"];
			$jornada->pvisit034 = $row["pvisit034"];
			$jornada->pvisit035 = $row["pvisit035"];

			$jornada->local04  = $row["local04"];
			$jornada->visit04  = $row["visit04"];
			$jornada->inst04   = $row["inst04"];
			$jornada->plocal04 = $row["plocal04"];
			$jornada->pvisit04 = $row["pvisit04"];
			$jornada->fecha04  = $row["fecha04"];
			$jornada->hour04   = $row["hour04"];
			// puntuaciones parciales
			$jornada->plocal041 = $row["plocal041"];
			$jornada->plocal042 = $row["plocal042"];
			$jornada->plocal043 = $row["plocal043"];
			$jornada->plocal044 = $row["plocal044"];
			$jornada->plocal045 = $row["plocal045"];
			$jornada->pvisit041 = $row["pvisit041"];
			$jornada->pvisit042 = $row["pvisit042"];
			$jornada->pvisit043 = $row["pvisit043"];
			$jornada->pvisit044 = $row["pvisit044"];
			$jornada->pvisit045 = $row["pvisit045"];

			$jornada->local05  = $row["local05"];
			$jornada->visit05  = $row["visit05"];
			$jornada->inst05   = $row["inst05"];
			$jornada->plocal05 = $row["plocal05"];
			$jornada->pvisit05 = $row["pvisit05"];
			$jornada->fecha05  = $row["fecha05"];
			$jornada->hour05   = $row["hour05"];
			// puntuaciones parciales
			$jornada->plocal051 = $row["plocal051"];
			$jornada->plocal052 = $row["plocal052"];
			$jornada->plocal053 = $row["plocal053"];
			$jornada->plocal054 = $row["plocal054"];
			$jornada->plocal055 = $row["plocal055"];
			$jornada->pvisit051 = $row["pvisit051"];
			$jornada->pvisit052 = $row["pvisit052"];
			$jornada->pvisit053 = $row["pvisit053"];
			$jornada->pvisit054 = $row["pvisit054"];
			$jornada->pvisit055 = $row["pvisit055"];

			$jornada->local06  = $row["local06"];
			$jornada->visit06  = $row["visit06"];
			$jornada->inst06   = $row["inst06"];
			$jornada->plocal06 = $row["plocal06"];
			$jornada->pvisit06 = $row["pvisit06"];
			$jornada->fecha06  = $row["fecha06"];
			$jornada->hour06   = $row["hour06"];
			// puntuaciones parciales
			$jornada->plocal061 = $row["plocal061"];
			$jornada->plocal062 = $row["plocal062"];
			$jornada->plocal063 = $row["plocal063"];
			$jornada->plocal064 = $row["plocal064"];
			$jornada->plocal065 = $row["plocal065"];
			$jornada->pvisit061 = $row["pvisit061"];
			$jornada->pvisit062 = $row["pvisit062"];
			$jornada->pvisit063 = $row["pvisit063"];
			$jornada->pvisit064 = $row["pvisit064"];
			$jornada->pvisit065 = $row["pvisit065"];

			$jornada->local07  = $row["local07"];
			$jornada->visit07  = $row["visit07"];
			$jornada->inst07   = $row["inst07"];
			$jornada->plocal07 = $row["plocal07"];
			$jornada->pvisit07 = $row["pvisit07"];
			$jornada->fecha07  = $row["fecha07"];
			$jornada->hour07   = $row["hour07"];
			// puntuaciones parciales
			$jornada->plocal071 = $row["plocal071"];
			$jornada->plocal072 = $row["plocal072"];
			$jornada->plocal073 = $row["plocal073"];
			$jornada->plocal074 = $row["plocal074"];
			$jornada->plocal075 = $row["plocal075"];
			$jornada->pvisit071 = $row["pvisit071"];
			$jornada->pvisit072 = $row["pvisit072"];
			$jornada->pvisit073 = $row["pvisit073"];
			$jornada->pvisit074 = $row["pvisit074"];
			$jornada->pvisit075 = $row["pvisit075"];

			$jornada->local08  = $row["local08"];
			$jornada->visit08  = $row["visit08"];
			$jornada->inst08   = $row["inst08"];
			$jornada->plocal08 = $row["plocal08"];
			$jornada->pvisit08 = $row["pvisit08"];
			$jornada->fecha08  = $row["fecha08"];
			$jornada->hour08   = $row["hour08"];
			// puntuaciones parciales
			$jornada->plocal081 = $row["plocal081"];
			$jornada->plocal082 = $row["plocal082"];
			$jornada->plocal083 = $row["plocal083"];
			$jornada->plocal084 = $row["plocal084"];
			$jornada->plocal085 = $row["plocal085"];
			$jornada->pvisit081 = $row["pvisit081"];
			$jornada->pvisit082 = $row["pvisit082"];
			$jornada->pvisit083 = $row["pvisit083"];
			$jornada->pvisit084 = $row["pvisit084"];
			$jornada->pvisit085 = $row["pvisit085"];

			$jornada->local09  = $row["local09"];
			$jornada->visit09  = $row["visit09"];
			$jornada->inst09   = $row["inst09"];
			$jornada->plocal09 = $row["plocal09"];
			$jornada->pvisit09 = $row["pvisit09"];
			$jornada->fecha09  = $row["fecha09"];
			$jornada->hour09   = $row["hour09"];
			// puntuaciones parciales
			$jornada->plocal091 = $row["plocal091"];
			$jornada->plocal092 = $row["plocal092"];
			$jornada->plocal093 = $row["plocal093"];
			$jornada->plocal094 = $row["plocal094"];
			$jornada->plocal095 = $row["plocal095"];
			$jornada->pvisit091 = $row["pvisit091"];
			$jornada->pvisit092 = $row["pvisit092"];
			$jornada->pvisit093 = $row["pvisit093"];
			$jornada->pvisit094 = $row["pvisit094"];
			$jornada->pvisit095 = $row["pvisit095"];

			$jornada->local10  = $row["local10"];
			$jornada->visit10  = $row["visit10"];
			$jornada->inst10   = $row["inst10"];
			$jornada->plocal10 = $row["plocal10"];
			$jornada->pvisit10 = $row["pvisit10"];
			$jornada->fecha10  = $row["fecha10"];
			$jornada->hour10   = $row["hour10"];
			// puntuaciones parciales
			$jornada->plocal101 = $row["plocal101"];
			$jornada->plocal102 = $row["plocal102"];
			$jornada->plocal103 = $row["plocal103"];
			$jornada->plocal104 = $row["plocal104"];
			$jornada->plocal105 = $row["plocal105"];
			$jornada->pvisit101 = $row["pvisit101"];
			$jornada->pvisit102 = $row["pvisit102"];
			$jornada->pvisit103 = $row["pvisit103"];
			$jornada->pvisit104 = $row["pvisit104"];
			$jornada->pvisit105 = $row["pvisit105"];

            $i++;

        } while ($row = mysql_fetch_array($result));
    }
    // Cierro la conexión con la base de datos
    mysql_close($link);
    // Devuelvo el objeto que contiene los resultados de la búsqueda
    return $jornada;
}

//
//
//
function obtenerUnaJornadaDeUnEquipo($idcat,$idjorn,$equipo) {
    // Inicializamos la veriable jornada
    $jornada = null;
	// Pongo i a cero
    $i = 0;
    // Obtengo conexión a la base de datos
    $link=Conectarse();
    // Construyo la query
    $sql = "select * from `CMS_jornadas` where idcat='" . $idcat . "' and idjorn='" . $idjorn . "';";
    //echo $sql."<br>";
    // Guardamos el log
    wlog("obtenerUnaJornada", $sql, 1);
    // Obtengo el resultado
    $result = mysql_query($sql, $link);
    // Para cada fila, guardo las columnas en el objeto
    if ($row = @mysql_fetch_array($result)) {
		do {  
            if((strcasecmp(trim($row["local01"]),trim($equipo))==0)||(strcasecmp(trim($row["visit01"]),trim($equipo))==0)) {
                
				$jornada->idcat  = $row["idcat"];
				$jornada->idjorn = $row["idjorn"];

				$jornada->local  = $row["local01"];
				$jornada->visit  = $row["visit01"];
				$jornada->inst   = $row["inst01"];
				$jornada->plocal = $row["plocal01"];
				$jornada->pvisit = $row["pvisit01"];
				$jornada->fecha  = $row["fecha01"];
				$jornada->hour   = $row["hour01"];
				$jornada->plocal1 = $row["plocal011"];
				$jornada->plocal2 = $row["plocal012"];
				$jornada->plocal3 = $row["plocal013"];
				$jornada->plocal4 = $row["plocal014"];
				$jornada->plocal5 = $row["plocal015"];
				$jornada->pvisit1 = $row["pvisit011"];
				$jornada->pvisit2 = $row["pvisit012"];
				$jornada->pvisit3 = $row["pvisit013"];
				$jornada->pvisit4 = $row["pvisit014"];
				$jornada->pvisit5 = $row["pvisit015"];
			}
			
			if((strcasecmp(trim($row["local02"]),trim($equipo))==0)||(strcasecmp(trim($row["visit02"]),trim($equipo))==0)) {

                $jornada->idcat  = $row["idcat"];
				$jornada->idjorn = $row["idjorn"];

				$jornada->local  = $row["local02"];
				$jornada->visit  = $row["visit02"];
				$jornada->inst   = $row["inst02"];
				$jornada->plocal = $row["plocal02"];
				$jornada->pvisit = $row["pvisit02"];
				$jornada->fecha  = $row["fecha02"];
				$jornada->hour   = $row["hour02"];
				$jornada->plocal1 = $row["plocal021"];
				$jornada->plocal2 = $row["plocal022"];
				$jornada->plocal3 = $row["plocal023"];
				$jornada->plocal4 = $row["plocal024"];
				$jornada->plocal5 = $row["plocal025"];
				$jornada->pvisit1 = $row["pvisit021"];
				$jornada->pvisit2 = $row["pvisit022"];
				$jornada->pvisit3 = $row["pvisit023"];
				$jornada->pvisit4 = $row["pvisit024"];
				$jornada->pvisit5 = $row["pvisit025"];
			}
			
			if((strcasecmp(trim($row["local03"]),trim($equipo))==0)||(strcasecmp(trim($row["visit03"]),trim($equipo))==0)) {

                $jornada->idcat  = $row["idcat"];
				$jornada->idjorn = $row["idjorn"];

				$jornada->local  = $row["local03"];
				$jornada->visit  = $row["visit03"];
				$jornada->inst   = $row["inst03"];
				$jornada->plocal = $row["plocal03"];
				$jornada->pvisit = $row["pvisit03"];
				$jornada->fecha  = $row["fecha03"];
				$jornada->hour   = $row["hour03"];
				$jornada->plocal1 = $row["plocal031"];
				$jornada->plocal2 = $row["plocal032"];
				$jornada->plocal3 = $row["plocal033"];
				$jornada->plocal4 = $row["plocal034"];
				$jornada->plocal5 = $row["plocal035"];
				$jornada->pvisit1 = $row["pvisit031"];
				$jornada->pvisit2 = $row["pvisit032"];
				$jornada->pvisit3 = $row["pvisit033"];
				$jornada->pvisit4 = $row["pvisit034"];
				$jornada->pvisit5 = $row["pvisit035"];
			}
			
			if((strcasecmp(trim($row["local04"]),trim($equipo))==0)||(strcasecmp(trim($row["visit04"]),trim($equipo))==0)) {

                $jornada->idcat  = $row["idcat"];
				$jornada->idjorn = $row["idjorn"];

				$jornada->local  = $row["local04"];
				$jornada->visit  = $row["visit04"];
				$jornada->inst   = $row["inst04"];
				$jornada->plocal = $row["plocal04"];
				$jornada->pvisit = $row["pvisit04"];
				$jornada->fecha  = $row["fecha04"];
				$jornada->hour   = $row["hour04"];
				$jornada->plocal1 = $row["plocal041"];
				$jornada->plocal2 = $row["plocal042"];
				$jornada->plocal3 = $row["plocal043"];
				$jornada->plocal4 = $row["plocal044"];
				$jornada->plocal5 = $row["plocal045"];
				$jornada->pvisit1 = $row["pvisit041"];
				$jornada->pvisit2 = $row["pvisit042"];
				$jornada->pvisit3 = $row["pvisit043"];
				$jornada->pvisit4 = $row["pvisit044"];
				$jornada->pvisit5 = $row["pvisit045"];
			}
			
			if((strcasecmp(trim($row["local05"]),trim($equipo))==0)||(strcasecmp(trim($row["visit05"]),trim($equipo))==0)) {

                $jornada->idcat  = $row["idcat"];
				$jornada->idjorn = $row["idjorn"];

				$jornada->local  = $row["local05"];
				$jornada->visit  = $row["visit05"];
				$jornada->inst   = $row["inst05"];
				$jornada->plocal = $row["plocal05"];
				$jornada->pvisit = $row["pvisit05"];
				$jornada->fecha  = $row["fecha05"];
				$jornada->hour   = $row["hour05"];
				$jornada->plocal1 = $row["plocal051"];
				$jornada->plocal2 = $row["plocal052"];
				$jornada->plocal3 = $row["plocal053"];
				$jornada->plocal4 = $row["plocal054"];
				$jornada->plocal5 = $row["plocal055"];
				$jornada->pvisit1 = $row["pvisit051"];
				$jornada->pvisit2 = $row["pvisit052"];
				$jornada->pvisit3 = $row["pvisit053"];
				$jornada->pvisit4 = $row["pvisit054"];
				$jornada->pvisit5 = $row["pvisit055"];
			}
			
			if((strcasecmp(trim($row["local06"]),trim($equipo))==0)||(strcasecmp(trim($row["visit06"]),trim($equipo))==0)) {

                $jornada->idcat  = $row["idcat"];
				$jornada->idjorn = $row["idjorn"];

				$jornada->local  = $row["local06"];
				$jornada->visit  = $row["visit06"];
				$jornada->inst   = $row["inst06"];
				$jornada->plocal = $row["plocal06"];
				$jornada->pvisit = $row["pvisit06"];
				$jornada->fecha  = $row["fecha06"];
				$jornada->hour   = $row["hour06"];
				$jornada->plocal1 = $row["plocal061"];
				$jornada->plocal2 = $row["plocal062"];
				$jornada->plocal3 = $row["plocal063"];
				$jornada->plocal4 = $row["plocal064"];
				$jornada->plocal5 = $row["plocal065"];
				$jornada->pvisit1 = $row["pvisit061"];
				$jornada->pvisit2 = $row["pvisit062"];
				$jornada->pvisit3 = $row["pvisit063"];
				$jornada->pvisit4 = $row["pvisit064"];
				$jornada->pvisit5 = $row["pvisit065"];
			}
			
			if((strcasecmp(trim($row["local07"]),trim($equipo))==0)||(strcasecmp(trim($row["visit07"]),trim($equipo))==0)) {

                $jornada->idcat  = $row["idcat"];
				$jornada->idjorn = $row["idjorn"];

				$jornada->local  = $row["local07"];
				$jornada->visit  = $row["visit07"];
				$jornada->inst   = $row["inst07"];
				$jornada->plocal = $row["plocal07"];
				$jornada->pvisit = $row["pvisit07"];
				$jornada->fecha  = $row["fecha07"];
				$jornada->hour   = $row["hour07"];
				$jornada->plocal1 = $row["plocal071"];
				$jornada->plocal2 = $row["plocal072"];
				$jornada->plocal3 = $row["plocal073"];
				$jornada->plocal4 = $row["plocal074"];
				$jornada->plocal5 = $row["plocal075"];
				$jornada->pvisit1 = $row["pvisit071"];
				$jornada->pvisit2 = $row["pvisit072"];
				$jornada->pvisit3 = $row["pvisit073"];
				$jornada->pvisit4 = $row["pvisit074"];
				$jornada->pvisit5 = $row["pvisit075"];
			}
			
			if((strcasecmp(trim($row["local08"]),trim($equipo))==0)||(strcasecmp(trim($row["visit08"]),trim($equipo))==0)) {

                $jornada->idcat  = $row["idcat"];
				$jornada->idjorn = $row["idjorn"];

				$jornada->local  = $row["local08"];
				$jornada->visit  = $row["visit08"];
				$jornada->inst   = $row["inst08"];
				$jornada->plocal = $row["plocal08"];
				$jornada->pvisit = $row["pvisit08"];
				$jornada->fecha  = $row["fecha08"];
				$jornada->hour   = $row["hour08"];
				$jornada->plocal1 = $row["plocal081"];
				$jornada->plocal2 = $row["plocal082"];
				$jornada->plocal3 = $row["plocal083"];
				$jornada->plocal4 = $row["plocal084"];
				$jornada->plocal5 = $row["plocal085"];
				$jornada->pvisit1 = $row["pvisit081"];
				$jornada->pvisit2 = $row["pvisit082"];
				$jornada->pvisit3 = $row["pvisit083"];
				$jornada->pvisit4 = $row["pvisit084"];
				$jornada->pvisit5 = $row["pvisit085"];
			}
			
			if((strcasecmp(trim($row["local09"]),trim($equipo))==0)||(strcasecmp(trim($row["visit09"]),trim($equipo))==0)) {

                $jornada->idcat  = $row["idcat"];
				$jornada->idjorn = $row["idjorn"];

				$jornada->local  = $row["local09"];
				$jornada->visit  = $row["visit09"];
				$jornada->inst   = $row["inst09"];
				$jornada->plocal = $row["plocal09"];
				$jornada->pvisit = $row["pvisit09"];
				$jornada->fecha  = $row["fecha09"];
				$jornada->hour   = $row["hour09"];
				$jornada->plocal1 = $row["plocal091"];
				$jornada->plocal2 = $row["plocal092"];
				$jornada->plocal3 = $row["plocal093"];
				$jornada->plocal4 = $row["plocal094"];
				$jornada->plocal5 = $row["plocal095"];
				$jornada->pvisit1 = $row["pvisit091"];
				$jornada->pvisit2 = $row["pvisit092"];
				$jornada->pvisit3 = $row["pvisit093"];
				$jornada->pvisit4 = $row["pvisit094"];
				$jornada->pvisit5 = $row["pvisit095"];
			}
			
			if((strcasecmp(trim($row["local10"]),trim($equipo))==0)||(strcasecmp(trim($row["visit10"]),trim($equipo))==0)) {

                $jornada->idcat  = $row["idcat"];
				$jornada->idjorn = $row["idjorn"];

				$jornada->local  = $row["local10"];
				$jornada->visit  = $row["visit10"];
				$jornada->inst   = $row["inst10"];
				$jornada->plocal = $row["plocal10"];
				$jornada->pvisit = $row["pvisit10"];
				$jornada->fecha  = $row["fecha10"];
				$jornada->hour   = $row["hour10"];
				$jornada->plocal1 = $row["plocal101"];
				$jornada->plocal2 = $row["plocal102"];
				$jornada->plocal3 = $row["plocal103"];
				$jornada->plocal4 = $row["plocal104"];
				$jornada->plocal5 = $row["plocal105"];
				$jornada->pvisit1 = $row["pvisit101"];
				$jornada->pvisit2 = $row["pvisit102"];
				$jornada->pvisit3 = $row["pvisit103"];
				$jornada->pvisit4 = $row["pvisit104"];
				$jornada->pvisit5 = $row["pvisit105"];
			}
        } while ($row = mysql_fetch_array($result));
    }
    // Cierro la conexión con la base de datos
    mysql_close($link);
    // Devuelvo el objeto que contiene los resultados de la búsqueda
    return $jornada;
}

//
//
//
function numRowsJornadas($idcat) {
    $link=Conectarse();
	$sql = "select * from `CMS_jornadas` WHERE `idcat` = '" . $idcat . "' order by idjorn";
    $result = mysql_query($sql, $link);
    $num_rows = mysql_num_rows($result);
    mysql_close($link);
    return $num_rows;
}

//
//
//
function numRowsJornadasDisputadas($idcat) {
    $link=Conectarse();
	$sql = "select * from `CMS_jornadas` WHERE `idcat` = '" . $idcat . "' and plocal01 is not null order by idjorn";
    $result = mysql_query($sql, $link);
    $num_rows = mysql_num_rows($result);
    mysql_close($link);
    return $num_rows;
}

//
// Identifica la última jornada introducida con marcadores finales
//
function ultimaJornadaDisputada($idcat) {
	$ultimaJornada = 0;
    $link=Conectarse();
	$sql = "select * from `CMS_jornadas` WHERE `idcat` = '" . $idcat . "' order by idjorn";
    $result = mysql_query($sql, $link);
    // Para cada fila, guardo las columnas en el objeto
    if ($row = @mysql_fetch_array($result)) {
        do {
			if(($row["plocal01"]!=0)or
				($row["pvisit01"]!=0)or
				($row["plocal02"]!=0)or
				($row["pvisit02"]!=0)or
				($row["plocal03"]!=0)or
				($row["pvisit03"]!=0)or
				($row["plocal04"]!=0)or
				($row["pvisit04"]!=0)or
				($row["plocal05"]!=0)or
				($row["pvisit05"]!=0)or
				($row["plocal06"]!=0)or
				($row["pvisit06"]!=0)or
				($row["plocal07"]!=0)or
				($row["pvisit07"]!=0)or
				($row["plocal08"]!=0)or
				($row["pvisit08"]!=0)or
				($row["plocal09"]!=0)or
				($row["pvisit09"]!=0)or
				($row["plocal10"]!=0)or
				($row["pvisit10"]!=0))
			
			$ultimaJornada++;
			
        } while ($row = mysql_fetch_array($result));
    }
    
    mysql_close($link);
    
    return $ultimaJornada;
}

//
// Identifica la última jornada introducida sin marcadores finales
//
function ultimaJornadaRegistrada($idcat) {
	$ultimaJornada = 0;
    $link=Conectarse();
	$sql = "select * from `CMS_jornadas` WHERE `idcat` = '" . $idcat . "' order by idjorn";
    $result = mysql_query($sql, $link);
    // Para cada fila, guardo las columnas en el objeto
    if ($row = @mysql_fetch_array($result)) {
        do {
			if((($row["plocal01"]==0)and($row["pvisit01"]==0))or
				(($row["plocal02"]==0)and($row["pvisit02"]==0))or
				(($row["plocal03"]==0)and($row["pvisit03"]==0))or
				(($row["plocal04"]==0)and($row["pvisit04"]==0))or
				(($row["plocal05"]==0)and($row["pvisit05"]==0))or
				(($row["plocal06"]==0)and($row["pvisit06"]==0))or
				(($row["plocal07"]==0)and($row["pvisit07"]==0))or
				(($row["plocal08"]==0)and($row["pvisit08"]==0))or
				(($row["plocal09"]==0)and($row["pvisit09"]==0))or
				(($row["plocal10"]==0)and($row["pvisit10"]==0)))
			
			$ultimaJornada = $row["idjorn"];

        } while ($row = mysql_fetch_array($result));
    }
    
    mysql_close($link);
    
    return $ultimaJornada;
}

//
//
//
function guardarJornada($jornada) { 
	// Inicializamos variable
	$numero = 0;
    // Conexión con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_jornadas` WHERE `idcat` = '" . $jornada->idcat . "' order by idjorn";
    // Registro de log
    //wlog("guardarJornada",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Obtenemos el ultimo idtemp
    if ($row = mysql_fetch_array($result)) {
        do {
            $numero2 = $row["idjorn"];
			if($numero2>$numero) $numero = $numero2;
        } while ($row = mysql_fetch_array($result));
    }
    // Sumamos uno al ultimo idtemp
    $numero++;
    // Construimos la nueva query para guardar los datos de la nueva temporada
    $sql = "INSERT INTO `CMS_jornadas` (`idjorn`,`idcat`,`local01`,`visit01`,`inst01`,`plocal01`,`pvisit01`,`fecha01`,`hour01`,`local02`,`visit02`,`inst02`,`plocal02`,`pvisit02`,`fecha02`,`hour02`,`local03`,`visit03`,`inst03`,`plocal03`,`pvisit03`,`fecha03`,`hour03`,`local04`,`visit04`,`inst04`,`plocal04`,`pvisit04`,`fecha04`,`hour04`,`local05`,`visit05`,`inst05`,`plocal05`,`pvisit05`,`fecha05`,`hour05`,`local06`,`visit06`,`inst06`,`plocal06`,`pvisit06`,`fecha06`,`hour06`,`local07`,`visit07`,`inst07`,`plocal07`,`pvisit07`,`fecha07`,`hour07`,`local08`,`visit08`,`inst08`,`plocal08`,`pvisit08`,`fecha08`,`hour08`,`local09`,`visit09`,`inst09`,`plocal09`,`pvisit09`,`fecha09`,`hour09`,`local10`,`visit10`,`inst10`,`plocal10`,`pvisit10`,`fecha10`,`hour10`) VALUES ('" . $numero . "' , '" . $jornada->idcat . "' , '" . $jornada->local01 . "' , '" . $jornada->visit01 . "' , '" . $jornada->inst01 . "' , '" . $jornada->plocal01 . "' , '" . $jornada->pvisit01 . "' , '" . $jornada->fecha01 . "' , '" . $jornada->hour01 . "' , '" . $jornada->local02 . "' , '" . $jornada->visit02 . "' , '" . $jornada->inst02 . "' , '" . $jornada->plocal02 . "' , '" . $jornada->pvisit02 . "' , '" . $jornada->fecha02 . "' , '" . $jornada->hour02 . "' , '" .  $jornada->local03 . "' , '" . $jornada->visit03 . "' , '" . $jornada->inst03 . "' , '" . $jornada->plocal03 . "' , '" . $jornada->pvisit03 . "' , '" . $jornada->fecha03 . "' , '" . $jornada->hour03 . "' , '" . $jornada->local04 . "' , '" . $jornada->visit04 . "' , '" . $jornada->inst04 . "' , '" . $jornada->plocal04 . "' , '" . $jornada->pvisit04 . "' , '" . $jornada->fecha04 . "' , '" . $jornada->hour04 . "' , '" . $jornada->local05 . "' , '" . $jornada->visit05 . "' , '" . $jornada->inst05 . "' , '" . $jornada->plocal05 . "' , '" . $jornada->pvisit05 . "' , '" . $jornada->fecha05 . "' , '" . $jornada->hour05 . "' , '" . $jornada->local06 . "' , '" . $jornada->visit06 . "' , '" . $jornada->inst06 . "' , '" . $jornada->plocal06 . "' , '" . $jornada->pvisit06 . "' , '" . $jornada->fecha06 . "' , '" . $jornada->hour06 . "' , '" . $jornada->local07 . "' , '" . $jornada->visit07 . "' , '" . $jornada->inst07 . "' , '" . $jornada->plocal07 . "' , '" . $jornada->pvisit07 . "' , '" . $jornada->fecha07 . "' , '" . $jornada->hour07 . "' , '" . $jornada->local08 . "' , '" . $jornada->visit08 . "' , '" . $jornada->inst08 . "' , '" . $jornada->plocal08 . "' , '" . $jornada->pvisit08 . "' , '" . $jornada->fecha08 . "' , '" . $jornada->hour08 . "' , '" . $jornada->local09 . "' , '" . $jornada->visit09 . "' , '" . $jornada->inst09 . "' , '" . $jornada->plocal09 . "' , '" . $jornada->pvisit09 . "' , '" . $jornada->fecha09 . "' , '" . $jornada->hour09 . "' , '" . $jornada->local10 . "' , '" . $jornada->visit10 . "' , '" . $jornada->inst10 . "' , '" . $jornada->plocal10 . "' , '" . $jornada->pvisit10 . "' , '" . $jornada->fecha10 . "' , '" . $jornada->hour10 . "');";
    // Guardamos el log
    //wlog("guardarJornada",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexion con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

//
//
//
function borrarJornada($idcate,$idjorn) {
    // Inicializo variable
    $result = false;
    // Conexión con la base de datos
    $link=Conectarse();
    $sql = "delete from `CMS_jornadas` where idcat='" . $idcate . "' and idjorn='" . $idjorn . "'";
    // Guardamos el log
    wlog("borrarJornada",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql,$link);
    // Cerramos conexión con la base de datos
    mysql_close($link);
    // Devolvemos el resultado de la ejecución
    if($result==true) return true;
    else return false;
}

//
//
//
function modificarJornada($jornada) {
    // Conexión con la base de datos
    $link=Conectarse();
    // Construimos la query
    $sql = "UPDATE `CMS_jornadas` SET 	`local01`  = '" . $jornada->local01  . "' ,`visit01`  = '" . $jornada->visit01  . "' ,`inst01`   = '" . $jornada->inst01   . "' ,`plocal01` = '" . $jornada->plocal01 . "' ,`pvisit01` = '" . $jornada->pvisit01 . "' ,`fecha01`  = '" . $jornada->fecha01  . "' ,`hour01`   = '" . $jornada->hour01   . "' ,`plocal011`= '" . $jornada->plocal011. "' ,`plocal012`= '" . $jornada->plocal012. "' ,`plocal013`= '" . $jornada->plocal013. "' ,`plocal014`= '" . $jornada->plocal014. "' ,`plocal015`= '" . $jornada->plocal015. "' ,`pvisit011`= '" . $jornada->pvisit011. "' ,`pvisit012`= '" . $jornada->pvisit012. "' ,`pvisit013`= '" . $jornada->pvisit013. "' ,`pvisit014`= '" . $jornada->pvisit014. "' ,`pvisit015`= '" . $jornada->pvisit015. "' ,
										`local02`  = '" . $jornada->local02  . "' ,`visit02`  = '" . $jornada->visit02  . "' ,`inst02`   = '" . $jornada->inst02   . "' ,`plocal02` = '" . $jornada->plocal02 . "' ,`pvisit02` = '" . $jornada->pvisit02 . "' ,`fecha02`  = '" . $jornada->fecha02  . "' ,`hour02`   = '" . $jornada->hour02   . "' ,`plocal021`= '" . $jornada->plocal021. "' ,`plocal022`= '" . $jornada->plocal022. "' ,`plocal023`= '" . $jornada->plocal023. "' ,`plocal024`= '" . $jornada->plocal024. "' ,`plocal025`= '" . $jornada->plocal025. "' ,`pvisit021`= '" . $jornada->pvisit021. "' ,`pvisit022`= '" . $jornada->pvisit022. "' ,`pvisit023`= '" . $jornada->pvisit023. "' ,`pvisit024`= '" . $jornada->pvisit024. "' ,`pvisit025`= '" . $jornada->pvisit025. "' ,										
										`local03`  = '" . $jornada->local03  . "' ,`visit03`  = '" . $jornada->visit03  . "' ,`inst03`   = '" . $jornada->inst03   . "' ,`plocal03` = '" . $jornada->plocal03 . "' ,`pvisit03` = '" . $jornada->pvisit03 . "' ,`fecha03`  = '" . $jornada->fecha03  . "' ,`hour03`   = '" . $jornada->hour03   . "' ,`plocal031`= '" . $jornada->plocal031. "' ,`plocal032`= '" . $jornada->plocal032. "' ,`plocal033`= '" . $jornada->plocal033. "' ,`plocal034`= '" . $jornada->plocal034. "' ,`plocal035`= '" . $jornada->plocal035. "' ,`pvisit031`= '" . $jornada->pvisit031. "' ,`pvisit032`= '" . $jornada->pvisit032. "' ,`pvisit033`= '" . $jornada->pvisit033. "' ,`pvisit034`= '" . $jornada->pvisit034. "' ,`pvisit035`= '" . $jornada->pvisit035. "' ,										
										`local04`  = '" . $jornada->local04  . "' ,`visit04`  = '" . $jornada->visit04  . "' ,`inst04`   = '" . $jornada->inst04   . "' ,`plocal04` = '" . $jornada->plocal04 . "' ,`pvisit04` = '" . $jornada->pvisit04 . "' ,`fecha04`  = '" . $jornada->fecha04  . "' ,`hour04`   = '" . $jornada->hour04   . "' ,`plocal041`= '" . $jornada->plocal041. "' ,`plocal042`= '" . $jornada->plocal042. "' ,`plocal043`= '" . $jornada->plocal043. "' ,`plocal044`= '" . $jornada->plocal044. "' ,`plocal045`= '" . $jornada->plocal045. "' ,`pvisit041`= '" . $jornada->pvisit041. "' ,`pvisit042`= '" . $jornada->pvisit042. "' ,`pvisit043`= '" . $jornada->pvisit043. "' ,`pvisit044`= '" . $jornada->pvisit044. "' ,`pvisit045`= '" . $jornada->pvisit045. "' ,
										`local05`  = '" . $jornada->local05  . "' ,`visit05`  = '" . $jornada->visit05  . "' ,`inst05`   = '" . $jornada->inst05   . "' ,`plocal05` = '" . $jornada->plocal05 . "' ,`pvisit05` = '" . $jornada->pvisit05 . "' ,`fecha05`  = '" . $jornada->fecha05  . "' ,`hour05`   = '" . $jornada->hour05   . "' ,`plocal051`= '" . $jornada->plocal051. "' ,`plocal052`= '" . $jornada->plocal052. "' ,`plocal053`= '" . $jornada->plocal053. "' ,`plocal054`= '" . $jornada->plocal054. "' ,`plocal055`= '" . $jornada->plocal055. "' ,`pvisit051`= '" . $jornada->pvisit051. "' ,`pvisit052`= '" . $jornada->pvisit052. "' ,`pvisit053`= '" . $jornada->pvisit053. "' ,`pvisit054`= '" . $jornada->pvisit054. "' ,`pvisit055`= '" . $jornada->pvisit055. "' ,
										`local06`  = '" . $jornada->local06  . "' ,`visit06`  = '" . $jornada->visit06  . "' ,`inst06`   = '" . $jornada->inst06   . "' ,`plocal06` = '" . $jornada->plocal06 . "' ,`pvisit06` = '" . $jornada->pvisit06 . "' ,`fecha06`  = '" . $jornada->fecha06  . "' ,`hour06`   = '" . $jornada->hour06   . "' ,`plocal061`= '" . $jornada->plocal061. "' ,`plocal062`= '" . $jornada->plocal062. "' ,`plocal063`= '" . $jornada->plocal063. "' ,`plocal064`= '" . $jornada->plocal064. "' ,`plocal065`= '" . $jornada->plocal065. "' ,`pvisit061`= '" . $jornada->pvisit061. "' ,`pvisit062`= '" . $jornada->pvisit062. "' ,`pvisit063`= '" . $jornada->pvisit063. "' ,`pvisit064`= '" . $jornada->pvisit064. "' ,`pvisit065`= '" . $jornada->pvisit065. "' ,
										`local07`  = '" . $jornada->local07  . "' ,`visit07`  = '" . $jornada->visit07  . "' ,`inst07`   = '" . $jornada->inst07   . "' ,`plocal07` = '" . $jornada->plocal07 . "' ,`pvisit07` = '" . $jornada->pvisit07 . "' ,`fecha07`  = '" . $jornada->fecha07  . "' ,`hour07`   = '" . $jornada->hour07   . "' ,`plocal071`= '" . $jornada->plocal071. "' ,`plocal072`= '" . $jornada->plocal072. "' ,`plocal073`= '" . $jornada->plocal073. "' ,`plocal074`= '" . $jornada->plocal074. "' ,`plocal075`= '" . $jornada->plocal075. "' ,`pvisit071`= '" . $jornada->pvisit071. "' ,`pvisit072`= '" . $jornada->pvisit072. "' ,`pvisit073`= '" . $jornada->pvisit073. "' ,`pvisit074`= '" . $jornada->pvisit074. "' ,`pvisit075`= '" . $jornada->pvisit075. "' ,
										`local08`  = '" . $jornada->local08  . "' ,`visit08`  = '" . $jornada->visit08  . "' ,`inst08`   = '" . $jornada->inst08   . "' ,`plocal08` = '" . $jornada->plocal08 . "' ,`pvisit08` = '" . $jornada->pvisit08 . "' ,`fecha08`  = '" . $jornada->fecha08  . "' ,`hour08`   = '" . $jornada->hour08   . "' ,`plocal081`= '" . $jornada->plocal081. "' ,`plocal082`= '" . $jornada->plocal082. "' ,`plocal083`= '" . $jornada->plocal083. "' ,`plocal084`= '" . $jornada->plocal084. "' ,`plocal085`= '" . $jornada->plocal085. "' ,`pvisit081`= '" . $jornada->pvisit081. "' ,`pvisit082`= '" . $jornada->pvisit082. "' ,`pvisit083`= '" . $jornada->pvisit083. "' ,`pvisit084`= '" . $jornada->pvisit084. "' ,`pvisit085`= '" . $jornada->pvisit085. "' ,
										`local09`  = '" . $jornada->local09  . "' ,`visit09`  = '" . $jornada->visit09  . "' ,`inst09`   = '" . $jornada->inst09   . "' ,`plocal09` = '" . $jornada->plocal09 . "' ,`pvisit09` = '" . $jornada->pvisit09 . "' ,`fecha09`  = '" . $jornada->fecha09  . "' ,`hour09`   = '" . $jornada->hour09   . "' ,`plocal091`= '" . $jornada->plocal091. "' ,`plocal092`= '" . $jornada->plocal092. "' ,`plocal093`= '" . $jornada->plocal093. "' ,`plocal094`= '" . $jornada->plocal094. "' ,`plocal095`= '" . $jornada->plocal095. "' ,`pvisit091`= '" . $jornada->pvisit091. "' ,`pvisit092`= '" . $jornada->pvisit092. "' ,`pvisit093`= '" . $jornada->pvisit093. "' ,`pvisit094`= '" . $jornada->pvisit094. "' ,`pvisit095`= '" . $jornada->pvisit095. "' ,
										`local10`  = '" . $jornada->local10  . "' ,`visit10`  = '" . $jornada->visit10  . "' ,`inst10`   = '" . $jornada->inst10   . "' ,`plocal10` = '" . $jornada->plocal10 . "' ,`pvisit10` = '" . $jornada->pvisit10 . "' ,`fecha10`  = '" . $jornada->fecha10  . "' ,`hour10`   = '" . $jornada->hour10   . "' ,`plocal101`= '" . $jornada->plocal101. "' ,`plocal102`= '" . $jornada->plocal102. "' ,`plocal103`= '" . $jornada->plocal103. "' ,`plocal104`= '" . $jornada->plocal104. "' ,`plocal105`= '" . $jornada->plocal105. "' ,`pvisit101`= '" . $jornada->pvisit101. "' ,`pvisit102`= '" . $jornada->pvisit102. "' ,`pvisit103`= '" . $jornada->pvisit103. "' ,`pvisit104`= '" . $jornada->pvisit104. "' ,`pvisit105`= '" . $jornada->pvisit105. "'
			WHERE `idcat` = '" . $jornada->idcat . "' AND `idjorn` = '" . $jornada->idjorn . "';";

    // Guardamos el log
    //wlog("modificarJornada",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexión con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}


//
//
//
function obtenerFotoDeUsuario($iduser) {
    // Inicializo variable
    $comentario = null;
	$foto = "images/usuario/user.jpg";
    // Pongo i a cero
    $i = 0;
    // Obtengo conexión a la base de datos
    $link=Conectarse();
    // Construyo la query
    $sql = "select * from `CMS_users` where usuario='" . $iduser . "'";
    // Obtengo el resultado
    $result = mysql_query($sql, $link);
    // Para cada fila, guardo las columnas en el objeto
    if ($row = @mysql_fetch_array($result)) {
        do{            
            $foto     = $row["foto"];

            $i++;

        } while ($row = mysql_fetch_array($result));
    }
    // Cierro la conexión con la base de datos
    mysql_close($link);
    // Devuelvo el objeto que contiene los resultados de la búsqueda
    return $foto;
}

//
// Redirect
//
function redirect_comentario($pagina, $tiempo) {
    ?><script>location.href='<?php echo $pagina; ?>';</script><?php
	die();
}

//
//
//
function obtenerComentarioDeArticulo($idart) {
    // Inicializo variable
    $comentario = null;
    // Para cada fila de la tabla inicializo una temporada
    for ($i=0;$i<numRows("CMS_comentarios");$i++) {
        $comentario[$i] = new comentario();
    }
    // Pongo i a cero
    $i = 0;
    // Obtengo conexión a la base de datos
    $link=Conectarse();
    // Construyo la query
    $sql = "select * from `CMS_comentarios` where idart='" . $idart . "' order by idcom";
    // Obtengo el resultado
    $result = mysql_query($sql, $link);
    // Para cada fila, guardo las columnas en el objeto
    if ($row = @mysql_fetch_array($result)) {
        do {
            $comentario[$i]->idcom     = $row["idcom"];
            $comentario[$i]->fecha     = $row["fecha"];
            $comentario[$i]->iduser    = $row["iduser"];
            $comentario[$i]->contenido = $row["contenido"];
            $comentario[$i]->idart     = $row["idart"];
            $comentario[$i]->conta     = $row["conta"];

            $i++;

        } while ($row = mysql_fetch_array($result));
    }
    // Cierro la conexión con la base de datos
    mysql_close($link);
    // Devuelvo el objeto que contiene los resultados de la búsqueda
    return $comentario;
}

//
// Devuelve el número de filas de una tabla según un identificador
//
function numRowsDeArticulo($table,$idart) {
    $link     = Conectarse();
    $result   = mysql_query("select * from " . $table . " where idart='" . $idart . "' order by idcom desc", $link);
    $num_rows = mysql_num_rows($result);
    mysql_close($link);
    return $num_rows;
}

//
// Guarda un texto de log en el fichero de logs del sistema
//
function wlog($funcion, $texto, $debug) { 
    // Inicializamos la variable
    $aux = true;
	/*    
    if($debug==1)
    {
        // Inicializamos la variable
        $aux = false;

        // Estadisticas caseras de visita
        $hora = date("H:i:s");
        $dia = date("d/m/Y");

        // Abre el archivo de texto
        $ar = fopen("log.txt","a+");

        // Si hemos posido abrir el archivo
        if($ar!=false)
        {
            // Guardamos el dia
            fputs($ar,"Dia:");
            fputs($ar,$dia);
            // Guardamos la hora
            fputs($ar," - Hora:");
            fputs($ar,$hora);
            // Guardamos la funcion
            fputs($ar," - Funcion:");
            fputs($ar,$funcion);
            // Guardamos el log
            fputs($ar," - Log:");
            fputs($ar,$texto);
            fputs($ar,"\r\n");

            // Cerramos el archivo
            fclose($ar);

            // Inicializamos la variable
            $aux = true;
        }
    }
	*/    
    // Salimos de la funcion
    return $aux;
}

/////////////////
//
// 20fotos
//
/////////////////

//
//
//
function obtenerAlbum() {
    // Inicializo variable
    $album = null;
    // Para cada fila de la tabla inicializo una temporada
    for ($i=0;$i<numRows("CMS_album");$i++) {
        $album[$i] = new album();
    }
    // Pongo i a cero
    $i = 0;
    // Obtengo conexión a la base de datos
    $link=Conectarse();
    // Construyo la query
    $sql = "select * from `CMS_album` order by idalbum desc";
    // Guardamos el log
    wlog("obtenerAlmbum", $sql, 1);
    // Obtengo el resultado
    $result = mysql_query($sql, $link);
    // Para cada fila, guardo las columnas en el objeto
    if ($row = @mysql_fetch_array($result)) {
        do {
            $album[$i]->idalbum   = $row["idalbum"];
            $album[$i]->nombre    = $row["nombre"];
            $album[$i]->ubicacion = $row["ubicacion"];
			$album[$i]->numero    = $row["numero"];
            $i++;

        } while ($row = mysql_fetch_array($result));
    }
    // Cierro la conexión con la base de datos
    mysql_close($link);
	
	// Devuelvo el objeto que contiene los resultados de la búsqueda
    return $album;
}

//
//
//
function guardarAlbum($album) {
    // Inicializamos la variable numero
    $numero = 0;
    // Conexión con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_album` order by idalbum";
    // Registro de log
    wlog("guardarAlbum",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Obtenemos el ultimo idtemp
    if ($row = mysql_fetch_array($result)) {
        do {
            $numero = $row["idalbum"];
        } while ($row = mysql_fetch_array($result));
    }
    // Sumamos uno al ultimo
    $numero++;
    // Construimos la nueva query para guardar los datos de la nueva temporada
    $sql = "INSERT INTO `CMS_album` (`idalbum`,`nombre`,`ubicacion`,`numero`) VALUES ('" . $numero . "' , '" . $album->nombre . "' , '" . $album->ubicacion . "' , '" . $album->numero . "');";
	// Guardamos el log
    wlog("guardarAlbum",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexion con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

//
//
//
function borrarAlbum($idalbum) {
    // Inicializo variable
    $result = false;
    // Conexión con la base de datos
    $link=Conectarse();
    $sql = "delete from `CMS_album` where idalbum=" . $idalbum;
    // Guardamos el log
    wlog("borrarAlbum",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql,$link);
    // Cerramos conexión con la base de datos
    mysql_close($link);
    // Devolvemos el resultado de la ejecución
    if($result==true) return true;
    else return false;
}

//
//
//
function obtenerUnAlbum($idalbum) {
	// Inicializo variable
	$album = null;
    // Conexion con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_album` where idalbum='" . $idalbum . "'";
    // GUardamos el log
    wlog("obtenerUnAlbum",$sql,1);
    // Ejecutamos y guardamos la query
    $result = mysql_query($sql, $link);
    // Por cada resultado, que sólo deberia ser uno
    if ($row = mysql_fetch_array($result)) {
        do {
            $album->idalbum   = $row["idalbum"];
            $album->nombre    = $row["nombre"];
            $album->ubicacion = $row["ubicacion"];
            $album->numero    = $row["numero"];            
        } while ($row = mysql_fetch_array($result));
    }
    // Cerramos conexion con la base de datos
    mysql_close($link);
    // Devolvemos el objeto temporada
    return $album;
}

//
//
//
function obtenerLastIdDeAlbum($idalbum) {
	// Inicializo variable
	$album = null;
	$numero = 0;
	$memoria = 0;
	
	// Conexion con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_fotos` where idalbum='" . $idalbum ."'";
    // GUardamos el log
    //wlog("obtenerUnAlbum",$sql,1);
    // Ejecutamos y guardamos la query
    $result = mysql_query($sql, $link);
    // Por cada resultado, que sólo deberia ser uno
    if ($row = mysql_fetch_array($result)) {
        do {
            $numero = intval($row["idfoto"]);
			if($numero > $memoria) $memoria = $numero;
        } while ($row = mysql_fetch_array($result));
    }
    // Cerramos conexion con la base de datos
    mysql_close($link);

    // Devolvemos el objeto temporada
    return $memoria+1;
}

//
//
//
function guardarNombreDeFotoEnAlbum($idfoto,$idalbum,$textopie,$ruta) {
    // Conexión con la base de datos
    $link=Conectarse();
    // Construimos la nueva query para guardar los datos de la nueva temporada
	$sql = "INSERT INTO `CMS_fotos` (`idalbum`,`idfoto`,`fecha`,`pie`,`ruta`,`conta`) VALUES ('$idalbum' , '$idfoto' , '".date('d/m/Y')."' , '$textopie', '$ruta' , '');";
	// Guardamos el log
    //wlog("guardarAlbum",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexion con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

//
//
//
function incrementaContaDeAlbum($idalbum,$contador) {
    // Conexión con la base de datos
    $link=Conectarse();
    // Construimos la query
    $sql = "UPDATE `CMS_album` SET `numero` = '" . $contador . "' WHERE `idalbum` = '" . $idalbum . "';";
    // Guardamos el log
    wlog("modificarPersona",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql, $link);
    // Cerramos la conexión con la base de datos
    mysql_close($link);
    // Devolvemos el resultado
    if($result==true) return true;
    else return false;
}

//
//
//
function borrarFotoDeAlbum($idalbum,$idfoto) {
    // Inicializo variable
    $result = false;
    // Conexión con la base de datos
    $link=Conectarse();
    $sql = "delete from `CMS_fotos` where idalbum=" . $idalbum . " and ruta='" . $idfoto."'";
    //echo $sql;die();
	// Guardamos el log
    wlog("borrarAlbum",$sql,1);
    // Ejecutamos la query y obtenemos el resultado
    $result = mysql_query($sql,$link);
    // Cerramos conexión con la base de datos
    mysql_close($link);
    // Devolvemos el resultado de la ejecución
    if($result==true) return true;
    else return false;
}

//
//
//
function obtenerUnaFotoDeUnAlbum($idalbum,$idfoto) {
	$fotografia = null;
	
	$idfoto = str_replace(".jpg","",$idfoto);
	// Inicializo variable
	$album = false;
    // Conexion con la base de datos
    $link=Conectarse();
    // Construcción de la query
    $sql = "select * from `CMS_fotos` where idalbum='" . $idalbum . "' and idfoto ='".$idfoto."'";
    // GUardamos el log
    wlog("obtenerUnAlbum",$sql,1);
    // Ejecutamos y guardamos la query
    $result = mysql_query($sql, $link);
    // Por cada resultado, que sólo deberia ser uno
    if ($row = mysql_fetch_array($result)) {
        do {
            $fotografia->idalbum = $row["idalbum"];
            $fotografia->idfoto  = $row["idfoto"];
			$fotografia->fecha   = $row["fecha"];
			$fotografia->pie     = $row["pie"];
			$fotografia->ruta    = $row["ruta"];
			$fotografia->conta   = $row["conta"];
			
        } while ($row = mysql_fetch_array($result));
    }
    // Cerramos conexion con la base de datos
    mysql_close($link);
    // Devolvemos el objeto temporada
    return $fotografia;
}

//
//
//
function directoryExists($filepath="") {
    
	$ismap=false;
    $isimage=false;
    $isfile=false;
    $result=false;
    
    if (in_array(strtoupper(substr($filepath,-4)),array(".MAP",".HTM"))) $ismap=true;
    else if (in_array(strtoupper(substr($filepath,-4)),array(".GIF",".JPG","JPEG"))) $isimage=true;
    else $isfile=true;
    
    try {
        if (stripos($filepath,"http://") !== false || stripos($filepath,"https://") !== false) {
            $ch=curl_init();

            curl_setopt($ch,CURLOPT_URL,$filepath);
            curl_setopt($ch,CURLOPT_POST,0);
            curl_setopt($ch,CURLOPT_POSTFIELDS,"");
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

            $result=curl_exec($ch);
            
            curl_close($ch);

            if ($ismap && stripos($result,"<map ") === false && stripos($result,"</map>") === false) return false;
            else if ($isimage && $result != false && stripos($result,"<script>") === false && stripos($result,"<meta ") === false && stripos($result,"generaltickets.com") === false) $result=true;
            else if ($isimage) $result = false;
            else if ($isfile && strlen($result) == 0) return false;
            else return true;
        } else $result=file_exists($filepath);
    } catch (Exception $e) {
        return false;
    }
    
    return $result;
}

function init($que, $salida='') {
    if(isset($_REQUEST[$que])) {
		$salida = htmlspecialchars($_REQUEST[$que], ENT_QUOTES, 'ISO-8859-1');
	}
    return $salida;
}

?>