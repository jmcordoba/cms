<?php

// eliminamos los datos de cookies
setcookie("cmsweb_user",     '', time()-3600, '/cmsweb/');
setcookie("cmsweb_pasw",     '', time()-3600, '/cmsweb/');
setcookie("cmsweb_foto",     '', time()-3600, '/cmsweb/');
setcookie("cmsweb_permisos", '', time()-3600, '/cmsweb/');
setcookie("cmsweb_mail",     '', time()-3600, '/cmsweb/');
setcookie("cmsweb_logged",   '', time()-3600, '/cmsweb/');

// redirect
ob_clean();
header('Location: ../index.php');
die();