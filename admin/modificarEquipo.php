<?php

// Incluímos Objetos necesarios
require("objetos/equipo.php");
// Incluímos funciones necesarias
require("fun/funciones.php");

// Obtenemos los datos del formulario
$team->idteam = init("idteam");
$team->idcat  = init("idcat");
$team->nombre = init("nombre");
$team->mister = init("mister");
$team->foto   = init("foto");
$team->conta  = init("conta");
$team->conta  = "";

// Capturamos los datos del fichero adjunto
$nombre_archivo = $_FILES['userfile']['name']; 
$tipo_archivo   = $_FILES['userfile']['type']; 
$tamano_archivo = $_FILES['userfile']['size'];

// Si viene un fichero adjunto
if($nombre_archivo!="") {
    //compruebo si las caracter�sticas del archivo son las que deseo 
    if ($tamano_archivo < 100000) {
		// Redimensionamos el fichero a 200x200
		include('simpleImage.php');
		$image = new SimpleImage();
		$image->load($_FILES['userfile']['tmp_name']);
		$image->resize(426,320);
		$image->save($_FILES['userfile']['tmp_name']);
        // Inicializamos la ruta y el nombre del avatar del usuario
        $team->foto    = "admin/images/equipo/team" . $team->idteam . ".gif";
        // Guardamos el fichero en la ruta especificada
        if (!move_uploaded_file($_FILES['userfile']['tmp_name'], "images/equipo/team" . $team->idteam . ".gif")){
            // Si hay algún tipo de error, redirigimos a otra página
            ?><script>location.href='index.php?origen=error';</script><?php
			die();
        }
    } else {
		?>
		<script>
			alert('La imagen que desea subir es mayor de 100kB.\nPor favor, suba una imagen con un peso inferior.');
		</script>
		<?php
	}
}

// Guardamos los cambios si es posible y redireccionamos en función del resultado
if(modificarEquipo($team,$team->idteam)==true) redirect("index.php?origen=equipo",0);
else                                           redirect("index.php?origen=error" ,0);

?>