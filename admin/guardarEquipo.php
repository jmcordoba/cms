<?php

// Incluye los objetos necesarios
require("objetos/equipo.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Obtenemos las variables del formulario
$team->idteam    = init("idteam");
$team->idcat     = init("idcat");
$team->nombre    = init("nombre");
$team->mister    = init("mister");
$team->foto      = init("foto");
$team->conta     = init("conta");
$team->conta     = "";

// Capturamos los datos del fichero adjunto
$nombre_archivo = $_FILES['userfile']['name']; 
$tipo_archivo   = $_FILES['userfile']['type']; 
$tamano_archivo = $_FILES['userfile']['size'];

// Inicializamos la variable numero de filas a cero
$numero = 0;

// Conexión con la base de datos
$link=Conectarse();
// Construcción de la query
$sql = "select * from `CMS_equipos` order by idteam";
// Registro de log
wlog("guardarEquipo",$sql,1);
// Ejecutamos la query y obtenemos el resultado
$result = mysql_query($sql, $link);
// Obtenemos el ultimo idtemp
if ($row = mysql_fetch_array($result))
{
    do{
        $numero = $row["idteam"];
    } while ($row = mysql_fetch_array($result));
}
$numero++;

// Cerramos la conexion con la base de datos
mysql_close($link);
// Si viene un fichero adjunto
if($nombre_archivo!="") {
    //compruebo si las caracter�sticas del archivo son las que deseo 
    if ($tamano_archivo < 100000) {
		// Redimensionamos el fichero a 200x200
		include('simpleImage.php');
		$image = new SimpleImage();
		$image->load($_FILES['userfile']['tmp_name']);
		$image->resize(426,320);
		$image->save($_FILES['userfile']['tmp_name']);
        // Inicializamos la ruta y el nombre del avatar del usuario
        $team->foto    = "admin/images/equipo/team" . $numero . ".gif";
        // Guardamos el fichero en la ruta especificada
        if (!move_uploaded_file($_FILES['userfile']['tmp_name'], "images/equipo/team" . $numero . ".gif")){
            // S ihay algún tipo de error, redirigimos a otra página
            ?><script>location.href='index.php?origen=error';</script><?php
        }
    } else {
		?>
		<script>
			alert('La imagen que desea subir es mayor de 100kB.\nPor favor, suba una imagen con un peso inferior.');
		</script>
		<?php
	}
}
// Intentamos guardar el usuario y redirigimos en función de si hemos tenido éxito o no
if(guardarEquipo($team)==true) redirect("index.php?origen=equipo",0);
else                           redirect("index.php?origen=error" ,0);

?>