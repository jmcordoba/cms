<?php

// Incluimos objetos y funciones
require("objetos/social.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
// Capturamos el id de la categoría a eliminar
$idsocial = $_GET["idsocial"];
// Eliminamos categoria y redirigimos
if(borrarSocial($idsocial)==true) redirect("index.php?origen=social" ,0);
else                              redirect("index.php?origen=error"  ,0);

?>
