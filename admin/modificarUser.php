<?php

// Incluímos Objetos necesarios
require("objetos/usuario.php");
// Incluímos funciones necesarias
require("fun/funciones.php");

// Obtenemos los datos del formulario
$usuario->nombre    = init("nombre");
$usuario->apellidos = init("apellidos");
$usuario->usuario   = init("usuario");
$usuario->password  = init("password");
$usuario->telefono  = init("telefono");
$usuario->mail      = init("mail");
$usuario->permisos  = init("permisos");
$usuario->conta     = init("conta");
$usuario->validado  = init("validado");
$usuario->foto      = init("foto");

// Capturamos los datos del fichero adjunto
$nombre_archivo = $_FILES['nfoto']['name']; 
$tipo_archivo   = $_FILES['nfoto']['type']; 
$tamano_archivo = $_FILES['nfoto']['size'];

// Si viene un fichero adjunto
if($nombre_archivo!="") {
    //compruebo si las caracter�sticas del archivo son las que deseo 
    if ($tamano_archivo < 100000) {        
		// Redimensionamos el fichero a 200x200
		include('simpleImage.php');
		$image = new SimpleImage();
		$image->load($_FILES['nfoto']['tmp_name']);
		$image->resize(60,60);
		$image->save($_FILES['nfoto']['tmp_name']);
		// Inicializamos la ruta y el nombre del avatar del usuario
		$usuario->foto = "admin/images/usuario/" . $usuario->mail . ".gif";
		// Guardamos el fichero en la ruta especificada
		if (!move_uploaded_file($_FILES['nfoto']['tmp_name'], "images/usuario/" . $usuario->mail . ".gif")){
			// Si hay algún tipo de error, redirigimos a otra página
			?><script>location.href='index.php?origen=error';</script><?php
			die();
		}
    } else {
		?>
		<script>
			alert('La imagen que desea subir es mayor de 100kB.\nPor favor, suba una imagen con un peso inferior.');
		</script>
		<?php
	}
}
// Guardamos los cambios si es posible y redireccionamos en función del resultado
if(modificarUser($usuario)==true) redirect("index.php?origen=user" ,0);
else                              redirect("index.php?origen=error",0);

?>
