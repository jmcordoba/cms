<?php

// Incluímos Objetos necesarios
require("objetos/tags.php");
// Incluímos funciones necesarias
require("fun/funciones.php");

// Obtenemos los datos del formulario
$tag->idtags = init("idtags");
$tag->nombre = init("nombre");
$tag->conta  = init("conta");

// Guardamos los cambios si es posible y redireccionamos en función del resultado
if(modificarTags($tag)==true) redirect("index.php?origen=tags" ,0);
else                          redirect("index.php?origen=error",0);

?>
