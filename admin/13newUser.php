<?php

// Incluye los objetos necesarios
require("objetos/usuario.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>

<!DOCTYPE html>

<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::usuarios::nuevo</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>
    
    <script>
        // Verifica la dirección de correo electrónico
        function verificar(direccion) {
            if (direccion.indexOf("@")>0) {
                if ((direccion.indexOf("@")+2)<direccion.lastIndexOf(".")) {
                    if (direccion.lastIndexOf(".")<(direccion.length-2)) {
                        return (true);
                    }
                }
            }
            return (false);
        }
        // Verifica los campos del formulario antes de guardar los datos
        function verificar_form() {
            if(document.datos.nombre.value.length==0) {
                alert("Error:\nDebe ingresar el nombre"); 
                document.frm.nombre.focus() 
                return 0; 
            }
            else if(document.datos.apellidos.value.length==0) {
                alert("Error:\nDebe ingresar el apellido"); 
                document.frm.apellidos.focus() 
                return 0; 
            }
            else if(document.datos.usuario.value.length==0) {
                alert("Error:\nDebe ingresar el usuario"); 
                document.frm.usuario.focus() 
                return 0; 
            }
            else if(document.datos.password.value.length==0) {
                alert("Error:\nDebe ingresar el password"); 
                document.frm.password.focus() 
                return 0; 
            }
            else if(document.datos.telefono.value.length==0) {
                alert("Error:\nDebe ingresar el telefono"); 
                document.frm.telefono.focus() 
                return 0; 
            }
            else if(isNaN(document.datos.telefono.value)) {
                alert("Error:\nEste campo debe tener sólo números.");
                document.frm.telefono.focus();
                return false;
            }
            else if(document.datos.mail.value.length==0) {
                alert("Error:\nDebe ingresar el mail"); 
                document.frm.mail.focus() 
                return 0; 
            }
            else if (!verificar(document.datos.mail.value)) {
                alert('Error:\nMail incorrecto');
            }
            else {
                document.forms['datos'].submit();
            }
        }
    </script>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - usuarios - nuevo'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=628 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <form name="datos" action="guardarUser.php" method=post enctype="multipart/form-data">
                        <table border=0 width=850>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="13users.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <table width=850>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Nombre</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=nombre size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Apellidos</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=apellidos size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Usuario</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=usuario size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Password</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=password size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Teléfono</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=telefono size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Mail</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=mail size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Permisos</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=permisos size=108></input></td>
                            </tr>
                            <!--tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">conta</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=conta size=108 readonly="readonly"></input></td>
                            </tr-->
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">¿Validado?</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=validado size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Foto</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input type="file" name=foto style="width:400px;border:0px solid #000000;"></input></td>
                            </tr>
                            <tr>
                                <td bgcolor=#ffffff><a href="#" onclick="verificar_form();"><font face="arial" style="font-size: 11px;; color: blue;">Guardar usuario</font></a></td>
                            </tr>
                        </table>
                        
                    </form>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>
