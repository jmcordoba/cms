<?php

// Incluye los objetos necesarios
require("objetos/comentario.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Control de usuario identificado
require("check.php");

?>

<!DOCTYPE html>

<html>
    <script language="JavaScript" type="text/javascript">
		function modificar(idcom){ document.location.href="15modComen.php?idcom=" + idcom; }
        function eliminar(idcom){  document.location.href="15delComen.php?idcom=" + idcom; }
        function nuevo(){          document.location.href="15newComen.php"; }
    </script>

    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>cms::administración::comentarios</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!-- cabecera -->
        <?php $titulo = 'cms - administración - comentarios'; require('cabecera.php'); ?>

        <!-- cuerpo -->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=800 align=center>
            <tr>
                <!-- menu -->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!-- contenido -->
                <td width=840 style="vertical-align:top">
                    
                    <table border=0 width=840>
                        <tr align="right">
                            <td width=840 bgcolor=#ffffff>
                                <a href="#" onClick="nuevo();">
                                    <font face="arial" style="font-size: 11px;; color: blue;">Introduce nuevo comentario</font>
                                </a>
                            </td>
                        </tr>
                    </table>
                    
                    <table width=840>
                        <tr height=10>
                            <td width=50  bgcolor=#c8c8c8><font class="admin_font">Idcom    </font></td>
                            <td width=50  bgcolor=#c8c8c8><font class="admin_font">Idart    </font></td>
							<td width=50  bgcolor=#c8c8c8><font class="admin_font">Fecha    </font></td>
                            <td width=50  bgcolor=#c8c8c8><font class="admin_font">Iduser   </font></td>
                            <td width=550 bgcolor=#c8c8c8><font class="admin_font">Contenido</font></td>
                            
                            <td width=50  bgcolor=#c8c8c8>&nbsp;</td>
                            <td width=50  bgcolor=#c8c8c8>&nbsp;</td>
                        </tr>

						<?php
						
						$comentario = obtenerComentario();
						
						for($i=0;$i<numRows("CMS_comentarios");$i++)
						{
							$_POST["idcom"]     = $comentario[$i]->idcom;
							$_POST["fecha"]     = $comentario[$i]->fecha;
							$_POST["iduser"]    = $comentario[$i]->iduser;
							$_POST["contenido"] = $comentario[$i]->contenido;
							$_POST["idart"]     = $comentario[$i]->idart;
							$_POST["conta"]     = $comentario[$i]->conta;
							?>

							<form name="datos" action="guardarComentario.php" method=post>
								<tr height=10>
									<td width=50  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["idcom"];?>     </font></td>
									<td width=50  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["idart"];?>     </font></td>
									<td width=50  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["fecha"];?>     </font></td>
									<td width=50  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["iduser"];?>    </font></td>
									<td width=550 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["contenido"];?> </font></td>
									
									<td width=50  bgcolor=#f8f8f8>
										<a href="#" onClick="modificar('<?php echo $_POST["idcom"];?>');">
											<font face="arial" style="font-size: 11px;; color: blue;">modificar</font>
										</a>
									</td>
									<td width=50  bgcolor=#f8f8f8>
										<a href="#" onClick="eliminar('<?php echo $_POST["idcom"];?>');">
											<font face="arial" style="font-size: 11px;; color: blue;">eliminar </font>
										</a>
									</td>
								</tr>
							</form>
							<?php
						}
						?>
                    </table>
                </td>
            </tr>		
        </table>
		
    </body>
</html>
