<?php

// Incluye los objetos necesarios
require("objetos/temp.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::categorías::modificar</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - categorías - modificar'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=800 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <?php
                    
                    $idcat = $_GET["idcate"];
                    
                    $categoria = obtenerUnaCate($idcat);
                    
                    $_POST["name"]      = $categoria->name;
                    $_POST["numteams"]  = $categoria->numteams;
                    $_POST["wpoints"]   = $categoria->wpoints;
                    $_POST["dpoints"]   = $categoria->dpoints;
                    $_POST["lpoints"]   = $categoria->lpoints;
                    $_POST["jornadas"]  = $categoria->jornadas;
                    $_POST["idavuelta"] = $categoria->idavuelta;
                    $_POST["idtemp"]    = $categoria->idtemp;
					
					$_POST["rival01"]   = $categoria->rival01;
					$_POST["rival02"]   = $categoria->rival02;
					$_POST["rival03"]   = $categoria->rival03;
					$_POST["rival04"]   = $categoria->rival04;
					$_POST["rival05"]   = $categoria->rival05;
					$_POST["rival06"]   = $categoria->rival06;
					$_POST["rival07"]   = $categoria->rival07;
					$_POST["rival08"]   = $categoria->rival08;
					$_POST["rival09"]   = $categoria->rival09;
					$_POST["rival10"]   = $categoria->rival10;
					$_POST["rival11"]   = $categoria->rival11;
					$_POST["rival12"]   = $categoria->rival12;
					$_POST["rival13"]   = $categoria->rival13;
					$_POST["rival14"]   = $categoria->rival14;
					$_POST["rival15"]   = $categoria->rival15;
					$_POST["rival16"]   = $categoria->rival16;
					$_POST["rival17"]   = $categoria->rival17;
					$_POST["rival18"]   = $categoria->rival18;
					$_POST["rival19"]   = $categoria->rival19;
					$_POST["rival20"]   = $categoria->rival20;
					
                    ?>

                    <form name="datos" action="modificarCate.php" method=post>
                        
						<input type="hidden" name="idcat" value="<?php echo $idcat;?>">
						
						<table border=0 width=850>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="03cate.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <table width=850>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Selecciona temporada</font></td>
                                <td height=20 width=700 bgcolor=#ffffff>
                                    <select name="idtemp">
                                        <?php
                                        $temporada = obtenerTemp();;
										for ($i=0;$i<numRows("CMS_temp");$i++){
                                            echo"<option value=\"" . $temporada[$i]->idtemp . "\">" . $temporada[$i]->name . "</option>";
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8>
									<font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Nombre</font>
								</td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=name size=108 value="<?php echo $_POST["name"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8>
									<font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Número de equipos</font>
								</td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=numteams size=108 value="<?php echo $_POST["numteams"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8>
									<font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Puntos por ganar</font>
								</td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=wpoints size=108 value="<?php echo $_POST["wpoints"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8>
									<font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Puntos por empatar</font>
								</td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=dpoints size=108 value="<?php echo $_POST["dpoints"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8>
									<font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Puntos por perder</font>
								</td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=lpoints size=108 value="<?php echo $_POST["lpoints"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8>
									<font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Número de jornadas</font>
								</td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=jornadas size=108 value="<?php echo $_POST["jornadas"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8>
									<font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">¿Ida y vuelta?</font>
								</td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=idavuelta size=108 value="<?php echo $_POST["idavuelta"];?>"></input></td>
                            </tr>
						</table>
						
						<!-- Rivales -->
						<table width=850>
							<tr>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival01</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival01 size=30 value="<?php echo $_POST["rival01"];?>"></input></td>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival02</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival02 size=30 value="<?php echo $_POST["rival02"];?>"></input></td>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival03</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival03 size=30 value="<?php echo $_POST["rival03"];?>"></input></td>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival04</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival04 size=30 value="<?php echo $_POST["rival04"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival05</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival05 size=30 value="<?php echo $_POST["rival05"];?>"></input></td>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival06</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival06 size=30 value="<?php echo $_POST["rival06"];?>"></input></td>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival07</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival07 size=30 value="<?php echo $_POST["rival07"];?>"></input></td>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival08</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival08 size=30 value="<?php echo $_POST["rival08"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival09</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival09 size=30 value="<?php echo $_POST["rival09"];?>"></input></td>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival10</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival10 size=30 value="<?php echo $_POST["rival10"];?>"></input></td>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival11</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival11 size=30 value="<?php echo $_POST["rival11"];?>"></input></td>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival12</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival12 size=30 value="<?php echo $_POST["rival12"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival13</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival13 size=30 value="<?php echo $_POST["rival13"];?>"></input></td>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival14</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival14 size=30 value="<?php echo $_POST["rival14"];?>"></input></td>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival15</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival15 size=30 value="<?php echo $_POST["rival15"];?>"></input></td>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival16</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival16 size=30 value="<?php echo $_POST["rival16"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival17</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival17 size=30 value="<?php echo $_POST["rival17"];?>"></input></td>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival18</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival18 size=30 value="<?php echo $_POST["rival18"];?>"></input></td>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival19</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival19 size=30 value="<?php echo $_POST["rival19"];?>"></input></td>
                                <td height=20 width=50  bgcolor=#c8c8c8><font  face="verdana" color="black" style="font-size: 11px;;">rival20</font></td>
                                <td height=20 width=150 bgcolor=#f8f8f8><input style="font-size: 11px;;" class="admin_input" type=text name=rival20 size=30 value="<?php echo $_POST["rival20"];?>"></input></td>
                            </tr>
						</table>
						<!-- Guardar -->
						<table width=850>
                            <tr><td bgcolor=#ffffff><a href="#" onclick="javascript:document.forms['datos'].submit(); return false;"><font face="arial" style="font-size: 11px;; color: blue;">Guardar categoría</font></a></td></tr>
                        </table>
                    </form>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>