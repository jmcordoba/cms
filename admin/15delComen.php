<?php

// Incluye los objetos y funciones
require("objetos/comentario.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");

// Capturamos el id de la categoría a eliminar
$idcom = $_GET["idcom"];
// Eliminamos categoria y redirigimos
if(borrarComentario($idcom)==true) redirect("index.php?origen=comen" ,0);
else                               redirect("index.php?origen=error",0);

?>