<?php

// Incluimos objetos y funciones
require("objetos/insta.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
// Capturamos el id de la categoría a eliminar
$insta = $_GET["idinst"];
// Eliminamos categoria y redirigimos
if(borrarInsta($insta)==true) redirect("index.php?origen=insta" ,0);
else                          redirect("index.php?origen=error",0);

?>
