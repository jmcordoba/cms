<?php

// Incluye los objetos necesarios
require("objetos/album.php");
require("objetos/fotografia.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>

<!DOCTYPE html>

<html>
	<script language="JavaScript" type="text/javascript">
		function eliminar(idalbum,ubicacion) {
			document.location.href="20delFotoDeAlbum.php?album="+idalbum+"&ubicacion="+ubicacion;
		}
    </script>
    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::album::agregar fotos</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!-- cabecera -->
        <?php $titulo = 'cms - administración - album - agregar fotos'; require('cabecera.php'); ?>

        <!-- cuerpo -->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=800 align=center>
            <tr>
                <!-- menu -->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!-- contenido -->
                <td width=850 style="vertical-align:top">
                    
                    <?php
                    
					// Obtenemos el directorio del album
                    $idalbum  = $_GET["idalbum"];
                    $album    = obtenerUnAlbum($idalbum);
                    $rutatemp = $album->ubicacion."/";
                    
					$textopie = isset($_POST["textopie"]) ? $_POST["textopie"] : "";
					
					// Obtenemos el nombre del archivo, por si queremos respetar el mismo nombre
					$imagenp  = isset($_FILES["imagenp"]["name"]) ? $_FILES["imagenp"]["name"] : "";

					// Si la variable viene definida para hacer Upload, subimos la imagen
					if((isset($_REQUEST['accion']))and($_REQUEST['accion']=="Upload")and($imagenp!="")) {
						
						// Modificamos las imagenes para redimensionarlas a conveniencia
						include('simpleImage.php');
						$image = new SimpleImage();
						$image->load($_FILES['imagenp']['tmp_name']);
						$image->resize(640,480);
						$image->save($_FILES['imagenp']['tmp_name']);

						// 1.- obtenemos el identificador de la ultima imagen de la tabla de fotos
						$num = obtenerLastIdDeAlbum($idalbum);
						// 2.- creamos el nombre de la imagen en funcion del identificador
						$nidfoto = str_pad($num, 10, "0", STR_PAD_LEFT);
						$nImage  = $nidfoto;
						// guardamos el archivo en el directorio adecuado
						move_uploaded_file($_FILES["imagenp"]["tmp_name"],$rutatemp.$nImage.".jpg");
						// 3.- guardamos el registro de la foto en la tabla de fotos
						guardarNombreDeFotoEnAlbum($nidfoto,$idalbum,$textopie,"$rutatemp$nImage.jpg");
						// 4.- incrementamos el contador de fotos en la tabla CMS_album
						if(directoryexists($album->ubicacion)) {												
							$num_ficheros = 0;
							// Abrimos el directorio
							$directorio=opendir($album->ubicacion); 																																																					// <cfdirectory action="LIST" directory="#ruta#logos/#dom#" name="logoTPV" sort="name ASC,datelastmodified" filter="LOGO*.*">
							// Obtenemos la lista de archivos y los vamos tratando uno a uno
							while ($archivo = readdir($directorio)) {
								if(($archivo!=".")and($archivo!="..")) {
									$num_ficheros++;
								}
							}
						}
						incrementaContaDeAlbum($idalbum,$num_ficheros);
						// Redirigimos a la misma pagina del album
						?><script>location.href="20addFotos2Album.php?idalbum=<?php echo $idalbum; ?>";</script><?php
					} else {
						?>
						<form id="datos" enctype="multipart/form-data" method="post">
						
							<table border="0" width="850">
								<tr align="left">
									<td width="850" bgcolor="#ffffff">
										<a href="20fotos.php">
											<font face="arial" style="font-size: 11px;; color: blue;">volver</font>
										</a>
									</td>
								</tr>
							</table>
							
							<table width="850">
								
								<tr>
									<td height="" width="700" bgcolor="#ffffff">								
										<input type="file"   name="imagenp" size="70">
									</td>
								</tr>
								
								<tr>
									<td height="" width="700" bgcolor="#ffffff">								
										Comentario de la foto:&nbsp;<input type="text" name="textopie" size="50">
									</td>
								</tr>
								
								<tr>
									<td height="20" width="150" bgcolor="#ffffff">								
										<input type="hidden" name="accion"  value="Upload">
										<input type="Submit" name="boton" value="Subir foto">
										* Las fotos se redimensionaran a 640x480
									</td>
								</tr>
								
								<tr height="50">
									<td>&nbsp;</td>
								</tr>
							</table>
							
							<table width="850">
								<tr>
									<?php
									//
									// Aqui mostraremos las imagenes que existan en el directorio del album
									//
									
									$contador = 0;
									
									if(directoryexists($album->ubicacion)) {												
										// Abrimos el directorio
										$directorio=opendir($album->ubicacion); 																																																					// <cfdirectory action="LIST" directory="#ruta#logos/#dom#" name="logoTPV" sort="name ASC,datelastmodified" filter="LOGO*.*">
										// Obtenemos la lista de archivos y los vamos tratando uno a uno
										while ($archivo = readdir($directorio)) {																																																											// <cfloop list="#imagen#" index="nombre">
											if(($archivo!=".")and($archivo!="..")) {
												$fotografia = obtenerUnaFotoDeUnAlbum($album->idalbum,$archivo);
												if($fotografia!=false){
													?>
													<td>
														<table border="0" cellspacing=0>
															<tr align="right">
																<td>
																	<a href="javascript:eliminar(<?php echo $fotografia->idalbum; ?>,'<?php echo rawurlencode($fotografia->ruta); ?>');">
																		<font face="arial" style="font-size: 11px;; color: blue;">eliminar fotografia</font>
																	</a>
																</td>
															</tr>
															<tr>
																<td>
																	<img width="400" height="250" src="<?php echo $fotografia->ruta; ?>"></img>
																</td>
															</tr>
															<tr bgcolor="#ffffff">
																<td>
																	<font face="arial" style="font-size: 12px;; color: #394752;">
																		<?php 
																			if($fotografia->pie!="") echo $fotografia->pie;
																			else                     echo $fotografia->idfoto;
																		?>
																	</font>
																</td>
															</tr>
														</table>
													</td>
													<?php
													$contador++;
													if(($contador%2)==0) {
														?></tr><tr><?php
													}
												}
											}
										}																																																																					// </cfoutput>
										// Cerramos el directorio
										closedir($directorio);
									}
									?>
								</tr>
							</table>
						</form>
						<?php
					}
					?>
                </td>
            </tr>		
        </table>
    </body>
</html>