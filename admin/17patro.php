<?php

// Incluye los objetos necesarios
require("objetos/patro.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <script language="JavaScript" type="text/javascript">
	function modificar(id){
		document.location.href="17modPatro.php?patro=" + id;
	}
	function eliminar(id){
		document.location.href="17delPatro.php?patro=" + id;
	}
	function nuevo(){
		document.location.href="17newPatro.php";
	}
    </script>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::patrocinadores</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - patrocinadores'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=700 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <table border=0 width=850>
                        <tr align="right">
                            <td width=850 bgcolor=#ffffff>
								<a href="#" onClick="nuevo();">
									<font face="arial" style="font-size: 11px;; color: blue;">Introduce nuevo patrocinador</font>
								</a>
							</td>
                        </tr>
                    </table>
                    
                    <table width=850>
                        <tr height=10>
                            <td width=50  bgcolor=#c8c8c8><font class="admin_font">Idpatro</font></td>
                            <td width=200 bgcolor=#c8c8c8><font class="admin_font">Patrocinador</font></td>
                            <td width=200 bgcolor=#c8c8c8><font class="admin_font">Importe</font></td>
                            <td width=300 bgcolor=#c8c8c8><font class="admin_font">Web</font></td>

                            <td width=50 bgcolor=#c8c8c8>&nbsp;</td>
                            <td width=50 bgcolor=#c8c8c8>&nbsp;</td>
                        </tr>

						<?php
						$patro = obtenerPatro();
						
						for($i=0;$i<numRows("CMS_patro");$i++) {
						
							$_POST["idpatro"]    = $patro[$i]->idpatro;
							$_POST["nombre"]     = $patro[$i]->nombre;
							$_POST["imagen"]     = $patro[$i]->imagen;
							$_POST["fecha_alta"] = $patro[$i]->fecha_alta;
							$_POST["fecha_baja"] = $patro[$i]->fecha_baja;
							$_POST["importe"]    = $patro[$i]->importe;
							$_POST["web"]        = $patro[$i]->web;
							$_POST["conta"]      = $patro[$i]->conta;
							?>

							<form name="datos" action="guardarPatro.php" method=post>
								<tr height=10>
									<td width=50  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["idpatro"];?>   </font></td>
									<td width=200 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["nombre"];?>    </font></td>
									<td width=200 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["importe"];?>   </font></td>
									<td width=300 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["web"];?>       </font></td>
									
									<td width=50  bgcolor=#f8f8f8>
										<a href="#" onClick="modificar(<?php echo $_POST["idpatro"]; ?>);">
											<font face="arial" style="font-size: 11px;; color: blue;">modificar</font>
										</a>
									</td>
									<td width=50  bgcolor=#f8f8f8>
										<a href="#" onClick="eliminar(<?php echo $_POST["idpatro"]; ?>);">
											<font face="arial" style="font-size: 11px;; color: blue;">eliminar</font>
										</a>
									</td>
								</tr>
							</form>
							<?php
						}
						?>
                    </table>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>
