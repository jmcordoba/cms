<?php

// Incluye los objetos necesarios
require("objetos/album.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>

<!DOCTYPE html>

<html>
    <script language="JavaScript" type="text/javascript">
	function modificar(id) {
		document.location.href="20addFotos2Album.php?idalbum=" + id;
	}
    function eliminar(id) {
		document.location.href="20delAlbum.php?idalbum=" + id;
	}
    function nuevo() {
		document.location.href="20newAlbum.php";
	}
    </script>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::fotos</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!-- cabecera -->
        <?php $titulo = 'cms - administración - fotos'; require('cabecera.php'); ?>

        <!-- cuerpo -->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=700 align=center>
            <tr>
                <!-- menu -->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                
				<!-- contenido -->
                <td width=850 style="vertical-align:top">
                    
                    <table border=0 width=850>
                        <tr align="right">
                            <td width=850 bgcolor=#ffffff><a href="#" onClick="nuevo();"><font face="arial" style="font-size: 11px;; color: blue;">Introduce nuevo álbum de fotos</font></a></td>
                        </tr>
                    </table>
                    
                    <table width=850>
                        <tr height=10>
                            <td width=50  bgcolor=#c8c8c8><font class="admin_font">Id       </font></td>
                            <td width=300 bgcolor=#c8c8c8><font class="admin_font">Álbum    </font></td>
							<td width=300 bgcolor=#c8c8c8><font class="admin_font">Ubicación</font></td>
                            <td width=50  bgcolor=#c8c8c8><font class="admin_font">Fotos    </font></td>
							<td width=75  bgcolor=#c8c8c8>
								<font class="admin_font">&nbsp;</font>
							</td>
							<td width=75  bgcolor=#c8c8c8>
								<font class="admin_font">&nbsp;</font>
							</td>
                        </tr>

						<?php
						
						$album = obtenerAlbum();
						
						for($i=0;$i<numRows("CMS_album");$i++) {
							$_POST["idalbum"]   = $album[$i]->idalbum;
							$_POST["nombre"]    = $album[$i]->nombre;
							$_POST["ubicacion"] = $album[$i]->ubicacion;
							$_POST["numero"]    = $album[$i]->numero;
							?>

							<form name="album" action="guardarAlbum.php" method=post>
								<tr height=10>                                
									<td width=50  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["idalbum"]; ?>  </font></td>
									<td width=300 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["nombre"]; ?>   </font></td>
									<td width=300 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["ubicacion"]; ?></font></td>
									<td width=50  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["numero"]; ?>   </font></td>									
									
									<td width=75  bgcolor=#f8f8f8>
										<a href="#" onClick="modificar(<?php echo $_POST["idalbum"]; ?>);">
											<font face="arial" style="font-size: 11px;; color: blue;">añadir fotos</font>
										</a>
									</td>
									<td width=75  bgcolor=#f8f8f8>
										<a href="#" onClick="eliminar(<?php echo $_POST["idalbum"]; ?>);">
											<font face="arial" style="font-size: 11px;; color: blue;">eliminar</font>
										</a>
									</td>
								</tr>
							</form>
							
							<?php
						}
						?>
                    </table>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>
