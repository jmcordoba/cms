<?php

// Incluímos Objetos necesarios
require("objetos/temp.php");

// Incluímos funciones necesarias
require("fun/funciones.php");

// Obtenemos los datos del formulario
$tempo->idtemp         = init("idtemp");
$tempo->name           = init("name");
$tempo->yearstart      = init("yearstart");
$tempo->yearfinish     = init("yearfinish");
$tempo->fede           = init("fede");

// Guardamos los cambios si es posible y redireccionamos en función del resultado
if(modificarTemp($tempo,$tempo->idtemp)==true) redirect("index.php?origen=temp" ,0);
else                                           redirect("index.php?origen=error",0);

?>