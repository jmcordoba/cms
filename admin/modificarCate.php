<?php

// Incluímos Objetos necesarios
require("objetos/cate.php");
require("objetos/temp.php");

// Incluímos funciones necesarias
require("fun/funciones.php");

// Obtenemos los datos del formulario
$categoria->idcat     = init("idcat");
$categoria->name      = init("name");
$categoria->numteams  = init("numteams");
$categoria->wpoints   = init("wpoints");
$categoria->dpoints   = init("dpoints");
$categoria->lpoints   = init("lpoints");
$categoria->jornadas  = init("jornadas");
$categoria->idavuelta = init("idavuelta");
$categoria->idtemp    = init("idtemp");

$categoria->rival01   = init("rival01");
$categoria->rival02   = init("rival02");
$categoria->rival03   = init("rival03");
$categoria->rival04   = init("rival04");
$categoria->rival05   = init("rival05");
$categoria->rival06   = init("rival06");
$categoria->rival07   = init("rival07");
$categoria->rival08   = init("rival08");
$categoria->rival09   = init("rival09");
$categoria->rival10   = init("rival10");
$categoria->rival11   = init("rival11");
$categoria->rival12   = init("rival12");
$categoria->rival13   = init("rival13");
$categoria->rival14   = init("rival14");
$categoria->rival15   = init("rival15");
$categoria->rival16   = init("rival16");
$categoria->rival17   = init("rival17");
$categoria->rival18   = init("rival18");
$categoria->rival19   = init("rival19");
$categoria->rival20   = init("rival20");

// Guardamos los cambios si es posible y redireccionamos en función del resultado
if(modificarCate($categoria,$categoria->idcat)==true) redirect("index.php?origen=cate" ,0);
else                                                  redirect("index.php?origen=error",0);

?>
