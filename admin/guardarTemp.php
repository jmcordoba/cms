<?php

// Incluye los objetos necesarios
require("objetos/temp.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Capturamos los datos que vienen por POST y los guardamos en el objeto equipo
$tempo->name           = init("name");
$tempo->yearstart      = init("yearstart");
$tempo->yearfinish     = init("yearfinish");
$tempo->fede           = init("fede");

// Intentamos guardar el usuario y redirigimos en función de si hemos tenido éxito o no
if(guardarTemp($tempo)==true) redirect("index.php?origen=temp",0);
else                          redirect("index.php?origen=error",0);
?>
