<?php

// Incluye los objetos necesarios
require("objetos/temp.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <script language="JavaScript" type="text/javascript">
	function modificar(id){
        document.location.href="02modTemp.php?idtemp=" + id;
	}
    function eliminar(id) {
        document.location.href="02delTemp.php?idtemp=" + id;
	}
    function nuevo() {
        document.location.href="02newTemp.php";
	}
    </script>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::temporadas</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - temporadas'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=700 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <table border=0 width=850>
                        <tr align="right">
                            <td width=850 bgcolor=#ffffff>
								<a href="#" onClick="nuevo();">
									<font face="arial" style="font-size: 11px;; color: blue;">Introduce nueva temporada</font>
								</a>
							</td>
                        </tr>
                    </table>
                    
                    <table width=850>
                        <tr height=10>
                            <td width=3   bgcolor=#c8c8c8><font class="admin_font">Id</font></td>
                            <td width=200 bgcolor=#c8c8c8><font class="admin_font">Temporada</font></td>
                            <td width=50  bgcolor=#c8c8c8><font class="admin_font">Inicio</font></td>
                            <td width=50  bgcolor=#c8c8c8><font class="admin_font">Fin</font></td>
                            <td width=200 bgcolor=#c8c8c8><font class="admin_font">Federación</font></td>
                            <td width=20  bgcolor=#c8c8c8>&nbsp;</td>
                            <td width=20  bgcolor=#c8c8c8>&nbsp;</td>
                        </tr>

						<?php
						$temporada = obtenerTemp();
						
						for($i=0;$i<numRows("CMS_temp");$i++) {
						
							$_POST["idtemp"]     = $temporada[$i]->idtemp;
							$_POST["name"]       = $temporada[$i]->name;
							$_POST["yearstart"]  = $temporada[$i]->yearstart;
							$_POST["yearfinish"] = $temporada[$i]->yearfinish;
							$_POST["fede"]       = $temporada[$i]->fede;
							?>

							<form name="datos" action="guardarTemp.php" method=post>
								<tr height=10>
									<td width=3   bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["idtemp"];?>    </font></td>
									<td width=200 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["name"];?>      </font></td>
									<td width=50  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["yearstart"];?> </font></td>
									<td width=50  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["yearfinish"];?></font></td>
									<td width=200 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["fede"];?>      </font></td>
									
									<td width=20  bgcolor=#f8f8f8>
										<a href="#" onClick="modificar(<?php echo $_POST["idtemp"]; ?>);">
											<font face="arial" style="font-size: 11px;; color: blue;">modificar</font>
										</a>
									</td>
									<td width=20  bgcolor=#f8f8f8>
										<a href="#" onClick="eliminar(<?php echo $_POST["idtemp"]; ?>);">
											<font face="arial" style="font-size: 11px;; color: blue;">eliminar</font>
										</a>
									</td>
								</tr>
							</form>
							<?php
						}
						?>
                    </table>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>
