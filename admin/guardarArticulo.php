<?php
// Incluye los objetos necesarios
require("objetos/articulo.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Obtenemos las variables del formulario
$articulo->idart     = init("idart");
$articulo->fecha     = date("d/m/Y");
$articulo->iduser    = $_COOKIE["cmsweb_user"];
$articulo->titulo    = str_replace("'","\'",init("titulo"));
$articulo->contenido = nl2br(str_replace("'","\'",$_POST["contenido"]));
$articulo->idtag     = init("idtag");
$articulo->fechasi   = init("fechasi");
$articulo->fechano   = init("fechano");
$articulo->conta     = "";

// Inicializamos la variable numero de filas a cero
$numero = 0;

// Capturamos los datos del fichero adjunto
$nombre_archivo = $_FILES['imagen']['name']; 
$tipo_archivo   = $_FILES['imagen']['type']; 
$tamano_archivo = $_FILES['imagen']['size'];

// Conexión con la base de datos
$link=Conectarse();
// Construcción de la query
$sql = "select * from `CMS_articulos` order by idart";
// Registro de log
wlog("guardarArticulo",$sql,1);
// Ejecutamos la query y obtenemos el resultado
$result = mysql_query($sql, $link);

// Obtenemos el ultimo idtemp
if ($row = mysql_fetch_array($result)) {
    do{
        $numero = $row["idart"];
    } while ($row = mysql_fetch_array($result));
}
$numero++;
// Cerramos la conexion con la base de datos
mysql_close($link);

// Si viene un fichero adjunto
if($nombre_archivo!="") {
    //compruebo si las caracter�sticas del archivo son las que deseo 
    if ($tamano_archivo < 100000) {
        // Redimensionamos el fichero a 200x200
		include('simpleImage.php');
		$image = new SimpleImage();
		$image->load($_FILES['imagen']['tmp_name']);
		$image->resize(426,320);
		$image->save($_FILES['imagen']['tmp_name']);
		// Inicializamos la ruta y el nombre del avatar del usuario
        $articulo->imagen    = "images/articulos/art" . $numero . ".gif";
        // Guardamos el fichero en la ruta especificada
        if (!move_uploaded_file($_FILES['imagen']['tmp_name'], "images/articulos/art" . $numero . ".gif")){
            // Si hay algún tipo de error, redirigimos a otra página
			?><script>location.href='index.php?origen=error';</script><?php
        }
    } else {
		?>
		<script>
			alert('La imagen que desea subir es mayor de 100kB.\nPor favor, suba una imagen con un peso inferior.');
		</script>
		<?php
	}
}

// Intentamos guardar el usuario y redirigimos en función de si hemos tenido éxito o no
if(guardarArticulo($articulo)==true) redirect("index.php?origen=arti",0);
else                                 redirect("index.php?origen=error",0);