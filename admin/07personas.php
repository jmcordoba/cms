<?php

// Incluye los objetos necesarios
require("objetos/persona.php");
require("objetos/equipo.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Control de usuario identificado
require("check.php");

// obtenemos el id del equipo seleccionado en el desplegable si es que se ha hecho
$idteam = isset($_REQUEST['idteam']) ? $_REQUEST['idteam'] : "";

?>

<!DOCTYPE html>

<html>
    <script language="JavaScript" type="text/javascript">
	function modificar(id) {
		document.location.href="07modPersona.php?idperson=" + id;
	}
	function eliminar(id) {
		document.location.href="07delPersona.php?idperson=" + id;
	}
	function nuevo() {
		document.location.href="07newPersona.php";
	}
    </script>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::personas</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - personas'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=700 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <table border=0 width=850>
                        <tr align="right">
                            <td width=850 bgcolor=#ffffff>
								<a href="#" onClick="nuevo();">
									<font face="arial" style="font-size: 11px;; color: blue;">Introduce nueva persona</font>
								</a>
							</td>
                        </tr>
                    </table>
                    
                    <table width=850>
						<tr>
							<td height=20 width=200 bgcolor=#ffffff><font face="arial" style="font-size: 11px;; color: black;"><b>Selecciona el equipo:</b></font></td>
							<td height=20 width=650 bgcolor=#ffffff>
								<form name="datos" action="07personas.php" method="post">
									<font face="verdana" color="red" style="margin-left: 0px;font-size: 12;">
										
										<select name="idteam" onchange="this.form.submit()" style="width:650px">
											<?php
											if(strlen($idteam)==0) $selected = " selected ";
											else            	   $selected = "";
											?>
											<option value="" <?php echo $selected; ?> >Todas los equipos</option>
											<?php
											$equipo = obtenerEquipo();
											for ($i=0;$i<numRows("CMS_cate");$i++) {
												if($idteam==$equipo[$i]->idteam) $selected = " selected ";
												else 							 $selected = " ";
												echo"<option $selected value=\"" . $equipo[$i]->idteam . "\">" . $equipo[$i]->nombre . "</option>";
											}
											?>
										</select>
										
									</font>
								</form>
							</td>
						</tr>
					</table>
                    
                    <table width=850>
                        <tr height=10>
                            <td width=50  bgcolor=#c8c8c8><font class="admin_font">Idteam</font></td>
                            <td width=350 bgcolor=#c8c8c8><font class="admin_font">Nombre</font></td>
                            <td width=350 bgcolor=#c8c8c8><font class="admin_font">Apellidos</font></td>
							<td width=50  bgcolor=#c8c8c8>&nbsp;</td>
                            <td width=50  bgcolor=#c8c8c8>&nbsp</td>
                        </tr>

                    <?php
					
					if($idteam!="") {
						$person = obtenerPersonaPorEquipo($idteam);
						$num    = count($person);
					}
					else {
						$person = obtenerPersona();
						$num    = count($person);
                    }
					
                    for($i=0;$i<$num;$i++) {
					
                        $_POST["idperson"]  = $person[$i]->idperson;
                        $_POST["idteam"]    = $person[$i]->idteam;
                        $_POST["nombre"]    = $person[$i]->nombre;
                        $_POST["surname"]   = $person[$i]->surname;
                        $_POST["foto"]      = $person[$i]->foto;
                        $_POST["fechan"]    = $person[$i]->fechan;
						$_POST["clubyears"] = $person[$i]->clubyears;
                        $_POST["cargo"]     = $person[$i]->cargo;
                        $_POST["palmares"]  = $person[$i]->palmares;
                        $_POST["facebook"]  = $person[$i]->facebook;
                        $_POST["twitter"]   = $person[$i]->twitter;
                        $_POST["rol"]       = $person[$i]->rol;
						$_POST["fechaalta"] = $person[$i]->fechaalta;
                        $_POST["fechabaja"] = $person[$i]->fechabaja;
                        $_POST["conta"]     = $person[$i]->conta;
                        ?>

                        <form name="datos" action="guardarPersona.php" method=post>
                            <tr height=10>                                
                                <td width=50  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["idteam"];?>  </font></td>
                                <td width=350 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["nombre"];?>  </font></td>
                                <td width=350 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["surname"];?> </font></td>
								
                                <td width=50  bgcolor=#f8f8f8>
									<a href="#" onClick="modificar(<?php echo $_POST["idperson"]; ?>);">
										<font face="arial" style="font-size: 11px;padding:4px;color: blue;">modificar</font>
									</a>
								</td>
                                <td width=50  bgcolor=#f8f8f8>
									<a href="#" onClick="eliminar(<?php echo $_POST["idperson"]; ?>);">
										<font face="arial" style="font-size: 11px;padding:4px;color: blue;">eliminar</font>
									</a>
								</td>
                            </tr>
                        </form>
						<?php
                    }
                    ?>
                    </table>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>