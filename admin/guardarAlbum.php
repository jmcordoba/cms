<?php

// Incluye los objetos necesarios
require("objetos/album.php");
// Incluye las funciones necesarios
require("fun/funciones.php");

// Obtenemos las variables del formulario
$album->idalbum   = init("idalbum");
$album->nombre    = init("nombre");
$album->ubicacion = "images/album/".str_replace(" ","-",$album->nombre);
$album->numero    = init("numero");

// Creamos la carpeta que acojera las fotos del album
$resultado = mkdir($album->ubicacion, 0777,true);

// Intentamos guardar el usuario y redirigimos en función de si hemos tenido éxito o no
if(guardarAlbum($album)==true) redirect("index.php?origen=album" ,0);
else                           redirect("index.php?origen=error",0);
?>
