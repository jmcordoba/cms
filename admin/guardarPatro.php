<?php

// Incluye los objetos necesarios
require("objetos/patro.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Capturamos los datos que vienen por POST y los guardamos en el objeto equipo
$patro->idpatro    = init("idpatro");
$patro->nombre     = str_replace("'","\'",init("nombre"));
$patro->fecha_alta = init("fecha_alta");
$patro->fecha_baja = init("fecha_baja");
$patro->importe    = init("importe");
$patro->web        = init("web");
$patro->conta      = init("conta");
$patro->imagen     = "";

// Capturamos los datos del fichero adjunto
$nombre_archivo = $_FILES['imagen']['name']; 
$tipo_archivo   = $_FILES['imagen']['type']; 
$tamano_archivo = $_FILES['imagen']['size'];

// Inicializamos la variable numero de filas a cero
$numero = 0;
// Conexión con la base de datos
$link=Conectarse();
// Construcción de la query
$sql = "select * from `CMS_patro` order by idpatro";
// Registro de log
wlog("guardarPatro",$sql,1);
// Ejecutamos la query y obtenemos el resultado
$result = mysql_query($sql, $link);
// Obtenemos el ultimo idtemp
if ($row = mysql_fetch_array($result))
{
    do{
        $numero = $row["idpatro"];
    } while ($row = mysql_fetch_array($result));
}
$numero++;
// Cerramos la conexion con la base de datos
mysql_close($link);
// Si viene un fichero adjunto
if($nombre_archivo!="") {
    //compruebo si las caracter�sticas del archivo son las que deseo 
    if ($tamano_archivo < 100000) {
        // Guardamos la ruta del fichero en la base de datos
        $patro->imagen = "admin/images/patrocinadores/patro" . $numero . ".gif";
        // Guardamos el fichero en la ruta especificada
        if (!move_uploaded_file($_FILES['imagen']['tmp_name'], "images/patrocinadores/patro" . $numero . ".gif")){
            // Si hay algún tipo de error, redirigimos a otra página
            ?><script>location.href='index.php?origen=error';</script><?php
        }
    } else {
		?>
		<script>
			alert('La imagen que desea subir es mayor de 100kB.\nPor favor, suba una imagen con un peso inferior.');
		</script>
		<?php
	}
}
// Intentamos guardar el usuario y redirigimos en función de si hemos tenido éxito o no
if(guardarPatro($patro)==true) redirect("index.php?origen=patro",0);
else                           redirect("index.php?origen=error",0);

?>