<?php

// Incluye los objetos necesarios
require("objetos/equipo.php");
require("objetos/persona.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::personas::modificar</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>
    
    <script>
        // Verifica los campos del formulario antes de guardar los datos
        function verificar_form() {
            if(document.datos.nombre.value.length==0) {
                alert("Error:\nDebe ingresar el nombre"); 
                document.datos.nombre.focus() 
                return 0; 
            }
			else {
                document.forms['datos'].submit();
            }
        }
    </script>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - personas - modificar'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=1000 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <?php
                    
                    $idperson = $_GET["idperson"];
                    
                    $person = obtenerUnaPersona($idperson);
                    
                    $_POST["idperson"]  = $person->idperson;
					$_POST["idteam"]    = $person->idteam;
					$_POST["nombre"]    = $person->nombre;
					$_POST["surname"]   = $person->surname;
					$_POST["foto"]      = $person->foto;
					$_POST["fechan"]    = $person->fechan;
					$_POST["clubyears"] = $person->clubyears;
					$_POST["cargo"]     = $person->cargo;
					$_POST["palmares"]  = $person->palmares;
					$_POST["facebook"]  = $person->facebook;
					$_POST["twitter"]   = $person->twitter;
					$_POST["rol"]       = $person->rol;
					$_POST["fechaalta"] = $person->fechaalta;
					$_POST["fechabaja"] = $person->fechabaja;
					$_POST["conta"]     = $person->conta;
					
					$team = obtenerUnEquipo($person->idteam);
                    ?>

                    <form name="datos" action="modificarPersona.php" method=post enctype="multipart/form-data">
                        
						<input type="hidden" name="idperson" value="<?php echo $idperson;?>">
						
						<table border=0 width=850>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="07personas.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <table width=850>
                            
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">idteam</font></td>
                                <td height=20 width=700 bgcolor=#ffffff>
                                    <select name="idteam">
                                        <option value="<?php echo $_POST["idteam"];?>" selected><?php echo $team->nombre; ?></option>
                                        <?php
                                        $equipos = obtenerEquipo();

                                        for ($i=0;$i<numRows("CMS_equipos");$i++)
                                        {
											if($equipos[$i]->idteam!=$team->idteam) {
												echo"<option value=\"" . $equipos[$i]->idteam . "\">" . $equipos[$i]->nombre . "</option>";
											}
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Nombre</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=nombre size=108 value="<?php echo $_POST["nombre"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Apellidos</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=surname size=108 value="<?php echo $_POST["surname"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Fecha de nacimiento</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=fechan size=108 value="<?php echo $_POST["fechan"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Años en el club</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=clubyears size=108 value="<?php echo $_POST["clubyears"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Actividad</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=cargo size=108 value="<?php echo $_POST["cargo"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Palmarés</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=palmares size=108 value="<?php echo $_POST["palmares"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Facebook</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=facebook size=108 value="<?php echo $_POST["facebook"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Twitter</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=twitter size=108 value="<?php echo $_POST["twitter"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Rol</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=rol size=108 value="<?php echo $_POST["rol"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Fecha de alta</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=fechaalta size=108 value="<?php echo $_POST["fechaalta"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Fecha de baja</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=fechabaja size=108 value="<?php echo $_POST["fechabaja"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Foto</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=foto size=108 value="<?php echo $_POST["foto"];?>" readonly></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Nueva foto</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input type="file" name="userfile" style="width:400px;border:0px solid #000000;"></input></td>
                            </tr>
                            <tr>
                                <td bgcolor=#ffffff><a href="#" onclick="verificar_form();"><font face="arial" style="font-size: 11px;; color: blue;">Guardar persona</font></a></td>
                            </tr>
                        </table>
                        <?php
                        if ($_POST["foto"]!="") {
                            ?>
                            <table width=850>
                                <tr valign="bottom" align="left">
                                    <td width=400 bgcolor=#ffffff><img width="240"  height="320" src="<?php echo conf_RUTA.$_POST["foto"];?>"></td>
                                </tr>
                            </table>
                            <?php
                        }
                        ?>
                    </form>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>