<?php

// Incluye los objetos necesarios
require("objetos/insta.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <script language="JavaScript" type="text/javascript">
	function modificar(id){
        document.location.href="08modInsta.php?idinst=" + id;
	}
    function eliminar(id){
        document.location.href="08delInsta.php?idinst=" + id;
	}
    function nuevo(){
        document.location.href="08newInsta.php";
	}
    </script>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::instalaciones</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - instalaciones'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=700 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <table border=0 width=850>
                        <tr align="right">
                            <td width=850 bgcolor=#ffffff><a href="#" onClick="nuevo();"><font face="arial" style="font-size: 11px;; color: blue;">Introduce nueva instalación</font></a></td>
                        </tr>
                    </table>
                    
                    <table width=850>
                        <tr height=10>
                            <td width=50  bgcolor=#c8c8c8><font class="admin_font">Idinst</font></td>
                            <td width=350 bgcolor=#c8c8c8><font class="admin_font">Instalación</font></td>
                            <td width=350 bgcolor=#c8c8c8><font class="admin_font">Dirección</font></td>
                            <td width=50  bgcolor=#c8c8c8>&nbsp;</td>
                            <td width=50  bgcolor=#c8c8c8>&nbsp;</td>
                        </tr>

						<?php
						$instalacion = obtenerInsta();
						
						for($i=0;$i<numRows("CMS_insta");$i++) {
						
							$_POST["idinst"]    = $instalacion[$i]->idinst;
							$_POST["name"]      = $instalacion[$i]->name;
							$_POST["direccion"] = $instalacion[$i]->direccion;
							$_POST["telefono"]  = $instalacion[$i]->telefono;
							$_POST["mail"]      = $instalacion[$i]->mail;
							$_POST["persona"]   = $instalacion[$i]->persona;
							$_POST["conta"]     = $instalacion[$i]->conta;
							$_POST["foto"]      = $instalacion[$i]->foto;
							?>

							<form name="datos" action="guardarInsta.php" method="post">
								<tr height=10>                                
									<td width=50  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["idinst"];?>   </font></td>
									<td width=350 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["name"];?>     </font></td>
									<td width=350 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["direccion"];?></font></td>
									
									<td width=50  bgcolor=#f8f8f8>
										<a href="#" onClick="modificar(<?php echo $_POST["idinst"]; ?>);">
											<font face="arial" style="font-size: 11px;; color: blue;">modificar</font>
										</a>
									</td>
									<td width=50  bgcolor=#f8f8f8>
										<a href="#" onClick="eliminar(<?php echo $_POST["idinst"]; ?>);">
											<font face="arial" style="font-size: 11px;; color: blue;">eliminar</font>
										</a>
									</td>
								</tr>
							</form>
							<?php
						}
						?>
                    </table>
                </td>
            </tr>		
        </table>
    </body>
</html>
