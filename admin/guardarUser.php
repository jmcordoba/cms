<?php

// Incluye los objetos necesarios
require("objetos/usuario.php");
// Incluye las funciones necesarios
require("fun/funciones.php");

// Obtenemos las variables del formulario
$usuario->nombre     = init("nombre");
$usuario->apellidos  = init("apellidos");
$usuario->usuario    = init("usuario");
$usuario->password   = init("password");
$usuario->telefono   = init("telefono");
$usuario->mail       = init("mail");
$usuario->permisos   = init("permisos");
$usuario->conta      = init("conta");
$usuario->validado   = init("validado");
$usuario->fecha_alta = date("d/m/Y"); 
$usuario->foto       = "";

// Capturamos los datos del fichero adjunto
$nombre_archivo = $_FILES['foto']['name']; 
$tipo_archivo   = $_FILES['foto']['type']; 
$tamano_archivo = $_FILES['foto']['size'];

// Si viene un fichero adjunto
if($nombre_archivo!="") {
    //compruebo si las caracter�sticas del archivo son las que deseo 
    if ($tamano_archivo < 100000) {
        // Redimensionamos el fichero a 200x200
		include('simpleImage.php');
		$image = new SimpleImage();
		$image->load($_FILES['foto']['tmp_name']);
		$image->resize(60,60);
		$image->save($_FILES['foto']['tmp_name']);
		// Inicializamos la ruta y el nombre del avatar del usuario
        $usuario->foto    = "admin/images/usuario/" . $usuario->mail . ".gif";
        // Guardamos el fichero en la ruta especificada
        if (!move_uploaded_file($_FILES['foto']['tmp_name'], "images/usuario/" . $usuario->mail . ".gif")){
            // Si hay algún tipo de error, redirigimos a otra página
            ?><script>location.href='index.php?origen=error';</script><?php
        }
    } else {
		?>
		<script>
			alert('La imagen que desea subir es mayor de 100kB.\nPor favor, suba una imagen con un peso inferior.');
		</script>
		<?php
	}
}

// Intentamos guardar el usuario y redirigimos en función de si hemos tenido éxito o no
if(guardarUser($usuario)==true) redirect("index.php?origen=user",0);
else                            redirect("index.php?origen=error",0);

?>
