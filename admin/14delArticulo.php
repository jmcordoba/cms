<?php

// Incluimos objetos necesarios
require("objetos/articulo.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
// Capturamos el id de la categoría a eliminar
$idart = $_GET["idart"];
// Eliminamos categoria y redirigimos
if(borrarArticulo($idart)==true) redirect("index.php?origen=arti" ,0);
else                             redirect("index.php?origen=error",0);

?>
