<?php

// Incluye los objetos necesarios
require("objetos/temp.php");
require("objetos/cate.php");
require("objetos/jornada.php");
require("objetos/insta.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
	<script language="JavaScript" type="text/javascript">
		function modificar(idcate,idjorn) {
			document.location.href="04editJornada.php?idcate=" + idcate + "&idjorn=" + idjorn;
		}
		function eliminar(idcate,idjorn) {
			document.location.href="04delJornada.php?idcate=" + idcate + "&idjorn=" + idjorn;
		}
    </script>
    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::jornada::editar</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - jornada - editar'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=700 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
					
					<?php
					$idcat      = $_GET["idcate"];
					$idjorn     = $_GET["idjorn"];
					$categoria  = obtenerUnaCate($idcat);
					$jornada    = obtenerUnaJornada($idcat,$idjorn);
					
					$_nombre1   = array();
					$_nombre2   = array();
					$_insta     = array();
					$_plocal    = array();
					$_pvisit    = array();
					$_fecha     = array();
					$_hour      = array();
					$_plocal1    = array();
					$_plocal2    = array();
					$_plocal3    = array();
					$_plocal4    = array();
					$_plocal5    = array();
					$_pvisit1    = array();
					$_pvisit2    = array();
					$_pvisit3    = array();
					$_pvisit4    = array();
					$_pvisit5    = array();
					
					$_nombre1[] = $jornada->local01;
					$_nombre2[] = $jornada->visit01;
					$_insta[]   = $jornada->inst01;
					$_plocal[]  = $jornada->plocal01;
					$_pvisit[]  = $jornada->pvisit01;
					$_fecha[]   = $jornada->fecha01;
					$_hour[]    = $jornada->hour01;
					$_plocal1[] = $jornada->plocal011;
					$_plocal2[] = $jornada->plocal012;
					$_plocal3[] = $jornada->plocal013;
					$_plocal4[] = $jornada->plocal014;
					$_plocal5[] = $jornada->plocal015;
					$_pvisit1[] = $jornada->pvisit011;
					$_pvisit2[] = $jornada->pvisit012;
					$_pvisit3[] = $jornada->pvisit013;
					$_pvisit4[] = $jornada->pvisit014;
					$_pvisit5[] = $jornada->pvisit015;

					$_nombre1[] = $jornada->local02;
					$_nombre2[] = $jornada->visit02;
					$_insta[]   = $jornada->inst02;
					$_plocal[]  = $jornada->plocal02;
					$_pvisit[]  = $jornada->pvisit02;
					$_fecha[]   = $jornada->fecha02;
					$_hour[]    = $jornada->hour02;
					$_plocal1[] = $jornada->plocal021;
					$_plocal2[] = $jornada->plocal022;
					$_plocal3[] = $jornada->plocal023;
					$_plocal4[] = $jornada->plocal024;
					$_plocal5[] = $jornada->plocal025;
					$_pvisit1[] = $jornada->pvisit021;
					$_pvisit2[] = $jornada->pvisit022;
					$_pvisit3[] = $jornada->pvisit023;
					$_pvisit4[] = $jornada->pvisit024;
					$_pvisit5[] = $jornada->pvisit025;

					$_nombre1[] = $jornada->local03;
					$_nombre2[] = $jornada->visit03;
					$_insta[]   = $jornada->inst03;
					$_plocal[]  = $jornada->plocal03;
					$_pvisit[]  = $jornada->pvisit03;
					$_fecha[]   = $jornada->fecha03;
					$_hour[]    = $jornada->hour03;
					$_plocal1[] = $jornada->plocal031;
					$_plocal2[] = $jornada->plocal032;
					$_plocal3[] = $jornada->plocal033;
					$_plocal4[] = $jornada->plocal034;
					$_plocal5[] = $jornada->plocal035;
					$_pvisit1[] = $jornada->pvisit031;
					$_pvisit2[] = $jornada->pvisit032;
					$_pvisit3[] = $jornada->pvisit033;
					$_pvisit4[] = $jornada->pvisit034;
					$_pvisit5[] = $jornada->pvisit035;

					$_nombre1[] = $jornada->local04;
					$_nombre2[] = $jornada->visit04;
					$_insta[]   = $jornada->inst04;
					$_plocal[]  = $jornada->plocal04;
					$_pvisit[]  = $jornada->pvisit04;
					$_fecha[]   = $jornada->fecha04;
					$_hour[]    = $jornada->hour04;
					$_plocal1[] = $jornada->plocal041;
					$_plocal2[] = $jornada->plocal042;
					$_plocal3[] = $jornada->plocal043;
					$_plocal4[] = $jornada->plocal044;
					$_plocal5[] = $jornada->plocal045;
					$_pvisit1[] = $jornada->pvisit041;
					$_pvisit2[] = $jornada->pvisit042;
					$_pvisit3[] = $jornada->pvisit043;
					$_pvisit4[] = $jornada->pvisit044;
					$_pvisit5[] = $jornada->pvisit045;

					$_nombre1[] = $jornada->local05;
					$_nombre2[] = $jornada->visit05;
					$_insta[]   = $jornada->inst05;
					$_plocal[]  = $jornada->plocal05;
					$_pvisit[]  = $jornada->pvisit05;
					$_fecha[]   = $jornada->fecha05;
					$_hour[]    = $jornada->hour05;
					$_plocal1[] = $jornada->plocal051;
					$_plocal2[] = $jornada->plocal052;
					$_plocal3[] = $jornada->plocal053;
					$_plocal4[] = $jornada->plocal054;
					$_plocal5[] = $jornada->plocal055;
					$_pvisit1[] = $jornada->pvisit051;
					$_pvisit2[] = $jornada->pvisit052;
					$_pvisit3[] = $jornada->pvisit053;
					$_pvisit4[] = $jornada->pvisit054;
					$_pvisit5[] = $jornada->pvisit055;

					$_nombre1[] = $jornada->local06;
					$_nombre2[] = $jornada->visit06;
					$_insta[]   = $jornada->inst06;
					$_plocal[]  = $jornada->plocal06;
					$_pvisit[]  = $jornada->pvisit06;
					$_fecha[]   = $jornada->fecha06;
					$_hour[]    = $jornada->hour06;
					$_plocal1[] = $jornada->plocal061;
					$_plocal2[] = $jornada->plocal062;
					$_plocal3[] = $jornada->plocal063;
					$_plocal4[] = $jornada->plocal064;
					$_plocal5[] = $jornada->plocal065;
					$_pvisit1[] = $jornada->pvisit061;
					$_pvisit2[] = $jornada->pvisit062;
					$_pvisit3[] = $jornada->pvisit063;
					$_pvisit4[] = $jornada->pvisit064;
					$_pvisit5[] = $jornada->pvisit065;

					$_nombre1[] = $jornada->local07;
					$_nombre2[] = $jornada->visit07;
					$_insta[]   = $jornada->inst07;
					$_plocal[]  = $jornada->plocal07;
					$_pvisit[]  = $jornada->pvisit07;
					$_fecha[]   = $jornada->fecha07;
					$_hour[]    = $jornada->hour07;
					$_plocal1[] = $jornada->plocal071;
					$_plocal2[] = $jornada->plocal072;
					$_plocal3[] = $jornada->plocal073;
					$_plocal4[] = $jornada->plocal074;
					$_plocal5[] = $jornada->plocal075;
					$_pvisit1[] = $jornada->pvisit071;
					$_pvisit2[] = $jornada->pvisit072;
					$_pvisit3[] = $jornada->pvisit073;
					$_pvisit4[] = $jornada->pvisit074;
					$_pvisit5[] = $jornada->pvisit075;

					$_nombre1[] = $jornada->local08;
					$_nombre2[] = $jornada->visit08;
					$_insta[]   = $jornada->inst08;
					$_plocal[]  = $jornada->plocal08;
					$_pvisit[]  = $jornada->pvisit08;
					$_fecha[]   = $jornada->fecha08;
					$_hour[]    = $jornada->hour08;
					$_plocal1[] = $jornada->plocal081;
					$_plocal2[] = $jornada->plocal082;
					$_plocal3[] = $jornada->plocal083;
					$_plocal4[] = $jornada->plocal084;
					$_plocal5[] = $jornada->plocal085;
					$_pvisit1[] = $jornada->pvisit081;
					$_pvisit2[] = $jornada->pvisit082;
					$_pvisit3[] = $jornada->pvisit083;
					$_pvisit4[] = $jornada->pvisit084;
					$_pvisit5[] = $jornada->pvisit085;

					$_nombre1[] = $jornada->local09;
					$_nombre2[] = $jornada->visit09;
					$_insta[]   = $jornada->inst09;
					$_plocal[]  = $jornada->plocal09;
					$_pvisit[]  = $jornada->pvisit09;
					$_fecha[]   = $jornada->fecha09;
					$_hour[]    = $jornada->hour09;
					$_plocal1[] = $jornada->plocal091;
					$_plocal2[] = $jornada->plocal092;
					$_plocal3[] = $jornada->plocal093;
					$_plocal4[] = $jornada->plocal094;
					$_plocal5[] = $jornada->plocal095;
					$_pvisit1[] = $jornada->pvisit091;
					$_pvisit2[] = $jornada->pvisit092;
					$_pvisit3[] = $jornada->pvisit093;
					$_pvisit4[] = $jornada->pvisit094;
					$_pvisit5[] = $jornada->pvisit095;

					$_nombre1[] = $jornada->local10;
					$_nombre2[] = $jornada->visit10;
					$_insta[]   = $jornada->inst10;
					$_plocal[]  = $jornada->plocal10;
					$_pvisit[]  = $jornada->pvisit10;
					$_fecha[]   = $jornada->fecha10;
					$_hour[]    = $jornada->hour10;
					$_plocal1[] = $jornada->plocal101;
					$_plocal2[] = $jornada->plocal102;
					$_plocal3[] = $jornada->plocal103;
					$_plocal4[] = $jornada->plocal104;
					$_plocal5[] = $jornada->plocal105;
					$_pvisit1[] = $jornada->pvisit101;
					$_pvisit2[] = $jornada->pvisit102;
					$_pvisit3[] = $jornada->pvisit103;
					$_pvisit4[] = $jornada->pvisit104;
					$_pvisit5[] = $jornada->pvisit105;
					?>

                    <form name="datos" action="modificarJornada.php" method=post>
                        <table border=0 width=850>
							<tr>
								<td colspan=1 height=20 width=850 bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">Editar jornada <?php echo $jornada->idjorn; ?> en la categoría <?php echo $categoria->name; ?></font></td>
							</tr>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="04newJornada.php?idcate=<?php echo $idcat; ?>">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <table width=850>

							<tr>
								<td height=20 width=80  bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">equipo     </font></td>
								<td height=20 width=20  bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">puntos     </font></td>
								
								<td height=20 width=2   bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">set1</font></td>
								<td height=20 width=2   bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">set2</font></td>
								<td height=20 width=2   bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">set3</font></td>
								<td height=20 width=2   bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">set4</font></td>
								<td height=20 width=2   bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">set5</font></td>
								
								<td height=20 width=25  bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">dia        </font></td>
								<td height=20 width=25  bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">hora       </font></td>
								<td height=20 width=100 bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">instalación</font></td>
							</tr>
							
							<input type="hidden" name="idcat"  value="<?php echo $idcat; ?>"> </input>
							<input type="hidden" name="idjorn" value="<?php echo $idjorn; ?>"></input>
							
							<?php
							for($j=0;$j<($categoria->numteams/2);$j++) {

								$nombre1 = 'local'  . str_pad($j+1, 2, "0", STR_PAD_LEFT);
								$nombre2 = 'visit'  . str_pad($j+1, 2, "0", STR_PAD_LEFT);
								$insta   = 'inst'   . str_pad($j+1, 2, "0", STR_PAD_LEFT);
								$plocal  = 'plocal' . str_pad($j+1, 2, "0", STR_PAD_LEFT);
								$pvisit  = 'pvisit' . str_pad($j+1, 2, "0", STR_PAD_LEFT);
								$fecha   = 'fecha'  . str_pad($j+1, 2, "0", STR_PAD_LEFT);
								$hour    = 'hour'   . str_pad($j+1, 2, "0", STR_PAD_LEFT);
								
								$plocal1  = 'plocal' . str_pad($j+1, 2, "0", STR_PAD_LEFT)."1";
								$plocal2  = 'plocal' . str_pad($j+1, 2, "0", STR_PAD_LEFT)."2";
								$plocal3  = 'plocal' . str_pad($j+1, 2, "0", STR_PAD_LEFT)."3";
								$plocal4  = 'plocal' . str_pad($j+1, 2, "0", STR_PAD_LEFT)."4";
								$plocal5  = 'plocal' . str_pad($j+1, 2, "0", STR_PAD_LEFT)."5";
								
								$pvisit1  = 'pvisit' . str_pad($j+1, 2, "0", STR_PAD_LEFT)."1";
								$pvisit2  = 'pvisit' . str_pad($j+1, 2, "0", STR_PAD_LEFT)."2";
								$pvisit3  = 'pvisit' . str_pad($j+1, 2, "0", STR_PAD_LEFT)."3";
								$pvisit4  = 'pvisit' . str_pad($j+1, 2, "0", STR_PAD_LEFT)."4";
								$pvisit5  = 'pvisit' . str_pad($j+1, 2, "0", STR_PAD_LEFT)."5";
								?>
								<!--
								Equipo local
								--->
								<tr>
									<td height=20 width=80 bgcolor=#f8f8f8>
										<select name="<?php echo $nombre1; ?>" style="font-size:11px;;">
											<option value="<?php echo $_nombre1[$j]; ?>" selected><?php echo $_nombre1[$j]; ?></option>
											<?php
												if(($categoria->rival01!="")and($categoria->rival01!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival01 . "\">" . $categoria->rival01 . "</option>";
												if(($categoria->rival02!="")and($categoria->rival02!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival02 . "\">" . $categoria->rival02 . "</option>";
												if(($categoria->rival03!="")and($categoria->rival03!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival03 . "\">" . $categoria->rival03 . "</option>";
												if(($categoria->rival04!="")and($categoria->rival04!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival04 . "\">" . $categoria->rival04 . "</option>";
												if(($categoria->rival05!="")and($categoria->rival05!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival05 . "\">" . $categoria->rival05 . "</option>";
												if(($categoria->rival06!="")and($categoria->rival06!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival06 . "\">" . $categoria->rival06 . "</option>";
												if(($categoria->rival07!="")and($categoria->rival07!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival07 . "\">" . $categoria->rival07 . "</option>";
												if(($categoria->rival08!="")and($categoria->rival08!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival08 . "\">" . $categoria->rival08 . "</option>";
												if(($categoria->rival09!="")and($categoria->rival09!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival09 . "\">" . $categoria->rival09 . "</option>";
												if(($categoria->rival10!="")and($categoria->rival10!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival10 . "\">" . $categoria->rival10 . "</option>";
												if(($categoria->rival11!="")and($categoria->rival11!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival11 . "\">" . $categoria->rival11 . "</option>";
												if(($categoria->rival12!="")and($categoria->rival12!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival12 . "\">" . $categoria->rival12 . "</option>";
												if(($categoria->rival13!="")and($categoria->rival13!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival13 . "\">" . $categoria->rival13 . "</option>";
												if(($categoria->rival14!="")and($categoria->rival14!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival14 . "\">" . $categoria->rival14 . "</option>";
												if(($categoria->rival15!="")and($categoria->rival15!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival15 . "\">" . $categoria->rival15 . "</option>";
												if(($categoria->rival16!="")and($categoria->rival16!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival16 . "\">" . $categoria->rival16 . "</option>";
												if(($categoria->rival17!="")and($categoria->rival17!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival17 . "\">" . $categoria->rival17 . "</option>";
												if(($categoria->rival18!="")and($categoria->rival18!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival18 . "\">" . $categoria->rival18 . "</option>";
												if(($categoria->rival19!="")and($categoria->rival19!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival19 . "\">" . $categoria->rival19 . "</option>";
												if(($categoria->rival20!="")and($categoria->rival20!=$_nombre1[$j])) echo"<option value=\"" . $categoria->rival20 . "\">" . $categoria->rival20 . "</option>";
											?>
										</select>
									</td>
									
									<td height=20 width=20 bgcolor=#f8f8f8><input align=left type=text name="<?php echo $plocal; ?>" size=3 value="<?php echo $_plocal[$j]; ?>"></input></td>
									
									<td height=20 width=2 bgcolor=#f8f8f8><input align=left type=text name="<?php echo $plocal1; ?>" size=2 maxlength=2 value="<?php echo $_plocal1[$j]; ?>"></input></td>
									<td height=20 width=2 bgcolor=#f8f8f8><input align=left type=text name="<?php echo $plocal2; ?>" size=2 maxlength=2 value="<?php echo $_plocal2[$j]; ?>"></input></td>
									<td height=20 width=2 bgcolor=#f8f8f8><input align=left type=text name="<?php echo $plocal3; ?>" size=2 maxlength=2 value="<?php echo $_plocal3[$j]; ?>"></input></td>
									<td height=20 width=2 bgcolor=#f8f8f8><input align=left type=text name="<?php echo $plocal4; ?>" size=2 maxlength=2 value="<?php echo $_plocal4[$j]; ?>"></input></td>
									<td height=20 width=2 bgcolor=#f8f8f8><input align=left type=text name="<?php echo $plocal5; ?>" size=2 maxlength=2 value="<?php echo $_plocal5[$j]; ?>"></input></td>
									
									<td height=20 width=25 bgcolor=#f8f8f8>dia</td>
									<td height=20 width=25 bgcolor=#f8f8f8>hora</input></td>
									<td height=20 width=100 bgcolor=#f8f8f8>instalacion</input></td>
								</tr>
								
								<!--
								Equipo visitante
								--->
								<tr>
									<td height=20 width=80 bgcolor=#f8f8f8>
										<select name="<?php echo $nombre2; ?>" style="font-size:11px;;">
											<option value="<?php echo $_nombre2[$j]; ?>" selected><?php echo $_nombre2[$j]; ?></option>
											<?php
												if($categoria->rival01!="") echo"<option value=\"" . $categoria->rival01 . "\">" . $categoria->rival01 . "</option>";
												if($categoria->rival02!="") echo"<option value=\"" . $categoria->rival02 . "\">" . $categoria->rival02 . "</option>";
												if($categoria->rival03!="") echo"<option value=\"" . $categoria->rival03 . "\">" . $categoria->rival03 . "</option>";
												if($categoria->rival04!="") echo"<option value=\"" . $categoria->rival04 . "\">" . $categoria->rival04 . "</option>";
												if($categoria->rival05!="") echo"<option value=\"" . $categoria->rival05 . "\">" . $categoria->rival05 . "</option>";
												if($categoria->rival06!="") echo"<option value=\"" . $categoria->rival06 . "\">" . $categoria->rival06 . "</option>";
												if($categoria->rival07!="") echo"<option value=\"" . $categoria->rival07 . "\">" . $categoria->rival07 . "</option>";
												if($categoria->rival08!="") echo"<option value=\"" . $categoria->rival08 . "\">" . $categoria->rival08 . "</option>";
												if($categoria->rival09!="") echo"<option value=\"" . $categoria->rival09 . "\">" . $categoria->rival09 . "</option>";
												if($categoria->rival10!="") echo"<option value=\"" . $categoria->rival10 . "\">" . $categoria->rival10 . "</option>";
												if($categoria->rival11!="") echo"<option value=\"" . $categoria->rival11 . "\">" . $categoria->rival11 . "</option>";
												if($categoria->rival12!="") echo"<option value=\"" . $categoria->rival12 . "\">" . $categoria->rival12 . "</option>";
												if($categoria->rival13!="") echo"<option value=\"" . $categoria->rival13 . "\">" . $categoria->rival13 . "</option>";
												if($categoria->rival14!="") echo"<option value=\"" . $categoria->rival14 . "\">" . $categoria->rival14 . "</option>";
												if($categoria->rival15!="") echo"<option value=\"" . $categoria->rival15 . "\">" . $categoria->rival15 . "</option>";
												if($categoria->rival16!="") echo"<option value=\"" . $categoria->rival16 . "\">" . $categoria->rival16 . "</option>";
												if($categoria->rival17!="") echo"<option value=\"" . $categoria->rival17 . "\">" . $categoria->rival17 . "</option>";
												if($categoria->rival18!="") echo"<option value=\"" . $categoria->rival18 . "\">" . $categoria->rival18 . "</option>";
												if($categoria->rival19!="") echo"<option value=\"" . $categoria->rival19 . "\">" . $categoria->rival19 . "</option>";
												if($categoria->rival20!="") echo"<option value=\"" . $categoria->rival20 . "\">" . $categoria->rival20 . "</option>";
											?>
										</select>
									</td>
									
									<td height=20 width=20 bgcolor=#f8f8f8><input align=left type=text name="<?php echo $pvisit; ?>" size=3 value="<?php echo $_pvisit[$j]; ?>"></input></td>
									
									<td height=20 width=2 bgcolor=#f8f8f8><input align=left type=text name="<?php echo $pvisit1; ?>" size=2 maxlength=2 value="<?php echo $_pvisit1[$j]; ?>"></input></td>
									<td height=20 width=2 bgcolor=#f8f8f8><input align=left type=text name="<?php echo $pvisit2; ?>" size=2 maxlength=2 value="<?php echo $_pvisit2[$j]; ?>"></input></td>
									<td height=20 width=2 bgcolor=#f8f8f8><input align=left type=text name="<?php echo $pvisit3; ?>" size=2 maxlength=2 value="<?php echo $_pvisit3[$j]; ?>"></input></td>
									<td height=20 width=2 bgcolor=#f8f8f8><input align=left type=text name="<?php echo $pvisit4; ?>" size=2 maxlength=2 value="<?php echo $_pvisit4[$j]; ?>"></input></td>
									<td height=20 width=2 bgcolor=#f8f8f8><input align=left type=text name="<?php echo $pvisit5; ?>" size=2 maxlength=2 value="<?php echo $_pvisit5[$j]; ?>"></input></td>
									
									<td height=20 width=25 bgcolor=#f8f8f8><input align=left type=text name="<?php echo $fecha;  ?>" size=8 value="<?php echo $_fecha[$j]; ?>"></input></td>
									<td height=20 width=25 bgcolor=#f8f8f8><input align=left type=text name="<?php echo $hour;   ?>" size=3 value="<?php echo $_hour[$j]; ?>"></input></td>
									
									<td height=20 width=100 bgcolor=#f8f8f8>
										<select name="<?php echo $insta; ?>" style="font-size:11px;;">
											<option value="<?php echo $_insta[$j]; ?>" selected="null"><?php echo $_insta[$j]; ?></option>
											<?php
												$insta = obtenerInsta();;

												for ($i=0;$i<numRows("CMS_insta");$i++) {
													echo"<option value=\"" . $insta[$i]->name . "\">" . $insta[$i]->name . "</option>";
												}
												?>
											?>
										</select>
									</td>
								</tr>
								
								<tr height="11px;"><td></td></tr>
								<?php
							}
							?>
						</table>
						
						<!-- Guardar -->
						<table width=850>
                            <tr><td colspan=6 bgcolor=#ffffff><a href="#" onclick="javascript:document.forms['datos'].submit(); return false;"><font face="arial" style="font-size: 11px;; color: blue;">Guardar jornada</font></a></td></tr>
                        </table>
                        
                    </form>
                </td>
            </tr>		
        </table>

        <!--- pie --->
        <!--?php require('pie.php'); ?-->
    </body>
</html>