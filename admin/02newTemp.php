<?php

// Incluye los objetos necesarios
require("objetos/temp.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::temporadas::nueva</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!-- cabecera -->
        <?php $titulo = 'cms - administración - temporadas - nueva'; require('cabecera.php'); ?>

        <!-- cuerpo -->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=628 align=center>
            <tr>
                <!-- menu -->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!-- contenido -->
                <td width=850 style="vertical-align:top">

                    <form name="datos" action="guardarTemp.php" method=post>
                        <table border=0 width=850>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="02temp.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <table width=850>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Temporada</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=name size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Inicio</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=yearstart size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Fin</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=yearfinish size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Federacion</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=fede size=108></input></td>
                            </tr>
                            <tr>
								<td bgcolor=#ffffff>
									<a href="#" onclick="javascript:document.forms['datos'].submit(); return false;">
										<font face="arial" style="font-size: 11px;; color: blue;">Guardar temporada</font>
									</a>
								</td>
							</tr>
                        </table>
                    </form>
                </td>
            </tr>		
        </table>

        <!-- pie -->
    </body>
</html>

<?php
