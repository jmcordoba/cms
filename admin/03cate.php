<?php

// Incluye los objetos necesarios
require("objetos/temp.php");
require("objetos/cate.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <script language="JavaScript" type="text/javascript">
	function modificar(id){
        document.location.href="03modCate.php?idcate=" + id;
	}
    function eliminar(id){
        document.location.href="03delCate.php?idcate=" + id;
	}
    function nuevo(){
        document.location.href="03newCate.php";
	}
    </script>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::categorías</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - categorías'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=700 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <table border=0 width=850>
                        <tr align="right">
                            <td width=850 bgcolor=#ffffff>
								<a href="#" onClick="nuevo();">
									<font face="arial" style="font-size: 11px;; color: blue;">Introduce nueva categoria</font>
								</a>
							</td>
                        </tr>
                    </table>
                    
                    <table width=850>
                        <tr height=10>
                            <td width=50  bgcolor=#c8c8c8><font class="admin_font">Id</font></td>
                            <td width=350 bgcolor=#c8c8c8><font class="admin_font">Idtemp</font></td>
							<td width=350 bgcolor=#c8c8c8><font class="admin_font">Categoría</font></td>
                            
                            <td width=50 bgcolor=#c8c8c8><font class="admin_font">&nbsp;</font></td>
                            <td width=50 bgcolor=#c8c8c8><font class="admin_font">&nbsp;</font></td>
                        </tr>


                    <?php
                    $categoria = obtenerCate();
                    
                    for($i=0;$i<numRows("CMS_cate");$i++)
                    {
                        $_POST["idcat"]         = $categoria[$i]->idcat;
                        $_POST["name"]          = $categoria[$i]->name;
                        $_POST["numteams"]      = $categoria[$i]->numteams;
                        $_POST["wpoints"]       = $categoria[$i]->wpoints;
                        $_POST["dpoints"]       = $categoria[$i]->dpoints;
                        $_POST["lpoints"]       = $categoria[$i]->lpoints;
                        $_POST["jornadas"]      = $categoria[$i]->jornadas;
                        $_POST["idavuelta"]     = $categoria[$i]->idavuelta;
                        $_POST["idtemp"]        = $categoria[$i]->idtemp;
                        ?>

                        <form name="datos" action="guardarCate.php" method=post>
                            <tr height=10>                                
                                <td width=50  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["idcat"];?>    </font></td>
								<td width=350 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["idtemp"];?>   </font></td>
                                <td width=350 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["name"];?>     </font></td>
								
                                <td width=50  bgcolor=#f8f8f8>
									<a href="#" onClick="modificar(<?php echo $_POST["idcat"]; ?>);">
										<font face="arial" style="font-size: 11px;; color: blue;">modificar</font>
									</a>
								</td>
                                <td width=50  bgcolor=#f8f8f8>
									<a href="#" onClick="eliminar(<?php echo $_POST["idcat"]; ?>);">
										<font face="arial" style="font-size: 11px;; color: blue;">eliminar</font>
									</a>
								</td>
                            </tr>
                        </form>
                    <?php
                    }
                    ?>
                    </table>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>
