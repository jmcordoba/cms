<?php

// Incluimos objetos necesarios
require("objetos/temp.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");

$temporada = $_GET["idtemp"];

if(borrarTemp($temporada)==true) redirect("index.php?origen=temp",0);
else                             redirect("index.php?origen=error",0);

?>
