<?php

// Incluímos Objetos necesarios
require("objetos/social.php");
// Incluímos funciones necesarias
require("fun/funciones.php");

// Obtenemos los datos del formulario
$social->idsocial = init("idsocial");
$social->nombre   = init("nombre");
$social->link     = init("link");
$social->icono    = init("icono");

// Capturamos los datos del fichero adjunto
$nombre_archivo = $_FILES['icono']['name']; 
$tipo_archivo   = $_FILES['icono']['type']; 
$tamano_archivo = $_FILES['icono']['size'];

// Si viene un fichero adjunto
if($nombre_archivo!="") {
    //compruebo si las caracter�sticas del archivo son las que deseo 
    if ($tamano_archivo < 100000) {
        // Inicializamos la ruta y el nombre del avatar del usuario
        $social->icono = "admin/images/sociales/social" . $social->idsocial . ".gif";
        // Guardamos el fichero en la ruta especificada
        if (!move_uploaded_file($_FILES['icono']['tmp_name'], "images/sociales/social" . $social->idsocial . ".gif")) {
            // Si hay algún tipo de error, redirigimos a otra página
            ?><script>location.href='index.php?origen=error';</script><?php
			die();
        }
    } else {
		?>
		<script>
			alert('La imagen que desea subir es mayor de 100kB.\nPor favor, suba una imagen con un peso inferior.');
		</script>
		<?php
	}
}

// Guardamos los cambios si es posible y redireccionamos en función del resultado
if(modificarSocial($social,$social->idsocial)==true) redirect("index.php?origen=social",0);
else                                                 redirect("index.php?origen=error" ,0);

?>