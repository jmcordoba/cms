<?php

// Incluye los objetos necesarios
require("objetos/jornada.php");
require("objetos/cate.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Capturamos los datos que vienen por POST y los guardamos en el objeto equipo

$jornada->idcat    = init("idcat");
$jornada->idjorn   = "";

if(isset($_REQUEST["local01"]))  $jornada->local01  = $_REQUEST["local01"];		else $jornada->local01  = "";
if(isset($_REQUEST["visit01"]))  $jornada->visit01  = $_REQUEST["visit01"];		else $jornada->visit01  = "";
if(isset($_REQUEST["inst01"]))   $jornada->inst01   = $_REQUEST["inst01"];		else $jornada->inst01   = "";
if(isset($_REQUEST["plocal01"])) $jornada->plocal01 = $_REQUEST["plocal01"];	else $jornada->plocal01 = "";
if(isset($_REQUEST["pvisit01"])) $jornada->pvisit01 = $_REQUEST["pvisit01"];	else $jornada->pvisit01 = "";
if(isset($_REQUEST["fecha01"]))  $jornada->fecha01  = $_REQUEST["fecha01"];		else $jornada->fecha01  = "";
if(isset($_REQUEST["hour01"]))   $jornada->hour01   = $_REQUEST["hour01"];		else $jornada->hour01   = "";

if(isset($_REQUEST["local02"]))  $jornada->local02  = $_REQUEST["local02"];		else $jornada->local02  = "";
if(isset($_REQUEST["visit02"]))  $jornada->visit02  = $_REQUEST["visit02"];		else $jornada->visit02  = "";
if(isset($_REQUEST["inst02"]))   $jornada->inst02   = $_REQUEST["inst02"];		else $jornada->inst02   = "";
if(isset($_REQUEST["plocal02"])) $jornada->plocal02 = $_REQUEST["plocal02"];	else $jornada->plocal02 = "";
if(isset($_REQUEST["pvisit02"])) $jornada->pvisit02 = $_REQUEST["pvisit02"];	else $jornada->pvisit02 = "";
if(isset($_REQUEST["fecha02"]))  $jornada->fecha02  = $_REQUEST["fecha02"];		else $jornada->fecha02  = "";
if(isset($_REQUEST["hour02"]))   $jornada->hour02   = $_REQUEST["hour02"];		else $jornada->hour02   = "";

if(isset($_REQUEST["local03"]))  $jornada->local03  = $_REQUEST["local03"];		else $jornada->local03  = "";
if(isset($_REQUEST["visit03"]))  $jornada->visit03  = $_REQUEST["visit03"];		else $jornada->visit03  = "";
if(isset($_REQUEST["inst03"]))   $jornada->inst03   = $_REQUEST["inst03"];		else $jornada->inst03   = "";
if(isset($_REQUEST["plocal03"])) $jornada->plocal03 = $_REQUEST["plocal03"];	else $jornada->plocal03 = "";
if(isset($_REQUEST["pvisit03"])) $jornada->pvisit03 = $_REQUEST["pvisit03"];	else $jornada->pvisit03 = "";
if(isset($_REQUEST["fecha03"]))  $jornada->fecha03  = $_REQUEST["fecha03"];		else $jornada->fecha03  = "";
if(isset($_REQUEST["hour03"]))   $jornada->hour03   = $_REQUEST["hour03"];		else $jornada->hour03   = "";

if(isset($_REQUEST["local04"]))  $jornada->local04  = $_REQUEST["local04"];		else $jornada->local04  = "";
if(isset($_REQUEST["visit04"]))  $jornada->visit04  = $_REQUEST["visit04"];		else $jornada->visit04  = "";
if(isset($_REQUEST["inst04"]))   $jornada->inst04   = $_REQUEST["inst04"];		else $jornada->inst04   = "";
if(isset($_REQUEST["plocal04"])) $jornada->plocal04 = $_REQUEST["plocal04"];	else $jornada->plocal04 = "";
if(isset($_REQUEST["pvisit04"])) $jornada->pvisit04 = $_REQUEST["pvisit04"];	else $jornada->pvisit04 = "";
if(isset($_REQUEST["fecha04"]))  $jornada->fecha04  = $_REQUEST["fecha04"];		else $jornada->fecha04  = "";
if(isset($_REQUEST["hour04"]))   $jornada->hour04   = $_REQUEST["hour04"];		else $jornada->hour04   = "";

if(isset($_REQUEST["local05"]))  $jornada->local05  = $_REQUEST["local05"];		else $jornada->local05  = "";
if(isset($_REQUEST["visit05"]))  $jornada->visit05  = $_REQUEST["visit05"];		else $jornada->visit05  = "";
if(isset($_REQUEST["inst05"]))   $jornada->inst05   = $_REQUEST["inst05"];		else $jornada->inst05   = "";
if(isset($_REQUEST["plocal05"])) $jornada->plocal05 = $_REQUEST["plocal05"];	else $jornada->plocal05 = "";
if(isset($_REQUEST["pvisit05"])) $jornada->pvisit05 = $_REQUEST["pvisit05"];	else $jornada->pvisit05 = "";
if(isset($_REQUEST["fecha05"]))  $jornada->fecha05  = $_REQUEST["fecha05"];		else $jornada->fecha05  = "";
if(isset($_REQUEST["hour05"]))   $jornada->hour05   = $_REQUEST["hour05"];		else $jornada->hour05   = "";

if(isset($_REQUEST["local06"]))  $jornada->local06  = $_REQUEST["local06"];		else $jornada->local06  = "";
if(isset($_REQUEST["visit06"]))  $jornada->visit06  = $_REQUEST["visit06"];		else $jornada->visit06  = "";
if(isset($_REQUEST["inst06"]))   $jornada->inst06   = $_REQUEST["inst06"];		else $jornada->inst06   = "";
if(isset($_REQUEST["plocal06"])) $jornada->plocal06 = $_REQUEST["plocal06"];	else $jornada->plocal06 = "";
if(isset($_REQUEST["pvisit06"])) $jornada->pvisit06 = $_REQUEST["pvisit06"];	else $jornada->pvisit06 = "";
if(isset($_REQUEST["fecha06"]))  $jornada->fecha06  = $_REQUEST["fecha06"];		else $jornada->fecha06  = "";
if(isset($_REQUEST["hour06"]))   $jornada->hour06   = $_REQUEST["hour06"];		else $jornada->hour06   = "";

if(isset($_REQUEST["local07"]))  $jornada->local07  = $_REQUEST["local07"];		else $jornada->local07  = "";
if(isset($_REQUEST["visit07"]))  $jornada->visit07  = $_REQUEST["visit07"];		else $jornada->visit07  = "";
if(isset($_REQUEST["inst07"]))   $jornada->inst07   = $_REQUEST["inst07"];		else $jornada->inst07   = "";
if(isset($_REQUEST["plocal07"])) $jornada->plocal07 = $_REQUEST["plocal07"];	else $jornada->plocal07 = "";
if(isset($_REQUEST["pvisit07"])) $jornada->pvisit07 = $_REQUEST["pvisit07"];	else $jornada->pvisit07 = "";
if(isset($_REQUEST["fecha07"]))  $jornada->fecha07  = $_REQUEST["fecha07"];		else $jornada->fecha07  = "";
if(isset($_REQUEST["hour07"]))   $jornada->hour07   = $_REQUEST["hour07"];		else $jornada->hour07   = "";

if(isset($_REQUEST["local08"]))  $jornada->local08  = $_REQUEST["local08"];		else $jornada->local08  = "";
if(isset($_REQUEST["visit08"]))  $jornada->visit08  = $_REQUEST["visit08"];		else $jornada->visit08  = "";
if(isset($_REQUEST["inst08"]))   $jornada->inst08   = $_REQUEST["inst08"];		else $jornada->inst08   = "";
if(isset($_REQUEST["plocal08"])) $jornada->plocal08 = $_REQUEST["plocal08"];	else $jornada->plocal08 = "";
if(isset($_REQUEST["pvisit08"])) $jornada->pvisit08 = $_REQUEST["pvisit08"];	else $jornada->pvisit08 = "";
if(isset($_REQUEST["fecha08"]))  $jornada->fecha08  = $_REQUEST["fecha08"];		else $jornada->fecha08  = "";
if(isset($_REQUEST["hour08"]))   $jornada->hour08   = $_REQUEST["hour08"];		else $jornada->hour08   = "";

if(isset($_REQUEST["local09"]))  $jornada->local09  = $_REQUEST["local09"];		else $jornada->local09  = "";
if(isset($_REQUEST["visit09"]))  $jornada->visit09  = $_REQUEST["visit09"];		else $jornada->visit09  = "";
if(isset($_REQUEST["inst09"]))   $jornada->inst09   = $_REQUEST["inst09"];		else $jornada->inst09   = "";
if(isset($_REQUEST["plocal09"])) $jornada->plocal09 = $_REQUEST["plocal09"];	else $jornada->plocal09 = "";
if(isset($_REQUEST["pvisit09"])) $jornada->pvisit09 = $_REQUEST["pvisit09"];	else $jornada->pvisit09 = "";
if(isset($_REQUEST["fecha09"]))  $jornada->fecha09  = $_REQUEST["fecha09"];		else $jornada->fecha09  = "";
if(isset($_REQUEST["hour09"]))   $jornada->hour09   = $_REQUEST["hour09"];		else $jornada->hour09   = "";

if(isset($_REQUEST["local10"]))  $jornada->local10  = $_REQUEST["local10"];		else $jornada->local10  = "";
if(isset($_REQUEST["visit10"]))  $jornada->visit10  = $_REQUEST["visit10"];		else $jornada->visit10  = "";
if(isset($_REQUEST["inst10"]))   $jornada->inst10   = $_REQUEST["inst10"];		else $jornada->inst10   = "";
if(isset($_REQUEST["plocal10"])) $jornada->plocal10 = $_REQUEST["plocal10"];	else $jornada->plocal10 = "";
if(isset($_REQUEST["pvisit10"])) $jornada->pvisit10 = $_REQUEST["pvisit10"];	else $jornada->pvisit10 = "";
if(isset($_REQUEST["fecha10"]))  $jornada->fecha10  = $_REQUEST["fecha10"];		else $jornada->fecha10  = "";
if(isset($_REQUEST["hour10"]))   $jornada->hour10   = $_REQUEST["hour10"];		else $jornada->hour10   = "";

// Intentamos de si r el usuario y redirigimos en función de si hemos tenido éxito o no
if(guardarJornada($jornada)==true) redirect("04newJornada.php?idcate=".$jornada->idcat,0);
else                               redirect("index.php?origen=error",0);

?>