<?php

// Incluye los objetos necesarios
require("objetos/torneo.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <script language="JavaScript" type="text/javascript">
		function modificar(idtorneo) { document.location.href="09modtorneo.php?idtorneo=" + idtorneo; }
        function eliminar(idtorneo) {  document.location.href="09deltorneo.php?idtorneo=" + idtorneo; }
        function nuevo(){              document.location.href="09newtorneo.php"; }
    </script>

    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>cms::administracion::torneos</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administracion - torneos'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=700 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <table border=0 width=850>
                        <tr align="right">
                            <td width=850 bgcolor=#ffffff>
                                <a href="#" onClick="nuevo();">
                                    <font face="arial" style="font-size: 11px;; color: blue;">Introduce nuevo torneo</font>
                                </a>
                            </td>
                        </tr>
                    </table>
                    
                    <table width=850>
                        <tr height=10>
                            <td width=50  bgcolor=#c8c8c8><font class="admin_font">Idtorneo</font></td>
                            <td width=350 bgcolor=#c8c8c8><font class="admin_font">Torneo  </font></td>
							<td width=350 bgcolor=#c8c8c8><font class="admin_font">Fecha   </font></td>
                            <td width=50  bgcolor=#c8c8c8></td>
                            <td width=50  bgcolor=#c8c8c8></td>
                        </tr>

						<?php
						$torneo = obtenerTorneo();
						
						for($i=0;$i<numRows("CMS_torneos");$i++) {
							
							$_POST["idtorneo"]    = $torneo[$i]->idtorneo;
							$_POST["idformu"]     = $torneo[$i]->idformu;
							$_POST["name"]        = $torneo[$i]->name;
							$_POST["cartel"]      = $torneo[$i]->cartel;
							$_POST["fecha"]       = $torneo[$i]->fecha;
							$_POST["hour"]        = $torneo[$i]->hour;
							$_POST["location"]    = $torneo[$i]->location;
							$_POST["gmaps"]       = $torneo[$i]->gmaps;
							$_POST["description"] = $torneo[$i]->description;
							$_POST["info"]        = $torneo[$i]->info;
							$_POST["cuenta"]      = $torneo[$i]->cuenta;
							$_POST["titular"]     = $torneo[$i]->titular;
							$_POST["precio"]      = $torneo[$i]->precio;
							$_POST["conta"]       = $torneo[$i]->conta;
							?>

							<form name="datos" action="guardarTorneo.php" method=post>
								<tr height=10>
									<td width=50  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["idtorneo"];?> </font></td>
									<td width=350 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["name"];?>     </font></td>
									<td width=350 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["fecha"];?>    </font></td>
									
									<td width=50  bgcolor=#f8f8f8>
										<a href="#" onClick="modificar('<?php echo $torneo[$i]->idtorneo; ?>');">
											<font face="arial" style="font-size: 11px;; color: blue;">modificar</font>
										</a>
									</td>
									<td width=50  bgcolor=#f8f8f8>
										<a href="#" onClick="eliminar('<?php echo $torneo[$i]->idtorneo; ?>');">
											<font face="arial" style="font-size: 11px;; color: blue;">eliminar </font>
										</a>
									</td>
								</tr>
							</form>
							<?php
						}
						?>
                    </table>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>