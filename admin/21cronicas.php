<?php

// Incluye los objetos necesarios
require("objetos/cronica.php");
require("objetos/equipo.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Control de usuario identificado
require("check.php");

// obtenemos el id del equipo seleccionado en el desplegable si es que se ha hecho
$idteam = isset($_REQUEST['idteam']) ? $_REQUEST['idteam'] : "";

?>

<!DOCTYPE html>

<html>
    <script language="JavaScript" type="text/javascript">
	function modificar(id,idteam,idcat){
        document.location.href="21modCronica.php?idcro=" + id+"&idteam="+idteam+"&idcat="+idcat;
	}
    function eliminar(id) {
        document.location.href="21delCronica.php?idcro=" + id;
	}
    function nuevo(id) {
        document.location.href="21newCronica.php?idteam=" + id;
	}
    </script>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::cronicas</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!-- cabecera -->
        <?php $titulo = 'cms - administración - cronicas'; require('cabecera.php'); ?>

        <!-- cuerpo -->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=628 align=center>
            <tr>
                <!-- menu -->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!-- contenido -->
                <td width=850 style="vertical-align:top">
                    
                    <table border=0 width=850>
                        <tr align="right">
                            <td width=850 bgcolor=#ffffff>
								<a href="#" onClick="nuevo(<?php if((isset($idteam))and($idteam!="")) { echo $idteam; } else { echo "false"; } ?>);">
									<font face="arial" style="font-size: 11px;; color: blue;">Introduce nueva crónica</font>
								</a>
							</td>
                        </tr>
                    </table>
                    
					<table width=850>
						<tr>
							<td height=20 width=200 bgcolor=#ffffff><font face="arial" style="font-size: 11px;; color: black;"><b>Selecciona el equipo:</b></font></td>
							<td height=20 width=650 bgcolor=#ffffff>
								<form name="datos" action="21cronicas.php" method="post">
									<font face="verdana" color="red" style="margin-left: 0px;font-size: 12;">
										
										<select name="idteam" onchange="this.form.submit()" style="width:650px">
											<?php
											if(strlen($idteam)==0) $selected = " selected ";
											else            	   $selected = "";
											?>
											<option value="" <?php echo $selected; ?> >Todos los equipos</option>
											<?php
											$equipo = obtenerEquipo();
											for ($i=0;$i<numRows("CMS_cate");$i++) {
												if($idteam==$equipo[$i]->idteam) $selected = " selected ";
												else 							 $selected = " ";
												echo"<option $selected value=\"" . $equipo[$i]->idteam . "\">" . $equipo[$i]->nombre . "</option>";
											}
											?>
										</select>
										
									</font>
								</form>
							</td>
						</tr>
					</table>
					
                    <table width=850>
                        <tr height=10>
                            <td width=50  bgcolor=#c8c8c8><font class="admin_font">Idcro</font></td>
							<td width=50  bgcolor=#c8c8c8><font class="admin_font">Idteam</font></td>
                            <td width=350 bgcolor=#c8c8c8><font class="admin_font">Título</font></td>
                            <td width=75  bgcolor=#c8c8c8><font class="admin_font">Fecha</font></td>
                            <td width=225 bgcolor=#c8c8c8><font class="admin_font">Iduser</font></td>

                            <td width=50  bgcolor=#c8c8c8>&nbsp;</td>
                            <td width=50  bgcolor=#c8c8c8>&nbsp;</td>
                        </tr>

						<?php
						
						if($idteam!="") {
							// obtenemos todas las cronicas registradas
							$cronica = obtenerCronicaPorEquipo($idteam);
							$num     = count($cronica);
						}
						else {
							// obtenemos todas las cronicas registradas
							$cronica = obtenerCronica();
							$num     = count($cronica);
						}
						
						// para cada una de las cronicas obtenidas
						for($i=0;$i<$num;$i++) {
							// obtenemos los datos
							$_POST["idcro"]   = $cronica[$i]->idcro;
							$_POST["idteam"]  = $cronica[$i]->idteam;
							$_POST["idcat"]   = $cronica[$i]->idcat;
							$_POST["fecha"]   = $cronica[$i]->fecha;
							$_POST["iduser"]  = $cronica[$i]->iduser;
							$_POST["titulo"]  = $cronica[$i]->titulo;
							$_POST["conta"]   = $cronica[$i]->conta;
							$_POST["fechasi"] = $cronica[$i]->fechasi;
							$_POST["fechano"] = $cronica[$i]->fechano;
							?>

							<form name="datos" action="guardarInsta.php" method=post>
								<tr height=10>                                
									<td width=50  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["idcro"];?> </font></td>
									<td width=50  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["idteam"];?></font></td>
									<td width=350 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["titulo"];?></font></td>
									<td width=75  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["fecha"];?> </font></td>
									<td width=225 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["iduser"];?></font></td>

									<td width=50  bgcolor=#f8f8f8>
										<a href="#" onClick="modificar(<?php echo $_POST["idcro"]; ?>,<?php echo $_POST["idteam"]; ?>,<?php echo $_POST["idcat"]; ?>);">
											<font face="arial" style="font-size: 11px;; color: blue;">modificar</font>
										</a>
									</td>
									<td width=50  bgcolor=#f8f8f8>
										<a href="#" onClick="eliminar(<?php echo $_POST["idcro"]; ?>);">
											<font face="arial" style="font-size: 11px;; color: blue;">eliminar</font>
										</a>
									</td>
								</tr>
							</form>
							<?php
						}
						?>
                    </table>
                </td>
            </tr>		
        </table>
    </body>
</html>
