<?php

/* ************************************************************************************************
*
* Guarda un texto de log en el fichero de logs del sistema
* 
************************************************************************************************ */
function wlog($funcion, $texto, $debug)
{ 
    // Inicializamos la variable
    $aux = true;
    
    if($debug==1)
    {
        // Inicializamos la variable
        $aux = false;

        // Estadisticas caseras de visita
        $hora = date("H:i:s");
        $dia = date("d/m/Y");

        // Abre el archivo de texto
        $ar = fopen("logs/log.txt","a+");

        // Si hemos posido abrir el archivo
        if($ar!=false)
        {
            // Guardamos el dia
            fputs($ar,"Dia:");
            fputs($ar,$dia);
            // Guardamos la hora
            fputs($ar," - Hora:");
            fputs($ar,$hora);
            // Guardamos la funcion
            fputs($ar," - Funcion:");
            fputs($ar,$funcion);
            // Guardamos el log
            fputs($ar," - Log:");
            fputs($ar,$texto);
            fputs($ar,"\r\n");

            // Cerramos el archivo
            fclose($ar);

            // Inicializamos la variable
            $aux = true;
        }
    }
    
    // Salimos de la funcion
    return $aux;
}

?>
