<?php

// Incluye los objetos necesarios
require("objetos/tags.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <script language="JavaScript" type="text/javascript">
		function modificar(idtag){ document.location.href="16modTags.php?idtags=" + idtag; }
        function eliminar(idtag){  document.location.href="16delTags.php?idtags=" + idtag; }
        function nuevo(){          document.location.href="16newTags.php"; }
    </script>

    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>cms::administración::tags</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - tags'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=700 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <table border=0 width=850>
                        <tr align="right">
                            <td width=850 bgcolor=#ffffff>
                                <a href="#" onClick="nuevo();">
                                    <font face="arial" style="font-size: 11px;; color: blue;">Introduce nuevo tag</font>
                                </a>
                            </td>
                        </tr>
                    </table>
                    
                    <table width=850>
                        <tr height=10>
                            <td width=50 bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">idtags</font></td>
                            <td width=650 bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">nombre</font></td>
                            <td width=50 bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">conta</font></td>
                            <td width=50 bgcolor=#c8c8c8></td>
                            <td width=50 bgcolor=#c8c8c8></td>
                        </tr>


                    <?php
                    $tags = obtenerTags();
                    
                    for($i=0;$i<numRows("CMS_tags");$i++)
                    {
                        $_POST["idtags"] = $tags[$i]->idtags;
                        $_POST["nombre"] = $tags[$i]->nombre;
                        $_POST["conta"]  = $tags[$i]->conta;
                        ?>

                        <form name="datos" action="guardarTags.php" method=post>
                            <tr height=10>
                                <td width=50  bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $_POST["idtags"];?> </font></td>
                                <td width=650 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $_POST["nombre"];?> </font></td>
                                <td width=50  bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $_POST["conta"];?>  </font></td>
                                <td width=50  bgcolor=#f8f8f8><a href="#" onClick="modificar('<?php echo $tags[$i]->idtags; ?>');"><font face="arial" style="font-size: 11px;; color: blue;">modificar</font></a></td>
                                <td width=50  bgcolor=#f8f8f8><a href="#" onClick="eliminar('<?php echo $tags[$i]->idtags; ?>');"> <font face="arial" style="font-size: 11px;; color: blue;">eliminar </font></a></td>
                            </tr>
                        </form>
                    <?php
                    }
                    ?>
                    </table>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>
