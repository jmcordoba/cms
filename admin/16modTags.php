<?php

// Incluye los objetos necesarios
require("objetos/tags.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::tags::modificar</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>
    
    <script>
        // Verifica los campos del formulario antes de guardar los datos
        function verificar_form() {
            if(document.datos.nombre.value.length==0) {
                alert("Error:\nDebe ingresar el nombre"); 
                document.frm.nombre.focus() 
                return 0; 
            }
            else {
                document.forms['datos'].submit();
            }
        }
    </script>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - tags - modificar'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=800 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <?php
                    
                    $idtags = $_GET["idtags"];
                    
                    $tag = obtenerUnTag($idtags);
                    
                    $_POST["idtags"] = $tag->idtags;
                    $_POST["nombre"] = $tag->nombre;
                    $_POST["conta"]  = $tag->conta;
                    ?>

                    <form name="datos" action="modificarTags.php" method=post enctype="multipart/form-data">
                        <table border=0 width=850>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="16tags.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <table width=850>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">idtags</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=idtags size=108 readonly="readonly" value="<?php echo $_POST["idtags"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">nombre</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=nombre size=108 value="<?php echo $_POST["nombre"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">conta</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=conta size=108 readonly="readonly" value="<?php echo $_POST["conta"];?>"></input></td>
                            </tr>
                            <tr><td bgcolor=#ffffff><a href="#" onclick="verificar_form();"><font face="arial" style="font-size: 11px;; color: blue;">Guardar tag</font></a></td></tr>
                        </table>
                    </form>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>
