<?php

// Incluye los objetos y funciones
require("objetos/tags.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");

// Capturamos el id de la categoría a eliminar
$idtags = $_GET["idtags"];
// Eliminamos categoria y redirigimos
if(borrarTags($idtags)==true) redirect("index.php?origen=tags" ,0);
else                          redirect("index.php?origen=error",0);

?>
