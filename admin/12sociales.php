<?php

// Incluye los objetos necesarios
require("objetos/social.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <script language="JavaScript" type="text/javascript">
	function modificar(social){ document.location.href="12modSociales.php?idsocial=" + social; }
	function eliminar(social){  document.location.href="12delSociales.php?idsocial=" + social; }
	function nuevo(){           document.location.href="12newSociales.php"; }
    </script>

    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>cms::administración::redes sociales</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - redes sociales'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=700 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <table border=0 width=850>
                        <tr align="right">
                            <td width=850 bgcolor=#ffffff>
                                <a href="#" onClick="nuevo();">
                                    <font face="arial" style="font-size: 11px;; color: blue;">Introduce nueva red social</font>
                                </a>
                            </td>
                        </tr>
                    </table>
                    
                    <table width=850>
                        <tr height=10>
                            <td width=10  bgcolor=#c8c8c8><font class="admin_font">Idsocial</font></td>
                            <td width=100 bgcolor=#c8c8c8><font class="admin_font">Red social</font></td>
                            <td width=250 bgcolor=#c8c8c8><font class="admin_font">Enlace</font></td>
                            <td width=50  bgcolor=#c8c8c8>&nbsp;</td>
                            <td width=50  bgcolor=#c8c8c8>&nbsp;</td>
                        </tr>

						<?php
						$social = obtenerSociales();
						for($i=0;$i<numRows("CMS_sociales");$i++) {
						
							$_POST["idsocial"] = $social[$i]->idsocial;
							$_POST["nombre"]   = $social[$i]->nombre;
							$_POST["link"]     = $social[$i]->link;
							$_POST["icono"]    = $social[$i]->icono;
							?>

							<form name="datos" action="guardarSociales.php" method=post>
								<tr height=10>
									<td width=10  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["idsocial"];?>   </font></td>
									<td width=100 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["nombre"];?></font></td>
									<td width=250 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["link"];?>  </font></td>
									
									<td width=50  bgcolor=#f8f8f8>
										<a href="#" onClick="modificar('<?php echo $social[$i]->idsocial; ?>');">
											<font face="arial" style="font-size: 11px;; color: blue;">modificar</font>
										</a>
									</td>
									<td width=50  bgcolor=#f8f8f8>
										<a href="#" onClick="eliminar('<?php echo $social[$i]->idsocial; ?>');">
											<font face="arial" style="font-size: 11px;; color: blue;">eliminar</font>
										</a>
									</td>
								</tr>
							</form>
							<?php
						}
						?>
                    </table>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>