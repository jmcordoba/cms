<?php

// Incluye los objetos necesarios
require("objetos/album.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>

<!DOCTYPE html>

<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::album::nuevo</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!-- cabecera -->
        <?php $titulo = 'cms - administración - album - nuevo'; require('cabecera.php'); ?>

        <!-- cuerpo -->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=800 align=center>
            <tr>
                <!-- menu -->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!-- contenido -->
                <td width=850 style="vertical-align:top">
                    
                    <form name="datos" action="guardarAlbum.php" method=post>
                        
						<table border=0 width=850>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="03cate.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        
						<table width=850>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">nombre del album</font></td>
                                <td height=20 width=700 bgcolor=#f8f8f8><input align=left type=text name=nombre size=108></input></td>
                            </tr>
						</table>
						
						<!-- Guardar -->
						<table width=850>
                            <tr><td bgcolor=#ffffff><a href="#" onclick="javascript:document.forms['datos'].submit(); return false;"><font face="arial" style="font-size: 11px;; color: blue;">Guardar nombre de album</font></a></td></tr>
                        </table>
                    </form>
                </td>
            </tr>		
        </table>

        <!-- pie -->
    </body>
</html>
