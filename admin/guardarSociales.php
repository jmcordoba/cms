<?php

// Incluye los objetos necesarios
require("objetos/social.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Obtenemos las variables del formulario
$social->idsocial = init("idsocial");
$social->nombre   = init("nombre");
$social->link     = init("link");
$social->icono    = "";

// Capturamos los datos del fichero adjunto
$nombre_archivo = $_FILES['icono']['name']; 
$tipo_archivo   = $_FILES['icono']['type']; 
$tamano_archivo = $_FILES['icono']['size'];

// Inicializamos la variable numero de filas a cero
$numero = 0;
// Conexión con la base de datos
$link=Conectarse();
// Construcción de la query
$sql = "select * from `CMS_sociales` order by idsocial";
// Registro de log
wlog("guardarSocial",$sql,1);
// Ejecutamos la query y obtenemos el resultado
$result = mysql_query($sql, $link);
// Obtenemos el ultimo idtemp
if ($row = mysql_fetch_array($result))
{
    do{
        $numero = $row["idsocial"];
    } while ($row = mysql_fetch_array($result));
}
$numero++;
// Cerramos la conexion con la base de datos
mysql_close($link);
// Si viene un fichero adjunto
if($nombre_archivo!="") {
    //compruebo si las caracter�sticas del archivo son las que deseo 
    if ($tamano_archivo < 100000) {
        // Inicializamos la ruta y el nombre del avatar del usuario
        $social->icono    = "admin/images/sociales/social" . $numero . ".gif";
        // Guardamos el fichero en la ruta especificada
        if (!move_uploaded_file($_FILES['icono']['tmp_name'], "images/sociales/social" . $numero . ".gif")){
            // S ihay algún tipo de error, redirigimos a otra página
            ?><script>location.href='index.php?origen=error';</script><?php
        }
    } else {
		?>
		<script>
			alert('La imagen que desea subir es mayor de 100kB.\nPor favor, suba una imagen con un peso inferior.');
		</script>
		<?php
	}
}
// Intentamos guardar el usuario y redirigimos en función de si hemos tenido éxito o no
if(guardarSocial($social,$numero)==true) redirect("index.php?origen=social",0);
else                                     redirect("index.php?origen=error" ,0);

?>