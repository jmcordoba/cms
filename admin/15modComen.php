<?php

// Incluye los objetos necesarios
require("objetos/comentario.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::comentarios::modificar</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>
    
    <script>
        // Verifica los campos del formulario antes de guardar los datos
        function verificar_form() {
            if(document.datos.contenido.value.length==0) {
                alert("Error:\nDebe ingresar el contenido"); 
                document.datos.contenido.focus() 
                return 0; 
            }
            else {
                document.forms['datos'].submit();
            }
        }
    </script>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - redes sociales - modificar'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=800 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <?php
                    
                    $idcom = $_GET["idcom"];
                    
                    $comen = obtenerUnComentario($idcom);
                    
                    $_POST["idart"]     = $comen->idart;
					$_POST["idcom"]     = $comen->idcom;
                    $_POST["fecha"]     = $comen->fecha;
                    $_POST["iduser"]    = $comen->iduser;
                    $_POST["contenido"] = $comen->contenido;
					$_POST["conta"]     = $comen->conta;
                    ?>

                    <form name="datos" action="modificarComentario.php" method="post">
                        
						<input type="hidden" name="idcom" readonly value="<?php echo $_POST["idcom"];?>">
						
						<table border=0 width=850>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="15comentarios.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <table width=850>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">idart</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=idart size=108 readonly="readonly" value="<?php echo $_POST["idart"];?>"></input></td>
                            </tr>
                            <!--tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">idcom</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=idcom size=108 readonly="readonly" value="<?php echo $_POST["idcom"];?>"></input></td>
                            </tr-->
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">fecha</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=fecha size=108 readonly="readonly" value="<?php echo $_POST["fecha"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">iduser</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=iduser size=108 readonly="readonly" value="<?php echo $_POST["iduser"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">contenido</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=contenido size=108 value="<?php echo $_POST["contenido"];?>"></input></td>
                            </tr>
                            <!--tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">conta</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=conta size=108 readonly="readonly" value="<?php echo $_POST["conta"];?>"></input></td>
                            </tr-->
							
                            <tr><td bgcolor=#ffffff><a href="#" onclick="verificar_form();"><font face="arial" style="font-size: 11px;; color: blue;">Guardar comentario</font></a></td></tr>
                        </table>
                    </form>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>
