<?php

// Incluimos objetos y funciones
require("objetos/torneo.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Control de usuario identificado
require("check.php");

// Capturamos el id de la categoría a eliminar
$idtorneo = $_GET["idtorneo"];

// Eliminamos categoria y redirigimos
if(borrarTorneo($idtorneo)==true) redirect("index.php?origen=torneo" ,0);
else                              redirect("index.php?origen=error",0);

?>