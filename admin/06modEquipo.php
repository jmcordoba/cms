<?php

// Incluye los objetos necesarios
require("objetos/equipo.php");
require("objetos/cate.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::equipos::modificar</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>
    
    <script>
        // Verifica los campos del formulario antes de guardar los datos
        function verificar_form() {
            if(document.datos.nombre.value.length==0) {
                alert("Error:\nDebe ingresar el nombre"); 
                document.datos.nombre.focus() 
                return 0; 
            }
            else {
                document.forms['datos'].submit();
            }
        }
    </script>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - equipos - modificar'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=800 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <?php
                    
                    $idteam = $_GET["idteam"];
                    
                    $team = obtenerUnEquipo($idteam);
                    
                    $_POST["idteam"]  = $team->idteam;
					$_POST["idcat"]   = $team->idcat;
					$_POST["nombre"]  = $team->nombre;
					$_POST["mister"]  = $team->mister;
					$_POST["foto"]    = $team->foto;
					$_POST["conta"]   = $team->conta;
					
					$cate = obtenerUnaCate($team->idcat);
                    ?>

                    <form name="datos" action="modificarEquipo.php" method=post enctype="multipart/form-data">
                        
						<input type="hidden" name="idteam" value="<?php echo $idteam;?>">
						
						<table border=0 width=850>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="06equipos.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <table width=850>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">idcat</font></td>
								<td height=20 width=700 bgcolor=#ffffff>                                    
                                    <select name="idcat">
                                        <option value="<?php echo $_POST["idcat"];?>" selected><?php echo $cate->name; ?></option>
                                        <?php
                                        $categoria = obtenerCate();
                                        for ($i=0;$i<numRows("CMS_cate");$i++){
											if($categoria[$i]->idcat!=$team->idcat) {
												echo"<option value=\"" . $categoria[$i]->idcat . "\">" . $categoria[$i]->name . "</option>";
											}
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">nombre</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=nombre size=108 value="<?php echo $_POST["nombre"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">mister</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=mister size=108 value="<?php echo $_POST["mister"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">foto</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=foto size=108 value="<?php echo $_POST["foto"];?>"></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">nfoto *</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input type="file" name="userfile" style="width:400px;border:0px solid #000000;"></input></td>
                            </tr>
                            <tr>
                                <td bgcolor=#ffffff><a href="#" onclick="verificar_form();"><font face="arial" style="font-size: 11px;; color: blue;">Guardar equipo</font></a></td>
                            </tr>
							<tr>
								<td colspan="3" height=20 width=850 bgcolor=#ffffff>
									<font face="verdana" color="red" style="margin-left: 4px;font-size: 11px;;">
									* Recomendamos que la imagen sea de 150x200px para que pueda mostrarse correctamente, si no es así la mostraremos centrada.
									</font>
								</td>
							<tr>
                        </table>
                        <?php
                        if ($_POST["foto"]!="") {
                            ?>
                            <table width=850>
                                <tr valign="bottom" align="left">
                                    <td width=400 bgcolor=#ffffff><img height=350 src="<?php echo conf_RUTA.$_POST["foto"];?>"></td>
                                </tr>
                            </table>
                            <?php
                        }
                        ?>
                    </form>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>