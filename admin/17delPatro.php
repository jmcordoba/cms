<?php

// Incluye los objetos necesarios
require("objetos/patro.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");

// Capturamos el id de la categoría a eliminar
$patro = $_GET["patro"];
// Eliminamos categoria y redirigimos
if(borrarPatro($patro)==true) redirect("index.php?origen=patro" ,0);
else                          redirect("index.php?origen=error",0);

?>
