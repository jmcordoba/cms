<?php

class jornada {
    
    // atributos
    public $idjorn     = "";
	public $idcat      = "";
    
	// Evento 01 de la jornada
	public $local01    = "";	// Equipo local
    public $visit01    = "";	// Equipo visitante
    public $inst01     = "";	// Instalacion del evento
    public $plocal01   = "0";	// Puntuacion del equipo local
    public $pvisit01   = "0";	// Puntuacion del equipo visitante
    public $fecha01    = "";	// Fecha del evento
    public $hour01     = "";	// Hora del evento
	// puntuaciones parciales
	public $plocal011  = "0";
	public $plocal012  = "0";
	public $plocal013  = "0";
	public $plocal014  = "0";
	public $plocal015  = "0";
	public $pvisit011  = "0";
	public $pvisit012  = "0";
	public $pvisit013  = "0";
	public $pvisit014  = "0";
	public $pvisit015  = "0";
	
	// Evento 02 de la jornada
	public $local02    = "";	// Equipo local
    public $visit02    = "";	// Equipo visitante
    public $inst02     = "";	// Instalacion del evento
    public $plocal02   = "0";	// Puntuacion del equipo local
    public $pvisit02   = "0";	// Puntuacion del equipo visitante
    public $fecha02    = "";	// Fecha del evento
    public $hour02     = "";	// Hora del evento
	// puntuaciones parciales
	public $plocal021  = "0";
	public $plocal022  = "0";
	public $plocal023  = "0";
	public $plocal024  = "0";
	public $plocal025  = "0";
	public $pvisit021  = "0";
	public $pvisit022  = "0";
	public $pvisit023  = "0";
	public $pvisit024  = "0";
	public $pvisit025  = "0";
	
	// Evento 03 de la jornada
	public $local03    = "";	// Equipo local
    public $visit03    = "";	// Equipo visitante
    public $inst03     = "";	// Instalacion del evento
    public $plocal03   = "0";	// Puntuacion del equipo local
    public $pvisit03   = "0";	// Puntuacion del equipo visitante
    public $fecha03    = "";	// Fecha del evento
    public $hour03     = "";	// Hora del evento
	// puntuaciones parciales
	public $plocal031  = "0";
	public $plocal032  = "0";
	public $plocal033  = "0";
	public $plocal034  = "0";
	public $plocal035  = "0";
	public $pvisit031  = "0";
	public $pvisit032  = "0";
	public $pvisit033  = "0";
	public $pvisit034  = "0";
	public $pvisit035  = "0";
	
	// Evento 04 de la jornada
	public $local04    = "";	// Equipo local
    public $visit04    = "";	// Equipo visitante
    public $inst04     = "";	// Instalacion del evento
    public $plocal04   = "0";	// Puntuacion del equipo local
    public $pvisit04   = "0";	// Puntuacion del equipo visitante
    public $fecha04    = "";	// Fecha del evento
    public $hour04     = "";	// Hora del evento
	// puntuaciones parciales
	public $plocal041  = "0";
	public $plocal042  = "0";
	public $plocal043  = "0";
	public $plocal044  = "0";
	public $plocal045  = "0";
	public $pvisit041  = "0";
	public $pvisit042  = "0";
	public $pvisit043  = "0";
	public $pvisit044  = "0";
	public $pvisit045  = "0";
	
	// Evento 05 de la jornada
	public $local05    = "";	// Equipo local
    public $visit05    = "";	// Equipo visitante
    public $inst05     = "";	// Instalacion del evento
    public $plocal05   = "0";	// Puntuacion del equipo local
    public $pvisit05   = "0";	// Puntuacion del equipo visitante
    public $fecha05    = "";	// Fecha del evento
    public $hour05     = "";	// Hora del evento
	// puntuaciones parciales
	public $plocal051  = "0";
	public $plocal052  = "0";
	public $plocal053  = "0";
	public $plocal054  = "0";
	public $plocal055  = "0";
	public $pvisit051  = "0";
	public $pvisit052  = "0";
	public $pvisit053  = "0";
	public $pvisit054  = "0";
	public $pvisit055  = "0";
	
	// Evento 06 de la jornada
	public $local06    = "";	// Equipo local
    public $visit06    = "";	// Equipo visitante
    public $inst06     = "";	// Instalacion del evento
    public $plocal06   = "0";	// Puntuacion del equipo local
    public $pvisit06   = "0";	// Puntuacion del equipo visitante
    public $fecha06    = "";	// Fecha del evento
    public $hour06     = "";	// Hora del evento
	// puntuaciones parciales
	public $plocal061  = "0";
	public $plocal062  = "0";
	public $plocal063  = "0";
	public $plocal064  = "0";
	public $plocal065  = "0";
	public $pvisit061  = "0";
	public $pvisit062  = "0";
	public $pvisit063  = "0";
	public $pvisit064  = "0";
	public $pvisit065  = "0";
	
	// Evento 07 de la jornada
	public $local07    = "";	// Equipo local
    public $visit07    = "";	// Equipo visitante
    public $inst07     = "";	// Instalacion del evento
    public $plocal07   = "0";	// Puntuacion del equipo local
    public $pvisit07   = "0";	// Puntuacion del equipo visitante
    public $fecha07    = "";	// Fecha del evento
    public $hour07     = "";	// Hora del evento
	// puntuaciones parciales
	public $plocal071  = "0";
	public $plocal072  = "0";
	public $plocal073  = "0";
	public $plocal074  = "0";
	public $plocal075  = "0";
	public $pvisit071  = "0";
	public $pvisit072  = "0";
	public $pvisit073  = "0";
	public $pvisit074  = "0";
	public $pvisit075  = "0";
	
	// Evento 08 de la jornada
	public $local08    = "";	// Equipo local
    public $visit08    = "";	// Equipo visitante
    public $inst08     = "";	// Instalacion del evento
    public $plocal08   = "0";	// Puntuacion del equipo local
    public $pvisit08   = "0";	// Puntuacion del equipo visitante
    public $fecha08    = "";	// Fecha del evento
    public $hour08     = "";	// Hora del evento
	// puntuaciones parciales
	public $plocal081  = "0";
	public $plocal082  = "0";
	public $plocal083  = "0";
	public $plocal084  = "0";
	public $plocal085  = "0";
	public $pvisit081  = "0";
	public $pvisit082  = "0";
	public $pvisit083  = "0";
	public $pvisit084  = "0";
	public $pvisit085  = "0";
	
	// Evento 09 de la jornada
	public $local09    = "";	// Equipo local
    public $visit09    = "";	// Equipo visitante
    public $inst09     = "";	// Instalacion del evento
    public $plocal09   = "0";	// Puntuacion del equipo local
    public $pvisit09   = "0";	// Puntuacion del equipo visitante
    public $fecha09    = "";	// Fecha del evento
    public $hour09     = "";	// Hora del evento
	// puntuaciones parciales
	public $plocal091  = "0";
	public $plocal092  = "0";
	public $plocal093  = "0";
	public $plocal094  = "0";
	public $plocal095  = "0";
	public $pvisit091  = "0";
	public $pvisit092  = "0";
	public $pvisit093  = "0";
	public $pvisit094  = "0";
	public $pvisit095  = "0";
	
	// Evento 10 de la jornada
	public $local10    = "";	// Equipo local
    public $visit10    = "";	// Equipo visitante
    public $inst10     = "";	// Instalacion del evento
    public $plocal10   = "0";	// Puntuacion del equipo local
    public $pvisit10   = "0";	// Puntuacion del equipo visitante
    public $fecha10    = "";	// Fecha del evento
    public $hour10     = "";	// Hora del evento
	// puntuaciones parciales
	public $plocal101  = "0";
	public $plocal102  = "0";
	public $plocal103  = "0";
	public $plocal104  = "0";
	public $plocal105  = "0";
	public $pvisit101  = "0";
	public $pvisit102  = "0";
	public $pvisit103  = "0";
	public $pvisit104  = "0";
	public $pvisit105  = "0";	
}
?>