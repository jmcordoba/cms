<?php

class formulario {
    
    // atributos
    public $idformu  = "";	// identificador único de formulario
	public $formu    = "";	// nombre en clave del formulario
    public $mail     = "";	// dirección a la que se enviarán los datos obtenidos en el formulario
    public $name     = "";	// nombre de la persona que recibirá los datos del formulario
    public $fecha     = "";	// fecha en la que se da de alta el formulario
    public $precio   = "";	// precio total que se debe ingresar por equipo
    public $teamname = "";	// nombre del equipo
	public $categ    = "";	// categoría a la que se inscribe el equipo
    public $capname  = "";	// nombre del capitan y/o responsable del equipo
    public $captel   = "";	// teléfono del capitan y/o responsable del equipo
    public $capmail  = "";	// mail del capitan y/o responsable del equipo
    public $n01name  = "";	// nombre del jugador n01
    public $n01tel   = "";	// telefono del jugador n01
    public $n01mail  = "";	// mail del jugador n01
	public $n02name  = "";	// nombre del jugador n02
    public $n02tel   = "";	// telefono del jugador n02
    public $n02mail  = "";	// mail del jugador n02
	public $n03name  = "";	// nombre del jugador n03
    public $n03tel   = "";	// telefono del jugador n03
    public $n03mail  = "";	// mail del jugador n03
	public $n04name  = "";	// nombre del jugador n04
    public $n04tel   = "";	// telefono del jugador n04
    public $n04mail  = "";	// mail del jugador n04
	public $n05name  = "";	// nombre del jugador n05
    public $n05tel   = "";	// telefono del jugador n05
    public $n05mail  = "";	// mail del jugador n05
}
?>