<?php

class torneo {
    
    // atributos
    public $idtorneo    = "";	// identificador unico de torneo
    public $idformu     = "";	// identificador del formulario que esta asociado al torneo
    public $name        = "";	// nombre del torneo
    public $cartel      = "";	// fotografia del cartel del torneo
    public $fecha       = "";	// fecha del torneo
    public $hour        = "";	// hora a la que empieza el torneo
    public $location    = "";	// lugar donde se celebra el torneo
    public $gmaps       = "";	// enlace de google maps del torneo
    public $description = "";	// descripción del torneo
    public $info        = "";	// informacion de inscripción del torneo
	public $cuenta      = "";	// cuenta corriente a la que hay que ingresar el dinero de la inscripción
    public $titular     = "";	// titular de la cuenta corriente de la cuenta anterior
    public $precio      = "";	// precio por participante
    public $conta       = "";	// numero de veces que se ha visitado el enlace del torneo
}
?>