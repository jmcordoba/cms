<?php

// Incluye los objetos necesarios
require("objetos/social.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::redes sociales::modificar</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>
    
    <script>
        // Verifica los campos del formulario antes de guardar los datos
        function verificar_form() {
            if(document.datos.nombre.value.length==0) {
                alert("Error:\nDebe ingresar el nombre"); 
                document.frm.nombre.focus() 
                return 0; 
            }
            else if(document.datos.link.value.length==0) {
                alert("Error:\nDebe ingresar el enlace"); 
                document.frm.apellidos.focus() 
                return 0; 
            }
            else {
                document.forms['datos'].submit();
            }
        }
    </script>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - redes sociales - modificar'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=800 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <?php
                    
                    $idsocial = $_GET["idsocial"];
                    
                    $social = obtenerUnaSocial($idsocial);
                    
                    $_POST["idsocial"] = $social->idsocial;
                    $_POST["nombre"]   = $social->nombre;
                    $_POST["link"]     = $social->link;
                    $_POST["icono"]    = $social->icono;
                    ?>

                    <form name="datos" action="modificarSociales.php" method=post enctype="multipart/form-data">
                        
						<input type="hidden" name="idsocial" readonly value="<?php echo $_POST["idsocial"];?>">
						
						<table border=0 width=850>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="12sociales.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <table width=850>
                            <!--tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">idsocial</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=idsocial size=108 readonly="readonly" value="<?php echo $_POST["idsocial"];?>"></input></td>
                            </tr-->
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Nombre</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=nombre size=108 value="<?php echo $_POST["nombre"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Link</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=link size=108 value="<?php echo $_POST["link"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Icono</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=icono size=108 value="<?php echo $_POST["icono"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Nuevo icono</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input type="file" name="icono" style="width:400px;border:0px solid #000000;"></input></td>
                            </tr>
                            <tr><td bgcolor=#ffffff><a href="#" onclick="verificar_form();"><font face="arial" style="font-size: 11px;; color: blue;">Guardar red social</font></a></td></tr>
                        </table>
                        <?php
                        if ($_POST["icono"]!="") {
                            ?>
                            <table width=850>
                                <tr valign="bottom" align="left">
                                    <td bgcolor=#ffffff><img border="1" width="100%" src="<?php echo conf_RUTA.$_POST["icono"];?>"></td>
                                </tr>
                            </table>
                            <?php
                        }
                        ?>
                    </form>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>
