<?php

// Incluye los objetos necesarios
require("objetos/patro.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::patrocinadores::modificar</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - patrocinadores - modificar'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=800 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <?php
                    
                    $idpatro = $_GET["patro"];
                    
                    $patro = obtenerUnPatro($idpatro);
                    
                    $_POST["idpatro"]    = $patro->idpatro;
                    $_POST["nombre"]     = $patro->nombre;
                    $_POST["imagen"]     = $patro->imagen;
                    $_POST["fecha_alta"] = $patro->fecha_alta;
                    $_POST["fecha_baja"] = $patro->fecha_baja;
                    $_POST["importe"]    = $patro->importe;
                    $_POST["web"]        = $patro->web;
                    $_POST["conta"]      = $patro->conta;
                    ?>

                    <form name="datos" action="modificarPatro.php" method=post enctype="multipart/form-data">
                        <table border=0 width=850>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="17patro.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <table width=850>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">idpatro</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=idpatro size=108 readonly="readonly" value="<?php echo $idpatro;?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">nombre</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=nombre size=108 value="<?php echo $_POST["nombre"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">conta</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=conta size=108 readonly="readonly" value="<?php echo $_POST["conta"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">fecha_alta</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=fecha_alta size=108 value="<?php echo $_POST["fecha_alta"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">fecha_baja</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=fecha_baja size=108 value="<?php echo $_POST["fecha_baja"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">importe</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=importe size=108 value="<?php echo $_POST["importe"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">web</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=web size=108 value="<?php echo $_POST["web"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">imagen</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=imagen size=108 value="<?php echo $_POST["imagen"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">imagen nueva</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input type="file" name="nimagen" style="width:400px;border:0px solid #000000;"></input></td>
                            </tr>
                            <tr><td bgcolor=#ffffff><a href="#" onclick="javascript:document.forms['datos'].submit(); return false;"><font face="arial" style="font-size: 11px;; color: blue;">Guardar patrocinador</font></a></td></tr>
                        </table>
                        <?php
                        if ($_POST["imagen"]!="") {
                            ?>
                            <table width=850>
                                <tr valign="bottom" align="left">
                                    <td width=400 bgcolor=#ffffff><img height=350 src="<?php echo conf_RUTA.$_POST["imagen"];?>"></td>
                                </tr>
                            </table>
                            <?php
                        }
                        ?>
                    </form>
                </td>
            </tr>       
        </table>

        <!--- pie --->
    </body>
</html>
