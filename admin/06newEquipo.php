<?php

// Incluye los objetos necesarios
require("objetos/equipo.php");
require("objetos/cate.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::equipo::nuevo</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>
    
    <script>
        // Verifica los campos del formulario antes de guardar los datos
        function verificar_form() {
            if(document.datos.nombre.value.length==0) {
                alert("Error:\nDebe ingresar el nombre"); 
                document.datos.nombre.focus() 
                return 0; 
            }
			else {
                document.forms['datos'].submit();
            }
        }
    </script>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - equipo - nuevo'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=628 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <form name="datos" action="guardarEquipo.php" method=post enctype="multipart/form-data">
                        <table border=0 width=850>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="06equipos.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <table width=850>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">idcat</font></td>
								<td height=20 width=700 bgcolor=#ffffff>
                                    <select name="idcat">
                                        <option value="" selected="null">Selecciona una categoria</option>
                                        <?php
                                        $categoria = obtenerCate();;
										for ($i=0;$i<numRows("CMS_cate");$i++){
                                            echo"<option value=\"" . $categoria[$i]->idcat . "\">" . $categoria[$i]->name . "</option>";
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">nombre</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=nombre size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">mister</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=mister size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">foto</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input type="file" name="userfile" style="width:400px;border:0px solid #000000;"></input></td>
                            </tr>
                            <tr>
                                <td bgcolor=#ffffff><a href="#" onclick="verificar_form();"><font face="arial" style="font-size: 11px;; color: blue;">Guardar equipo</font></a></td>
                            </tr>
                        </table>
                        
                    </form>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>