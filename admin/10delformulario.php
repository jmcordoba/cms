<?php

// Incluye los objetos y funciones
require("objetos/formulario.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");

// Capturamos el id de la categoría a eliminar
$idformu = $_GET["idformu"];

// Eliminamos categoria y redirigimos
if(borrarFormulario($idformu)==true) redirect("index.php?origen=formu" ,0);
else                                 redirect("index.php?origen=error",0);

?>