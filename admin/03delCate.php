<?php

// Incluimos objetos y funciones
require("objetos/temp.php");
require("objetos/cate.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
// Capturamos el id de la categoría a eliminar
$categ = $_GET["idcate"];
// Eliminamos categoria y redirigimos
if(borrarCate($categ)==true) redirect("index.php?origen=cate" ,0);
else                         redirect("index.php?origen=error",0);

?>
