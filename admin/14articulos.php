<?php

// Incluye los objetos necesarios
require("objetos/articulo.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <script language="JavaScript" type="text/javascript">
	function modificar(id){
        document.location.href="14modArticulo.php?idart=" + id;
	}
    function eliminar(id){
		document.location.href="14delArticulo.php?idart=" + id;
	}
	function nuevo(){
		document.location.href="14newArticulo.php";
	}
    </script>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::instalaciones</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - articulos'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=628 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <table border=0 width=850>
                        <tr align="right">
                            <td width=850 bgcolor=#ffffff><a href="#" onClick="nuevo();"><font face="arial" style="font-size: 11px;; color: blue;">Introduce nuevo artículo</font></a></td>
                        </tr>
                    </table>
                    
                    <table width=850>
                        <tr height=10>
                            <td width=50  bgcolor=#c8c8c8><font class="admin_font">Idart</font></td>
                            <td width=350 bgcolor=#c8c8c8><font class="admin_font">Título</font></td>
                            <td width=75  bgcolor=#c8c8c8><font class="admin_font">Fecha</font></td>
                            <td width=275 bgcolor=#c8c8c8><font class="admin_font">Iduser</font></td>

                            <td width=50  bgcolor=#c8c8c8>&nbsp;</td>
                            <td width=50  bgcolor=#c8c8c8>&nbsp;</td>
                        </tr>

						<?php
						$articulo = obtenerArticulo();
						
						for($i=0;$i<numRows("CMS_articulos");$i++) {
						
							$_POST["idart"]   = $articulo[$i]->idart;
							$_POST["fecha"]   = $articulo[$i]->fecha;
							$_POST["iduser"]  = $articulo[$i]->iduser;
							$_POST["titulo"]  = $articulo[$i]->titulo;
							$_POST["conta"]   = $articulo[$i]->conta;
							$_POST["fechasi"] = $articulo[$i]->fechasi;
							$_POST["fechano"] = $articulo[$i]->fechano;
							?>

							<form name="datos" action="guardarInsta.php" method=post>
								<tr height=10>                                
									<td width=50  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["idart"];?>  </font></td>
									<td width=350 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["titulo"];?> </font></td>
									<td width=75  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["fecha"];?>  </font></td>
									<td width=275 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["iduser"];?> </font></td>

									<td width=50  bgcolor=#f8f8f8>
										<a href="#" onClick="modificar(<?php echo $_POST["idart"]; ?>);">
											<font face="arial" style="font-size: 11px;; color: blue;">modificar</font>
										</a>
									</td>
									<td width=50  bgcolor=#f8f8f8>
										<a href="#" onClick="eliminar(<?php echo $_POST["idart"]; ?>);">
											<font face="arial" style="font-size: 11px;; color: blue;">eliminar</font>
										</a>
									</td>
								</tr>
							</form>
							<?php
						}
						?>
                    </table>
                </td>
            </tr>		
        </table>
    </body>
</html>
