<?php

// Incluímos Objetos necesarios
require("objetos/formulario.php");
// Incluímos funciones necesarias
require("fun/funciones.php");

// Obtenemos los datos del formulario
$form->idformu  = init("idformu");
$form->formu    = init("formu");
$form->mail     = init("mail");
$form->name     = init("name");
$form->fecha    = init("fecha");
$form->precio   = init("precio");
$form->teamname = init("teamname");
$form->categ    = init("categ");
$form->capname  = init("capname");
$form->captel   = init("captel");
$form->capmail  = init("capmail");
$form->n01name  = init("n01name");
$form->n01tel   = init("n01tel");
$form->n01mail  = init("n01mail");
$form->n02name  = init("n02name");
$form->n02tel   = init("n02tel");
$form->n02mail  = init("n02mail");
$form->n03name  = init("n03name");
$form->n03tel   = init("n03tel");
$form->n03mail  = init("n03mail");
$form->n04name  = init("n04name");
$form->n04tel   = init("n04tel");
$form->n04mail  = init("n04mail");
$form->n05name  = init("n05name");
$form->n05tel   = init("n05tel");
$form->n05mail  = init("n05mail");

// Guardamos los cambios si es posible y redireccionamos en función del resultado
if(modificarFormulario($form)==true) redirect("index.php?origen=formu",0);
else                                 redirect("index.php?origen=error",0);

?>