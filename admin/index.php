<?php

// Funciones
require_once("../conf.php");
require_once("fun/funciones.php");

// Objetos
require_once("objetos/club.php");

// Ejecutamos la función para obtener los datos de usuario
$usuario = loginUsuario_login($_COOKIE["cmsweb_mail"],$_COOKIE["cmsweb_pasw"]);

// Si el usuario existe
if($usuario->usuario!="") {

    // Dependiendo de cual sea el origen, redireccionamos
	switch(trim(init('origen'))){
        case "":        redirect(conf_RUTA."admin/01club.php");        break;
		case "club":    redirect(conf_RUTA."admin/01club.php");        break;
		case "temp":    redirect(conf_RUTA."admin/02temp.php");        break;
		case "cate":    redirect(conf_RUTA."admin/03cate.php");        break;
		case "jorna":   redirect(conf_RUTA."admin/04jornada.php");     break;
		case "equipo":  redirect(conf_RUTA."admin/06equipos.php");     break;
		case "persona": redirect(conf_RUTA."admin/07personas.php");    break;
		case "insta":   redirect(conf_RUTA."admin/08insta.php");       break;
		case "torneo":  redirect(conf_RUTA."admin/09torneos.php");     break;
		case "formu":   redirect(conf_RUTA."admin/10formularios.php"); break;
		case "social":  redirect(conf_RUTA."admin/12sociales.php");    break;
		case "user":    redirect(conf_RUTA."admin/13users.php");       break;
		case "arti":    redirect(conf_RUTA."admin/14articulos.php");   break;
		case "comen":   redirect(conf_RUTA."admin/15comentarios.php"); break;
		case "tags":    redirect(conf_RUTA."admin/16tags.php");        break;
		case "patro":   redirect(conf_RUTA."admin/17patro.php");       break;
		case "album":   redirect(conf_RUTA."admin/20fotos.php");       break;
		case "error":   redirect(conf_RUTA."admin/logs/log.txt");      break;
		case "croni":   redirect(conf_RUTA."admin/21cronicas.php");    break;
		default:        redirect(conf_RUTA."admin/01club.php");        break;
	}

} else {
    // redireccionamos a la pagina de inicio porque no hay login
	redirect(conf_RUTA."index.php",0);
}
?>