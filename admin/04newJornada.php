<?php

// Incluye los objetos necesarios
require("objetos/temp.php");
require("objetos/cate.php");
require("objetos/jornada.php");
require("objetos/insta.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
	<script language="JavaScript" type="text/javascript">
		function modificar(idcate,idjorn) {
			document.location.href="04modJornada.php?idcate=" + idcate + "&idjorn=" + idjorn;
		}
		function eliminar(idcate,idjorn) {
			document.location.href="04delJornada.php?idcate=" + idcate + "&idjorn=" + idjorn;
		}
    </script>
    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::jornada::nueva</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - jornada - nueva'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=800 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
					
					<?php
					$idcat = $_GET["idcate"];
					$categoria = obtenerUnaCate($idcat);
					?>
                    
                    <form name="datos" action="guardarJornada.php" method=post>
                        <table border=0 width=850>
							<tr>
								<td colspan=1 height=20 width=850 bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">Nueva jornada en la categoría<?php echo " " . $categoria->name; ?></font></td>
							</tr>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="04jornada.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        
						<table width=850>
							<tr>
								<td height=20 width=10  bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">partido    </font></td>
								<td height=20 width=100 bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">local      </font></td>
								<td height=20 width=100 bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">visitante  </font></td>
								<td height=20 width=25  bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">dia        </font></td>
								<td height=20 width=25  bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">hora       </font></td>
								<td height=20 width=100 bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">instalación</font></td>
							</tr>
							
							<input type="hidden" name="idcat" value="<?php echo $idcat; ?>"></input>
							
							<?php
							for($j=0;$j<($categoria->numteams/2);$j++) {
							
								$nombre1 = 'local' . str_pad($j+1, 2, "0", STR_PAD_LEFT);
								$nombre2 = 'visit' . str_pad($j+1, 2, "0", STR_PAD_LEFT);
								$insta   = 'inst'  . str_pad($j+1, 2, "0", STR_PAD_LEFT);
								$fecha   = 'fecha' . str_pad($j+1, 2, "0", STR_PAD_LEFT);
								$hour    = 'hour'  . str_pad($j+1, 2, "0", STR_PAD_LEFT);
							
								?>
								<tr>
									<td height=20 width=10 bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $j+1; ?></font></td>
								
									<td height=20 width=100 bgcolor=#f8f8f8>
										<select name="<?php echo $nombre1; ?>" style="font-size:11px;;">
											<option value="" selected="null">local</option>
											<?php
												if($categoria->rival01!="") echo"<option value=\"" . $categoria->rival01 . "\">" . $categoria->rival01 . "</option>";
												if($categoria->rival02!="") echo"<option value=\"" . $categoria->rival02 . "\">" . $categoria->rival02 . "</option>";
												if($categoria->rival03!="") echo"<option value=\"" . $categoria->rival03 . "\">" . $categoria->rival03 . "</option>";
												if($categoria->rival04!="") echo"<option value=\"" . $categoria->rival04 . "\">" . $categoria->rival04 . "</option>";
												if($categoria->rival05!="") echo"<option value=\"" . $categoria->rival05 . "\">" . $categoria->rival05 . "</option>";
												if($categoria->rival06!="") echo"<option value=\"" . $categoria->rival06 . "\">" . $categoria->rival06 . "</option>";
												if($categoria->rival07!="") echo"<option value=\"" . $categoria->rival07 . "\">" . $categoria->rival07 . "</option>";
												if($categoria->rival08!="") echo"<option value=\"" . $categoria->rival08 . "\">" . $categoria->rival08 . "</option>";
												if($categoria->rival09!="") echo"<option value=\"" . $categoria->rival09 . "\">" . $categoria->rival09 . "</option>";
												if($categoria->rival10!="") echo"<option value=\"" . $categoria->rival10 . "\">" . $categoria->rival10 . "</option>";
												if($categoria->rival11!="") echo"<option value=\"" . $categoria->rival11 . "\">" . $categoria->rival11 . "</option>";
												if($categoria->rival12!="") echo"<option value=\"" . $categoria->rival12 . "\">" . $categoria->rival12 . "</option>";
												if($categoria->rival13!="") echo"<option value=\"" . $categoria->rival13 . "\">" . $categoria->rival13 . "</option>";
												if($categoria->rival14!="") echo"<option value=\"" . $categoria->rival14 . "\">" . $categoria->rival14 . "</option>";
												if($categoria->rival15!="") echo"<option value=\"" . $categoria->rival15 . "\">" . $categoria->rival15 . "</option>";
												if($categoria->rival16!="") echo"<option value=\"" . $categoria->rival16 . "\">" . $categoria->rival16 . "</option>";
												if($categoria->rival17!="") echo"<option value=\"" . $categoria->rival17 . "\">" . $categoria->rival17 . "</option>";
												if($categoria->rival18!="") echo"<option value=\"" . $categoria->rival18 . "\">" . $categoria->rival18 . "</option>";
												if($categoria->rival19!="") echo"<option value=\"" . $categoria->rival19 . "\">" . $categoria->rival19 . "</option>";
												if($categoria->rival20!="") echo"<option value=\"" . $categoria->rival20 . "\">" . $categoria->rival20 . "</option>";
											?>
										</select>
									</td>
									<td height=20 width=100 bgcolor=#f8f8f8>
										<select name="<?php echo $nombre2; ?>" style="font-size:11px;;">
											<option value="" selected="null">visitante</option>
											<?php
												if($categoria->rival01!="") echo"<option value=\"" . $categoria->rival01 . "\">" . $categoria->rival01 . "</option>";
												if($categoria->rival02!="") echo"<option value=\"" . $categoria->rival02 . "\">" . $categoria->rival02 . "</option>";
												if($categoria->rival03!="") echo"<option value=\"" . $categoria->rival03 . "\">" . $categoria->rival03 . "</option>";
												if($categoria->rival04!="") echo"<option value=\"" . $categoria->rival04 . "\">" . $categoria->rival04 . "</option>";
												if($categoria->rival05!="") echo"<option value=\"" . $categoria->rival05 . "\">" . $categoria->rival05 . "</option>";
												if($categoria->rival06!="") echo"<option value=\"" . $categoria->rival06 . "\">" . $categoria->rival06 . "</option>";
												if($categoria->rival07!="") echo"<option value=\"" . $categoria->rival07 . "\">" . $categoria->rival07 . "</option>";
												if($categoria->rival08!="") echo"<option value=\"" . $categoria->rival08 . "\">" . $categoria->rival08 . "</option>";
												if($categoria->rival09!="") echo"<option value=\"" . $categoria->rival09 . "\">" . $categoria->rival09 . "</option>";
												if($categoria->rival10!="") echo"<option value=\"" . $categoria->rival10 . "\">" . $categoria->rival10 . "</option>";
												if($categoria->rival11!="") echo"<option value=\"" . $categoria->rival11 . "\">" . $categoria->rival11 . "</option>";
												if($categoria->rival12!="") echo"<option value=\"" . $categoria->rival12 . "\">" . $categoria->rival12 . "</option>";
												if($categoria->rival13!="") echo"<option value=\"" . $categoria->rival13 . "\">" . $categoria->rival13 . "</option>";
												if($categoria->rival14!="") echo"<option value=\"" . $categoria->rival14 . "\">" . $categoria->rival14 . "</option>";
												if($categoria->rival15!="") echo"<option value=\"" . $categoria->rival15 . "\">" . $categoria->rival15 . "</option>";
												if($categoria->rival16!="") echo"<option value=\"" . $categoria->rival16 . "\">" . $categoria->rival16 . "</option>";
												if($categoria->rival17!="") echo"<option value=\"" . $categoria->rival17 . "\">" . $categoria->rival17 . "</option>";
												if($categoria->rival18!="") echo"<option value=\"" . $categoria->rival18 . "\">" . $categoria->rival18 . "</option>";
												if($categoria->rival19!="") echo"<option value=\"" . $categoria->rival19 . "\">" . $categoria->rival19 . "</option>";
												if($categoria->rival20!="") echo"<option value=\"" . $categoria->rival20 . "\">" . $categoria->rival20 . "</option>";
											?>
										</select>
									</td>
									
									<td height=20 width=25 bgcolor=#f8f8f8><input align=left type=text name="<?php echo $fecha; ?>" size=8 value=""></input></td>
									<td height=20 width=25 bgcolor=#f8f8f8><input align=left type=text name="<?php echo $hour; ?>"  size=3 value=""></input></td>
									
									<td height=20 width=100 bgcolor=#f8f8f8>
										<select name="<?php echo $insta; ?>" style="font-size:11px;;">
											<option value="" selected="null">instalación</option>
											<?php
												$insta = obtenerInsta();;

												for ($i=0;$i<numRows("CMS_insta");$i++) {
													echo"<option value=\"" . $insta[$i]->name . "\">" . $insta[$i]->name . "</option>";
												}
												?>
											?>
										</select>
									</td>
								</tr>
							<?php
							}
							?>
							
						</table>
						
						<!-- Guardar -->
						<table width=850>
                            <tr><td colspan=6 bgcolor=#ffffff><a href="#" onclick="javascript:document.forms['datos'].submit(); return false;"><font face="arial" style="font-size: 11px;; color: blue;">Guardar jornada</font></a></td></tr>
                        </table>
                        
						<?php
						
						$jornadas = obtenerJornadas($idcat);
						$valores = array();
						
						for($j=0;$j<numRowsJornadas($idcat);$j++) {
							?>
							<table width=850>
								<tr>
									<td height=20 width=650 bgcolor=#ffffff align="left"><font face="verdana" color="black" style="font-size: 11px;;">Jornada<?php echo " " . $jornadas[$j]->idjorn; ?></font></td>
									<td height=20 width=100 bgcolor=#ffffff align="right"><a href="#" onClick="modificar(<?php echo $idcat; ?>,<?php echo $jornadas[$j]->idjorn; ?>);"><font face="arial" style="font-size: 11px;; color: blue;">editar jornada  </font></a></td>
									<td height=20 width=100 bgcolor=#ffffff align="right"><a href="#" onClick="eliminar(<?php echo $idcat; ?>,<?php echo $jornadas[$j]->idjorn; ?>);"> <font face="arial" style="font-size: 11px;; color: blue;">eliminar jornada</font></a></td>
								</tr>
							</table>
							<table width=850>
								<tr>
									<td height=20 width=470 bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">equipo     </font></td>
									<td height=20 width=10  bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">P</font></td>
									<td height=20 width=10  bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">S1</font></td>
									<td height=20 width=10  bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">S2</font></td>
									<td height=20 width=10  bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">S3</font></td>
									<td height=20 width=10  bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">S4</font></td>
									<td height=20 width=10  bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">S5</font></td>
									<td height=20 width=120 bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">fecha</font></td>
									<td height=20 width=100 bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">hora       </font></td>
									<td height=20 width=100 bgcolor=#c8c8c8><font face="verdana" color="black" style="font-size: 11px;;">instalación</font></td>
								</tr>
								<?php if(($jornadas[$j]->local01!="")and($jornadas[$j]->local01!="null")) {  ?>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->local01;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal01; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal011; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal012; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal013; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal014; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal015; ?></font></td>
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->fecha01;  ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->hour01;   ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->inst01;   ?></font></td>
									</tr>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->visit01;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit01; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit011; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit012; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit013; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit014; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit015; ?></font></td>										
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
									</tr>
									<tr height=5><td colspan=10 width=850 bgcolor=#ffffff></td></tr>
								<?php
								} if(($jornadas[$j]->local02!="")and($jornadas[$j]->local02!="null")) {  ?>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->local02;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal02; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal021; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal022; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal023; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal024; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal025; ?></font></td>
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->fecha02;  ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->hour02;   ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->inst02;   ?></font></td>
									</tr>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->visit02;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit02; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit021; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit022; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit023; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit024; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit025; ?></font></td>										
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
									</tr>
									<tr height=5><td colspan=10 width=850 bgcolor=#ffffff></td></tr>
								<?php
								} if(($jornadas[$j]->local03!="")and($jornadas[$j]->local03!="null")) {  ?>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->local03;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal03; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal031; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal032; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal033; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal034; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal035; ?></font></td>
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->fecha03;  ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->hour03;   ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->inst03;   ?></font></td>
									</tr>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->visit03;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit03; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit031; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit032; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit033; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit034; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit035; ?></font></td>										
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
									</tr>
									<tr height=5><td colspan=10 width=850 bgcolor=#ffffff></td></tr>
								<?php
								} if(($jornadas[$j]->local04!="")and($jornadas[$j]->local04!="null")) {  ?>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->local04;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal04; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal041; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal042; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal043; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal044; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal045; ?></font></td>
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->fecha04;  ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->hour04;   ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->inst04;   ?></font></td>
									</tr>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->visit04;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit04; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit041; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit042; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit043; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit044; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit045; ?></font></td>										
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
									</tr>
									<tr height=5><td colspan=10 width=850 bgcolor=#ffffff></td></tr>
								<?php
								} if(($jornadas[$j]->local05!="")and($jornadas[$j]->local05!="null")) {  ?>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->local05;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal05; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal051; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal052; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal053; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal054; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal055; ?></font></td>
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->fecha05;  ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->hour05;   ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->inst05;   ?></font></td>
									</tr>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->visit05;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit05; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit051; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit052; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit053; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit054; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit055; ?></font></td>										
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
									</tr>
									<tr height=5><td colspan=10 width=850 bgcolor=#ffffff></td></tr>
								<?php
								} if(($jornadas[$j]->local06!="")and($jornadas[$j]->local06!="null")) {  ?>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->local06;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal06; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal061; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal062; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal063; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal064; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal065; ?></font></td>
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->fecha01;  ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->hour01;   ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->inst01;   ?></font></td>
									</tr>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->visit06;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit06; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit061; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit062; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit063; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit064; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit065; ?></font></td>										
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
									</tr>
									<tr height=5><td colspan=10 width=850 bgcolor=#ffffff></td></tr>
								<?php
								} if(($jornadas[$j]->local07!="")and($jornadas[$j]->local07!="null")) {  ?>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->local07;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal07; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal071; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal072; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal073; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal074; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal075; ?></font></td>
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->fecha07;  ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->hour07;   ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->inst07;   ?></font></td>
									</tr>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->visit07;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit07; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit071; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit072; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit073; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit074; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit075; ?></font></td>										
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
									</tr>
									<tr height=5><td colspan=10 width=850 bgcolor=#ffffff></td></tr>
								<?php
								} if(($jornadas[$j]->local08!="")and($jornadas[$j]->local08!="null")) {  ?>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->local08;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal08; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal081; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal082; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal083; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal084; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal085; ?></font></td>
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->fecha08;  ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->hour08;   ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->inst08;   ?></font></td>
									</tr>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->visit08;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit08; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit081; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit082; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit083; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit084; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit085; ?></font></td>										
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
									</tr>
									<tr height=5><td colspan=10 width=850 bgcolor=#ffffff></td></tr>
								<?php
								} if(($jornadas[$j]->local09!="")and($jornadas[$j]->local09!="null")) {  ?>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->local09;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal09; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal091; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal092; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal093; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal094; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal095; ?></font></td>
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->fecha09;  ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->hour09;   ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->inst09;   ?></font></td>
									</tr>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->visit09;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit09; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit091; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit092; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit093; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit094; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit095; ?></font></td>										
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
									</tr>
									<tr height=5><td colspan=10 width=850 bgcolor=#ffffff></td></tr>
								<?php
								} if(($jornadas[$j]->local10!="")and($jornadas[$j]->local10!="null")) {  ?>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->local10;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal10; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal101; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal102; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal103; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal104; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->plocal105; ?></font></td>
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->fecha10;  ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->hour10;   ?></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->inst10;   ?></font></td>
									</tr>
									<tr>
										<td height=20 width=470 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->visit10;  ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit10; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit101; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit102; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit103; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit104; ?></font></td>
										<td height=20 width=10  bgcolor=#f8f8f8 align=center><font face="verdana" color="black" style="font-size: 11px;;"><?php echo $jornadas[$j]->pvisit105; ?></font></td>										
										<td height=20 width=120 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
										<td height=20 width=100 bgcolor=#f8f8f8><font face="verdana" color="black" style="font-size: 11px;;"></font></td>
									</tr>
								<?php } 
								?>
								<tr height=20><td colspan=10 width=850 bgcolor=#ffffff></td></tr>
							</table>
							<?php
						}
						?>
						
                    </form>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>