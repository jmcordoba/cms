<?php

// Incluímos Objetos necesarios
require("objetos/persona.php");
// Incluímos funciones necesarias
require("fun/funciones.php");

// Obtenemos las variables del formulario
$person->idperson  = init("idperson");
$person->idteam    = init("idteam");
$person->nombre    = init("nombre");
$person->surname   = init("surname");
$person->fechan    = init("fechan");
$person->foto      = init("foto");
$person->clubyears = init("clubyears");
$person->cargo     = init("cargo");
$person->palmares  = init("palmares");
$person->facebook  = init("facebook");
$person->twitter   = init("twitter");
$person->rol       = init("rol");
$person->fechaalta = init("fechaalta");
$person->fechabaja = init("fechabaja");
$person->conta     = init("conta");
$person->conta     = "";

// Capturamos los datos del fichero adjunto
$nombre_archivo = $_FILES['userfile']['name']; 
$tipo_archivo   = $_FILES['userfile']['type']; 
$tamano_archivo = $_FILES['userfile']['size'];

// Si viene un fichero adjunto
if($nombre_archivo!="") {
    //compruebo si las caracter�sticas del archivo son las que deseo 
    if ($tamano_archivo < 100000) {
        // Redimensionamos el fichero a 200x200
		include('simpleImage.php');
		$image = new SimpleImage();
		$image->load($_FILES['userfile']['tmp_name']);
		$image->resize(240,320);
		$image->save($_FILES['userfile']['tmp_name']);
        // Inicializamos la ruta y el nombre del avatar del usuario
        $person->foto    = "admin/images/persona/person" . $person->idperson . ".gif";
        if (!move_uploaded_file($_FILES['userfile']['tmp_name'], "images/persona/person" . $person->idperson . ".gif")){
            // Si hay algún tipo de error, redirigimos a otra página
            ?><script>location.href='index.php?origen=error';</script><?php
			die();
        }
    } else {
		?>
		<script>
			alert('La imagen que desea subir es mayor de 100kB.\nPor favor, suba una imagen con un peso inferior.');
		</script>
		<?php
	}
}

// Guardamos los cambios si es posible y redireccionamos en función del resultado
if(modificarPersona($person,$person->idperson)==true) redirect("index.php?origen=persona",0);
else                                                  redirect("index.php?origen=error"  ,0);

?>