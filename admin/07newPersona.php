<?php

// Incluye los objetos necesarios
require("objetos/equipo.php");
require("objetos/persona.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::persona::nuevo</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>
    
    <script>
        // Verifica los campos del formulario antes de guardar los datos
        function verificar_form() {
            if(document.datos.nombre.value.length==0) {
                alert("Error:\nDebe ingresar el nombre"); 
                document.datos.nombre.focus() 
                return 0; 
            } else {
                document.forms['datos'].submit();
            }
        }
    </script>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - equipo - nuevo'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=628 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <form name="datos" action="guardarPersona.php" method=post enctype="multipart/form-data">
                        <table border=0 width=850>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="07personas.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <table width=850>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8>
									<font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Selecciona un equipo</font>
								</td>
								<td height=20 width=700 bgcolor=#ffffff>
                                    <select name="idteam">
                                        <option value="" selected="null">Equipo</option>
                                        <?php
                                        $team = obtenerEquipo();;
										for ($i=0;$i<numRows("CMS_equipos");$i++) {
                                            echo"<option value=\"" . $team[$i]->idteam . "\">" . $team[$i]->nombre . "</option>";
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Nombre</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=nombre size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Apellidos</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=surname size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Fecha de nacimiento</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=fechan size=108></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Años en el club</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=clubyears size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Actividad</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=cargo size=108></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Palmarés</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=palmares size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Facebook</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=facebook size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Twitter</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=twitter size=108></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Rol</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=rol size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Fecha de alta</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=fechaalta size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Fecha de baja</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=fechabaja size=108></input></td>
                            </tr>
							
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Foto</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input type="file" name="userfile" style="width:400px;border:0px solid #000000;"></input></td>
                            </tr>
                            <tr>
                                <td bgcolor=#ffffff><a href="#" onclick="verificar_form();"><font face="arial" style="font-size: 11px;; color: blue;">Guardar persona</font></a></td>
                            </tr>
                        </table>
                        
                    </form>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>