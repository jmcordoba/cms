<?php

// Incluye los objetos necesarios
require("objetos/articulo.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::atículos::modificar</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>
    
    <script>
        // Verifica los campos del formulario antes de guardar los datos
        function verificar_form() {
            if(document.datos.titulo.value.length==0) {
                alert("Error:\nDebe ingresar el titulo"); 
                document.frm.nombre.focus() 
                return 0; 
            }
            else if(document.datos.contenido.value.length==0) {
                alert("Error:\nDebe ingresar el contenido"); 
                document.frm.usuario.focus() 
                return 0; 
            }
            else {
                document.forms['datos'].submit();
            }
        }
    </script>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - artículos - modificar'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=1000 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <?php
                    
                    $idart = $_GET["idart"];
                    
                    $articulo = obtenerUnArticulo($idart);
                    
                    $_POST["idart"]     = $articulo->idart;
                    $_POST["fecha"]     = $articulo->fecha;
                    $_POST["iduser"]    = $articulo->iduser;
                    $_POST["titulo"]    = $articulo->titulo;
                    $_POST["contenido"] = strip_tags($articulo->contenido);
                    $_POST["idtag"]     = $articulo->idtag;
                    $_POST["conta"]     = $articulo->conta;
                    $_POST["fechasi"]   = $articulo->fechasi;
                    $_POST["fechano"]   = $articulo->fechano;
                    $_POST["imagen"]    = $articulo->imagen;
                    ?>

                    <form name="datos" action="modificarArticulo.php" method=post enctype="multipart/form-data">
                        
						<input type="hidden" name="idart" readonly value="<?php echo $idart;?>">
						
						<table border=0 width=850>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="14articulos.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <table width=850>
                            <!--tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">idart</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=idart size=108 readonly="readonly" value="<?php echo $idart;?>"></input></td>
                            </tr-->
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">fecha</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=fecha size=108 readonly="readonly" value="<?php echo date("d/m/Y");?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">iduser</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=iduser size=108 readonly="readonly" value="<?php echo $_POST["iduser"];?>"></input></td>
                            </tr>
                            <!--tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">idtag</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=idtag size=108 value="<?php echo $_POST["idtag"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">conta</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=conta size=108 readonly="readonly" value="<?php echo $_POST["conta"];?>"></input></td>
                            </tr-->
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">fecha publicación</font></td>
                                <td height=20 width=700 bgcolor=#ffffff>
                                    <input class="admin_input" type=text name=fechasi size=10 value="<?php echo $_POST["fechasi"];?>"></input><font>(Formato de fecha dd/mm/yyyy)</font>
                                </td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">fechano</font></td>
                                <td height=20 width=700 bgcolor=#ffffff>
                                    <input class="admin_input" type=text name=fechano size=10 value="<?php echo $_POST["fechano"];?>"></input><font>(Formato de fecha dd/mm/yyyy)</font>
                                </td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">imagen</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input type="file" name="imagen" style="width:400px;border:0px solid #000000;"></input></td>
                            </tr>
                        </table>
                        <table>
                            <tr><td height=20 width=850 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">titulo</font></td></tr>
                            <tr><td height=20 width=850 bgcolor=#ffffff><input class="admin_input" style="width:840px;" type=text name=titulo size=132 value="<?php echo $_POST["titulo"];?>"></input></td></tr>
                            <tr><td height=20 width=850 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">contenido</font></td></tr>
                            <tr><td height=20 width=850 bgcolor=#ffffff><textarea class="admin_input" style="width:840px;height:300px;" type=text name=contenido><?php echo $_POST["contenido"];?></textarea></td></tr>
                            <tr><td bgcolor=#ffffff><a href="#" onclick="verificar_form();"><font face="arial" style="font-size: 11px;; color: blue;">Guardar artículo</font></a></td></tr>
                        </table>
                        <?php
                        if ($_POST["imagen"]!="") {
                            ?>
                            <table width=850>
                                <tr valign="bottom" align="left">
                                    <td bgcolor=#ffffff><img border="1" width="100%" src="<?php echo conf_RUTA."admin/".$_POST["imagen"];?>"></td>
                                </tr>
                            </table>
                            <?php
                        }
                        ?>
                    </form>
                </td>
            </tr>		
        </table>
    </body>
</html>
