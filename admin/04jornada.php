<?php

// Incluye los objetos necesarios
require("objetos/jornada.php");
require("objetos/cate.php");
require("objetos/insta.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <script language="JavaScript" type="text/javascript">
	function nuevo(id) {
        document.location.href="04newJornada.php?idcate=" + id;
	}
    </script>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::jornadas</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - jornadas'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=600 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <table width=850>
                        <tr height=10>
							<td width=320 bgcolor=#c8c8c8><font class="admin_font">Categoria </font></td>
                            <td width=320 bgcolor=#c8c8c8><font class="admin_font">Equipo    </font></td>
                            <td width=80  bgcolor=#c8c8c8><font class="admin_font">Jornandas </font></td>
                            <td width=80  bgcolor=#c8c8c8><font class="admin_font">Disputadas</font></td>
                            <td width=50  bgcolor=#c8c8c8><font class="admin_font">&nbsp;</font></td>
                        </tr>


						<?php
						
						$categoria = obtenerCate();
						
						for($i=0;$i<numRows("CMS_cate");$i++) {
						
							$_POST["idcat"]         = $categoria[$i]->idcat;
							$_POST["name"]          = $categoria[$i]->name;
							$_POST["numteams"]      = $categoria[$i]->numteams;
							$_POST["wpoints"]       = $categoria[$i]->wpoints;
							$_POST["dpoints"]       = $categoria[$i]->dpoints;
							$_POST["lpoints"]       = $categoria[$i]->lpoints;
							$_POST["jornadas"]      = $categoria[$i]->jornadas;
							$_POST["idavuelta"]     = $categoria[$i]->idavuelta;
							$_POST["idtemp"]        = $categoria[$i]->idtemp;
							
							$team = obtenerUnEquipoByCat($_POST["idcat"]);
							?>

							<form name="datos" action="guardarCate.php" method=post>
								<tr height=10>                                
									<td width=320 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["name"]; ?>    </font></td>
									<td width=320 bgcolor=#f8f8f8><font class="admin_font"><?php echo $team->nombre; ?>     </font></td>
									<td width=80  bgcolor=#f8f8f8><font class="admin_font"><?php echo (($categoria[$i]->numteams-1)*2);?> </font></td>
									<td width=80  bgcolor=#f8f8f8><font class="admin_font"><?php echo numRowsJornadasDisputadas($categoria[$i]->idcat) . " de " . (($categoria[$i]->numteams-1)*2);?>  </font></td>
									
									<td width=50 bgcolor=#f8f8f8>
										<a href="#" onClick="nuevo(<?php echo $_POST["idcat"]; ?>);">
											<font face="arial" style="font-size: 11px;; color: blue;">editar</font>
										</a>
									</td>
								</tr>
							</form>
							<?php
						}
						?>
                    </table>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>