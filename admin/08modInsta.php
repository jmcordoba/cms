<?php

// Incluye los objetos necesarios
require("objetos/insta.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::instalaciones::modificar</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>
    
    <script>
        // Verifica los campos del formulario antes de guardar los datos
        function verificar_form() {
            if(document.datos.name.value.length==0) {
                alert("Error:\nDebe ingresar el nombre"); 
                document.datos.nombre.focus() 
                return 0; 
            }
            else if(document.datos.direccion.value.length==0) {
                alert("Error:\nDebe ingresar la dirección"); 
                document.datos.apellidos.focus() 
                return 0; 
            }
            else if(document.datos.telefono.value.length==0) {
                alert("Error:\nDebe ingresar el telefono"); 
                document.datos.usuario.focus() 
                return 0; 
            }
            else if(document.datos.mail.value.length==0) {
                alert("Error:\nDebe ingresar el mail"); 
                document.datos.password.focus() 
                return 0; 
            }
            else if(document.datos.persona.value.length==0) {
                alert("Error:\nDebe ingresar la persona de contacto"); 
                document.datos.telefono.focus() 
                return 0; 
            }
            else {
                document.forms['datos'].submit();
            }
        }
    </script>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - instalaciones - modificar'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=800 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <?php
                    
                    $idinst = $_GET["idinst"];
                    
                    $instalacion = obtenerUnaInsta($idinst);
                    
                    $_POST["idinst"]    = $instalacion->idcat;
                    $_POST["name"]      = $instalacion->name;
                    $_POST["direccion"] = $instalacion->direccion;
                    $_POST["telefono"]  = $instalacion->telefono;
                    $_POST["mail"]      = $instalacion->mail;
                    $_POST["persona"]   = $instalacion->persona;
					$_POST["conta"]     = str_replace("\\\"","",rawurldecode($instalacion->conta));
                    $_POST["foto"]      = $instalacion->foto;
                    ?>

                    <form name="datos" action="modificarInsta.php" method=post enctype="multipart/form-data">
                        
						<input type="hidden" name="idinst" readonly value="<?php echo $idinst;?>">
						
						<table border=0 width=850>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="08insta.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <table width=850>
                            
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Nombre</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=name size=108 value="<?php echo $_POST["name"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Dirección</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=direccion size=108 value="<?php echo $_POST["direccion"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Teléfono</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=telefono size=108 value="<?php echo $_POST["telefono"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Mail</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=mail size=108 value="<?php echo $_POST["mail"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Persona de contacto</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=persona size=108 value="<?php echo $_POST["persona"];?>"></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Foto</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=foto size=108 value="<?php echo $_POST["foto"];?>" readonly></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">Nueva foto</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input type="file" name="userfile" style="width:400px;border:0px solid #000000;"></input></td>
                            </tr>
                        </table>
                        
						
						<table width="850">
							<tr valign="bottom" align="left">
								<?php
								// si existe la foto de la instalacion
								if ($_POST["foto"]!="") {
									?><td width="400" bgcolor="#ffffff"><img width="400" height="250" src="<?php echo conf_RUTA.$_POST["foto"];?>"></td><?php
								} else {
									?><td width="400" bgcolor="#ffffff"></td><?php
								}
								?>
							</tr>
						</table>
						
						<table width="850">
							<tr align="left">
								<td colspan="2" bgcolor="#ffffff">
									<a href="#" onclick="verificar_form();">
										<font face="arial" style="font-size: 11px;; color: blue;">Guardar instalación</font>
									</a>
								</td>
							</tr>
                        </table>
                    </form>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>