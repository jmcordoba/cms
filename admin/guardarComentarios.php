<?php

// Incluye los objetos necesarios
require("objetos/comentario.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Obtenemos las variables del formulario
$comen->idart     = init("idart");
$comen->idcom     = init("idcom");
$comen->fecha     = init("fecha");
$comen->iduser    = init("iduser");
$comen->contenido = str_replace("'","\'",init("contenido"));
$comen->conta     = init("conta");

// Intentamos guardar el usuario y redirigimos en función de si hemos tenido éxito o no
if(guardarComentario($comen)==true) redirect("index.php?origen=comen" ,0);
else                                redirect("index.php?origen=error",0);
?>