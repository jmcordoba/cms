<?php

// Incluye los objetos necesarios
require("objetos/tags.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Obtenemos las variables del formulario
$tag->idtag  = init("idtag");
$tag->nombre = init("nombre");
$tag->conta  = init("conta");

// Intentamos guardar el usuario y redirigimos en función de si hemos tenido éxito o no
if(guardarTags($tag)==true) redirect("index.php?origen=tags" ,0);
else                        redirect("index.php?origen=error",0);
?>
