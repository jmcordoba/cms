<?php

// Incluimos objetos y funciones
require("objetos/usuario.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Control de usuario identificado
require("check.php");

// Capturamos el id de la categoría a eliminar
$user = $_GET["usuario"];

// Eliminamos categoria y redirigimos
if(borrarUser($user)==true) redirect("index.php?origen=user" ,0);
else                        redirect("index.php?origen=error",0);

?>
