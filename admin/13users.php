<?php

// Incluye los objetos necesarios
require("objetos/usuario.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <script language="JavaScript" type="text/javascript">
	function modificar(user){
		document.location.href="13modUser.php?usuario=" + user;
	}
    function eliminar(user){
		document.location.href="13delUser.php?usuario=" + user;
	}
    function nuevo(){
		document.location.href="13newUser.php";
	}
    </script>

    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>cms::administración::usuaros</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - usuarios'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=700 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <table border=0 width=850>
                        <tr align="right">
                            <td width=850 bgcolor=#ffffff>
                                <a href="#" onClick="nuevo();">
                                    <font face="arial" style="font-size: 11px;; color: blue;">Introduce nuevo usuario</font>
                                </a>
                            </td>
                        </tr>
                    </table>
                    
                    <table width=850>
                        <tr height=10>
                            <td width=175 bgcolor=#c8c8c8><font class="admin_font">Nombre</font></td>
                            <td width=175 bgcolor=#c8c8c8><font class="admin_font">Apellidos</font></td>
                            <td width=150 bgcolor=#c8c8c8><font class="admin_font">Usuario</font></td>
                            <td width=150 bgcolor=#c8c8c8><font class="admin_font">Password</font></td>
                            
                            <td width=50 bgcolor=#c8c8c8>&nbsp;</td>
                            <td width=50 bgcolor=#c8c8c8>&nbsp;</td>
                        </tr>

						<?php
						$usuario = obtenerUsers();
						
						for($i=0;$i<numRows("CMS_users");$i++) {
						
							$_POST["nombre"]    = $usuario[$i]->nombre;
							$_POST["apellidos"] = $usuario[$i]->apellidos;
							$_POST["usuario"]   = $usuario[$i]->usuario;
							$_POST["password"]  = $usuario[$i]->password;
							$_POST["telefono"]  = $usuario[$i]->telefono;
							$_POST["mail"]      = $usuario[$i]->mail;
							$_POST["permisos"]  = $usuario[$i]->permisos;
							$_POST["conta"]     = $usuario[$i]->conta;
							$_POST["validado"]  = $usuario[$i]->validado;
							$_POST["foto"]      = $usuario[$i]->foto;
							?>

							<form name="datos" action="guardarInsta.php" method=post>
								<tr height=10>
									<td width=175 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["nombre"];?>   </font></td>
									<td width=175 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["apellidos"];?></font></td>
									<td width=150 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["usuario"];?>  </font></td>
									<td width=150 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["password"];?> </font></td>
									
									<td width=50  bgcolor=#f8f8f8>
										<a href="#" onClick="modificar('<?php echo $usuario[$i]->usuario; ?>');">
											<font face="arial" style="font-size: 11px;; color: blue;">modificar</font>
										</a>
									</td>
									<td width=50  bgcolor=#f8f8f8>
										<a href="#" onClick="eliminar('<?php echo $usuario[$i]->usuario; ?>');">
											<font face="arial" style="font-size: 11px;; color: blue;">eliminar</font>
										</a>
									</td>
								</tr>
							</form>
							<?php
						}
						?>
                    </table>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>
