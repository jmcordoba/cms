<?php

// Incluye los objetos necesarios
require("objetos/insta.php");
// Incluye las funciones necesarios
require("fun/funciones.php");

// Obtenemos las variables del formulario
$insta->idinst    = init("idinst");
$insta->name      = str_replace("'","\'",init("name"));
$insta->direccion = str_replace("'","\'",init("direccion"));
$insta->telefono  = init("telefono");
$insta->mail      = init("mail");
$insta->persona   = init("persona");
$insta->conta     = rawurlencode($_POST["conta"]);

// Capturamos los datos del fichero adjunto
$nombre_archivo = $_FILES['userfile']['name']; 
$tipo_archivo   = $_FILES['userfile']['type']; 
$tamano_archivo = $_FILES['userfile']['size'];
// Inicializamos la variable numero de filas a cero
$numero = 0;
// Conexión con la base de datos
$link=Conectarse();
// Construcción de la query
$sql = "select * from `CMS_insta` order by idinst";
// Registro de log
wlog("guardarInsta",$sql,1);
// Ejecutamos la query y obtenemos el resultado
$result = mysql_query($sql, $link);
// Obtenemos el ultimo idtemp
if ($row = mysql_fetch_array($result))
{
    do{
        $numero = $row["idinst"];
    } while ($row = mysql_fetch_array($result));
}
$numero++;
// Cerramos la conexion con la base de datos
mysql_close($link);
// Si viene un fichero adjunto
if($nombre_archivo!="") {
    //compruebo si las caracter�sticas del archivo son las que deseo 
    if ($tamano_archivo < 100000) {
        // Redimensionamos el fichero a 200x200
		include('simpleImage.php');
		$image = new SimpleImage();
		$image->load($_FILES['userfile']['tmp_name']);
		$image->resize(600,400);
		$image->save($_FILES['userfile']['tmp_name']);
		// Inicializamos la ruta y el nombre del avatar del usuario
        $insta->foto    = "admin/images/instalaciones/insta" . $numero . ".gif";
        // Guardamos el fichero en la ruta especificada
        if (!move_uploaded_file($_FILES['userfile']['tmp_name'], "images/instalaciones/insta" . $numero . ".gif")){
            // S ihay algún tipo de error, redirigimos a otra página
            ?><script>location.href='index.php?origen=error';</script><?php
        }
    } else {
		?>
		<script>
			alert('La imagen que desea subir es mayor de 100kB.\nPor favor, suba una imagen con un peso inferior.');
		</script>
		<?php
	}
}
// Intentamos guardar el usuario y redirigimos en función de si hemos tenido éxito o no
if(guardarInsta($insta)==true) redirect("index.php?origen=insta",0);
else                           redirect("index.php?origen=error",0);

?>