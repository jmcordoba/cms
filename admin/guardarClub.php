<?php

// Incluye los objetos necesarios
require("objetos/club.php");
// Incluye las funciones necesarios
require("fun/funciones.php");

require_once("../conf.php");

// Capturamos los datos que vienen por POST y los guardamos en el objeto equipo
$equipo->name     = init("name");
$equipo->presi    = init("presi");
$equipo->street   = init("street");
$equipo->cpostal  = init("cpostal");
$equipo->telefono = init("telefono");
$equipo->fax      = init("fax");
$equipo->mail     = init("mail");
$equipo->foto     = 'images/escudo/escudo.jpg';
$equipo->palmares = nl2br(init("palmares"));
$equipo->historia = nl2br(init("historia"));

// Capturamos los datos del fichero adjunto
$nombre_archivo   = $_FILES["userfile"]["name"]; 
$tipo_archivo     = $_FILES["userfile"]["type"]; 
$tamano_archivo   = $_FILES["userfile"]["size"];

// Si viene un fichero adjunto
if($nombre_archivo!="") {
    //compruebo si las caracter�sticas del archivo son las que deseo 
    if ($tamano_archivo < 100000) {
        // Redimensionamos el fichero a 200x200
		include('simpleImage.php');
		$image = new SimpleImage();
		$image->load($_FILES['userfile']['tmp_name']);
		$image->resize(200,200);
		$image->save($_FILES['userfile']['tmp_name']);
		// Guardamos el fichero en la ruta especificada
		if (!move_uploaded_file($_FILES['userfile']['tmp_name'], "images/escudo/escudo.jpg")) {
            // S ihay algún tipo de error, redirigimos a otra página
            ?><script>location.href='index.php?origen=error';</script><?php
        }
    } else {
		?>
		<script>
			alert('La imagen que desea subir es mayor de 100kB.\nPor favor, suba una imagen con un peso inferior.');
		</script>
		<?php
	}
}

// Si no hay datos de equipo los tenemos que insertar
if(numRows("CMS_club")==0) {
    // Intentamos guardar el usuario y redirigimos en función de si hemos tenido éxito o no
    if(guardarClub($equipo)==true)
        redirect("index.php?origen=club" ,0);
    else
        redirect("index.php?origen=error",0);
}

// Si hay datos de equipo los tenemos que modificar
if(numRows("CMS_club")==1) {
    // Intentamos guardar el usuario y redirigimos en función de si hemos tenido éxito o no
    if(modificarClub($equipo)==true) redirect("index.php?origen=club" ,0);
    else                             redirect("index.php?origen=error",0);
}

?>
