<?php

// Incluimos objetos y funciones
require("objetos/persona.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
// Capturamos el id de la categoría a eliminar
$idperson = $_GET["idperson"];
// Eliminamos categoria y redirigimos
if(borrarPersona($idperson)==true) redirect("index.php?origen=persona" ,0);
else                               redirect("index.php?origen=error"   ,0);

?>