<?php

// Incluye los objetos necesarios
require("objetos/cronica.php");
require("objetos/equipo.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Control de usuario identificado
require("check.php");

// obtenemos el id del equipo seleccionado en el desplegable si es que se ha hecho
$idteam = isset($_REQUEST['idteam']) ? $_REQUEST['idteam'] : "";
?>

<!DOCTYPE html>

<html>

    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>cms::administración::crónicas::nuevo</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>
    
    <script>
        // Verifica los campos del formulario antes de guardar los datos
        function verificar_form() {
            if(document.datos.titulo.value.length==0) {
                alert("Error:\nDebe ingresar el titulo"); 
                document.datos.nombre.focus() 
                return 0; 
            }
            else if(document.datos.contenido.value.length==0) {
                alert("Error:\nDebe ingresar el contenido"); 
                document.datos.usuario.focus() 
                return 0; 
            }
			else if(document.datos.idteam.value.length==0) {
                alert("Error:\nDebe seleccionar el equipo al que le escribe la cronica"); 
                document.datos.idteam.focus() 
                return 0; 
            }
			else {
                document.forms['datos'].submit();
            }
        }
		// recargamos la pagina con el identificador de equipo que hemos seleccionado
		function reload() {
			location.href="21newCronica.php?idteam="+document.forms['datos'].idteam.value;
		}
    </script>

    <body link="#004080" vlink="#004080" alink="#004080" bgcolor="#ffffff" style="margin:0px">   
        
		<!-- cabecera -->
        <?php $titulo = 'cms - administración - crónicas - nuevo'; require('cabecera.php'); ?>

        <!-- cuerpo -->
        <table style="position: relative;top: 60px;" border="0" color="black" cellspacing="0" width="1000" height="800" align="center">
            <tr>
                
				<!-- menu -->
                <td width="150" style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                
				<!-- contenido -->
                <td width="850" style="vertical-align:top">
                    
					<form name="datos" action="guardarCronica.php" method="post" enctype="multipart/form-data">
                        <table border="0" width="850">
                            <tr align="left">
                                <td width="850" bgcolor="#ffffff">
                                    <a href="21cronicas.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        <table width="850">
                            <tr>
                                <td height="20" width="150" bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">idteam</font></td>
                                <td height="20" width="700" bgcolor=#ffffff>
									
									<select name="idteam" onChange="reload();" style="width:650px">
										<?php
										if(strlen($idteam)==0) $selected = " selected ";
										else            	   $selected = "";
										?>
										<option value="" <?php echo $selected; ?> >Todos los equipos</option>
										<?php
										
										$idcat = "";
										
										$equipo = obtenerEquipo();
										
										for ($i=0;$i<numRows("CMS_cate");$i++) {
											if($idteam==$equipo[$i]->idteam) {
												$selected = " selected ";
												$idcat = $equipo[$i]->idcat;
											} else {
												$selected = " ";
											}
											echo"<option $selected value=\"" . $equipo[$i]->idteam . "\">" . $equipo[$i]->nombre . "</option>";
										}
										?>
									</select>
								</td>
                            </tr>
							<tr>
                                <td height="20" width="150" bgcolor="#c8c8c8"><font  face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">idcat</font></td>
                                <td height="20" width="700" bgcolor="#ffffff"><input class="admin_input" type="text"   name="idcat" size="108" readonly="readonly" value="<?php echo $idcat; ?>"></input></td>
                            </tr>
                            <tr>
                                <td height="20" width="150" bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">fecha</font></td>
                                <td height="20" width="700" bgcolor=#ffffff><input class="admin_input" type=text name=fecha size=108 readonly="readonly" value="<?php echo date("d/m/Y");?>"></input></td>
                            </tr>
                            <!--tr>
                                <td height="20" width="150" bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">idtag</font></td>
                                <td height="20" width="700" bgcolor=#ffffff><input class="admin_input" type=text name=idtag size=108 readonly="readonly"></input></td>
                            </tr>
                            <tr>
                                <td height="20" width="150" bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">conta</font></td>
                                <td height="20" width="700" bgcolor=#ffffff><input class="admin_input" type=text name=conta size=108 readonly="readonly"></input></td>
                            </tr-->
                            <tr>
                                <td height="20" width="150" bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">fecha publicación</font></td>
                                <td height="20" width="700" bgcolor=#ffffff>
                                    <input class="admin_input" type=text name=fechasi size=10></input><font>(Formato de fecha dd/mm/yyyy)</font>
                                </td>
                            </tr>
                            <tr>
                                <td height="20" width="150" bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">fecha retirada</font></td>
                                <td height="20" width="700" bgcolor=#ffffff>
                                    <input class="admin_input" type=text name=fechano size=10></input><font>(Formato de fecha dd/mm/yyyy)</font>
                                </td>
                            </tr>
                            <tr>
                                <td height="20" width="150" bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">imagen</font></td>
                                <td height="20" width="700" bgcolor=#ffffff><input type="file" name="imagen" style="width:400px;border:0px solid #000000;"></input></td>
                            </tr>
                        </table>
                        <table>
                            <tr><td height="20" width=850 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">titulo</font></td></tr>
                            <tr><td height="20" width=850 bgcolor=#ffffff><input class="admin_input" style="width:840px;" type=text name=titulo size=132></input></td></tr>
                            <tr><td height="20" width=850 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">contenido</font></td></tr>
                            <tr><td height="20" width=850 bgcolor=#ffffff><textarea class="admin_input" style="width:840px;height:300px;" type=text name=contenido></textarea></td></tr>
                            <tr><td bgcolor=#ffffff><a href="#" onclick="verificar_form();"><font face="arial" style="font-size: 11px;; color: blue;">Guardar crónica</font></a></td></tr>
                        </table>
                    </form>
                </td>
            </tr>		
        </table>
    </body>
</html>
