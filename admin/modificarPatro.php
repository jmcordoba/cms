<?php

// Incluímos Objetos necesarios
require("objetos/patro.php");
// Incluímos funciones necesarias
require("fun/funciones.php");

// Obtenemos los datos del formulario
$patro->idpatro    = init("idpatro");
$patro->nombre     = str_replace("'","\'",init("nombre"));
$patro->fecha_alta = init("fecha_alta");
$patro->fecha_baja = init("fecha_baja");
$patro->importe    = init("importe");
$patro->web        = init("web");
$patro->conta      = init("conta");
$patro->imagen     = init("imagen");

// Capturamos los datos del fichero adjunto
$nombre_archivo = $_FILES['nimagen']['name']; 
$tipo_archivo   = $_FILES['nimagen']['type']; 
$tamano_archivo = $_FILES['nimagen']['size'];

// Si viene un fichero adjunto
if($nombre_archivo!="") {
    //compruebo si las caracter�sticas del archivo son las que deseo 
    if ($tamano_archivo < 100000) {
		// Inicializamos la ruta y el nombre del avatar del usuario
        $patro->imagen    = "admin/images/patrocinadores/patro" . $patro->idpatro . ".gif";
        if (!move_uploaded_file($_FILES['nimagen']['tmp_name'], "images/patrocinadores/patro" . $patro->idpatro . ".gif")) {
            // Si hay algún tipo de error, redirigimos a otra página
            ?><script>location.href='index.php?origen=error';</script><?php
			die();
        }
    } else {
		?>
		<script>
			alert('La imagen que desea subir es mayor de 100kB.\nPor favor, suba una imagen con un peso inferior.');
		</script>
		<?php
	}
}
// Guardamos los cambios si es posible y redireccionamos en función del resultado
if(modificarPatro($patro,$patro->idpatro)==true) redirect("index.php?origen=patro",0);
else                                             redirect("index.php?origen=error",0);

?>