<?php

// Incluye los objetos necesarios
require("objetos/comentario.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::comentarios::nuevo</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>
    
    <script>
        // Verifica los campos del formulario antes de guardar los datos
        function verificar_form() {
            if(document.datos.idcom.value.length==0) {
                alert("Error:\nDebe ingresar el identificador de comentario"); 
                document.datos.idcom.focus() 
                return 0; 
            }
			else if(document.datos.fecha.value.length==0) {
                alert("Error:\nDebe ingresar la fecha del comentario"); 
                document.datos.fecha.focus() 
                return 0; 
            }
			else if(document.datos.iduser.value.length==0) {
                alert("Error:\nDebe ingresar el identificador del usuario"); 
                document.datos.iduser.focus() 
                return 0; 
            }
			else if(document.datos.contenido.value.length==0) {
                alert("Error:\nDebe ingresar el contenido del comentario"); 
                document.datos.contenido.focus() 
                return 0; 
            }
			else if(document.datos.idart.value.length==0) {
                alert("Error:\nDebe ingresar el identificador del artículo"); 
                document.datos.idart.focus() 
                return 0; 
            }
			else if(document.datos.conta.value.length==0) {
                alert("Error:\nDebe ingresar el contador del comentario"); 
                document.datos.conta.focus() 
                return 0; 
            }
            else {
                document.forms['datos'].submit();
            }
        }
    </script>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - comentario - nuevo'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=628 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <form name="datos" action="guardarComentarios.php" method=post enctype="multipart/form-data">
                        <table border=0 width=850>
                            <tr align="left">
                                <td width=850 bgcolor=#ffffff>
                                    <a href="15comentarios.php">
                                        <font face="arial" style="font-size: 11px;; color: blue;">volver</font>
                                    </a>
                                </td>
                            </tr>
                        </table>
                        
						<table width="850">
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">fecha</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=fecha size=108></input></td>
                            </tr>
                            <tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">iduser</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=iduser size=108></input></td>
                            </tr>
							<tr>
                                <td height=20 width=150 bgcolor=#c8c8c8><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">idart</font></td>
                                <td height=20 width=700 bgcolor=#ffffff><input class="admin_input" type=text name=idart size=108></input></td>
                            </tr>
							
							<tr><td height=20 width=850 bgcolor=#c8c8c8 colspan="2"><font face="verdana" color="black" style="margin-left: 4px;font-size: 11px;;">comentario</font></td></tr>
							<tr>
								<td colspan="2">
									<textarea name=contenido class="admin_textarea"></textarea>
								</td>
							</tr>
							
                            <tr>
                                <td bgcolor=#ffffff><a href="#" onclick="verificar_form();"><font face="arial" style="font-size: 11px;; color: blue;">Guardar tag</font></a></td>
                            </tr>
                        </table>
                        
                    </form>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>