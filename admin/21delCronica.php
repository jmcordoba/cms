<?php

// Incluimos objetos necesarios
require("objetos/cronica.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Control de usuario identificado
require("check.php");

// Capturamos el id de la categoría a eliminar
$idcro = isset($_REQUEST['idcro']) ? $_REQUEST['idcro'] : "";

// Eliminamos categoria y redirigimos
if(borrarCronica($idcro)==true) redirect("index.php?origen=croni" ,0);
else                            redirect("index.php?origen=error",0);

?>
