<?php

// Incluimos objetos y funciones
require("objetos/album.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");

$idalbum   = isset($_REQUEST['album'])     ? $_REQUEST['album']                   : "";
$ubicacion = isset($_REQUEST['ubicacion']) ? rawurldecode($_REQUEST['ubicacion']) : "";

unlink($ubicacion);

// Eliminamos categoria y redirigimos
if(borrarFotoDeAlbum($idalbum,$ubicacion)==true) redirect("20addFotos2Album.php?idalbum=$idalbum" ,0);
else                            			     redirect("index.php?origen=error",0);

?>
