<?php

// Incluye los objetos necesarios
require("objetos/jornada.php");
require("objetos/cate.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Capturamos los datos que vienen por POST y los guardamos en el objeto equipo

$jornada->idcat    = init("idcat");
$jornada->idjorn   = init("idjorn");

$jornada->local01   = (isset($_REQUEST["local01"]))   ? $_REQUEST["local01"]   : "";		
$jornada->visit01   = (isset($_REQUEST["visit01"]))   ? $_REQUEST["visit01"]   : "";		
$jornada->inst01    = (isset($_REQUEST["inst01"]))    ? $_REQUEST["inst01"]    : "";
$jornada->plocal01  = (isset($_REQUEST["plocal01"]))  ? $_REQUEST["plocal01"]  : "";	
$jornada->pvisit01  = (isset($_REQUEST["pvisit01"]))  ? $_REQUEST["pvisit01"]  : "";	
$jornada->fecha01   = (isset($_REQUEST["fecha01"]))   ? $_REQUEST["fecha01"]   : "";		
$jornada->hour01    = (isset($_REQUEST["hour01"]))    ? $_REQUEST["hour01"]    : "";
$jornada->plocal011 = (isset($_REQUEST["plocal011"])) ? $_REQUEST["plocal011"] : "";
$jornada->plocal012 = (isset($_REQUEST["plocal012"])) ? $_REQUEST["plocal012"] : "";
$jornada->plocal013 = (isset($_REQUEST["plocal013"])) ? $_REQUEST["plocal013"] : "";
$jornada->plocal014 = (isset($_REQUEST["plocal014"])) ? $_REQUEST["plocal014"] : "";
$jornada->plocal015 = (isset($_REQUEST["plocal015"])) ? $_REQUEST["plocal015"] : "";
$jornada->pvisit011 = (isset($_REQUEST["pvisit011"])) ? $_REQUEST["pvisit011"] : "";
$jornada->pvisit012 = (isset($_REQUEST["pvisit012"])) ? $_REQUEST["pvisit012"] : "";
$jornada->pvisit013 = (isset($_REQUEST["pvisit013"])) ? $_REQUEST["pvisit013"] : "";
$jornada->pvisit014 = (isset($_REQUEST["pvisit014"])) ? $_REQUEST["pvisit014"] : "";
$jornada->pvisit015 = (isset($_REQUEST["pvisit015"])) ? $_REQUEST["pvisit015"] : "";

$jornada->local02   = (isset($_REQUEST["local02"]))   ? $_REQUEST["local02"]   : "";		
$jornada->visit02   = (isset($_REQUEST["visit02"]))   ? $_REQUEST["visit02"]   : "";		
$jornada->inst02    = (isset($_REQUEST["inst02"]))    ? $_REQUEST["inst02"]    : "";
$jornada->plocal02  = (isset($_REQUEST["plocal02"]))  ? $_REQUEST["plocal02"]  : "" ;
$jornada->pvisit02  = (isset($_REQUEST["pvisit02"]))  ? $_REQUEST["pvisit02"]  : "";	
$jornada->fecha02   = (isset($_REQUEST["fecha02"]))   ? $_REQUEST["fecha02"]   : "";		
$jornada->hour02    = (isset($_REQUEST["hour02"]))    ? $_REQUEST["hour02"]    : "";
$jornada->plocal021 = (isset($_REQUEST["plocal021"])) ? $_REQUEST["plocal021"] : "";
$jornada->plocal022 = (isset($_REQUEST["plocal022"])) ? $_REQUEST["plocal022"] : "";
$jornada->plocal023 = (isset($_REQUEST["plocal023"])) ? $_REQUEST["plocal023"] : "";
$jornada->plocal024 = (isset($_REQUEST["plocal024"])) ? $_REQUEST["plocal024"] : "";
$jornada->plocal025 = (isset($_REQUEST["plocal025"])) ? $_REQUEST["plocal025"] : "";
$jornada->pvisit021 = (isset($_REQUEST["pvisit021"])) ? $_REQUEST["pvisit021"] : "";
$jornada->pvisit022 = (isset($_REQUEST["pvisit022"])) ? $_REQUEST["pvisit022"] : "";
$jornada->pvisit023 = (isset($_REQUEST["pvisit023"])) ? $_REQUEST["pvisit023"] : "";
$jornada->pvisit024 = (isset($_REQUEST["pvisit024"])) ? $_REQUEST["pvisit024"] : "";
$jornada->pvisit025 = (isset($_REQUEST["pvisit025"])) ? $_REQUEST["pvisit025"] : "";

$jornada->local03   = (isset($_REQUEST["local03"]))   ? $_REQUEST["local03"]   : "";		
$jornada->visit03   = (isset($_REQUEST["visit03"]))   ? $_REQUEST["visit03"]   : "";		
$jornada->inst03    = (isset($_REQUEST["inst03"]))    ? $_REQUEST["inst03"]    : "";
$jornada->plocal03  = (isset($_REQUEST["plocal03"]))  ? $_REQUEST["plocal03"]  : "";	
$jornada->pvisit03  = (isset($_REQUEST["pvisit03"]))  ? $_REQUEST["pvisit03"]  : "";	
$jornada->fecha03   = (isset($_REQUEST["fecha03"]))   ? $_REQUEST["fecha03"]   : "";		
$jornada->hour03    = (isset($_REQUEST["hour03"]))    ? $_REQUEST["hour03"]    : "";
$jornada->plocal031 = (isset($_REQUEST["plocal031"])) ? $_REQUEST["plocal031"] : "";
$jornada->plocal032 = (isset($_REQUEST["plocal032"])) ? $_REQUEST["plocal032"] : "";
$jornada->plocal033 = (isset($_REQUEST["plocal033"])) ? $_REQUEST["plocal033"] : "";
$jornada->plocal034 = (isset($_REQUEST["plocal034"])) ? $_REQUEST["plocal034"] : "";
$jornada->plocal035 = (isset($_REQUEST["plocal035"])) ? $_REQUEST["plocal035"] : "";
$jornada->pvisit031 = (isset($_REQUEST["pvisit031"])) ? $_REQUEST["pvisit031"] : "";
$jornada->pvisit032 = (isset($_REQUEST["pvisit032"])) ? $_REQUEST["pvisit032"] : "";
$jornada->pvisit033 = (isset($_REQUEST["pvisit033"])) ? $_REQUEST["pvisit033"] : "";
$jornada->pvisit034 = (isset($_REQUEST["pvisit034"])) ? $_REQUEST["pvisit034"] : "";
$jornada->pvisit035 = (isset($_REQUEST["pvisit035"])) ? $_REQUEST["pvisit035"] : "";

$jornada->local04   = (isset($_REQUEST["local04"]))   ? $_REQUEST["local04"]   : "";		
$jornada->visit04   = (isset($_REQUEST["visit04"]))   ? $_REQUEST["visit04"]   : "";		
$jornada->inst04    = (isset($_REQUEST["inst04"]))    ? $_REQUEST["inst04"]    : "";
$jornada->plocal04  = (isset($_REQUEST["plocal04"]))  ? $_REQUEST["plocal04"]  : "";	
$jornada->pvisit04  = (isset($_REQUEST["pvisit04"]))  ? $_REQUEST["pvisit04"]  : "";	
$jornada->fecha04   = (isset($_REQUEST["fecha04"]))   ? $_REQUEST["fecha04"]   : "";		
$jornada->hour04    = (isset($_REQUEST["hour04"]))    ? $_REQUEST["hour04"]    : "";
$jornada->plocal041 = (isset($_REQUEST["plocal041"])) ? $_REQUEST["plocal041"] : "";
$jornada->plocal042 = (isset($_REQUEST["plocal042"])) ? $_REQUEST["plocal042"] : "";
$jornada->plocal043 = (isset($_REQUEST["plocal043"])) ? $_REQUEST["plocal043"] : "";
$jornada->plocal044 = (isset($_REQUEST["plocal044"])) ? $_REQUEST["plocal044"] : "";
$jornada->plocal045 = (isset($_REQUEST["plocal045"])) ? $_REQUEST["plocal045"] : "";
$jornada->pvisit041 = (isset($_REQUEST["pvisit041"])) ? $_REQUEST["pvisit041"] : "";
$jornada->pvisit042 = (isset($_REQUEST["pvisit042"])) ? $_REQUEST["pvisit042"] : "";
$jornada->pvisit043 = (isset($_REQUEST["pvisit043"])) ? $_REQUEST["pvisit043"] : "";
$jornada->pvisit044 = (isset($_REQUEST["pvisit044"])) ? $_REQUEST["pvisit044"] : "";
$jornada->pvisit045 = (isset($_REQUEST["pvisit045"])) ? $_REQUEST["pvisit045"] : "";

$jornada->local05   = (isset($_REQUEST["local05"]))   ? $_REQUEST["local05"]   : "";		
$jornada->visit05   = (isset($_REQUEST["visit05"]))   ? $_REQUEST["visit05"]   : "";		
$jornada->inst05    = (isset($_REQUEST["inst05"]))    ? $_REQUEST["inst05"]    : "";
$jornada->plocal05  = (isset($_REQUEST["plocal05"]))  ? $_REQUEST["plocal05"]  : "";	
$jornada->pvisit05  = (isset($_REQUEST["pvisit05"]))  ? $_REQUEST["pvisit05"]  : "";	
$jornada->fecha05   = (isset($_REQUEST["fecha05"]))   ? $_REQUEST["fecha05"]   : "";		
$jornada->hour05    = (isset($_REQUEST["hour05"]))    ? $_REQUEST["hour05"]    : "";
$jornada->plocal051 = (isset($_REQUEST["plocal051"])) ? $_REQUEST["plocal051"] : "";
$jornada->plocal052 = (isset($_REQUEST["plocal052"])) ? $_REQUEST["plocal052"] : "";
$jornada->plocal053 = (isset($_REQUEST["plocal053"])) ? $_REQUEST["plocal053"] : "";
$jornada->plocal054 = (isset($_REQUEST["plocal054"])) ? $_REQUEST["plocal054"] : "";
$jornada->plocal055 = (isset($_REQUEST["plocal055"])) ? $_REQUEST["plocal055"] : "";
$jornada->pvisit051 = (isset($_REQUEST["pvisit051"])) ? $_REQUEST["pvisit051"] : "";
$jornada->pvisit052 = (isset($_REQUEST["pvisit052"])) ? $_REQUEST["pvisit052"] : "";
$jornada->pvisit053 = (isset($_REQUEST["pvisit053"])) ? $_REQUEST["pvisit053"] : "";
$jornada->pvisit054 = (isset($_REQUEST["pvisit054"])) ? $_REQUEST["pvisit054"] : "";
$jornada->pvisit055 = (isset($_REQUEST["pvisit055"])) ? $_REQUEST["pvisit055"] : "";

$jornada->local06   = (isset($_REQUEST["local06"]))   ? $_REQUEST["local06"]   : "";		
$jornada->visit06   = (isset($_REQUEST["visit06"]))   ? $_REQUEST["visit06"]   : "";		
$jornada->inst06    = (isset($_REQUEST["inst06"]))    ? $_REQUEST["inst06"]    : "";
$jornada->plocal06  = (isset($_REQUEST["plocal06"]))  ? $_REQUEST["plocal06"]  : "";	
$jornada->pvisit06  = (isset($_REQUEST["pvisit06"]))  ? $_REQUEST["pvisit06"]  : "";	
$jornada->fecha06   = (isset($_REQUEST["fecha06"]))   ? $_REQUEST["fecha06"]   : "";		
$jornada->hour06    = (isset($_REQUEST["hour06"]))    ? $_REQUEST["hour06"]    : "";
$jornada->plocal061 = (isset($_REQUEST["plocal061"])) ? $_REQUEST["plocal061"] : "";
$jornada->plocal062 = (isset($_REQUEST["plocal062"])) ? $_REQUEST["plocal062"] : "";
$jornada->plocal063 = (isset($_REQUEST["plocal063"])) ? $_REQUEST["plocal063"] : "";
$jornada->plocal064 = (isset($_REQUEST["plocal064"])) ? $_REQUEST["plocal064"] : "";
$jornada->plocal065 = (isset($_REQUEST["plocal065"])) ? $_REQUEST["plocal065"] : "";
$jornada->pvisit061 = (isset($_REQUEST["pvisit061"])) ? $_REQUEST["pvisit061"] : "";
$jornada->pvisit062 = (isset($_REQUEST["pvisit062"])) ? $_REQUEST["pvisit062"] : "";
$jornada->pvisit063 = (isset($_REQUEST["pvisit063"])) ? $_REQUEST["pvisit063"] : "";
$jornada->pvisit064 = (isset($_REQUEST["pvisit064"])) ? $_REQUEST["pvisit064"] : "";
$jornada->pvisit065 = (isset($_REQUEST["pvisit065"])) ? $_REQUEST["pvisit065"] : "";

$jornada->local07   = (isset($_REQUEST["local07"]))   ? $_REQUEST["local07"]   : "";		
$jornada->visit07   = (isset($_REQUEST["visit07"]))   ? $_REQUEST["visit07"]   : "";		
$jornada->inst07    = (isset($_REQUEST["inst07"]))    ? $_REQUEST["inst07"]    : "";
$jornada->plocal07  = (isset($_REQUEST["plocal07"]))  ? $_REQUEST["plocal07"]  : "";	
$jornada->pvisit07  = (isset($_REQUEST["pvisit07"]))  ? $_REQUEST["pvisit07"]  : "";	
$jornada->fecha07   = (isset($_REQUEST["fecha07"]))   ? $_REQUEST["fecha07"]   : "";		
$jornada->hour07    = (isset($_REQUEST["hour07"]))    ? $_REQUEST["hour07"]    : "";
$jornada->plocal071 = (isset($_REQUEST["plocal071"])) ? $_REQUEST["plocal071"] : "";
$jornada->plocal072 = (isset($_REQUEST["plocal072"])) ? $_REQUEST["plocal072"] : "";
$jornada->plocal073 = (isset($_REQUEST["plocal073"])) ? $_REQUEST["plocal073"] : "";
$jornada->plocal074 = (isset($_REQUEST["plocal074"])) ? $_REQUEST["plocal074"] : "";
$jornada->plocal075 = (isset($_REQUEST["plocal075"])) ? $_REQUEST["plocal075"] : "";
$jornada->pvisit071 = (isset($_REQUEST["pvisit071"])) ? $_REQUEST["pvisit071"] : "";
$jornada->pvisit072 = (isset($_REQUEST["pvisit072"])) ? $_REQUEST["pvisit072"] : "";
$jornada->pvisit073 = (isset($_REQUEST["pvisit073"])) ? $_REQUEST["pvisit073"] : "";
$jornada->pvisit074 = (isset($_REQUEST["pvisit074"])) ? $_REQUEST["pvisit074"] : "";
$jornada->pvisit075 = (isset($_REQUEST["pvisit075"])) ? $_REQUEST["pvisit075"] : "";

$jornada->local08   = (isset($_REQUEST["local08"]))   ? $_REQUEST["local08"]   : "";		
$jornada->visit08   = (isset($_REQUEST["visit08"]))   ? $_REQUEST["visit08"]   : "";		
$jornada->inst08    = (isset($_REQUEST["inst08"]))    ? $_REQUEST["inst08"]    : "";
$jornada->plocal08  = (isset($_REQUEST["plocal08"]))  ? $_REQUEST["plocal08"]  : "";	
$jornada->pvisit08  = (isset($_REQUEST["pvisit08"]))  ? $_REQUEST["pvisit08"]  : "";	
$jornada->fecha08   = (isset($_REQUEST["fecha08"]))   ? $_REQUEST["fecha08"]   : "";		
$jornada->hour08    = (isset($_REQUEST["hour08"]))    ? $_REQUEST["hour08"]    : "";
$jornada->plocal081 = (isset($_REQUEST["plocal081"])) ? $_REQUEST["plocal081"] : "";
$jornada->plocal082 = (isset($_REQUEST["plocal082"])) ? $_REQUEST["plocal082"] : "";
$jornada->plocal083 = (isset($_REQUEST["plocal083"])) ? $_REQUEST["plocal083"] : "";
$jornada->plocal084 = (isset($_REQUEST["plocal084"])) ? $_REQUEST["plocal084"] : "";
$jornada->plocal085 = (isset($_REQUEST["plocal085"])) ? $_REQUEST["plocal085"] : "";
$jornada->pvisit081 = (isset($_REQUEST["pvisit081"])) ? $_REQUEST["pvisit081"] : "";
$jornada->pvisit082 = (isset($_REQUEST["pvisit082"])) ? $_REQUEST["pvisit082"] : "";
$jornada->pvisit083 = (isset($_REQUEST["pvisit083"])) ? $_REQUEST["pvisit083"] : "";
$jornada->pvisit084 = (isset($_REQUEST["pvisit084"])) ? $_REQUEST["pvisit084"] : "";
$jornada->pvisit085 = (isset($_REQUEST["pvisit085"])) ? $_REQUEST["pvisit085"] : "";

$jornada->local09   = (isset($_REQUEST["local09"]))   ? $_REQUEST["local09"]   : "";		
$jornada->visit09   = (isset($_REQUEST["visit09"]))   ? $_REQUEST["visit09"]   : "";		
$jornada->inst09    = (isset($_REQUEST["inst09"]))    ? $_REQUEST["inst09"]    : "";
$jornada->plocal09  = (isset($_REQUEST["plocal09"]))  ? $_REQUEST["plocal09"]  : "";	
$jornada->pvisit09  = (isset($_REQUEST["pvisit09"]))  ? $_REQUEST["pvisit09"]  : "";	
$jornada->fecha09   = (isset($_REQUEST["fecha09"]))   ? $_REQUEST["fecha09"]   : "";		
$jornada->hour09    = (isset($_REQUEST["hour09"]))    ? $_REQUEST["hour09"]    : "";
$jornada->plocal091 = (isset($_REQUEST["plocal091"])) ? $_REQUEST["plocal091"] : "";
$jornada->plocal092 = (isset($_REQUEST["plocal092"])) ? $_REQUEST["plocal092"] : "";
$jornada->plocal093 = (isset($_REQUEST["plocal093"])) ? $_REQUEST["plocal093"] : "";
$jornada->plocal094 = (isset($_REQUEST["plocal094"])) ? $_REQUEST["plocal094"] : "";
$jornada->plocal095 = (isset($_REQUEST["plocal095"])) ? $_REQUEST["plocal095"] : "";
$jornada->pvisit091 = (isset($_REQUEST["pvisit091"])) ? $_REQUEST["pvisit091"] : "";
$jornada->pvisit092 = (isset($_REQUEST["pvisit092"])) ? $_REQUEST["pvisit092"] : "";
$jornada->pvisit093 = (isset($_REQUEST["pvisit093"])) ? $_REQUEST["pvisit093"] : "";
$jornada->pvisit094 = (isset($_REQUEST["pvisit094"])) ? $_REQUEST["pvisit094"] : "";
$jornada->pvisit095 = (isset($_REQUEST["pvisit095"])) ? $_REQUEST["pvisit095"] : "";

$jornada->local10   = (isset($_REQUEST["local10"]))   ? $_REQUEST["local10"]   : "";		
$jornada->visit10   = (isset($_REQUEST["visit10"]))   ? $_REQUEST["visit10"]   : "";		
$jornada->inst10    = (isset($_REQUEST["inst10"]))    ? $_REQUEST["inst10"]    : "";
$jornada->plocal10  = (isset($_REQUEST["plocal10"]))  ? $_REQUEST["plocal10"]  : "";	
$jornada->pvisit10  = (isset($_REQUEST["pvisit10"]))  ? $_REQUEST["pvisit10"]  : "";	
$jornada->fecha10   = (isset($_REQUEST["fecha10"]))   ? $_REQUEST["fecha10"]   : "";		
$jornada->hour10    = (isset($_REQUEST["hour10"])) 	  ? $_REQUEST["hour10"]    : "";
$jornada->plocal101 = (isset($_REQUEST["plocal101"])) ? $_REQUEST["plocal101"] : "";
$jornada->plocal102 = (isset($_REQUEST["plocal102"])) ? $_REQUEST["plocal102"] : "";
$jornada->plocal103 = (isset($_REQUEST["plocal103"])) ? $_REQUEST["plocal103"] : "";
$jornada->plocal104 = (isset($_REQUEST["plocal104"])) ? $_REQUEST["plocal104"] : "";
$jornada->plocal105 = (isset($_REQUEST["plocal105"])) ? $_REQUEST["plocal105"] : "";
$jornada->pvisit101 = (isset($_REQUEST["pvisit101"])) ? $_REQUEST["pvisit101"] : "";
$jornada->pvisit102 = (isset($_REQUEST["pvisit102"])) ? $_REQUEST["pvisit102"] : "";
$jornada->pvisit103 = (isset($_REQUEST["pvisit103"])) ? $_REQUEST["pvisit103"] : "";
$jornada->pvisit104 = (isset($_REQUEST["pvisit104"])) ? $_REQUEST["pvisit104"] : "";
$jornada->pvisit105 = (isset($_REQUEST["pvisit105"])) ? $_REQUEST["pvisit105"] : "";

// Intentamos guardar el usuario y redirigimos en función de si hemos tenido éxito o no
if(modificarJornada($jornada)==true) redirect("04newJornada.php?idcate=".$jornada->idcat,0);
else                                 redirect("index.php?origen=error",0);

?>