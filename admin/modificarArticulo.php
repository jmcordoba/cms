<?php

// Incluímos Objetos necesarios
require("objetos/articulo.php");

// Incluímos funciones necesarias
require("fun/funciones.php");

// Buscamos si el artículo a modificar ya tenía una imagen asociada
$art = obtenerUnArticulo(init("idart"));

// Obtenemos los datos del formulario
$articulo->idart     = init("idart");
$articulo->fecha     = date("d/m/Y");
$articulo->iduser    = init("iduser");
$articulo->titulo    = str_replace("'","\'",init("titulo"));

$articulo->contenido = nl2br(str_replace("'","\'",$_POST["contenido"]));
//$articulo->contenido = $_POST["contenido"];

$articulo->idtag     = init("idtag");
$articulo->fechasi   = init("fechasi");
$articulo->fechano   = init("fechano");
$articulo->conta     = "";

// Si ya había imagen y no la hemos cambiado por una nueva, respetamos la anterior
if(isset($_POST["imagen"])and($_POST["imagen"]!="")) $articulo->imagen = init("imagen");
else                     							 $articulo->imagen = $art->imagen;

// Capturamos los datos del fichero adjunto
$nombre_archivo = $_FILES['imagen']['name']; 
$tipo_archivo   = $_FILES['imagen']['type']; 
$tamano_archivo = $_FILES['imagen']['size'];

// Si viene un fichero adjunto
if($nombre_archivo!="") {
    //compruebo si las caracter�sticas del archivo son las que deseo 
    if ($tamano_archivo < 100000) {
		// Redimensionamos el fichero a 200x200
		include('simpleImage.php');
		$image = new SimpleImage();
		$image->load($_FILES['imagen']['tmp_name']);
		$image->resize(426,320);
		$image->save($_FILES['imagen']['tmp_name']);
		// Inicializamos la ruta y el nombre del avatar del usuario
		$articulo->imagen    = "images/articulos/art" . $articulo->idart . ".gif";
		// Guardamos el fichero en la ruta especificada
		if (!move_uploaded_file($_FILES['imagen']['tmp_name'], "images/articulos/art" . $articulo->idart . ".gif")){
			// S ihay algún tipo de error, redirigimos a otra página
			?><script>location.href='index.php?origen=error';</script><?php
			die();
		}
    } else {
		?>
		<script>
			alert('La imagen que desea subir es mayor de 100kB.\nPor favor, suba una imagen con un peso inferior.');
		</script>
		<?php
	}
}

// Guardamos los cambios si es posible y redireccionamos en función del resultado
if(modificarArticulo($articulo,$articulo->idart)==true) redirect("index.php?origen=arti" ,0);
else                                                    redirect("index.php?origen=error",0);

?>
