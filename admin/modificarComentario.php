<?php

// Incluímos Objetos necesarios
require("objetos/comentario.php");
// Incluímos funciones necesarias
require("fun/funciones.php");

// Obtenemos los datos del formulario
$comen->idart     = init("idart");
$comen->idcom     = init("idcom");
$comen->fecha     = init("fecha");
$comen->iduser    = init("iduser");
$comen->contenido = str_replace("'","\'",init("contenido"));
$comen->conta     = init("conta");

// Guardamos los cambios si es posible y redireccionamos en función del resultado
if(modificarComentario($comen,$comen->idcom)==true) redirect("index.php?origen=comen",0);
else                                                redirect("index.php?origen=error" ,0);

?>