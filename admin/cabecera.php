<table style="position:fixed; 
			left:0px;
			box-shadow: 0 4px 6px -5px #000000;
			-webkit-box-shadow: 0 5px 6px -6px #000000;
			-moz-box-shadow: 0 5px 6px -6px #000000;
			z-index:10;
			filter: progid:DXImageTransform.Microsoft.Shadow(color='#777777', Direction=160, Strength=4);"
			bgcolor="#e8e8e8" border="0" color="black" cellspacing="0" cellpadding="0" width="100%" height="40">
	<tr align="left">        
		<td width="100%" style="vertical-align:middle">
			<form name="frm" action="login.php" method=post enctype="multipart/form-data">
				<table border="0" width="100%" align="left" bgcolor="#e8e8e8">
					<tr style="vertical-align:middle">
						<td width="5%" bgcolor="#e8e8e8"></td>
						<td width="13%" bgcolor="#e8e8e8" align="left"></td>
						<?php
						/////////////////////////////////////////////////////////////////////////////////////////////////////////
						//
						// Si el usuario no esta loginado
						//
						/////////////////////////////////////////////////////////////////////////////////////////////////////////
						if ($_COOKIE["cmsweb_logged"]=="") {
							?>
							<td width="15%" bgcolor="#e8e8e8">
								<input align="left" type="text" name="mail" size="25" value="<? if($_POST["mail"]!="") echo $_POST["mail"]; else echo "mail";?>" onclick="this.value=''"></input>
							</td>
							<td width="10%" bgcolor="#e8e8e8">
								<input align="left" type="password" name="pasw" size="15" value="<? if($_POST["pasw"]!="") echo $_POST["pasw"]; else echo "password";?>" onclick="this.value=''"></input>
							</td>
							<td width="40" bgcolor="#e8e8e8">
								<a href="#" onclick="javascript:document.forms['frm'].submit(); return false;">
									<font face="arial" style="font-size:12px; color:blue;">Entrar</font>
								</a>
							</td>
							<td width="35%" bgcolor=#e8e8e8 align=right></td>
							<td width=70 bgcolor=#e8e8e8 align=right>
								<a href="login_remember.php"><font face="arial" style="font-size: 12px; color: blue;">Recordar</font></a>
							</td>
							<td width=70 bgcolor=#e8e8e8 align=right>
								<a href="login_register.php"><font face="arial" style="font-size: 12px; color: blue;">¡Registrate!</font></a>
							</td>
							<?php
						}	
						/////////////////////////////////////////////////////////////////////////////////////////////////////////
						//
						// Si el usuario esta loginado
						//
						/////////////////////////////////////////////////////////////////////////////////////////////////////////
						else {
							?>
							<td width="55%" bgcolor=#e8e8e8 align=right></td>
							<?php
							if($_COOKIE["cmsweb_permisos"]=="administrador") {
								?>
								<td width="50" bgcolor=#e8e8e8 align=right>
									<a href="../index.php"><font face="arial" style="font-size: 12px; color: blue;">Web</font></a>
								</td>
								<?php
							} else {
								?>
								<td width="50" bgcolor=#e8e8e8>
									<script>location.href=conf_RUTA."index.php";</script>
								</td>
								<?php
							}
							?>
							<td width="50" bgcolor=#e8e8e8 align=right>
								<a href="fun/borrasesion.php"><font face="arial" style="font-size: 12px; color: blue;">Desconexión</font></a>
							</td>
							<td width="50" bgcolor=#e8e8e8 align=right>
								<font face="arial" style="font-size: 12px; color: black;"><?php echo $_COOKIE["cmsweb_user"]; ?></font>
							</td>
							<td width="30" bgcolor=#e8e8e8 align=right>
								<img style="margin-top:0px;" height="25" width="25" src="<?php echo conf_RUTA.$_COOKIE["cmsweb_foto"]; ?>">
							</td>
							<?php
						}
						?>
						<td width="5%" bgcolor="#e8e8e8"></td>
					</tr>
				</table>
			</form>
		</td>
	</tr>		
</table>
