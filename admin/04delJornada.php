<?php

// Incluimos objetos y funciones
require("objetos/temp.php");
require("objetos/cate.php");
require("objetos/jornada.php");
require("objetos/insta.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
// Capturamos el id de la categoría a eliminar
$idcate = $_GET["idcate"];
$idjorn = $_GET["idjorn"];
// Eliminamos categoria y redirigimos
if(borrarJornada($idcate,$idjorn)==true) redirect("04newJornada.php?idcate=".$idcate ,0);
else                                     redirect("index.php?origen=error"           ,0);

?>
