<?php

// Incluye los objetos necesarios
require("objetos/torneo.php");

// Incluye las funciones necesarios
require("fun/funciones.php");

// Obtenemos las variables del formulario
$torneo->idtorneo    = init("idtorneo");
$torneo->idformu     = init("idformu");
$torneo->name        = str_replace("'","\'",init("name"));
$torneo->fecha       = init("fecha");
$torneo->hour        = init("hour");
$torneo->location    = init("location");
$torneo->gmaps       = init("gmaps");
$torneo->description = str_replace("'","\'",init("description"));
$torneo->info        = init("info");
$torneo->cuenta      = init("cuenta");
$torneo->titular     = init("titular");
$torneo->precio      = init("precio");
$torneo->conta       = init("conta");
$torneo->conta       = "";

// Capturamos los datos del fichero adjunto
$nombre_archivo = $_FILES['cartel']['name']; 
$tipo_archivo   = $_FILES['cartel']['type']; 
$tamano_archivo = $_FILES['cartel']['size'];

// Inicializamos la variable numero de filas a cero
$numero = 0;

// Conexión con la base de datos
$link=Conectarse();
// Construcción de la query
$sql = "select * from `CMS_torneos` order by idtorneo";
// Registro de log
wlog("guardarTorneo",$sql,1);
// Ejecutamos la query y obtenemos el resultado
$result = mysql_query($sql, $link);
// Obtenemos el ultimo idtemp
if ($row = mysql_fetch_array($result))
{
    do{
        $numero = $row["idtorneo"];
    } while ($row = mysql_fetch_array($result));
}
$numero++;

// Cerramos la conexion con la base de datos
mysql_close($link);
// Si viene un fichero adjunto
if($nombre_archivo!="") {
    //compruebo si las caracter�sticas del archivo son las que deseo 
    if ($tamano_archivo < 100000) {
		// Redimensionamos el fichero a 200x200
		include('simpleImage.php');
		$image = new SimpleImage();
		$image->load($_FILES['cartel']['tmp_name']);
		$image->resize(400,564);
		$image->save($_FILES['cartel']['tmp_name']);
        // Inicializamos la ruta y el nombre del avatar del usuario
        $torneo->cartel = "admin/images/torneos/torneo" . $numero . ".gif";
        // Guardamos el fichero en la ruta especificada
        if (!move_uploaded_file($_FILES['cartel']['tmp_name'], "images/torneos/torneo" . $numero . ".gif")){
            // Si hay algún tipo de error, redirigimos a otra página
            ?><script>location.href='index.php?origen=error';</script><?php
        }
    } else {
		?>
		<script>
			alert('La imagen que desea subir es mayor de 100kB.\nPor favor, suba una imagen con un peso inferior.');
		</script>
		<?php
	}
}

// Intentamos guardar el usuario y redirigimos en función de si hemos tenido éxito o no
if(guardarTorneo($torneo)==true) redirect("index.php?origen=torneo",0);
else                             redirect("index.php?origen=error",0);

?>