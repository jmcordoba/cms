<?php

// Incluye los objetos necesarios
require("objetos/equipo.php");
// Incluye las funciones necesarios
require("fun/funciones.php");
// Control de usuario identificado
require("check.php");
?>
<!DOCTYPE html>
<html>
    <script language="JavaScript" type="text/javascript">
	function modificar(id) {
		document.location.href="06modEquipo.php?idteam=" + id;
	}
	function eliminar(id) {
		document.location.href="06delEquipo.php?idteam=" + id;
	}
	function nuevo() {
		document.location.href="06newEquipo.php";
	}
    </script>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms::administración::equipos</title>
		<link rel="stylesheet" href="css/estilos.css" type="text/css" />
    </head>

    <body link=#004080 vlink=#004080 alink=#004080 bgcolor=#FFFFFF style="margin:0px">   
        <!--- cabecera --->
        <?php $titulo = 'cms - administración - equipos'; require('cabecera.php'); ?>

        <!--- cuerpo --->
        <table style="position: relative;top: 60px;" border=0 color=black cellspacing=0 width=1000 height=700 align=center>
            <tr>
                <!--- menu --->
                <td width=150 style="vertical-align:top">
                    <?php require('menu.php'); ?>
                </td>
                <!--- contenido --->
                <td width=850 style="vertical-align:top">
                    
                    <table border=0 width=850>
                        <tr align="right">
                            <td width=850 bgcolor=#ffffff>
								<a href="#" onClick="nuevo();">
									<font face="arial" style="font-size: 11px;; color: blue;">Introduce nuevo equipo</font>
								</a>
							</td>
                        </tr>
                    </table>
                    
                    <table width=850>
                        <tr height=10>
                            <td width=50  bgcolor=#c8c8c8><font class="admin_font">Idteam</font></td>
							<td width=50  bgcolor=#c8c8c8><font class="admin_font">Idcat</font></td>
                            <td width=375 bgcolor=#c8c8c8><font class="admin_font">Equipo</font></td>
                            <td width=375 bgcolor=#c8c8c8><font class="admin_font">Entrenador</font></td>

                            <td width=50  bgcolor=#c8c8c8>&nbsp;</td>
                            <td width=50  bgcolor=#c8c8c8>&nbsp;</td>
                        </tr>

						<?php
						$team = obtenerEquipo();
						
						for($i=0;$i<numRows("CMS_equipos");$i++) {
						
							$_POST["idteam"]  = $team[$i]->idteam;
							$_POST["idcat"]   = $team[$i]->idcat;
							$_POST["nombre"]  = $team[$i]->nombre;
							$_POST["mister"]  = $team[$i]->mister;
							$_POST["foto"]    = $team[$i]->foto;
							$_POST["conta"]   = $team[$i]->conta;
							?>

							<form name="datos" action="guardarEquipo.php" method=post>
								<tr height=10>                                
									<td width=50  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["idteam"];?></font></td>
									<td width=50  bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["idcat"];?> </font></td>
									<td width=375 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["nombre"];?></font></td>
									<td width=375 bgcolor=#f8f8f8><font class="admin_font"><?php echo $_POST["mister"];?></font></td>
									
									<td width=50  bgcolor=#f8f8f8>
										<a href="#" onClick="modificar(<?php echo $_POST["idteam"]; ?>);">
											<font face="arial" style="font-size: 11px;; color: blue;">modificar</font>
										</a>
									</td>
									<td width=50  bgcolor=#f8f8f8>
										<a href="#" onClick="eliminar(<?php echo $_POST["idteam"]; ?>);">
											<font face="arial" style="font-size: 11px;; color: blue;">eliminar</font>
										</a>
									</td>
								</tr>
							</form>
							<?php
						}
						?>
                    </table>
                </td>
            </tr>		
        </table>

        <!--- pie --->
    </body>
</html>