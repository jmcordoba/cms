<?php

// Incluímos Objetos necesarios
require("objetos/cronica.php");
// Incluímos funciones necesarias
require("fun/funciones.php");

// Buscamos si el artículo a modificar ya tenía una imagen asociada
$cro = obtenerUnaCronica($_POST["idcro"]);

// Obtenemos los datos del formulario
$cronica->idcro     = init("idcro");
$cronica->idteam    = init("idteam");
$cronica->idcat     = init("idcat");
$cronica->fecha     = date("d/m/Y");
$cronica->iduser    = init("iduser");
$cronica->titulo    = str_replace("'","\'",init("titulo"));
$cronica->contenido = nl2br(str_replace("'","\'",$_POST["contenido"]));
$cronica->idtag     = init("idtag");
$cronica->fechasi   = init("fechasi");
$cronica->fechano   = init("fechano");
$cronica->conta     = "";

// Si ya había imagen y no la hemos cambiado por una nueva, respetamos la anterior
if(isset($_POST["imagen"])and($_POST["imagen"]!="")) $cronica->imagen = init("imagen");
else                     							 $cronica->imagen = $cro->imagen;

// Capturamos los datos del fichero adjunto
$nombre_archivo = $_FILES['imagen']['name']; 
$tipo_archivo   = $_FILES['imagen']['type']; 
$tamano_archivo = $_FILES['imagen']['size'];

// Si viene un fichero adjunto
if($nombre_archivo!="") {
    //compruebo si las caracter�sticas del archivo son las que deseo 
    if ($tamano_archivo < 100000) {
		// Redimensionamos el fichero a 200x200
		include('simpleImage.php');
		$image = new SimpleImage();
		$image->load($_FILES['imagen']['tmp_name']);
		$image->resize(426,320);
		$image->save($_FILES['imagen']['tmp_name']);
		// Inicializamos la ruta y el nombre del avatar del usuario
		$cronica->imagen    = "images/cronicas/cro" . $cronica->idcro . ".gif";
		// Guardamos el fichero en la ruta especificada
		if (!move_uploaded_file($_FILES['imagen']['tmp_name'], "images/cronicas/cro" . $cronica->idcro . ".gif")){
			// S ihay algún tipo de error, redirigimos a otra página
			?><script>location.href='index.php?origen=error';</script><?php
			die();
		}
    } else {
		?>
		<script>
			alert('La imagen que desea subir es mayor de 100kB.\nPor favor, suba una imagen con un peso inferior.');
		</script>
		<?php
	}
}

// Guardamos los cambios si es posible y redireccionamos en función del resultado
if(modificarCronica($cronica,$cronica->idcro)==true) redirect("index.php?origen=croni",0);
else                                                 redirect("index.php?origen=error",0);

?>
