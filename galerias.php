<?php

// Incluye los objetos necesarios
require("admin/objetos/club.php");
require("admin/objetos/equipo.php");

require("admin/objetos/album.php");
require("admin/objetos/fotografia.php");

// Incluye las funciones necesarios
require("admin/fun/funciones.php");

?>

<!DOCTYPE html>

<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms</title>
		
		<script type="text/javascript"> 
			function modificar(id) {
					document.location.href="galerias_fotos.php?idalbum=" + id;
			}
		</script>
		<link rel="stylesheet"    type="text/css"     href="css/menu_izquierda.css" />
		<link rel="stylesheet"    type="text/css"     href="css/galerias.css" />
    </head>
	

    <body bgcolor="#e8e8e8" style="margin:0px;">
	
		<!--- cabecera --->
        <?php require('cabecera.php'); ?>
        <!--- cuerpo --->
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="35" align="center"><tr><td></td></tr></table>
        <table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="700" align="center">
            <tr>
				<td bgcolor="#ffffff" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    <?php
					include("cuerpo1.php");
					include("cuerpo3.php");
					include("cuerpo4.php");
					?>
                </td>
                <td></td>
				<td bgcolor="#ffffff" width="800" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
					
                    <table style="position:relative;top:30px;margin-left:5px;text-align:justify;" width="99%" align="center">
						
						<tr>
							<td colspan="3" style="border-bottom:1px dashed #333333;">
								<font class="titulo_pagina"><?php echo "Galería de fotos"; ?></font>
							</td>
						</tr>
						
						<tr height="10" bgcolor="#ffffff"><td></td></tr>
						
						<tr>
							
							<?php
							$album = obtenerAlbum();
							//var_dump($album);die();
							for($i=0;$i<numRows("CMS_album");$i++) {
								?>
								<td bgcolor=#ffffff align=center width="30%">
									
									<table class="tabla_foto" width="200" height="200" style="border: 1px solid #000000;">
										<tr height="25">
											<td>
												<font face="verdana" color="black" style="font-size: 10px;">
													<?php echo $album[$i]->nombre; ?>
												</font>
											</td>
										</tr>
										<tr height="150" bgcolor="#0000">
											<td>
												<font face="verdana" color="white" style="font-size: 10px;">
													<?php
													if($album[$i]->numero=="") {
														echo "Album vacío"; 
													} else {
														//
														//
														//
														if(directoryexists("admin/".$album[$i]->ubicacion)) {												
															
															// Abrimos el directorio
															$directorio=opendir("admin/".$album[$i]->ubicacion);
															// Obtenemos la lista de archivos y los vamos tratando uno a uno
															while ($archivo = readdir($directorio)) {
																if(($archivo!=".")and($archivo!="..")) {
																	$fotografia = obtenerUnaFotoDeUnAlbum($album[$i]->idalbum,$archivo);
																	if($fotografia!=false){
																		?>
																			<a href="#" onClick="modificar(<?php echo $album[$i]->idalbum; ?>);">
																				<img border="0" width="200" height="150" src="admin/<?php echo $fotografia->ruta; ?>"></img>
																			</a>
																		<?php
																		break;
																	}
																}
															}
															// Cerramos el directorio
															closedir($directorio);
														}
													}
													?>
												</font>
											</td>
										</tr>
										<tr height="25" align="right">
											<td>
												<font face="verdana" color="black" style="font-size: 10px;">
													<?php echo $album[$i]->numero." fotos"; ?>
												</font>
											</td>
										</tr>
									</table>
								</td>
								<?php
								if((($i+1)%3)==0) {
									echo "</tr><tr>";
								}
							}
							
							if(numRows("CMS_album")<3) {
								for($i=0;$i<(3-numRows("CMS_album"));$i++) {
									echo "</td><td bgcolor=#ffffff width=\"30%\">";
								}
							}
							
							if(numRows("CMS_album")>3) {
								for($i=0;$i<((numRows("CMS_album")+1)%3);$i++) {
									echo "</td><td bgcolor=#ffffff width=\"30%\">";
								}
							}
							?>

						</tr>
						
					</table>

                </td>
            </tr>
        </table>
        <!--- pie --->
        <?php require('pie.php'); ?>

    </body>
</html>