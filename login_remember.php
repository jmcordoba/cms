<?php

require("fun/funciones_recuerdaUsuario.php");
require("objetos/usuario.php");
?>

<!DOCTYPE html>

<html>

    <head>
        <meta http-equiv=content-type content="text/html; charset=utf-8">
        <title>cms</title>
		<link rel="stylesheet"    type="text/css"     href="css/menu_izquierda.css" />
		<style type="text/css">
			.td_titulo {
				height: 20px;
				width: 150px;
			}
			.td_info {
				height: 20px;
				width: 500px;
			}
			.td_imagen {
				width: 500px;
				align: left;
			}
			
			.titulo_pagina {
				font-family:Arial;
				font-size:26px; 
				font-style:normal; 
				color:#94752;
			}
			.titulo {
				font-family: Arial;
				font-size: 14px;
				color:#394752;
				text-align: center;
			}
			.informacion {
				font-family: Tahoma;
				margin-left: 4px;
				font-size: 18px;
				color:#394752;
			}
			
			.input {
				align: left;
				width: 550px;
			}
			
			.input_remember {
				align: left;
				height: 25px;
				border-style:solid;
				border-width:1px;
				border-color: #666666;
				
				padding-left: 4px;
			}
			.input_remember:hover {
				background-color: #ffffff;
				border-color: #394752;
				box-shadow: 0px 0px 2px #394752 inset;
			}
			.input_remember:focus {
				background-color: #ffffff;
				border-color: #394752;
				box-shadow: 0px 0px 2px #394752 inset;
			}
		</style>
    </head>
	
	<script>
		function form() {
			document.forms['remember'].submit();
		}
	</script>
    <body bgcolor="#e8e8e8" style="margin:0px;">
		
		<!-- cabecera -->
        <?php require('cabecera.php'); ?>
        
		<!-- cuerpo -->
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="35" align="center"><tr><td></td></tr></table>
        
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="700" align="center">
            <tr>
				<td bgcolor="#ffffff" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    <?php
					include("cuerpo1.php");
					include("cuerpo3.php");
					include("cuerpo4.php");
					?>
                </td>
                
				<td></td>
				
				<td bgcolor="#ffffff" width="800" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">

                    <form name="remember" action="fun/recuerdaUsuario.php" method="post">
                        
						<table style="position:relative;top:30px;margin-left:5px;text-align:justify;" width="99%" align="center">
						
							<tr>
								<td colspan="2" style="border-bottom:1px dashed #333333;">
									<font class="titulo_pagina"><?php echo "Recuerde su contraseña"; ?></font>
								</td>
							</tr>
							
							<tr height="20" bgcolor="#ffffff"><td></td></tr>
							
                            <tr align="center">
								<td class="td_titulo">
									<font class="titulo">Escriba su mail y le enviaremos su contraseña.</font>
								</td>
							</tr>
							
                            <tr align="center">
								<td height="20" width="200" bgcolor="#ffffff">
									<input class="input_remember" type="text" name="mail" size="50"></input>
								</td>
                            </tr>
                            
							<tr height="20" bgcolor="#ffffff"><td></td></tr>
							
                            <tr>
                                <td colspan="2" width="800" bgcolor="#ffffff" align="center">
                                    <a onclick="form()" href="#" style="text-decoration: none;"><font class="titulo">Guardar</font></a>
                                </td>
                            </tr>
                        </table>
                    </form>
                </td>
            </tr>		
        </table>

        <!--- pie --->
        <?php require('pie.php'); ?>
    </body>
</html>
