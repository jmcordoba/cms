<?php

require("objetos/usuario.php");
?>

<!DOCTYPE html>

<html>

    <head>
        <title>cms</title>
		<meta http-equiv=content-type content="text/html; charset=utf-8">
		<link rel="stylesheet" type="text/css" href="css/login_register.css" />
		<link rel="stylesheet"    type="text/css"     href="css/menu_izquierda.css" />
    </head>
    
    <script>
        // Verifica la dirección de correo electrónico
        function verificar(direccion) {
            if (direccion.indexOf("@")>0) {
                if ((direccion.indexOf("@")+2)<direccion.lastIndexOf(".")) {
                    if (direccion.lastIndexOf(".")<(direccion.length-2)) {
                        return (true);
                    }
                }
            }
            return (false);
        }
        // Verifica los campos del formulario antes de guardar los datos
        function verificar_form() {
		
			if(document.datos.nombre.value.length==0) {
                alert("Error:\nDebe ingresar el nombre"); 
                document.datos.nombre.focus() 
                return 0; 
            }
            else if(document.datos.apellidos.value.length==0) {
                alert("Error:\nDebe ingresar el apellido"); 
                document.datos.apellidos.focus() 
                return 0; 
            }
            else if(document.datos.password.value.length==0) {
                alert("Error:\nDebe ingresar el password"); 
                document.datos.password.focus() 
                return 0; 
            }
            else if(document.datos.password.value!=document.datos.repassword.value) {
                alert("Error:\nDebe ingresar el mismo password que en el campo anterior"); 
                document.datos.repassword.focus() 
                return 0; 
            }
            else if(document.datos.telefono.value.length==0) {
                alert("Error:\nDebe ingresar el telefono"); 
                document.datos.telefono.focus() 
                return 0; 
            }
            else if(isNaN(document.datos.telefono.value)) {
                alert("Error:\nEste campo debe tener sólo números.");
                document.datos.telefono.focus();
                return false;
            }
            else if(document.datos.mail.value.length==0) {
                alert("Error:\nDebe ingresar el mail"); 
                document.datos.mail.focus() 
                return 0; 
            }
            else if (!verificar(document.datos.mail.value)) {
                alert('Error:\nMail incorrecto');
            }
            else {
				document.forms["datos"].submit();
            }
        }
    </script>

    <body bgcolor="#e8e8e8" style="margin:0px;">
		
		<!-- cabecera -->
        <?php require('cabecera.php'); ?>
        
		<!-- cuerpo -->
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="35" align="center"><tr><td></td></tr></table>
        
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="700" align="center">
            <tr>
				<td bgcolor="#ffffff" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    <?php
					include("cuerpo1.php");
					include("cuerpo3.php");
					include("cuerpo4.php");
					?>
                </td>
                <td></td>
				<td bgcolor="#ffffff" width="800" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">

                    <form name="datos" action="fun/guardarUsuario.php" method="post" enctype="multipart/form-data">
						
						<table style="position:relative;top:30px;margin-left:5px;text-align:justify;" width="99%" align="center">
						
							<tr>
								<td colspan="2" style="border-bottom:1px dashed #333333;">
									<font class="titulo_pagina"><?php echo "Formulario de registro"; ?></font>
								</td>
							</tr>
							
							<tr height="20" bgcolor="#ffffff"><td></td></tr>
						
                            <tr valign="bottom" align="left">
                                <td colspan="2" width="100%" bgcolor=#ffffff>
									<img height="150" src="images/usuario/user.jpg">
								</td>
                            </tr>
                            <tr>
                                <td colspan="2" width="100%">
									<input type="file" name="userfile" style="width:500px;border:0px solid #000000;">
								</td>
                            <tr>
                        
							<tr height="20" bgcolor="#ffffff"><td></td></tr>
							
                            <tr>
                                <td class="td_titulo"><font class="titulo">nombre</font></td>
								<td class="td_info"><input class="input_register" type="text" name="nombre"></td>
                            </tr>
                            <tr>
                                <td class="td_titulo"><font class="titulo">apellidos</font></td>
								<td class="td_info"><input class="input_register" type="text" name="apellidos"></td>
                            </tr>
                            <tr>
                                <td class="td_titulo"><font class="titulo">mail</font></td>
								<td class="td_info"><input class="input_register" type="text" name="mail"></td>
                            </tr>
                            <tr>
                                <td class="td_titulo"><font class="titulo">password</font></td>
								<td class="td_info"><input class="input_register" type="password" name="password"></td>
                            </tr>
                            <tr>
                                <td class="td_titulo"><font class="titulo">repite password</font></td>
								<td class="td_info"><input class="input_register" type="password" name="repassword"></td>
                            </tr>
                            
							<input class="input_register" type="hidden" name="telefono" value="666666666">
							<input class="input_register" type="hidden" name="usuario">
							
							<tr height="20" bgcolor="#ffffff"><td></td></tr>
							
                            <tr>
                                <td colspan="2" width="800" bgcolor="#ffffff">
                                    <a onclick="verificar_form()" href="#" style="text-decoration:none;">
										<font class="titulo">Guardar</font>
									</a>
                                </td>
                            </tr>
                        </table>
						
                    </form>
                </td>
            </tr>		
        </table>
		
		<?php require('pie.php'); ?>
		
    </body>
</html>
