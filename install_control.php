<?php

require_once($_SERVER['DOCUMENT_ROOT']."/cmsweb/conf.php");

/*
 * Conecta con la base de datos disponible
 */
function ConectarseControl() {
    // inicializa variable de retorno
    $link = false;
    // Conecta con la base de datos
    // @param1: URL donde est� alojada la bbdd
    // @param2: nombre de usuario con acceso a la bbdd
    // @param3: password de la bbdd
    if (!($link=mysql_connect(conf_HOST,conf_USER,conf_PSWD))) {
        $link = false;
    }
    // Selecciona la bbdd y obtiene un puntero de conexi�n
    // @param1: nombre de la bbdde
    // @param2: puntero a la bbdd
    if (!mysql_select_db(conf_BBDD,$link)) {
        $link = false;
    }
    return $link;
}

$result = ConectarseControl();
if($result!=false) {
    $sql    = "SELECT * FROM `CMS_users` limit 1";
    $result = mysql_query($sql, $link);
} else {
    $result = false;
}

if($result==false) {
    ?><script>location.href="install.php";</script><?php
}
?>