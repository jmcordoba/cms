<?php

//ob_start(”ob_gzhandler”);

// Obtiene el idioma de la web
require('control.php');

// si la aplicacion NO esta instalada redirigimos a install.php
//include("install_control.php");

// incluimos fichero de idiomas
include("txt/index.php");
?>

<!DOCTYPE html>

<html>

    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title><?php echo $TxT_00001; ?></title>
		<link rel="shortcut icon" type="image/x-icon" href="/images/favicon.png"></link>
		<link rel="stylesheet"    type="text/css"     href="css/menu_izquierda.css" />
    </head>
	
	<body bgcolor="#e8e8e8" style="margin:0px;padding:0px;">

        <?php require('cabecera.php'); ?>
        
		<!-- cuerpo -->
		<table style="z-index:9;" border="0" color="black" cellspacing="0" width="1000" height="35"  align="center"><tr><td></td></tr></table>
        <table style="z-index:9;" border="0" color="black" cellspacing="0" width="1020" height="700" align="center">
            <tr>
				<td bgcolor="#ffffff" width="200" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    <?php
                    # menu de selección izquierdo
					include("cuerpo1.php");
					include("cuerpo3.php");
					include("cuerpo4.php");
					?>
                </td>
				<td>&nbsp;</td>
                <td bgcolor="#ffffff" width="800" style="vertical-align:top;margin-top:0px;margin-left:0px;border-right:1px solid #c8c8c8;border-left:1px solid #c8c8c8;">
                    <?php
					# listado de artículos
					include("cuerpo2.php");
					?>
                </td>
            </tr>
        </table>
        
		<?php require('pie.php'); ?>

    </body>
</html>

<?php 
//ob_end_flush();