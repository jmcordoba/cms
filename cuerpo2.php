<!-- PAGINA DE ARTICULOS -->

<script language="JavaScript" type="text/javascript">
    function articulo(idart) {
        document.location.href="articulo.php?idart="+idart;
    }
</script>

<?php 

include("txt/cuerpo2.php");

require_once("admin/objetos/articulo.php");
require_once("admin/objetos/comentario.php");
require_once("admin/fun/funciones.php");

$articulo = obtenerArticulo();

$MAX_NUM_ART   = 8;
$conta         = numRows("CMS_articulos");
$maxpages      = (int)($conta/$MAX_NUM_ART)+1;
$num_articulos = 0;
$page          = isset($_REQUEST["page"]) ? $_REQUEST["page"] : 0;

for($i=0;$i<$conta;$i++) {
	
	if(($i>=($page*$MAX_NUM_ART))and($i<(($page*$MAX_NUM_ART)+$MAX_NUM_ART))) {
		$idart     = $articulo[$i]->idart;
		$fecha     = $articulo[$i]->fecha;
		$user      = $articulo[$i]->iduser;
		$titulo    = $articulo[$i]->titulo;
		$contenido = $articulo[$i]->contenido;
		$imagen    = $articulo[$i]->imagen;
		$contador  = $articulo[$i]->conta;
		$articulo[$i]->fechasi;
		$articulo[$i]->fechano;
		$foto_user = obtenerFotoDeUsuario($articulo[$i]->iduser);
		
		?>
		<table bgcolor="#ffffff" border="0" color="black" cellspacing="0" width="98%" style="margin-top:<?php if($i==($page*$MAX_NUM_ART)) { echo "30px"; } else { echo "10px"; } ?>;margin-left:5px;">
			<tr>
				<td style="border-bottom:1px dashed #333333;">
					<a href="#" style="text-decoration:none;" onClick="articulo(<?php echo $idart; ?>);">
						<font face="Arial" color="#394752" style="font-size:26px; font-style:normal; color:#000000;">
							<?php echo $titulo; ?>
						</font>
					</a>
				</td>
			</tr>
		</table>
		<table bgcolor="#ffffff" border="0" color="black" cellspacing="0" width="95%" style="margin-top:0px;margin-left:5px;">
			<tr>
				<td width="60" height="60" style="padding-top:8px;">
					<img height="60" width="60" src="<?php echo $foto_user; ?>">
				</td>
				<td>
					<table height="50">
						<tr height="15">
							<td height="15" style="padding:0;">
								<font style="font-family:arial;font-size:14px;font-style:normal;color:#888888;">
									<?php echo $TxT_04001 . $user ; ?>
								</font>
							</td>
						</tr>
						<tr height="15">
							<td height="15" style="padding:0;">
								<font style="font-family:arial;font-size:14px;font-style:normal;color:#888888;">
									<?php echo $TxT_04002 . $fecha ; ?>
								</font>
							</td>
						</tr>
						<tr height="15">
							<td height="15" style="padding:0;">
								<a href="#" onClick="articulo(<?php echo $idart; ?>);" style="text-decoration: underline;color:#888888">
									<font style="font-family:arial;font-size:14px;font-style:normal;color:#888888;">
										<?php 
										echo numRowsDeArticulo("CMS_comentarios",$idart) . "&nbsp;";
										if(numRowsDeArticulo("CMS_comentarios",$idart)!=1) echo $TxT_04006;
										else                                               echo "comentario";
										?>
									</font>
								</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		
		<table bgcolor="#ffffff" border="0" color="black" cellspacing="0" width="98%" style="margin-top:0px;margin-left:5px;text-align:justify;">
			<tr>
				<td width="100%">
					<?php
					if ($imagen!="") {
						?>
						<img style="margin-top:5px;margin-left:0px;margin-right:10px;float:left;" width="20%" heigh="20%" src="<?php echo conf_RUTA."admin/".$imagen; ?>"/>
						<?php
						}
					?>
					<font face="verdana" color="black"  style="font-size:13px;text-align:justify;">
						<?php 
						$contenido = str_replace("<br />","",$contenido);
						if(strlen($contenido)>410) $letras = 410;
						else                       $letras = strlen($contenido);
						for($j=0;$j<$letras;$j++) {
							echo $contenido[$j];
						}
						echo ".... ";
						?>
						<a href="#" onClick="articulo(<?php echo $idart; ?>);"><b><?php echo $TxT_04003; ?></b></a>
					</font>
				</td>
			</tr>
		</table>
		
		<table bgcolor="#ffffff" border="0" color="black" cellspacing="0" width="100%" height="10" 
			   style="vertical-align: top;
					   margin-top:    0px;
					   margin-left:   0px;
					   border-bottom: 0px solid #CCCCCC;
					   text-align:    justify;">
		</table>

		<?php
		
		$num_articulos++;
	}
}

if($num_articulos<2) {
	?>
	<table bgcolor="#ffffff" border="0" color="black" cellspacing="0" width="95%" height="100"><tr><td></td></tr></table>
	<?php
}
?>

<?php
if($maxpages>1) {
	?>
	<table bgcolor="#ffffff" border="0" color="black" cellspacing="0" width="98%" height="20"
		   style="vertical-align:   bottom;
					margin-top:     20px;
					padding-top:    20px;
					padding-bottom: 20px;
					margin-bottom:   0px;
					margin-left:    5px;
					border-top:     1px dashed #394752;
					text-align:     justify;">
		<tr>
			<td bgcolor="#ffffff" width="20%"height="25"></td>
			<td bgcolor="#ffffff" width="20%" align="center" height="25">
				<table>
					<tr>
						<?php 
						if($page>0) {
							?>
							<td bgcolor="#394752" height="25" width="400" align="center">
								<a href="index.php?page=<?php echo $page-1; ?>" onclick="anterior();" style="text-decoration:none;">
									<font face="arial" style="font-size: 18px; color: #c8c8c8;">
										<?php echo $TxT_04004; ?>
									</font>
								</a>
							</td>
							<?php
						}
						?>
					</tr>
				</table>
			</td>
			<td bgcolor="#ffffff" width="20%" align="center">
				<table>
					<tr>
						<?php 
						for($i=0;$i<$maxpages;$i++) { 
							if(($i<6)or($i=$maxpages-1)) {
								?>
								<td bgcolor="#394752" height="25" width="25" align="center">
									<a href="index.php?page=<?php echo $i; ?>" onclick="siguiente();" style="text-decoration:none;">
										<font face="arial" style="font-size: 18px; color: #c8c8c8;">
											<b><?php echo $i+1; ?></b>
										</font>
									</a>
								</td>
								<?php
							}
						}
						?>
					</tr>
				</table>
			</td>
			<td bgcolor="#ffffff" width="20%" align="center" height="25">
				<table>
					<tr>
						<?php
						if($page<$maxpages-1) {
							?>
							<td bgcolor="#394752" height="25" width="400" align="center">
								<a bgcolor="#394752" href="index.php?page=<?php echo $page+1; ?>" onclick="siguiente();" style="text-decoration:none;">
									<font style="font-family:arial;font-size: 18px; color: #c8c8c8;">
										<?php echo $TxT_04005; ?>
									</font>
								</a>
							</td>
							<?php
						}
						?>
					</tr>
				</table>
			</td>
			<td bgcolor="#ffffff" width="20%"height="25"></td>
		</tr>
	</table>
	<?php
}
?>